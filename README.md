## sKonnect Setup

sKonnect is Skylo’s C language-based library for the ALT1250 chipset, repurposed for this project to demonstrate connectivity and switching capabilities. The library provides a porting layer in the `/port` directory, which can be modified to work with any OS that has threading/processing functionality, as long as the API requirements in `/port` are fulfilled.

### 1 Requirements

- MCU of your choice
- FreeRTOS Kernel V10.3.1
- Type1SC TB User Guide

### 1.2 STM32 MCU

#### 1.2.1 Steps for Software Setup

1. Install STM32CUBE IDE and programmer.
2. Create a new project based on your board (FreeRTOS based).
3. Remove or don't use CMSIS wrappers for FreeRTOS (optional).
4. Use/copy settings provided in `FreeRTOSConfig.h` by us.
5. Copy our Application from `/Src` folder to `/src/skylo`.
6. Check Chapter 6 for more details.

#### 1.2.2 Links

- [How to STM32Cube FreeRTOS project](https://www.digikey.com/en/maker/projects/getting-started-with-stm32-introduction-to-freertos/ad275395687e4d85935351e16ec575b1)
- [Using Native FreeRTOS in STM32 Microcontrollers](https://jaywang.info/using-native-freertos-in-stm32-microcontrollers/)

### 1.3 Non-STM32 MCU

#### 1.3.1 Steps for Software Setup

1. Setup MCU Development Environment.
2. Create a new FreeRTOS-based project.
3. Use/copy settings provided in `FreeRTOSConfig.h` by us.
4. Copy our Application from `/Src` folder to `/src/skylo`.
5. Check Chapter 6 for more details.

### 2 Porting sKonnect

#### 2.1 Port Directory

The port directory contains 6 files which are essential for sKonnect to work. They are wrappers for the OS running underneath and contain a general set of APIs needed for multithreaded applications to run.

#### 2.2 Utils Directory

This directory provides basic helper functions for sKonnect like logging and conversion APIs. You may want to recheck API implementation according to your system.

#### 2.3 InitThread

In RefImpl -> main.c there is main thread that initializes the library/application and is the first thing you want to run after the MCU is initialized.

Most of API/Functions in sKonnect are well documented in the header files you can feel free to ping us for any confusion/suggestions.
### 3 Switching

sKonnect provides two types of switching from terrestrial to Non-terrestrial NW and vice versa:

#### 3.1 Time-based switching

This is the simplest approach which switches on a simple timer controlled by macros `TIME_BASED_SWITCHING(flag)` and `TIMER_BASED_SWTCH_TIME`.

#### 3.2 Automatic switching

Here the application decides when to switch based on no Attach in the last X time and if in NTN mode to check if TN is available. Macros that control this behavior are `MAX_DISCON_TIME_ALWD` (No attach timer) and `NT_AVAIL_CHECK_TIME` (NTN->TN check).

### 4 Non-FreeRTOS Implementation

If you are using any OS other than FreeRTOS, you can still use sKonnect; you just need to take care of porting info provided in Chapter 6.
