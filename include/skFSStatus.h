
/**
* @file         skFSStatus.h
* @brief        //TODO
* @todo         Update the brief and details
* @details      Header file for Skylo Konnect SDK File System related error codes.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK__FS_STATUS__H__
#define __SK__FS_STATUS__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/** @addtogroup skFSStatus
@{ */

/** @name File system status Codes
 *
 * The following definitions gives the status codes for generic file operation
 * @todo write guideline for porting
 * @{
 */

#define SK_FS_EEXIST             __SK_ERROR(SK_MODULE_FS, 1)    /**< pathname already exists and O_CREAT and O_EXCL were used. */
#define SK_FS_EINVAL             __SK_ERROR(SK_MODULE_FS, 2)    /**< Invalid value in flags. */
#define SK_FS_EOPEN              __SK_ERROR(SK_MODULE_FS, 3)    /**< Failed to open file */
#define SK_FS_EREAD              __SK_ERROR(SK_MODULE_FS, 4)    /**< Failed to read file */
#define SK_FS_EWRITE             __SK_ERROR(SK_MODULE_FS, 5)    /**< Failed to write file */

/** @} */ /* end namegroup "File system status code"*/

/** @} end addtogroup skFSStatus */

#endif //__SK__FS_Status__H__