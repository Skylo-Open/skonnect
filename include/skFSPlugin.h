
/**
* @file         skFSPlugin.h
* @brief        //TODO
* @todo         Update the brief and details
* @details      Header file for Skylo Konnect SDK File System plugin end points for porting.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK__FS_PLUGIN__H__
#define __SK__FS_PLUGIN__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/** @addtogroup FS
@{ */

/** @name FS function pointers.
 * @{
 */

/**
 * @brief This function is a porting end point to open file
 *
 * @details This function is a porting end point to open file
 *
 * @param[in]   path    Path
 * @param[in]   flags   Flags
 * @param[out]  fd      File descriptor
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skFSErrorCode.
 *
 */
typedef skStatus_t (*open_fp)(const char *path, int flags, int *fd);

/**
 * @brief This function is a porting end point to close file
 *
 * @details This function is a porting end point to close file
 *
 * @param[in]  fd      File descriptor
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skFSErrorCode.
 *
 */
typedef skStatus_t (*close_fp)(int fd);

/**
 * @brief This function is a porting end point to read file
 *
 * @details This function is a porting end point to read file
 *
 * @param[in]   fd          File descriptor
 * @param[out]  buffer      Buffer
 * @param[in]   bufferSize  Buffer size
 * @param[out]  bytesRead   Bytes read
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skFSErrorCode.
 *
 */

typedef skStatus_t (*read_fp)(int fd, char *buffer, uint32_t bufferSize, uint32_t *bytesRead);

/**
 * @brief This function is a porting end point to read file from a position.
 *
 * @details This function is a porting end point to read file from a position.
 *
 * @param[in]   fd          File descriptor
 * @param[out]  buffer      Buffer
 * @param[in]   bufferSize  Buffer size
 * @param[out]  bytesRead   Bytes read
 * @param[in]   position    Position
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skFSErrorCode.
 *
 */
typedef skStatus_t (*pRead_fp)(int fd, char *buffer, uint32_t bufferSize, uint32_t *bytesRead, uint32_t position);

/**
 * @brief This function is a porting end point to write file.
 *
 * @details This function is a porting end point to write file.
 *
 * @param[in]   fd              File descriptor
 * @param[in]   buffer          Buffer
 * @param[in]   bufferSize      Buffer size
 * @param[out]  bytesWritten    Bytes Written
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skFSErrorCode.
 *
 */
typedef skStatus_t (*write_fp)(int fd, char *buffer, uint32_t bufferSize, uint32_t *bytesWritten);

/**
 * @brief This function is a porting end point to write file from a position
 *
 * @details This function is a porting end point to write file from a position
 *
 * @param[in]   fd              File descriptor
 * @param[in]   buffer          Buffer
 * @param[in]   bufferSize      Buffer size
 * @param[out]  bytesWritten    Bytes Written
 * @param[in]   position        Position
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skFSErrorCode.
 *
 */
typedef skStatus_t (*pWrite_fp)(int fd, char *buffer, uint32_t bufferSize, uint32_t *bytesWritten, uint32_t position);

/**
 * @brief This function is a porting end point to traverse in file.
 *
 * @details This function is a porting end point to traverse in file.
 *
 * @param[in]   fd              File descriptor
 * @param[in]   offset          Offset
 * @param[in]   whence          Whence
 * @param[out]  actualOffset    ActualOffset
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skFSErrorCode.
 *
 */
typedef skStatus_t (*lseek_fp)(int fd, uint32_t offset, int whence, uint32_t *actualOffset);

/**
 * @brief This function is a porting end point to remove a file.
 *
 * @details This function is a porting end point to remove a file.
 *
 * @param[in]   path    Path
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skFSErrorCode.
 *
 */
typedef skStatus_t (*unlink_fp)(const char *path);

/**
 * @brief This function is a porting end point to get file size.
 *
 * @details This function is a porting end point to get file size.
 *
 * @param[in]   path        Path
 * @param[out]  fileSize    File Size
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skFSErrorCode.
 *
 */
typedef skStatus_t (*getFileSize_fp)(const char *path, int32_t *fileSize);

/**
 * @brief This function is a porting end point to check whether a file is present or not.
 *
 * @details This function is a porting end point to check whether a file is present or not.
 *           value written in filePresentFlag (1--> present, 0 -> not present)
 *
 * @param[in]   path                Path
 * @param[out]  filePresentFlag     File Present Flag
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skFSErrorCode.
 */
typedef skStatus_t (*isFilePresent_fp)(const char *path, uint8_t *filePresentFlag);

/** @} */ /* end namegroup FS function pointers */

/** @name FS endpoint context.
 * @{
 */

/**
 * @struct      Structure of Skylo Konnect SDK File System end points for porting.
 * @details     This structure will be used to port the necessary file system endpoints which can be plugged at SDK initialisation time
 *              by application programmer.
 * @todo        add more details
 */
typedef struct skPepFS_s {

    open_fp fpOpen;                      /**< Function to open file */
    read_fp fpRead;                      /**< Function to read file */
    pRead_fp fpPRead;                    /**< Function to read file from particular position */
    write_fp fpWrite;                    /**< Function to write file */
    pWrite_fp fpPWrite;                  /**< Function to write file from particular position */
    close_fp fpClose;                    /**< Function to close file */
    unlink_fp fpUnlink;                  /**< Function to remove file */
    getFileSize_fp fpGetFileSize;        /**< Function to get the size of file */
    isFilePresent_fp gpIsFilePresent;    /**< Function to check if file is present */
} skPepFS_t;

/** @} */ /* end namegroup FS endpoint context */

/** @} */ /* end group */

#endif //__SK__FS_PLUGIN__H__