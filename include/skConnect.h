
/**
* @file         skConnect.h
* @brief        //TODO
* @details      Header file for Skylo Konnect SDK Connectivity access and porting end points.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK__CONNECT__H__
#define __SK__CONNECT__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <stdbool.h>

#include <skModemAccess.h>
#include <skOSPlugin.h>
#include <skFSPlugin.h>
#include <skHALPlugin.h>
#include <skTimePlugin.h>
#include <skGPSPlugin.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

#define TX_ARRAY_SIZE               (512)
#define CIRC_BUFFER_LENGTH          (5)
#define MAX_DISCON_TIME_ALWD        (30*60*1000)        //msec
#define NT_AVAIL_CHECK_TIME         (30*60*1000)        //msec
#define TIME_BASED_SWITCHING        (1)
#define TIMER_BASED_SWTCH_TIME      (60*60*1000)        //msec

// #define DEBUG_SKCONNECT

#define BAD_AT_COUNT                (5)                //restart UART after this count exceeds

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/** @addtogroup skConnect_types
@{ */

/**
 * @brief
 * Function to handle two way message ack.
 *
 * @details
 * Function to handle two way message ack.
 *
 * @param[in]   msgId       Message ID that was given by Skylo connect library when sending message.
 * @param[in]   status      Status of message.
 */
typedef skStatus_t (*handleTwoWayMsgAck_fp)(int32_t msgId, skStatus_t status);

/**
 * @struct      Structure of Skylo Konnect SDK porting end points.
 * @details     This structure will be used to port the necessary endpoints which can be plugged at SDK initialisation time
 *              by application programmer.
 */
typedef struct skPEP_s {

    skPepOS_t   os;  /**< OS Porting end points */
    skPepFS_t   fs;  /**< FS Porting end points */
    skPepHAL_t  hal; /**< HAL Porting end points */
    skPepGPS_t  gps; /**< GPS Porting end points */
} skPEP_t;

/**
 * @struct      Structure of Skylo Konnect SDK access end points.
 * @details     This structure will be used to access the necessary endpoints which can be used by application proram
 *              to know the status of SDK and execute connection and data transfer related operations.
 */
typedef struct skAEP_s {

    skAepModem_t  modem;                /**< modem Acess end points */
} skAEP_t;

/**
 * @struct      Structure for log context.
 * @details     Structure for log context.
 */
typedef struct skLog_s {

    skMutex_t mutex;               /**< Mutex */
} skLog_t;

/** @} */ /* end group */ /* skConnect_types */

/** @addtogroup Modem_types
@{ */

/**
 * @struct      Structure for modem GNSS lte state.
 * @details     Structure for modem GNSS lte state.
 */
typedef enum modemGnssLteState_s {

    GNSS_STATE_E,
    LTE_STATE_E,
} modemGnssLteState_e;

/**
 * @enum      Enum for modem Notify EV.
 * @details   Enum for modem Notify EV.
 */
typedef enum modemNotifyEv_s {

    NOTIFY_PDN_ATTACH_E,
    NOTIFY_PDN_DETACH_E,
    NOTIFY_UE_RRC_CONNECTED_E,
    NOTIFY_UE_RRC_IDLE_E,
    NOTIFY_UE_RRC_UNKNOWN_E,
    NOTIFY_GNSS_COLD_BOOT_E,
    NOTIFY_GNSS_EPHUPD_E,
    NOTIFY_RTT_REPORT_E,
    NOTIFY_TA_REPORT_E,
    NOTIFY_NMEA_RMC_E,
    NOTIFY_NMEA_GGA_E,
    NOTIFY_NMEA_GSV_E,
    // RK_3.2 ONLY
    NOTIFY_CELL_STATEV_E,
    NOTIFY_IGNSS_FIX_ACQ_E,
    NOTIFY_IGNSS_FIX_LOST_E,
    NOTIFY_IGNSS_BLANKING_STATE_E,
    NOTIFY_MODEM_FAILURE_STATE_E,
    NOTIFY_BOOT_EVENT_E,
    // RK_3.2 ONLY END
    NOTIFY_TOTAL_NUM_NOTIFY_EVS_E,
} modemNotifyEv_e;

/**
 * @enum      Enum for modem RRC state.
 * @details   Enum for modem RRC state.
 */
typedef enum modemRrcState_s {

    UE_RRC_IDLE_E,
    UE_RRC_CONNECTED_E,
    UE_RRC_UNKNOWN_E,
} modemRrcState_e;

/**
 * @enum  Modem Socket types.
 *
 * @details Modem Socket types.
 */
typedef enum modemsockType_s {

    MODEM_SOCK_TCP_E,
    MODEM_SOCK_UDP_E,
	MODEM_SOCK_NONE_E
} modemsockType_e;

/**
 * @enum      Enum for modem Attach state.
 * @details   Enum for modem Attach state.
 */
typedef enum modemAttachState_s {

    PDN_DETACH_E,
    PDN_ATTACH_E,
} modemAttachState_e;

/**
 * @enum      Enum for modem phase.
 * @details   Enum for modem phase.
 */
typedef enum modemPhaseVer_s {

    MODEM_PHASE_2_E,
    MODEM_PHASE_3_E,
} modemPhaseVer_t;

/**
 * @struct    Structure for sim parameters.
 * @details   Structure for sim parameters.
 */
typedef struct sim_s {

    const char * imsi;      /**< IMSI */
    const char * iccid;     /**<  ICCID */
} skSim_t;

/**
 * @struct    Structure for signal metics.
 * @details   Structure for signal metics.
 */
typedef struct signalMetrics_s {

    int16_t rssi;       /**< rssi  */
    int16_t rsrp;       /**< rsrp */
    int16_t rsrq;       /**< rsrq */
    int16_t sinr;       /**< sinr */
} signalMetrics_t;

/**
 * @struct    Structure for modem init options.
 * @details   Structure for modem init options.
 */
typedef struct modemOptions_s {

    uint16_t        rfBand;         /**< radio freq. band */
    uint32_t        rfEarfcnStart;  /**< radio freq. band range Start */
    uint32_t        rfEarfcnEnd;    /**< radio freq. band range End */
    skBool_t        cliMode;        /**< cli mode enabled or not */
    char*           ip;             /**< Ip address for socket communication when */
    uint16_t        port;           /**< port for socket communication when */
    modemsockType_e socketType;     /**< socket type for given IP and port */
    float           ueLat;          /**< Ue lattitude, needs update peridically if NTN */
    float           ueLong;         /**< Ue longitude, needs update peridically if NTN */
    float           ueAlt;          /**< Ue laltititude, needs update peridically if NTN */
    float           gpsFix;         /**< gps fix */
} modemOptions_t;

/**
 * @struct    Structure for boot mode data.
 * @details   Structure for boot mode data.
 */
typedef struct bootMode_s {

    unsigned long long flashStart;  /**< flash start pos */
    unsigned long long flashSize;   /**< flash size */
    uint32_t baudrate;              /**< baudrate */
} bootMode_t;

/**
 * @struct    Structure for RTC time context.
 * @details   Structure for RTC time context.
 */
typedef struct rtcTm_ts {

    uint8_t year;   /**< year */
    uint8_t month;  /**< month */
    uint8_t day;    /**< day */
    uint8_t hour;   /**< hour */
    uint8_t minute; /**< minute */
    uint8_t second; /**< second */
    uint8_t tz;     /**< time zone */
} rtcTm_t;

/**
 * @enum  SDK init types
 *
 * @details SDK init types
 */
typedef enum skSDKInitTypes_s {

    SK_FULL_INIT,       /**< Initializes full SDK i.e. HW boot */
    SK_PARTIAL_INIT     /**< Initializes SDK partially i.e. Modem SW boot */
} skSDKInitTypes_e;

/**
 * @enum      Enum for modem states.
 * @details   Enum for modem states.
 */
typedef enum modemSkyloState_s {

    MODEM_STATE_ACQUIRING_GNSS_E,
    MODEM_STATE_CELL_CAMPING_E,
    MODEM_STATE_ACQUIRING_GEO_SYNC_E,
    MODEM_STATE_WAITING_FOR_VALID_SINR_E,
    MODEM_STATE_UE_IDLE_NW_DETACHED_E,
    MODEM_STATE_UE_IDLE_NW_ATTACHED_E,
    MODEM_STATE_UE_CONN_NW_ATTACHED_E,
    MODEM_STATE_UE_CONN_NW_DETACHED_E,
    MODEM_STATE_UE_DICONN_NW_DETACHED_E,
    MODEM_STATE_UE_DICONN_NW_ATTACHED_E,
    MODEM_STATE_GNSS_UPDATE_E,
    MODEM_STATE_GNSS_EPH_UPDATE_E,
} modemSkyloState_e;

/**
 * @enum  Modem LTE image types.
 *
 * @details Modem LTE image types.
 */
typedef enum modemImage_s {

    LTE_IMAGE_NONE_E,
    LTE_IMAGE_CAT_M1_E,
    LTE_IMAGE_NB_IOT_E
} modemImage_e;

/**
 * @enum  Modem NbIoT image types.
 *
 * @details Modem NbIoTimage types.
 */
typedef enum modemNbiotImage_s {

    NBIOT_IMAGE_NTN_E,
    NBIOT_IMAGE_TN_E,
    NBIOT_IMAGE_OTHER_E,
    NBIOT_IMAGE_NONE_E,
} modemNbiotImage_e;

/**
 * @enum  Modem pdp types.
 *
 * @details Modem pdp types.
 */
typedef enum modempdpType_s {

    MODEM_PDP_IP_E,
    MODEM_PDP_NON_IP_E,
    MODEM_PDP_UNKNOWN_E,
} modempdpType_e;

/**
 * @struct     Structure for dataArray for TX circbuffer.
 * @details    Structure for dataArray for TX circbuffer.
 */
typedef struct __attribute__((packed, aligned(1))) {
    
    char data[TX_ARRAY_SIZE+1];
} DataArray;

/**
 * @struct     Structure for TX circbuffer.
 * @details    Structure for TX circbuffer.
 */
typedef struct __attribute__((packed, aligned(1))) txCircularBuffer_s {
    
    DataArray array[CIRC_BUFFER_LENGTH];
    int writeIndex;
    int readIndex;
    int count; // Number of valid elements in the buffer
} txCircularBuffer_t;

/**
 * @struct     NW Switching context
 * @details    NW Switching context
 */
typedef struct __attribute__((packed, aligned(1))) skNWSwitchingContext_s {
    
    uint8_t     swtchNW;            /**< Holds RATACT to switch to */
    uint8_t     switchInitiated;    /**< Flag to cehck if app switch is initated */
    skTime_t    timeofLastConn;     /**< Epoch Time since last connection */
    skTime_t    modemInitTimer;     /**< Epoch Time when modem was initialzed in NTN mode */
} skNWSwitchingContext_t;

/**
 * @struct     Structure for modem connectivity data.
 * @details    Structure for modem connectivity data.
 */
typedef struct __attribute__((packed, aligned(1))) smodem_s {

    uint16_t band;              /**< band */
    uint32_t earfcnStart;       /**< earfcn range start */
    uint32_t earfcnEnd;         /**< earfcn skConnect->m->modemConf.earfcnStart end */
} skSmodem_t;

/**
 * @brief
 * Handler for modem GPS data.
 *
 * @details
 * Handler for modem GPS data.
 */
typedef int32_t (*modemGpsHandler_fp)(void * event);

/**
 * @struct     Structure for modem context.
 * @details    Structure for modem context.
 */
typedef struct skModem_s {

    const char              * appVersion;           /**< app version */
    const char              * nbiotVersion;         /**< nbiot version */
    const char              * nbiotRkVersion;       /**< nbiot RK version */
    char                    * atCmdBuf;             /**< at command buffer */
    char                    * atRespBuf;            /**< at command response buffer */
    char                    * rxDataBuf;            /**< rx data buffer */
    const char              * imei;                 /**< Imei */
    uint8_t                 isRma;                  /**< is rma? */
    skSim_t                 sim;                    /**< sim context */
    skBool_t                hsuConnectedState;      /**< is hsu connected flag */
    skBool_t                atReadyState;           /**< is at ready flag */
    modemAttachState_e      attachedState;          /**< modem attached state */
    modemRrcState_e         rrcState;               /**< rrc state */
    skMutex_t               transactionMutex;       /**< mutex */
    skMutex_t               stateMutex;             /**< state mutex */
    skSignal_t              eventSignal;            /**< event signal */
    signalMetrics_t         signal;                 /**< signal metrics */
    skBool_t                modemRtcValid;          /**< is morem rtx valid flag */
    skSmodem_t              modemConf;              /**< modem configuration */
    volatile uint8_t        isInit;                 /**< is init flag */
    uint8_t                 isSimDataAvailable;     /**< is sim data available flag */
    skBool_t                resetTrig;              /**< reset trigger flag */
    skBool_t                partReset;              /**< part reset flag */
    modemSkyloState_e       modemState;             /**< modem state flag/enum */
    modemGpsHandler_fp      gpsNMEAHandler_fp;      /**< gps handler function */
    skBool_t                gpsTimeAvail;           /**< gps time available flag */
    skBool_t                gpsEphupdStat;          /**< gps ephemeral update state */
    skBool_t                gpsFix;                 /**< gps fix flag */
    uint32_t                modemResponseTimeoutMsec;/**< modem response timeout seconds */
    volatile skBool_t       onlineMode;             /**< If true, tx/rx. If false, then rx only */
    uint32_t                cellId;                 /**< cell ID */
    uint8_t                 modemPhaseVer;          /**< modem phase version */
    volatile skBool_t       bootMode;               /**< Decides boot mode in AT CLI or U-Boot */
    bootMode_t              bootParam;              /**< boot parameters */
    uint8_t                 modemResetCnter;        /**< modem reset counter */
    uint8_t                 inInitApi;              /**< Shows that init API is running */
    uint8_t                 isModemCrash;           /**< Indicates modem crash */
    uint8_t                 badATCount;             /**< On some modem uart hangs after few mins */
    uint32_t                earfcn;                 /**< earfcn */
    modemNbiotImage_e       ratActImg;              /**< ratact image */
    modemImage_e            runningImage;           /**< Running LTE image */
    int16_t                 firstSocketId;          /**< first socket ID that is created */
    uint8_t                 pdpType;                /**< pdy type being used */
    volatile uint8_t        bootevFlag;             /**< bootev flag, Notification for altair reboot */
    skNWSwitchingContext_t  swtchContext;           /**< switching context */
} skModem_t;

/** @} */ /* end group */ /* Modem_types */

/** @addtogroup skConnect_types
@{ */

/**
 * @struct      Main Structure of Skylo Konnect SDK.
 * @details     This structure will be used to port the necessary endpoints at SDK initialisation time and provide
 *              and provide access points, which are to be used by Application program
 */
typedef struct skConnect_s {

    skPEP_t  pep;                                   /**< Porting end points */
    skAEP_t  aep;                                   /**< Access end points */
    skModem_t *m;                                   /**< Context for modem object */
    modemOptions_t * options;              /**< modem options */
    skLog_t skLog;                                  /**< Log context */
    handleTwoWayMsgAck_fp fpTwoWayMsgAckHandler;    /**< Two way message ack handler */
    txCircularBuffer_t  *txCircBuffer;              /**< Tx queue circular buffer for holding UL msgs */
} skConnect_t;

/** @} */ /* end group */ /* skConnect_types */

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

#endif //__SK__CONNECT__H__
