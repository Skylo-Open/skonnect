
/**
* @file         skHALPlugin.h
* @brief        //TODO
* @todo         Update the brief and details
* @details      Header file for Skylo Konnect SDK Connectivity HAL plugin end points for porting.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK__HAL_PLUGIN__H__
#define __SK__HAL_PLUGIN__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/** @addtogroup Modem_HAL
@{ */

/** @name Modem HAL function pointers.
 * @{
 */

/**
 * @brief
 * porting end point to Initialise modem hardware.
 *
 * @details
 * porting end point to Initialise modem hardware, it could be GPIO as well as UART initialisation.
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
typedef skStatus_t (*modemHWInit_fp)(void);

/**
 * @brief
 * porting end point to DeInitialise modem hardware.
 *
 * @details
 * porting end point to DeInitialise modem hardware,
 *          Complete hw level deinit of modem (do reverse order of init)
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
typedef skStatus_t (*modemHWDeInit_fp)(void);

/**
 * @brief
 * porting end point to DeInitialise modem hardware.
 *
 * @details
 * porting end point to DeInitialise modem hardware,
 * Complete hw level deinit of modem (do reverse order of init)
 *
 * @param[in]   enable  1 means to enable flow control, 0 means disable flow control\
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
typedef skStatus_t (*modemSetHwFlowCtrlStatus_fp)(uint8_t enable);

/**
 * @brief
 * porting end point to control hardware flow control of UART device.
 *
 * @details
 * porting end point to control hardware flow control of UART device.
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
typedef skStatus_t (*modemGetHwFlowCtrlStatus_fp)(void);

/**
 * @brief
 * porting end point to send data to modem over hardware interface.
 *
 * @details
 * porting end point to send data to modem over hardware interface.
 *
 * @param[in]   cmd     Command (String)
 * @param[in]   length  Length of command string
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
typedef skStatus_t (*modemSend_fp)(char *cmd, uint32_t length);

/**
 * @brief
 * porting end point to receive data from modem over hardware interface.
 *
 * @details
 * porting end point to receive data from modem over hardware interface.
 *          LineBuffered Message must only be called with single line at a time.
 *
 * @param[out]      lineBufferedMsg     messsage
 * @param[in,out]   length              length of messge
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
typedef skStatus_t (*modemRecv_fp)(char *lineBufferedMsg, uint32_t *length);

/**
 * @brief
 * porting end point to Hard reset modem hardware.
 *
 * @details
 * porting end point to hard reset modem hardware,
 *          This could be GPIO based modem reset.
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
typedef skStatus_t (*modemHardReboot_fp)(void);

/** @} */ /* end namegroup Modem HAL function pointers */

/** @name Modem HAL endpoint context.
 * @{
 */


/**
 * @struct      Structure of Skylo Konnect SDK HAL porting end points.
 * @details     This structure will be used to port the necessary endpoints which can be plugged at SDK initialisation time
 *              by application programmer.
 * @todo        Add more info.
 */
typedef struct skPepHAL_s {
    modemHWInit_fp fpModemHWInit;
    modemHWDeInit_fp fpModemHWDeInit;
    modemSetHwFlowCtrlStatus_fp fpModemSetHwFlowCtrlStatus;
    modemGetHwFlowCtrlStatus_fp fpModemGetHwFlowCtrlStatus;
    modemSend_fp fpModemSend;
    modemRecv_fp fpModemRecv;
    modemHardReboot_fp fpModemHardReboot;
} skPepHAL_t;

/** @} */ /* end namegroup Modem HAL endpoint context */

/** @} */ /* end group */

#endif //__SK__HAL_PLUGIN__H__