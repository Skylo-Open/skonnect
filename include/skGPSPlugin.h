
/**
* @file         skGPSPlugin.h
* @brief        //TODO
* @todo         Update the brief and details
* @details      Header file for Skylo Konnect SDK GPS plugin end points for porting.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK__GPS_PLUGIN__H__
#define __SK__GPS_PLUGIN__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/** @addtogroup GPS
@{ */

/** @name GPS function pointers.
 * @{
 */

/**
 * @brief This function is a porting end point to get GPS location data from the application program.
 *
 * @details This function is a porting end point to get GPS location data from the application program.
 *
 * @param[out]   valid           Valid flag
 * @param[out]   lat             lattitude
 * @param[out]   lng             Longitude
 * @param[out]   speed           Speed (knts)
 * @param[out]   timeUnixEpoch   Unix Epoch time
 * @param[out]   ddmmyy          Date in string format (dd/mm/yy)
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skGPSErrorCode.
 */
typedef skStatus_t (*getVLLASTD_fp)(uint8_t *valid, float *lat, float *lng, float *altitude, float *speed, uint32_t *timeUnixEpoch, char *ddmmyy);

/** @} */ /* end namegroup GPS function pointers */

/** @name GPS endpoint context.
 * @{
 */

/**
 * @struct      Structure of Skylo Konnect GPS end points for porting.
 * @details     This structure will be used to port the necessary file system endpoints which can be plugged at SDK initialisation time
 *              by application programmer.
 * @todo        Add more info.
 */
typedef struct skPepGPS_s {

    getVLLASTD_fp fpGetVLLASTD;    /**< Function to get Valid Latitude, Longitude, Altitude, Time, Speed, Date*/
} skPepGPS_t;

/** @} */ /* end namegroup GPS endpoint context */

/** @} */ /* end group */

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

#endif //__SK__GPS_PLUGIN__H__