/**
* @file         skCore.h
* @brief        //TODO
* @details      Header file for Skylo Konnect core file.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK__CORE__H__
#define __SK__CORE__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skConnect.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/**
 * @brief Creates SKonnect context.
 *
 * @param sKonnect  Pointer to Object to be created.
 *
 * @return skStatus_t 0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
// skStatus_t sKonnectNew(skConnect_t *sKonnect);

/**
 * @brief
 * SDK init API.
 *
 * @details
 * SDK init API.
 *
 * @param[in]   sKonnect    Pointer to sKonnect.
 * @param[in]   initType    Init types i.e. Full/Partial @see skSDKInitTypes_e
 *
 * @return skStatus_t 0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
skStatus_t sKonnectInit(skConnect_t *skConnect, skSDKInitTypes_e initType);

/**
 * @brief
 * Method to get skConnect main context.
 *
 * @details
 * Method to get skConnect main context.
 *
 * @return skConnect_t* or NULL.
 */
skConnect_t *sKonnectGetContext(void);

/**
 * @brief
 * Method to Set skConnect main context pointer.
 *
 * @details
 * Method to Set skConnect main context pointer.
 *
 * @param[in] pcreatedContext   Newly created context pointer.
 *
 * @return None.
 */
void sKonnectGSetContext(skConnect_t *pcreatedContext);

/**
 * @brief
 * Function to sent Hearbeat message.
 *
 * @param[in]   sKonnect    Pointer to sKonnect.
 * 
 * @return skStatus_t
 */
skStatus_t sKonnectSendHB(skConnect_t *sKonnect);

/**
 * @brief
 * Core wrapper for sleep .
 *
 * @param[in]   time    time in msec for sleep.
 * 
 * @return skStatus_t
 */
void sleepCoreImpl(uint32_t time);

/**
 * @brief Checks for data availability in the circular buffer.
 *
 * Retrieves data from the circular buffer and places it in the destination buffer.
 * The length parameter indicates the maximum number of bytes to copy.
 * 
 * @note Caller to this API should make sure circBuffer supports only certain lenght of data.
 *
 * @param[in]   sKonnect    Pointer to sKonnect.
 * 
 * @param[out]  buff        Buffer.
 * 
 * @param[in]   length      Max Length of buffer.
 * 
 * @return The actual number of bytes copied, 0 if no data is available.
 */
int txCircBuffCheckDataAvailable(skConnect_t *sKonnect, uint8_t* buff, int length);

/**
 * @brief Writes data into the circular buffer.
 *
 * The length parameter indicates the number of bytes in the source data.
 *
 * @note Caller to this API should make sure circBuffer supports only certain lenght of data.
 * 
 * @param[in]   sKonnect    Pointer to sKonnect.
 * 
 * @param[in]   buff        Buffer
 * 
 * @param[in]   length      Length of buffer.
 *
 * @return skStatus_t
 */
skStatus_t txCircBuffwriteData(skConnect_t *sKonnect, uint8_t* source, int length);

#endif //__SK__CORE__H__