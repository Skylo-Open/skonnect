
/**
* @file         skStatus.h
* @brief        Skylo Konnect SDK status codes.
* @details      Header file for Skylo Konnect SDK status codes, and base error codes for other modules in SDK.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK__STATUS__H__
#define __SK__STATUS__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <inttypes.h>
#include <stddef.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/** @addtogroup skStatus
@{ */

typedef int32_t skStatus_t;

/** @name Error Code Formats
 *
 * Format : -((1000 * <Module ID>) + <Status Code>).
 * @{
 */

#define __SK_ERROR_MOD_OFFSET                (10000)
#define __SK_ERROR_GET_MOD_ID(__moduleID__)  ((__moduleID__) * __SK_ERROR_MOD_OFFSET)
#define __SK_ERROR__(__moduleID__, __error__)  (0 - (__SK_ERROR_GET_MOD_ID(__moduleID__) + (__error__)))
/** @} */ /* end namegroup Error Code Formats */

/** @name Module IDs
 *
 * The following definitions gives the IDs for the different modules of
 * the Skylo Konnect SDK.
 *
 * If Skylo Konnect SDK user want to added their own module IDs, it is recommended
 * to start at 200 to avoid possible conflicts with updates of the Skylo Konnect SDK in future.
 * @{
 */
#define SK_MODULE_BASE                        (0)
#define SK_MODULE_MODEM                       (1)
#define SK_MODULE_SMP                         (2)
#define SK_MODULE_UART                        (3)
#define SK_MODULE_IPC                         (4)
#define SK_MODULE_GPS                         (5)
#define SK_MODULE_FS                          (6)
#define SK_MODULE_TIME                        (7)

/** @} */ /* end namegroup Module IDs */

/** @name Generic Status Codes
 *
 * The following definitions gives the status codes generic to all of
 * the Skylo Konnect SDK modules.
 * @{
 */

#define SK_OK                              ((skStatus_t)(0))                              /**< Success.                   */
#define SK_ERROR                           ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE,  1))) /**< General error.             */
#define SK_ERR_INVALID_PARAM               ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE,  2))) /**< Invalid parameter.         */
#define SK_ERR_NO_MEMORY                   ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE,  3))) /**< Memory allocation error.   */
#define SK_ERR_NO_RESOURCE                 ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE,  4))) /**< Resource allocation error. */
#define SK_ERR_BUSY                        ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE,  6))) /**< Operation is busy.          */
#define SK_ERR_NO_ENTRY                    ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE,  7))) /**< Entry was not found.       */
#define SK_ERR_NOT_SUPPORTED               ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE,  8))) /**< Feature is not supported.  */
#define SK_ERR_TIMEOUT                     ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE,  9))) /**< Operation timed out.       */
#define SK_ERR_BOUNDS                      ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE, 10))) /**< Out of bounds.             */
#define SK_ERR_BAD_PAYLOAD                 ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE, 11))) /**< Bad payload.               */
#define SK_ERR_EXISTS                      ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE, 12))) /**< Entry already exists.      */
#define SK_ERR_EAGAIN                      ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE, 13))) /**< Try again */
#define SK_ERR_FATAL                       ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE, 14))) /**< Error that should never happen. */
#define SK_ERR_VAL                         ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE, 15))) /**< Value does not exists.     */
#define SK_ERR_UNKNOWN                     ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE, 16))) /**< Not implemented yet */
#define SK_ERR_FAILED                      ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE, 17))) /**< operation failed */
#define SK_ERR_NOTALLOWED                  ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE, 18))) /**< Operation is not allowed */
#define SK_ERR_INVALID                     ((skStatus_t)(__SK_ERROR__(SK_MODULE_BASE, 19))) /**< Invalid key */


/** @} */ /* end namegroup Generic Status code*/

/** @} */

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

#endif //__SK__STATUS__H__