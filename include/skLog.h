/**
* @file         skLog.h
* @brief        Skylo Log module macro and declarations.
* @details      Header file for Skylo Konnect log module.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK__LOG__H__
#define __SK__LOG__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skDefs.h>
#include <inttypes.h>

#include <skCoreConfig.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

#define _(Arg) SK_THROWAWAY_LOG(Arg)

#ifndef SK_WITH_THROWAWAY_LOGS
#    define SK_THROWAWAY_LOG(Arg) Arg
#else
#    define SK_THROWAWAY_LOG(Arg) " "
#endif // SK_WITH_THROWAWAY_LOGS

#define _sk_log(Module, ...) \
    SKLOG__(SK_2_STR(Module), __VA_ARGS__)

#define SKLOG__(ModuleStr,Level, ...) \
    skLogFunction(                    \
            ModuleStr,Level, __FILENAME__, __LINE__, __VA_ARGS__)

#define SK_F_PRINTF(fmt_idx, ellipsis_idx) \
        __attribute__((format(printf, fmt_idx, ellipsis_idx)))

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/**
 * @enum        enum to for skylo log levels.
 * @details     enum for skylo log levels.
 */
typedef enum skLogLevel_s {
    SKYLO_EMERG_E,	/* system is unusable */
    SKYLO_ALERT_E,	/* action must be taken immediately */
    SKYLO_CRIT_E,	    /* critical conditions */
    SKYLO_ERR_E,	    /* error conditions */
    SKYLO_INFO_E,    	/* informational */
    SKYLO_WARNING_E,	/* warning conditions */
    SKYLO_NOTICE_E,	/* normal but significant condition */
    SKYLO_DEBUG_E,	/* debug-level messages */
    // SKYLO_NONE  	/* do not print message level */
} skLogLevel_e;


/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/**
 * @brief
 * Function to print sk konnect logs.
 *
 * @details
 * Function to print sk konnect logs.
 *
 * @param[in]   module Module name as string.
 * @param[in]   level  Log level.
 * @param[in]   file   File name as string.
 * @param[in]   line   Line number.
 * @param[in]   msg    Log message.
 *
 * @return      None
 */
void skLogFunction(const char *module,
                    skLogLevel_e level,
                    const char *file,
                    unsigned line,
                    const char *msg,
                    ...) SK_F_PRINTF(5, 6);

#endif //__SK__LOG__H__