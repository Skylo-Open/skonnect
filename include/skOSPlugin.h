
/**
* @file         skOSPlugin.h
* @brief        //TODO
* @todo         Update the brief and details
* @details      Header file for Skylo Konnect SDK Connectivity OS end points for porting.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK__OS_PLUGIN__H__
#define __SK__OS_PLUGIN__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skStatus.h>
#include <skLog.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

#define SK_THREAD_ATTR_OBJ_SIZE 16*8

#define SK_SIG_ATTRIBUTE_WAIT_FOR_ANY   0x00000001U /**< Wait for any signal */
#define SK_SIG_ATTRIBUTE_WAIT_FOR_ALL   0x00000002U /**< Wait for all signals */
#define SK_SIG_ATTRIBUTE_CLEAR_MASK     0x00000004U /**< clear specified signal */

#define SK_TIME_NO_WAIT       0x00000000 /**< Return without waiting. */
#define SK_TIME_WAIT_FOREVER  0xFFFFFFFF /**< Blocking */

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/** @addtogroup OS_types
@{ */

/**
 * @struct      Thread attributes opaque structure.
 *
 * @details     Structure will be used as opaque object to represent thread attributes irrrespective of system used.
 */
typedef struct skThreadAttributes_s {

    uint64_t buff[SK_THREAD_ATTR_OBJ_SIZE];
} skThreadAttributes_t;

/**
 * @brief Sconnect thread type.
 */
typedef uint64_t skThread_t;

/**
 * @brief Sconnect mutex type.
 */
typedef void*  skMutex_t;

/**
 * @brief Sconnect time(second) type.
 */
typedef uint32_t skTime_t;

/**
 * @brief Sconnect time (msec) type.
 */
typedef uint64_t skMSectime_t;

/**
 * @struct      Skylo Konnect signal type structure.
 */
typedef void *skSignal_t;

/**
 * @brief Sconnect initialize once type.
 */
typedef void skyloInitOnceHandle_t;

/** @} */   /* end group */

/*-------------------------------------------------------------------------
 * Thread start
 *-----------------------------------------------------------------------*/

/** @addtogroup OS_thread
@{ */

/** @name Thread function pointers.
 * @{
 */

/**
 * @brief
 * Thread Handler Type.
 *
 * @details
 * Thread Handler Type.
 *
 * @param[in]   arg Thread argument.
 *
 * @return None.
 */
typedef void *(*threadHandler_fp)(void *arg);

/**
 * @brief
 * Create thread function pointer.
 *
 * @details
 * Create thread function pointer.
 *
 * @param[out]    threadHandle     Thread Handle.
 * @param[out,in] threadAttr       Thread attributes.
 * @param[in]     handler          Thread handler.
 * @param[in]     arg              Thread argument.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*create_fp)(skThread_t* threadHandle,skThreadAttributes_t* threadAttr, threadHandler_fp handler,void *arg);

/**
 * @brief
 * Thread attribute init function pointer.
 *
 * @details
 * Thread attribute init function pointer.
 *
 * @param[out,in] threadAttr       Thread attributes.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*attrInit_fp)(skThreadAttributes_t *attr);

/**
 * @brief
 * Thread attribute set name function pointer.
 *
 * @details
 * Thread attribute set name function pointer.
 *
 * @param[out,in] threadAttr       Thread attributes.
 * @param[in]     name             Thread name.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*attrSetName_fp)(skThreadAttributes_t *attr, const char* name);

/**
 * @brief
 * Thread attribute set priority function pointer.
 *
 * @details
 * Thread attribute set priority function pointer.
 *
 * @param[out,in] threadAttr       Thread attributes.
 * @param[in]     priority         Thread priority.
 *
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*attrSetPriority_fp)(skThreadAttributes_t *attr, uint16_t priority);

/**
 * @brief
 * Thread attribute set stack depth function pointer.
 *
 * @details
 * Thread attribute set stack depth function pointer.
 *
 * @param[out,in] threadAttr       Thread attributes.
 * @param[in]     stackDepth       Thread stack size.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*attrSetStackDepth_fp)(skThreadAttributes_t *attr, uint16_t stackDepth);

/**
 * @brief
 * Suspend thread function pointer.
 *
 * @details
 * Suspend thread function pointer.
 *
 * @param[in]    threadHandle     Thread Handle.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*suspendFp)(skThread_t threadHandle);

/**
 * @brief
 * Resume thread function pointer.
 *
 * @details
 * Resume thread function pointer.
 *
 * @param[in]    threadHandle     Thread Handle.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*resume_fp)(skThread_t threadHandle);

/**
 * @brief
 * Stop thread function pointer.
 *
 * @details
 * Stop thread function pointer.
 *
 * @param[in]    threadHandle     Thread Handle.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*stop_fp)(skThread_t threadHandle);
/** @} */ /* end namegroup Thread function pointers */

/** @name Thread endpoint Context.
 * @{
 */

/**
 * @struct      Structure of Skylo Konnect SDK Thread Porting Endpoints.
 * @details     This structure will be used to port the necessary thread endpoints which can be plugged at SDK initialisation time
 *              by application programmer.
 *
 */
typedef struct skPepOSThread_s {

    create_fp fpCreate;                       /**< Function pointer to create thread */
    attrInit_fp fpAttrInit;                   /**< Function pointer to init thread attributes */
    attrSetName_fp fpAttrSetName;             /**< Function pointer to set thread name */
    attrSetStackDepth_fp fpAttrSetStackDepth; /**< Function pointer to set thread stack depth */
    attrSetPriority_fp fpAttrSetPriority;     /**< Function pointer to set thread priority */
    suspendFp fpSuspend;                     /**< Function pointer to suspend thread */
    resume_fp fpResume;                       /**< Function pointer to resume thread */
    stop_fp fpStop;                           /**< Function pointer to stop thread */
} skPepOSThread_t;
/** @} */ /* end namegroup Thread endpoint Context */

/** @} */   /* end group */

/** @addtogroup OS_IPC
@{ */

/** @name IPC function pointers.
 * @{
 */

/**
 * @brief
 * Mutex init function pointer.
 *
 * @details
 * Mutex init function pointer.
 *
 * @param[out]  mutex     Pointer to mutex object.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*mutexInit_fp)(skMutex_t *mutex);

/**
 * @brief
 * Mutex destroy function pointer.
 *
 * @details
 * Mutex destroy function pointer.
 *
 * @param[in]  mutex     Pointer to mutex object.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*mutexDestroy_fp)(skMutex_t *mutex);

/**
 * @brief
 * Mutex lock function pointer.
 *
 * @details
 * Mutex lock function pointer.
 *
 * @param[in]  mutex     Pointer to mutex object.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*mutexLock_fp)(skMutex_t *mutex);

/**
 * @brief
 * Mutex Lock timed function pointer.
 *
 * @details
 * Mutex Lock timed function pointer.
 *
 * @param[in]  mutex        Pointer to mutex object.
 * @param[in]  msecTimeout  timeout in msec.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*mutexLockTimed_fp)(skMutex_t *mutex, skMSectime_t msecTimeout);

/**
 * @brief
 * Mutex Unlock timed function pointer.
 *
 * @details
 * Mutex Unlock timed function pointer.
 *
 * @param[out]  mutex     Pointer to mutex object.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*mutexUnlock_fp)(skMutex_t *mutex);

/**
 * @brief
 * Signal Init function pointer.
 *
 * @details
 * Signal Init function pointer.
 *
 * @param[out]  signal     Pointer to signal object.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*signalInit_fp)(skSignal_t *signal);

/**
 * @brief
 * Signal Destroy function pointer.
 *
 * @details
 * Signal Destroy function pointer.
 *
 * @param[in]  signal     Pointer to signal object.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*signalDestroy_fp)(skSignal_t *signal);

/**
 * @brief
 * Signal Wait function pointer.
 *
 * @details
 * Signal Wait function pointer.
 *
 * @param[in]  signal     Pointer to signal object.
 * @param[in]  mask       Signal mask.
 * @param[in]  attr       Signal attributes.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef uint32_t (*signalWait_fp)(skSignal_t *signal, uint32_t mask, uint32_t attr);

/**
 * @brief
 * Signal Wait timed function pointer.
 *
 * @details
 * Signal Wait timed function pointer.
 *
 * @param[in]  signal       Pointer to signal object.
 * @param[in]  attr         Signal attributes.
 * @param[in]  mask         Signal mask.
 * @param[out] currSignals  Pointer to currently set signals.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*signalWaitTimed_fp)(skSignal_t *signal, uint32_t mask,uint32_t attribute, uint32_t *currSignals, skTime_t timeout);

/**
 * @brief
 * Signal Set function pointer.
 *
 * @details
 * Signal Set function pointer.
 *
 * @param[in]  signal       Pointer to signal object.
 * @param[in]  mask         Signal mask.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*signalSet_fp)(skSignal_t *signal, uint32_t mask);

/**
 * @brief
 * Signal Clear function pointer.
 *
 * @details
 * Signal Clear function pointer.
 *
 * @param[in]  signal       Pointer to signal object.
 * @param[in]  mask         Signal mask.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*signalClear_fp)(skSignal_t *signal, uint32_t mask);

/**
 * @brief
 * Signal Get function pointer.
 *
 * @details
 * Signal Get function pointer.
 *
 * @param[in]  signal       Pointer to signal object.
 * @param[in]  mask         Signal mask.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*signalGet_fp)(skSignal_t *signal, uint32_t mask);
/** @} */ /* end namegroup IPC function pointers */

/** @name IPC endpoint Context.
 * @{
 */

/**
 * @struct      Structure of Skylo Konnect SDK Thread IPC Endpoints.
 * @details     This structure will be used to port the necessary IPC endpoints which can be plugged at SDK initialisation time
 *              by application programmer.
 */
typedef struct skPepOSIPC_s {

    //mutex
    mutexInit_fp fpMutexInit;             /**< Mutex init funtion pointer */
    mutexInit_fp fpMutexDestroy;          /**< Mutex destroy funtion pointer */
    mutexLock_fp fpMutexLock;             /**< Mutex lock funtion pointer */
    mutexLockTimed_fp fpMutexLockTimed;   /**< Mutex lock timed funtion pointer */
    mutexUnlock_fp fpMutexUnlock;         /**< Mutex unlock funtion pointer */
    //signal
    signalInit_fp fpSignalInit;           /**< Signal Init funtion pointer */
    signalDestroy_fp fpSignalDestroy;     /**< Signal destroy funtion pointer */
    signalWait_fp fpSignalWait;           /**< Signal Wait funtion pointer */
    signalWaitTimed_fp fpSignalWaitTimed; /**< Signal wait timed funtion pointer */
    signalSet_fp fpSignalSet;             /**< Signal Set funtion pointer */
    signalClear_fp fpSignalClear;         /**< Signal Clear funtion pointer */
    signalGet_fp fpSignalGet;             /**< Signal Get funtion pointer */
} skPepOSIPC_t;
/** @} */ /* end namegroup IPC endpoint Context */

/** @} */ /* end group */

/** @addtogroup OS_time
@{ */

/** @name OS time function pointers.
 * @{
 */

/**
 * @brief
 * Time Sleep function pointer.
 *
 * @details
 * Time Sleep function pointer.
 *
 * @param[in]  timeMSec       time in msec.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*sleep_fp)(skMSectime_t timeMSec);

/**
 * @brief
 * Time Delay function pointer.
 *
 * @details
 * Time Delay function pointer.
 *
 * @param[in]  timeMSec       time in msec.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*delay_fp)(skMSectime_t timeMSec);

/**
 * @brief
 * Get system tick time in msec function pointer.
 *
 * @details
 * Get system tick time in msec function pointer.
 *
 * @return  Sys tick time as skTime_t
 */
typedef skTime_t (*getSystickInmSec_fp)(void);

/**
 * @brief
 * Get GPS epoch in sec function pointer.
 *
 * @details
 * Get GPS epoch in sec function pointer.
 *
 * @return  GPS epoch time as skTime_t
 */
typedef skTime_t (*getGpsEpochInSec_fp)(void);

/**
 * @brief
 * Get GPS epoch in msec function pointer.
 *
 * @details
 * Get GPS epoch in msec function pointer.
 *
 * @return  GPS epoch time as skTime_t
 */
typedef skTime_t (*getGpsEpochInMSec_fp)(void);

/**
 * @brief
 * Get Unix epoch in sec function pointer.
 *
 * @details
 * Get Unix epoch in sec function pointer.
 *
 * @return  Unix epoch time as skTime_t
 */
typedef skTime_t (*getUnixEpochInSec_fp)(void);

/**
 * @brief
 * Get Unix epoch in msec function pointer.
 *
 * @details
 * Get Unix epoch in msec function pointer.
 *
 * @return  Unix epoch time as skTime_t
 */
typedef skTime_t (*getUnixEpochInMSec_fp)(void);

/** @} */ /* end namegroup time function pointers */

/** @name Time endpoint Context.
 * @{
 */


/**
 * @struct      Structure of Skylo Konnect SDK Thread IPC Endpoints.
 * @details     This structure will be used to port the necessary IPC endpoints which can be plugged at SDK initialisation time
 *              by application programmer.
 *
 */
typedef struct skPepOSTime_s {

    sleep_fp fpSleep;                             /**< sleep function pointer */
    delay_fp fpDelay;                             /**< delay function pointer */
    getSystickInmSec_fp fpGetSystickInmSec;       /**< Get system tick time in msec function pointer */
    getGpsEpochInSec_fp getGpsEpochInSec_fp;      /**< Get GPS epoch time in sec function pointer */
    getGpsEpochInMSec_fp getGpsEpochInMSec_fp;    /**< Get GPS epoch time in msec function pointer */
    getUnixEpochInSec_fp gpGetUnixEpochInSec;     /**< Get Unix epoch time in sec function pointer */
    getUnixEpochInMSec_fp fpGetUnixEpochInMSec;   /**< Get Unix epoch time in msec function pointer */
} skPepOSTime_t;

/** @} */ /* end namegroup Time endpoint Context */

/** @} */ /* end group */

/** @addtogroup OS_Memory
@{ */

/** @name OS Memory function pointers.
 * @{
 */

/**
 * @brief
 * Malloc function pointer.
 *
 * @details
 * Malloc function pointer.
 *
 * @return Pointer to the allocated memory. On error return NULL.
 */
typedef void *(*malloc_fp)(size_t size);

/**
 * @brief
 * Free function pointer.
 *
 * @details
 * Free function pointer.
 */
typedef void (*free_fp)(void *ptr);

/**
 * @brief
 * Calloc function pointer.
 *
 * @details
 * Calloc function pointer.
 *
 * @return Pointer to the allocated memory. On error return NULL.
 */
typedef void *(*calloc_fp)(size_t nmemb, size_t size);

/**
 * @brief
 * Realloc function pointer.
 *
 * @details
 * Realloc function pointer.
 *
 * @return Pointer to the newly allocated memory, or NULL if the request fails.If size was equal to 0, either NULL or
 *          a pointer suitable to be passed to free() is returned.  If realloc() fails, the original block is left untouched; it is not
 *          freed or moved.
 */
typedef void *(*realloc_fp)(void *ptr, size_t size);

/** @} */ /* end namegroup Memory function pointers */

/** @} */ /* end group */

/** @addtogroup OS_miscellaneous
@{ */

/** @name OS miscellaneous function pointers.
 * @{
 */

/**
 * @brief
 * Init Once function typedef.
 *
 * @details
 * Init Once function typedef.
 *
 * @param[in]   funcArg function argument
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*skyloInitOnceFunc_fp)(void *funcArg);

/**
 * @brief Init Once function pointer.
 *
 * @details This Api initializes given function pointer once only, all calls to init once with same handle
 *          which is already initialized should be ignored.
 *
 * @param[in]   handle  Handle to init object
 * @param[in]   func    Function pointer
  * @param[in]  funcArg Function argument
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERROR on failure.
 */
typedef skStatus_t (*initOnceApi_fp)(volatile skyloInitOnceHandle_t *handle,
                skyloInitOnceFunc_fp *func,
                void *funcArg);

/**
 * @brief
 * Assert.
 *
 * @details
 * Assert.
 */
typedef void (*assert_fp)(void);

/**
 * @brief
 * Function to handler logs.
 *
 * @details
 * Function to handler logs.
 *
 * @param[in]   skLogLevel_e    log level.
 * @param[in]   module          Module string.
 * @param[in]   msg             Log message.
 */
typedef void (*logHandler_fp)(int skLogLevel_e,const char *module, const char *msg);

/** @} */ /* end namegroup miscellaneous function pointers */

/** @} */ /* end group */

/** @addtogroup OS_Main_context
@{ */

/** @name OS endpoint Main context.
 * @{
 */

/**
 * @struct      Structure of Skylo Konnect SDK File System end points for porting.
 * @details     This structure will be used to port the necessary file system endpoints which can be plugged at SDK initialisation time
 *              by application programmer.
 */
typedef struct skPepOS_s {

    skPepOSThread_t thread;       /**< Thread  Endpoint structure variable */
    skPepOSIPC_t ipc;             /**< ipc Endpoint structure variable */
    skPepOSTime_t time;           /**< Time Endpoint structure variable */
    malloc_fp fpMalloc;           /**< Malloc function pointer */
    free_fp fpFree;               /**< Free function pointer */
    calloc_fp fpCalloc;           /**< Calloc function pointer */
    realloc_fp fpRealloc;         /**< Realloc function pointer */
    initOnceApi_fp fpInitOnceApi; /**< Init once function pointer */
    assert_fp fpAssert;           /**< assert */
    logHandler_fp fpLogHandler;   /**< log Handler */
} skPepOS_t;

/** @} */ /* end namegroup endpoint Main context */

/** @} */ /* end group */

#endif //__SK__OS_PLUGIN__H__