
/**
* @file         skModemStatus.h
* @brief        //TODO
* @todo         Update the brief and details
* @details      Header file for Skylo Konnect SDK Modem related error codes.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
* @copyright    //TODO< given by sreedhar>
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK__MODEM_STATUS__H__
#define __SK__MODEM_STATUS__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/** @addtogroup skModemStatus
@{ */

/** @name Modem status Codes
 *
 * The following definitions gives the status codes for various Modem operations
 * @todo write guideline for porting
 * @{
 */

/**< General modem error */
#define SK_MODEM_ERROR                 ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 1)))
/**< UE not attached to network */
#define SK_MODEM_ERR_NOT_ATTACHED      ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 2)))
/**< Invalid callback handler */
#define SK_MODEM_ERR_INV_HANDLER       ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 3)))
/**< Cannot fire command because busy */
#define SK_MODEM_ERR_FIRE_BUSY         ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 4)))
/**< Modem Bus i.e. UART is not available */
#define SK_MODEM_ERR_BUS_NOT_AVAIL     ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 5)))
/**< SIM is not ready or available */
#define SK_MODEM_ERR_SIM_NOT_READY     ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 6)))
/**< AT command line is not ready yet */
#define SK_MODEM_ERR_AT_NOT_READY      ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 7)))
/**< Modem already initialized */
#define SK_MODEM_ERR_ALREADY_INIT      ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 8)))
/**< Modem transmit data too big to send. No segmentation for now. */
#define SK_MODEM_ERR_DATA_TOO_BIG      ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 9)))
/**< Cannot create socket */
#define SK_MODEM_ERR_CREATE_SOCK       ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 10)))
/**< Cannot activate socket */
#define SK_MODEM_ERR_ACTIVATE_SOCK     ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 11)))
/**< Modem socket is not active */
#define SK_MODEM_ERR_SOCK_NOT_ACTIVE   ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 12)))
/**< Modem socket is not available */
#define SK_MODEM_ERR_SOCK_NOT_AVAIL    ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 13)))
/**< Cannot configure Packet Domain reporting */
#define SK_MODEM_ERR_PGW_REPORT        ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 14)))
/**< Notification already exists */
#define SK_MODEM_ERR_NOTIFY_EXISTS     ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 15)))
/**< Cannot register because modem already init. */
#define SK_MODEM_ERR_NOTIFY_REGISTER   ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 16)))
/**< Invalid memory location */
#define SK_MODEM_ERR_INVALID_MEM       ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 17)))
/**< Size exceeded */
#define SK_MODEM_ERR_SIZE_EXCEEDED     ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 18)))
/**< Invalid AT command */
#define SK_MODEM_ERR_INVALID_AT_CMD    ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 19)))
/**< Cannot convert hex string to byte array */
#define SK_MODEM_ERR_HEX_CONV          ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 20)))
/**< Cannot retrieve valid GPS time from module */
#define SK_MODEM_ERR_INVALID_GPS_TIME  ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 21)))
/**< Cannot retrieve valid GPS date from module */
#define SK_MODEM_ERR_INVALID_GPS_DATE  ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 21)))
/**< Cannot configure NOTIFYEV reporting */
#define SK_MODEM_ERR_NOTIFYEV          ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 22)))
/**< Cannot get response from AT command */
#define SK_MODEM_ERR_AT_NO_RESP        ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 23)))
/**< Logging error */
#define SK_MODEM_ERR_LOGGING           ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 24)))
/**< No memory */
#define SK_MODEM_ERR_NO_MEM            ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 25)))
/**< Cannot turn off modem functionality */
#define SK_MODEM_ERR_FUN_OFF           ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 26)))
/**< Cannot turn on modem functionality */
#define SK_MODEM_ERR_FUN_ON            ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 27)))
/**< Cannot soft reset modem */
#define SK_MODEM_ERR_RESET             ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 28)))
/**< Invalid Modem Version */
#define SK_MODEM_ERR_INVALID_MDM_VER   ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 29)))
/**< Invalid SIM ICCID */
#define SK_MODEM_ERR_INVALID_ICCID     ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 30)))
/**< Invalid SIM IMSI */
#define SK_MODEM_ERR_INVALID_IMSI      ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 31)))
/**< Invalid Modem IMEI */
#define SK_MODEM_ERR_INVALID_IMEI      ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 32)))
/**< Invalid GPS handler */
#define SK_MODEM_ERR_INVALID_GPS_HDL   ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 33)))
/**< SIM state is invalid and cannot be read */
#define SK_MODEM_ERR_INVALID_SIM_STATE ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 34)))
/**< Cannot fire command because mutex lock is captured by other thread */
#define SK_MODEM_ERR_RESOURCE_IN_USE   ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 35)))
/**< Cannot configure GNSSCBOOT event */
#define SK_MODEM_ERR_GNSSCBOOT_EV      ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 40)))
/**< Cannot configure EPHUPD event */
#define SK_MODEM_ERR_EPHUPD_EV         ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 41)))
/**< Cannot configure Channel Raster Offset */
#define SK_MODEM_ERR_RASTER_OFFSET     ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 42)))
/**< Cannot configure TA offset */
#define SK_MODEM_ERR_TA_OFFSET         ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 43)))
/**< Cannot enable sattelite mode */
#define SK_MODEM_ERR_ENABLE_SAT        ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 44)))
/**< Cannot configure RTT / TA events */
#define SK_MODEM_ERR_RTT_TA_EV         ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 45)))
/**< Cannot activate GNSS (GPS) */
#define SK_MODEM_ERR_GNSS_ACT          ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 46)))
/**< Cannot acquire GNSS fix */
#define SK_MODEM_ERR_GNSS_NO_FIX       ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 47)))
/**< Cannot update RTT one-way delay */
#define SK_MODEM_ERR_RTT_UPDATE_DELAY  ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 48)))
/**< Cannot turn cell camp on */
#define SK_MODEM_ERR_CELL_CAMP_ON      ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 49)))
/**< Cannot attach to network */
#define SK_MODEM_ERR_NW_ATTACH         ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 50)))
/**< Cannot enable NMEA events from IGNSS */
#define SK_MODEM_ERR_NMEA_EV           ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 51)))
/**< Cannot configure NMEA events from IGNSS */
#define SK_MODEM_ERR_NMEA_CFG          ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 52)))
/* RK_3.2 ONLY */
/**< Cannot configure SKYLO GNSS params */
#define SK_MODEM_ERR_GNSSCFG_SKYLO     ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 53)))
/**< Cannot configure URC FIX for IGNSS */
#define SK_MODEM_ERR_GNSS_FIX_URC      ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 54)))
/**< Cannot enable I-GNSS blanking URC notifications */
#define SK_MODEM_ERR_GNSS_BLANKING_URC ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 55)))
/**< Cannot send NIP data because of modem is in GNSS mode */
#define SK_MODEM_ERR_DATA_SEND_GNSS    ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 56)))
/**< Cannot send NIP data because modem is not IDLE or ATTACHED */
#define SK_MODEM_ERR_DATA_SEND         ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 57)))
/**< More multicast data available after this RX */
#define MODEM_MC_MORE_DATA             ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 58)))
/**< Modem is not init  */
#define SK_MODEM_ERR_NOT_INIT          ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 59)))
/* RK_3.2 ONLY END */

/**< Cannot configure modem to disable auto-connect mode */
#define SK_MODEM_ERR_SETACFG_AC         ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 60)))
/**< Cannot configure to disable sleep mode */
#define SK_MODEM_ERR_SETACFG_SLEEP_MODE ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 61)))
/**< Cannot configure locsrv to enable */
#define SK_MODEM_ERR_SETACFG_LOCSRV     ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 62)))
/**< Cannot configure locsrv to auto-restart */
#define SK_MODEM_ERR_SETACFG_LOCSRV_AR  ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 63)))
/**< Cannot configure locsrv blanking guard */
#define SK_MODEM_ERR_SETACFG_LOCSRV_BG  ((skStatus_t)(__SK_ERROR__(SK_MODULE_MODEM, 64)))
/**< General OK status */
#define SK_MODEM_OK                     ((skStatus_t)(0))

/** @} */ /* end namegroup "Modem status code"*/

/** @} end addtogroup skModemStatus */

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

#endif //__SK__MODEM_Status__H__