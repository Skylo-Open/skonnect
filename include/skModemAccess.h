
/**
* @file         skModemAccess.h
* @brief        //TODO
* @todo         Update the brief and details
* @details      Header file for Skylo Konnect SDK Connectivity access end points for application programming.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK__MODEM_ACCESS__H__
#define __SK__MODEM_ACCESS__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skStatus.h>
#include <skDefs.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/** @addtogroup Modem_AEP
@{ */

/** @name Modem AEP types.
 * @{
 */

/**
 * @enum     Enum to Control the message delivery over network
 * @details  This enum will decide retry policy of message that is to be delivered.
 */
typedef enum skQOS_e {

    SK_QOS_AT_MOST_E,      /**< Send only once, fire and forget delivery is not gauranteed */
    SK_QOS_AT_ONCE_E,      /**< Send exactly once, Make sure that no reperation occurs using three way handshake */
    SK_QOS_AT_LEAST_E,     /**< Send message multiple time until ACK is received, duplication at either end is acceptable */

    SK_QOS_MAX_E
} skQOS_e;

/**
 * @enum     Enum for Modem state machine
 * @details  This enum will help to maintain and know modem connectivity state.
 */
typedef enum skConnState_e {

    //TODO add states here
    SK_MODEM_STATE_MAX_E,
} skConnState_e;

/** @} */ /* end namegroup Modem AEP types */

/** @name Modem AEP function pointers.
 * @{
 */

/**
 * @brief access end point to create skylo Konnect library object
 *
 * @details access end point to open Skylo Konnect library object
 *
 * @param[out]   skConnect  skConnect
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skMODEMErrorCode.
 *
 */
typedef skStatus_t (*createObj_fp)(void *skConnect);

/**
 * @brief access end point to initialise Skylo Konnect library
 *
 * @details access end point to initialise Skylo Konnect Library
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skMODEMErrorCode.
 *
 */
typedef skStatus_t (*init_fp)(void);    // initialize modem sw

/**
 * @brief porting end point to connect to base station
 *
 * @details porting end point to connect to base station
 *          connect modem (may be to power save) only call after successful disconnect.
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skMODEMErrorCode.
 *
 */
typedef skStatus_t (*connect_fp)(void);

/**
 * @brief porting end point to disconnect from base station
 *
 * @details porting end point to disconnect from base station
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skMODEMErrorCode.
 *
 */
typedef skStatus_t (*disconnect_fp)(void);    // disconnect modem (power save?)

/**
 * @brief porting end point to check is modem connected to BS right now.
 *
 * @details porting end point to check is modem connected to BS right now.
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skMODEMErrorCode.
 *
 */
typedef skBool_t (*isConnected_fp)(void);

/**
 * @brief porting end point to share modem connection status code.
 *
 * @details porting end point to share modem connection status code.
 *          share last modem connection error code.
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skMODEMErrorCode.
 *
 */
typedef skStatus_t (*getModemConnStatusCode_fp)(void);

/**
 * @brief porting end point to share modem signal status.
 *
 * @details porting end point to share modem signal status.
 *          share last modem connection error code.
 *
 * @param[out]  rssi    Rssi
 * @param[out]  rsrq    Rsrq
 * @param[out]  rsrp    Rsrp
 * @param[out]  sinr    Sinr
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skMODEMErrorCode.
 *
 */
typedef skStatus_t (*getModemSignalStatus_fp)(int8_t *rssi, int8_t *rsrq, int8_t *rsrp, int8_t *sinr);

/**
 * @brief porting end point to share modem connection state.
 *
 * @details porting end point to share modem connection state.
 *          This state will indicate modem is in which state of connectivity state machine cycle.
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skMODEMErrorCode.
 *
 */
typedef skConnState_e (*getConnState_fp)(void);

/**
 * @brief porting end point to send message to skylo SkyHose over SMP.
 *
 * @details porting end point to send message to skylo SkyHose over SMP.
 *
 * @param[in]   msg         Message
 * @param[in]   length      Lenght of msg
 * @param[in]   qosLevel    Qos level for message
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skMODEMErrorCode.
 *
 */
typedef skStatus_t (*sendMsg_fp)(uint8_t *msg, uint32_t length, skQOS_e qosLevel);

/**
 * @brief porting end point to receive message from skylo SkyHose over SMP.
 *
 * @details porting end point to receive message from skylo SkyHose over SMP.
 *          // Recv app level message  Length is in/out (non Blocking)
 *
 * @param[out]   msg         Message
 * @param[out]   length      Lenght of msg
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skMODEMErrorCode.
 *
 */
typedef skStatus_t (*recvMsg_fp)(uint8_t *msg, uint32_t *length);

/** @} */ /* end namegroup Modem AEP function pointers */

/** @name Modem AEP endpoint context.
 * @{
 */

/**
 * @struct      Structure of Skylo Konnect Modem access end points.
 * @details     This structure will be used to port the necessary endpoints which can be plugged at SDK initialisation time
 *              by application programmer.
 * @todo        Add more info.
 */
typedef struct skAepModem_s {

    createObj_fp fpCreateObj;                                   /**<//TODO*/
    init_fp fpInit;                                             /**<//TODO*/
    connect_fp fpConnect;                                       /**<//TODO*/
    disconnect_fp fpDisconnect;                                 /**<//TODO*/
    isConnected_fp fpIsConnected;                               /**<//TODO*/
    getModemConnStatusCode_fp fpGetModemConnStatusCode;         /**<//TODO*/
    getModemSignalStatus_fp fpGetModemSignalStatus;             /**<//TODO*/
    getConnState_fp fpGetConnState;                             /**<//TODO*/
    sendMsg_fp fpSendMsg;                                       /**<//TODO*/
    recvMsg_fp fpRecvMsg;                                       /**<//TODO*/
} skAepModem_t;

/** @} */ /* end namegroup Modem AEP endpoint context */

/** @} */ /* end group */

#endif //__SK__MODEM_ACCESS__H__