/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

#include <stdarg.h>
#include <stdio.h>

#include "stm32l496g_discovery.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdlib.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"

#include <altConsole.h>

#include "main.h"

#include <portOSPlugin.h>
#include <portHALPlugin.h>
#include <portGPSPlugin.h>

#include <skCore.h>
#include <skLog.h>
#include <skStatus.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;
ADC_HandleTypeDef hadc3;

DCMI_HandleTypeDef hdcmi;

DFSDM_Channel_HandleTypeDef hdfsdm1_channel1;
DFSDM_Channel_HandleTypeDef hdfsdm1_channel2;

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;

UART_HandleTypeDef hlpuart1;
//UART_HandleTypeDef huart1;
//UART_HandleTypeDef huart2;

QSPI_HandleTypeDef hqspi;

SAI_HandleTypeDef hsai_BlockA1;
SAI_HandleTypeDef hsai_BlockB1;

SD_HandleTypeDef hsd1;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;

SRAM_HandleTypeDef hsram1;
SRAM_HandleTypeDef hsram2;

//osThreadId defaultTaskHandle;
/* USER CODE BEGIN PV */
TaskHandle_t gThreadHandle = NULL;
TaskHandle_t gThreadHandleLed = NULL;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void PeriphCommonClock_Config(void);
static void MX_GPIO_Init(void);
void StartDefaultTask(void const * argument);

/* USER CODE BEGIN PFP */
void initSconnectThread();
static void initThread(void *arg);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void initSconnectThread()
{
    xTaskCreate(initThread, /* The function that implements the task. */
                "Init", /* The text name assigned to the task - for debug only as it is not used by
                            the kernel. */
                5*256, /* The size of the stack to allocate to the task. */
                NULL,          /* The parameter passed to the task */
                1, /* The priority assigned to the task - minimal priority. */
                &gThreadHandle);                    /* The task handle is not required, so NULL is passed. */
}
static void initThread(void *arg)
{
    skConnect_t *skConnect = malloc(sizeof(skConnect_t));
    skStatus_t stat = SK_ERROR;

    if ( !skConnect ) {
        altprint(ALT_ERR, "Malloc failed\n");
        vTaskDelete(gThreadHandle);
        return;
    }

    sKonnectGSetContext(skConnect);

    skConnect->pep.os.thread.fpCreate = portThreadCreate;
    skConnect->pep.os.thread.fpAttrInit = portThreadAttrInit;
    skConnect->pep.os.thread.fpAttrSetName = portThreadAttrSetName;
    skConnect->pep.os.thread.fpAttrSetPriority = portThreadAttrSetPri;
    skConnect->pep.os.thread.fpAttrSetStackDepth = portThreadAttrSetStackSize;
    skConnect->pep.os.thread.fpStop = portThreadStop;

    skConnect->pep.os.ipc.fpMutexInit = portMutexInit;
    skConnect->pep.os.ipc.fpMutexDestroy = portMutexDestroy;
    skConnect->pep.os.ipc.fpMutexLock = portMutexLock;
    skConnect->pep.os.ipc.fpMutexLockTimed = portMutexlockTimed;
    skConnect->pep.os.ipc.fpMutexUnlock = portMutexUnlock;

    skConnect->pep.os.ipc.fpSignalInit = portSignalInit;
    skConnect->pep.os.ipc.fpSignalDestroy = portSignalDestroy;
    skConnect->pep.os.ipc.fpSignalWait = portSignalWait;
    skConnect->pep.os.ipc.fpSignalWaitTimed = portSignalWaitTimed;
    skConnect->pep.os.ipc.fpSignalSet = portSignalSet;
    skConnect->pep.os.ipc.fpSignalClear = portSignalclear;

    skConnect->pep.os.time.fpSleep = portSleep;
    skConnect->pep.os.time.fpDelay = portDelay;
    skConnect->pep.os.time.fpGetSystickInmSec = portgetSystickInmSec;
    skConnect->pep.os.time.getGpsEpochInSec_fp = portgetGpsEpochInSec;
    skConnect->pep.os.time.getGpsEpochInMSec_fp = portgetGpsEpochMInSec;
    skConnect->pep.os.time.gpGetUnixEpochInSec = portgetUnixEpochInSec;
    skConnect->pep.os.time.fpGetUnixEpochInMSec = portgetUnixEpochMInSec;

    skConnect->pep.os.fpMalloc = portMalloc;
    skConnect->pep.os.fpFree = portFree;
    skConnect->pep.os.fpCalloc = portCalloc;
    skConnect->pep.os.fpRealloc = portRealloc;

    skConnect->pep.hal.fpModemHWInit = portModemHWInit;
    skConnect->pep.hal.fpModemHWDeInit = portModemHWDeInit;
    skConnect->pep.hal.fpModemSetHwFlowCtrlStatus = portModemSetHwFlowCtrlStatus;
    skConnect->pep.hal.fpModemGetHwFlowCtrlStatus = portModemGetHwFlowCtrlStatus;
    skConnect->pep.hal.fpModemSend = portModemSend;
    skConnect->pep.hal.fpModemRecv = portModemRecv;
    skConnect->pep.hal.fpModemHardReboot = portModemHardReboot;

    skConnect->pep.os.fpLogHandler = portLogHandler;
    skConnect->pep.gps.fpGetVLLASTD = NULL;
    skConnect->fpTwoWayMsgAckHandler = portTwoWayMsgHandler;

    modemOptions_t smodem_options = {0,};

    /* Note: band and earfcn is used for NTN only */
    smodem_options.rfBand   = 256;
    smodem_options.rfEarfcnStart = 229366;
    smodem_options.rfEarfcnEnd = 229366;
    smodem_options.ip = "35.185.230.30";
    smodem_options.port = 5555;
    smodem_options.socketType = MODEM_SOCK_UDP_E;

    // Need to upgade periodically with real GPS data
    smodem_options.ueLat = 60.187;
    smodem_options.ueLong = 24.828;
    smodem_options.ueAlt = 0.0;
    smodem_options.gpsFix = 1;
    
    skConnect->options = &smodem_options;

    if ( sKonnectInit(skConnect, SK_FULL_INIT) == SK_OK ) {

        altprint(ALT_ERR, "SDK init successfull\n");

        while ( 1 ) {

            sKonnectSendHB(skConnect);
            portSleep(5*60*1000);
        }
    }
}
void vApplicationIdleHook(void) {
    /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
    to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
    task.  It is essential that code added to this hook function never attempts
    to block in any way (for example, call xQueueReceive() with a block time
    specified, or call vTaskDelay()).  If the application makes use of the
    vTaskDelete() API function (as this demo application does) then it is also
    important that vApplicationIdleHook() is permitted to return to its calling
    function, because it is the responsibility of the idle task to clean up
    memory allocated by the kernel to any task that has since been deleted. */
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName) {
    (void)pcTaskName;
    (void)pxTask;

    altprint(ALT_ERR, "\n\n\n\nvApplicationStackOverflowHook\n\n\n\n");

    /* Run time stack overflow checking is performed if
    configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
    function is called if a stack overflow is detected. */
    taskDISABLE_INTERRUPTS();
    for (;;)
    ;
}
/*-----------------------------------------------------------*/

void vApplicationTickHook(void) {
    /* This function will be called by each tick interrupt if
    configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h.  User code can be
    added here, but the tick hook is called from an interrupt context, so
    code must not attempt to block, and only the interrupt safe FreeRTOS API
    functions can be used (those that end in FromISR()). */
}

/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook(void) {

    altprint(ALT_ERR, "\n\n\n\vApplicationMallocFailedHook\n\n\n\n");
    /* vApplicationMallocFailedHook() will only be called if
    configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
    function that will get called if a call to pvPortMalloc() fails.
    pvPortMalloc() is called internally by the kernel whenever a task, queue,
    timer or semaphore is created.  It is also called by various parts of the
    demo application.  If heap_1.c or heap_2.c are used, then the size of the
    heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
    FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
    to query the size of free heap space that remains (although it does not
    provide information on how the remaining heap might be fragmented). */
    configASSERT(0);
    for (;;)
    ;
}
/*-----------------------------------------------------------*/


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

/* Configure the peripherals common clocks */
  PeriphCommonClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */
  BSP_LED_Init(LED_ORANGE);
  BSP_LED_Init(LED_GREEN);
  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
//  osThreadDef(defaultTask, StartDefaultTask, 1, 0, 128);
//  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  if ( !altConsoleInit() )    // initialize console
          return 0;

  // HAL_Delay(12000);

  initSconnectThread();

  xTaskCreate(StartDefaultTask, /* The function that implements the task. */
              "led", /* The text name assigned to the task - for debug only as it is not used by
                          the kernel. */
              1*256, /* The size of the stack to allocate to the task. */
              NULL,          /* The parameter passed to the task */
              1, /* The priority assigned to the task - minimal priority. */
              &gThreadHandleLed);

  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  vTaskStartScheduler();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_9;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 5;
  RCC_OscInitStruct.PLL.PLLN = 71;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV4;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }

  /** Enable MSI Auto calibration
  */
  HAL_RCCEx_EnableMSIPLLMode();
}

/**
  * @brief Peripherals Common Clock Configuration
  * @retval None
  */
void PeriphCommonClock_Config(void)
{
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the peripherals clock
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_SAI1|RCC_PERIPHCLK_USB
                              |RCC_PERIPHCLK_SDMMC1|RCC_PERIPHCLK_ADC;
  PeriphClkInit.Sai1ClockSelection = RCC_SAI1CLKSOURCE_PLLSAI1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLLSAI1;
  PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 5;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 20;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_SAI1CLK|RCC_PLLSAI1_48M2CLK
                              |RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOI_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  HAL_PWREx_EnableVddIO2();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();

  // // /*Configure GPIO pin Output Level */
  // HAL_GPIO_WritePin(GPIOH, MFX_WAKEUP_Pin|LCD_PWR_ON_Pin|MIC_VDD_Pin, GPIO_PIN_RESET);


  // // /*Configure GPIO pin Output Level */
  // HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);

  // /*Configure GPIO pin : LED1_Pin */
  // GPIO_InitStruct.Pin = LED1_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // HAL_GPIO_Init(LED1_GPIO_Port, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */

  HAL_GPIO_WritePin(MODEM_LDO_EN_Port, MODEM_LDO_EN_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(MODEM_RST_GPIO_Port, MODEM_RST_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(MODEM_WAKEUP_Port, MODEM_WAKEUP_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : MODEM_LDO_EN_Pin */
  GPIO_InitStruct.Pin = MODEM_LDO_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MODEM_LDO_EN_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : MODEM_RST_Pin */
  GPIO_InitStruct.Pin = MODEM_RST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MODEM_RST_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : MODEM_WAKEUP_Pin */
  GPIO_InitStruct.Pin = MODEM_WAKEUP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MODEM_WAKEUP_Port, &GPIO_InitStruct);
  
  //ldo en
  HAL_GPIO_WritePin(MODEM_LDO_EN_Port, MODEM_LDO_EN_Pin, GPIO_PIN_SET);

  //mdoem reset
  // HAL_Delay(5000);
  HAL_GPIO_WritePin(MODEM_RST_GPIO_Port, MODEM_RST_Pin, GPIO_PIN_RESET);

  HAL_Delay(150);
  
  HAL_GPIO_WritePin(MODEM_RST_GPIO_Port, MODEM_RST_Pin, GPIO_PIN_SET);

  /*wait for modem to reboot, when modem is up it prompts 10 sec to press any key and haly normal boot */
  //HAL_Delay(10000);

  // // //modem wake up
  // HAL_GPIO_WritePin(MODEM_WAKEUP_Port, MODEM_WAKEUP_Pin, GPIO_PIN_SET);

  // HAL_Delay(150);

  // HAL_GPIO_WritePin(MODEM_WAKEUP_Port, MODEM_WAKEUP_Pin, GPIO_PIN_RESET);
  // HAL_Delay(3000);
/* USER CODE END MX_GPIO_Init_2 */
}


char buffer[512]; // Adjust the buffer size as needed
/* USER CODE BEGIN 4 */
void printTaskStates(void)
{

    // Get a human-readable table of task states
    vTaskList(buffer);
    // Print the table to the console or log
    altprint(ALT_DEBUG, "\n\n\nTask Name     State  Priority MinStack TskNum\n");
    altprint(ALT_DEBUG, buffer);
    altprint(ALT_DEBUG, "\n\n\n");
}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
  uint32_t count = 0;
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {
	   //    osDelay(1);
	  vTaskDelay(1000);
	  // my_printf("Turning Orange Toggling\r\n");
	  BSP_LED_Toggle(LED_ORANGE);
	  vTaskDelay(1000);
	  // my_printf("Turning Orange Toggling\r\n");
	  BSP_LED_Toggle(LED_ORANGE);
	  // my_printf("Turning Green Toggling\r\n");
	  // BSP_LED_Toggle(LED_GREEN);
    count++;
    // if ( count % 15 == 0) //every 30 seconds
    //   printTaskStates();
    // vTaskDelay(pdMS_TO_TICKS(1000));

  }
  /* USER CODE END 5 */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
