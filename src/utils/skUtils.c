/**
* @file         skUtils.c
* @details      Skylo Konnect API utilities file.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech)
* @copyright    Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <stdlib.h>
#include <string.h>

#include <skUtils.h>
#include <skLog.h>
#include <skCore.h>


#include "stm32l496g_discovery.h"
#include "main.h"
/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

#define sk_log(...) _sk_log(skUtils, __VA_ARGS__)

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

static const char * gHEXTags = "0123456789ABCDEF";
static uint8_t gResetReason = 0;

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Definitions
 *-----------------------------------------------------------------------*/

void skSetSystemResetReason(int reason)
{
	sk_log(SKYLO_DEBUG_E,"System reset set with reset reason %d\n",reason);
	gResetReason = reason;
    HAL_NVIC_SystemReset();
    
    /* FOR EAP demo avoiding storing reset reason in FS */
}

void skGetSystemResetReason(int *reason)
{
	if ( gResetReason != 0 )
	sk_log(SKYLO_DEBUG_E,"System reset get reset reason %d\n",gResetReason);
	*reason = gResetReason;

}


int skHexString2Binary(char * hex, uint16_t bLen, uint8_t * data)
{
    if ( !data || !hex ) return -1;

    char * ptr = hex;
    uint8_t byte = 0x00;

    uint16_t counter = 0;
    uint8_t i;

    while ( *ptr != '\0' && counter != bLen ) {

        byte = 0x00;
        for (i = 0; i < 2 && *ptr != '\0'; i++) {
            switch (*ptr) {
                case '0': byte |= (i == 0) ? ((0 << 4 ) & 0xF0) : 0  & 0x0F; break;
                case '1': byte |= (i == 0) ? ((1 << 4 ) & 0xF0) : 1  & 0x0F; break;
                case '2': byte |= (i == 0) ? ((2 << 4 ) & 0xF0) : 2  & 0x0F; break;
                case '3': byte |= (i == 0) ? ((3 << 4 ) & 0xF0) : 3  & 0x0F; break;
                case '4': byte |= (i == 0) ? ((4 << 4 ) & 0xF0) : 4  & 0x0F; break;
                case '5': byte |= (i == 0) ? ((5 << 4 ) & 0xF0) : 5  & 0x0F; break;
                case '6': byte |= (i == 0) ? ((6 << 4 ) & 0xF0) : 6  & 0x0F; break;
                case '7': byte |= (i == 0) ? ((7 << 4 ) & 0xF0) : 7  & 0x0F; break;
                case '8': byte |= (i == 0) ? ((8 << 4 ) & 0xF0) : 8  & 0x0F; break;
                case '9': byte |= (i == 0) ? ((9 << 4 ) & 0xF0) : 9  & 0x0F; break;
                case 'A': case 'a': byte |= (i == 0) ? ((10 << 4) & 0xF0) : 10 & 0x0F; break;
                case 'B': case 'b': byte |= (i == 0) ? ((11 << 4) & 0xF0) : 11 & 0x0F; break;
                case 'C': case 'c': byte |= (i == 0) ? ((12 << 4) & 0xF0) : 12 & 0x0F; break;
                case 'D': case 'd': byte |= (i == 0) ? ((13 << 4) & 0xF0) : 13 & 0x0F; break;
                case 'E': case 'e': byte |= (i == 0) ? ((14 << 4) & 0xF0) : 14 & 0x0F; break;
                case 'F': case 'f': byte |= (i == 0) ? ((15 << 4) & 0xF0) : 15 & 0x0F; break;
                default: sk_log(SKYLO_DEBUG_E, "Invalid hex value: %c\n", *ptr); return -1;
            }
            ptr++;
        }
        data[counter] = byte;
        counter++;
    }

    return 0;
}

int skBinary2HexString(uint8_t * data, uint16_t len, char * hex)
{
    if ( !data || !hex ) return -1;

    uint16_t counter = 0;
    uint8_t * ptr = data;

    while ( counter < len*2 ) {
        // sk_log(SKYLO_DEBUGSKYLO_DEBUGSKYLO_DEBUG, "%c. UN: %c, LN: %c\n", *ptr, gHEXTags[(*ptr & 0xF0) >> 4], gHEXTags[*ptr & 0x0F]);
        hex[counter]   = gHEXTags[(*ptr & 0xF0) >> 4];
        hex[counter+1] = gHEXTags[*ptr & 0x0F];

        counter += 2;
        ptr++;
    }
    return 0;
}

uint64_t skHex2Int(char *hex)
{
    uint64_t val = 0;

    if ( hex == NULL ) {

        sk_log(SKYLO_DEBUG_E,"Invalid Parameter\n");
        return SK_ERR_INVALID_PARAM;
    }

    while ( *hex ) {

        // get current character then increment
        uint8_t byte = *hex++;
        // transform hex character to the 4bit equivalent number, using the ascii table indexes
        if (byte >= '0' && byte <= '9') byte = byte - '0';
        else if (byte >= 'a' && byte <='f') byte = byte - 'a' + 10;
        else if (byte >= 'A' && byte <='F') byte = byte - 'A' + 10;
        // shift 4 to make space for new digit, and add the 4 bits of the new digit
        val = (val << 4) | (byte & 0xF);
    }
    return val;
}

void skMSecSleep(uint32_t ms)
{
    sleepCoreImpl(ms);
}

skTime_t skGetTimeInMsec(void)
{
    skConnect_t *skConnect = sKonnectGetContext();
    static skTime_t time_at_start = 0;

    if ( time_at_start == 0 )
        time_at_start = skConnect->pep.os.time.fpGetSystickInmSec();
    return (skConnect->pep.os.time.fpGetSystickInmSec() - time_at_start);
}

float skStof(const char* fStr)
{
    float rez = 0, fracDiv = 1;
    int digit = 0;
    int dot = 0;

    if(!fStr)
        return -1.0;

    /* Check if value is negative */
    if (*fStr == '-')
    {
        fStr++;
        fracDiv = -1;
    }

    for (dot = 0; *fStr; fStr++)
    {
        if (*fStr == '.')
        {
            dot = 1;
            continue;
        }

        /* String Digit To Integer */
        digit = *fStr - '0';
        if (digit >= 0 && digit <= 9)
        {
            if (dot)
                fracDiv /= 10.0f;
            rez = (float)(rez * (float)10.0 + (float)digit);
        }
    }

    return (float) rez * fracDiv;
}
