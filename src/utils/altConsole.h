/**
* @file         altConsole.h
* @brief        Header File for API to init,read, write and deinit altair MCU console.
* @details      Header File for API to init,read, write and deinit altair MCU console..
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __ALT__CONSOLE__H__

#define __ALT__CONSOLE__H__

#include "stm32l496g_discovery.h"
#include <skLog.h>

#define WELCOME_PRINT	"Welcome to skConnect Console\r\n"

/**
 * @enum        enum to for altair log levels.
 * @details     enum for altair log levels.
 */
typedef enum altLogLevel_s {

    ALT_EMERG,	/* system is unusable */
    ALT_ALERT,	/* action must be taken immediately */
    ALT_CRIT,	    /* critical conditions */
    ALT_ERR,	    /* error conditions */
    ALT_INFO,    	/* informational */
    ALT_WARNING,	/* warning conditions */
    ALT_NOTICE,	/* normal but significant condition */
    ALT_DEBUG	/* debug-level messages */
} altLogLevel_e;

/**
 * @brief
 * Function to initizlize alt console.
 *
 * @return serial_handle*   Pointer to initialized console.
 */
UART_HandleTypeDef *altConsoleInit(void);

/**
 * @brief
 * Function to print log on console.
 *
 * @param level     log level
 * @param msg       msg
 * @param msgLen    msg length
 *
 * @return Number of characters printed on console. Negative value on failure.
 */
int altConsoleWrite(char *msg, int msgLen);

/**
 * @brief
 * Function to read input from console.
 *
 * @param buff      Pointer to buffer where dat gets stored.
 * @param buffLen   Buffer max length.
 *
 * @return Number of characters read from console. Negative value on failure.
 */
int altConsoleRead(char *buff, int buffLen);

/**
 * @brief
 * Function to print null terminated string on console.
 *
 * @param level     log level
 * @param msg       msg
 * @param msgLen    msg length
 *
 * @return Number of characters printed on console. Negative value on failure.
 */
int altprint(altLogLevel_e level, char *msg);

#endif  //__ALT__CONSOLE__H__
