/**
* @file         skUtils.h
* @details      Header file for skylo sKonnect API utilities file.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech)
* @copyright    Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK_UTILS__
#define __SK_UTILS__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <modemCore.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/* Base for deadlock Reset code */
#define DEADLOCK_RESET_REASON_BASE  SK_RESET_REASON_DEADLOCK_MODEM_FSM_INDEX_NO

#define RTC_ERR_INVALID_BOUNDS_YEAR  (-100)
#define RTC_ERR_INVALID_BOUNDS_MONTH (-101)
#define RTC_ERR_INVALID_BOUNDS_DAY   (-102)
#define RTC_ERR_INVALID_BOUNDS_HOUR  (-103)
#define RTC_ERR_INVALID_BOUNDS_MIN   (-104)
#define RTC_ERR_INVALID_BOUNDS_SEC   (-105)

#define LWM2M_FOTA_STATE_CIRC_BUFF_SIZE     (10)                   /* fota states circular buffer size */

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/**
 * @enum
 * enum for sk reset reasons.
 *
 * @details
 * enum for sk reset reasons.
 */
typedef enum sk_resetReason_e {

    SK_RESET_REASON_MALLOC_FAILURE_E = 301,             /**< Memory error */
    SK_RESET_REASON_SOCK_FAILURE_E,                     /**< Memory error */

    /* MODEM range 800-899 */
    SK_RESET_REASON_MODEM_CANNOT_RESET = 800,           /**< Cannot reset modem on timeout */
    SK_RESET_REASON_MODEM_EXCEED_RESET_THRES,           /**< Modem reset count exceeded threshold */
    SK_RESET_REASON_MODEM_FAILED_INIT,                  /**< Modem failed to init */
    SK_RESET_REASON_MODEM_IGNSS_FAIL_INI,
    SK_RESET_REASON_MODEM_COMM_FAIL_INIT,
    SK_RESET_REASON_MODEM_RESET_CRASH_E,
    SK_RESET_REASON_MODEM_REQUIRED_REBOOT_E,

    /* GPS range 1000-1099 */
    SK_RESET_REASON_MODEM_NO_GPS = 1000,                /**< If GPS is not aquired within certain time limit */
    SK_RESET_REASON_MODEM_FAIL_QUERY_GPS,               /**< If cannot query GNSS satellites and / or fix */
    SK_RESET_REASON_MODEM_IGNSS_NA,                     /**< I-GNSS NMEA are no longer being received */


    /* HEALTH range 1100-1199 */
    SK_RESET_REASON_SMDM_RX_THD= 1100,                  /**< smodem Rx thread stopped and requested for reboot */
    SK_RESET_REASON_MODEM_RX_THD,                       /**< modem RX thread stopped and requested for reboot */
    SK_RESET_REASON_MODEM_FSM_THD,                      /**< modem FSM thread stopped and requested for reboot */
    SK_RESET_REASON_MODEM_HLT_THD,                      /**< modem Health Monitor thread stopped and requested for reboot */
    SK_RESET_REASON_MODEM_GEO_THD,                      /**< modem GEO thread stopped and requested for reboot */
    SK_RESET_REASON_SMDM_TX_THD,                        /**< smodem Tx thread stopped and requested for reboot */

    /* SK_RESET_REASON_DEADLOCK starts */
    /* THREAD DEADLOCK range 1200-1299 */
    SK_RESET_REASON_DEADLOCK_MODEM_FSM_INDEX_NO= 1200,  /**< Deadlock of Modem FSM Thread */
    SK_RESET_REASON_DEADLOCK_MODEM_HEALTH_INDEX_NO,     /**< Deadlock of Modem Health Thread */
    SK_RESET_REASON_DEADLOCK_MODEM_RX_INDEX_NO,         /**< Deadlock of Modem RX Thread */
    SK_RESET_REASON_DEADLOCK_DRTT_INDEX_NO,             /**< Deadlock of DRTT Thread */
    SK_RESET_REASON_DEADLOCK_SM_RX_INDEX_NO,            /**< Deadlock of SM RX Thread */

    /*$$$$$$$$$$$$$$$$$$$$$ Add new threads reasons above this $$$$$$$$$$$$$$$$$$$*/
    SK_RESET_REASON_DEADLOCK_END,                       /**< End of Deadlock region NOT actual reset reason */
    /* SK_RESET_REASON_DEADLOCK ends */

    SK_RESET_REASON_SIZE_ENUM  = 0x7FFFFFFF             /* Size enum to 32 bits. */
} sk_resetReason_e;


/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/**
 * @brief
 * Set the System Reset Reason API.
 *
 * @details
 * Set the System Reset Reason API.
 *
 * @param[in] reason    Reset reason to be set.
 */
void skSetSystemResetReason(int reason);

/**
 * @brief
 * API to Get the System Reset Reason.
 *
 * @details
 * API to Get the System Reset Reason.
 *
 * @param[out] reason Pointer to reset reason.
 */
void skGetSystemResetReason(int *reason);

/**
 * @brief
 * API to convert hex string to binary.
 *
 * @details
 * API to convert hex string to binary.
 *
 * @param[in]  hex   Hex string buffer.
 * @param[in]  bLen  Buffer length.
 * @param[out] data  Pointer to converted Bianry data
 *
 * @return 0 on success negative value on error.
 */
int skHexString2Binary(char * hex, uint16_t bLen, uint8_t * data);

/**
 * @brief
 * API to convert binary data to hex string.
 *
 * @details
 * API to convert binary data to hex string.
 *
 * @param[in]  data Input binary stream poiunter.
 * @param[in]  len  Data Lenght.
 * @param[out] hex  Pointer to converted data stream.
 *
 * @return 0 on success negative value on error.
 */
int skBinary2HexString(uint8_t * data, uint16_t len, char * hex);

/**
 * @brief
 * API for millisecond sleep.
 *
 * @details
 * API for millisecond sleep.
 *
 * @param[in] ms Milliseconds to sleep for.
 */
void skMSecSleep(uint32_t ms);

/**
 * @brief
 * API to get current time in milliseconds.
 *
 * @details
 * API to get current time in milliseconds.*
 *
 * @return skTime_t
 */
skTime_t skGetTimeInMsec(void);

/**
 * @brief
 * API to convert hex stream to int.
 *
 * @details
 * APi to convert hex stream to int.
 *
 * @param[in] hex   Hex stream to be converted.
 *
 * @return uint64_t converted data.
 */
uint64_t skHex2Int(char *hex);

/**
 * @brief
 * API to convert string to float.
 *
 * @details
 * API to convert string to float.
 *
 * @param[in] fStr  String
 *
 * @return float converted data.
 */
float skStof(const char* fStr);

#endif /* __SK_UTILS__ */