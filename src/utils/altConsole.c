/**
* @file         altConsole.c
* @brief        File for API to init,read, write and deinit altair MCU console.
* @details      File for API to init,read, write and deinit altair MCU console..
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "stm32l496g_discovery.h"

#include <altConsole.h>

char *colorCoding[] = {
    NULL,
    "\033[1;37;41mEMG:\033[0m",
    "\033[1;33;41mALR:\033[0m",
    "\033[1;37;43mCRT:\033[0m",
    "\033[1;31mERR:\033[0m",
    "\033[1;37mINF:\033[0m",
    "\033[1;33mWAR:\033[0m",
    "\033[1;32mNTC:\033[0m",
    "\033[1;30mDEB:\033[0m"
};

#define HAL_MAX_DELAY      0xFFFFFFFFU

UART_HandleTypeDef huart2;
uint8_t initFlag = 0;

static int MX_USART2_UART_Init(void);

UART_HandleTypeDef *altConsoleInit(void)
{
	//TODO init mutex for print
	int ret = -1;
	//init console uart
	if ( (ret = MX_USART2_UART_Init()) ) {
		
		// pLog(SKYLO_ERR_E, "HAL Console UART init error(%d) in func %s\n",ret,__func__);
		return NULL;
	} else {

		initFlag = 1;
	}

	return &huart2;;
}

int altConsoleWrite(char *msg, int msgLen)
{
	if ( !msg || !initFlag )
		return -1;

	return HAL_UART_Transmit(&huart2,(uint8_t*)msg,msgLen, HAL_MAX_DELAY);
}

int altConsoleRead(char *buff, int buffLen)
{
	if ( !buff || buffLen || !initFlag )
		return -1;

	return HAL_UART_Receive(&huart2,(uint8_t*) buff, buffLen,HAL_MAX_DELAY);
}

//char logBuff[256*4] = {0,};

int altprint(altLogLevel_e level, char *msg)
{
    int next = 0;
	int len = 0;
	int i = 0;

	if ( (!msg && !msg[0]) || !initFlag )
		return -1;

	len = strlen(msg);

	for ( i = 0; i < len; i++ ) {

		if ( msg[i] == '\n' ) {

			if ( next-i != 0 )
				altConsoleWrite(&msg[next], i-next);
			altConsoleWrite("\r\n",2);
			next = i+1;
		} else {

			if ( i == len -1 && next == 0 ) {

				altConsoleWrite(&msg[0], len);
				altConsoleWrite("\r\n",2);
			}
			continue;
		}
	}

	return len;
}


static int MX_USART2_UART_Init(void)
{

  int ret = -1;

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if ( (ret = HAL_UART_Init(&huart2)) != HAL_OK)
  {
//    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */
  return ret;
}
