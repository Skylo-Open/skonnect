/**
* @file         skLog.c
* @details      Skylo Konnect log module file.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
* @copyright    Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdarg.h>
#include <skLog.h>
#include <string.h>

#include <skCore.h>
#include <inttypes.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/**
 * @brief
 * Converts log level to strin for log printing.
 *
 * @details
 * Converts log level to strin for log printing.
 *
 * @param[in]   Level   Log level.
 *
 * @return      Level as a const String.
 */
static const char *level2Str(skLogLevel_e level);

/*-------------------------------------------------------------------------
 * Function Definitions
 *-----------------------------------------------------------------------*/

static const char *level2Str(skLogLevel_e level)
{
    switch ( level ) {

        case SKYLO_EMERG_E:
            return "EMG";

        case SKYLO_ALERT_E:
            return "ALT";

        case SKYLO_CRIT_E:
            return "CRT";

        case SKYLO_ERR_E:
            return "ERR";

        case SKYLO_INFO_E:
            return "INF";

        case SKYLO_WARNING_E:
            return "WAR";

        case SKYLO_NOTICE_E:
            return "NTC";

        case SKYLO_DEBUG_E:
            return "DEB";

        default:
            return "";
    }
}

#define logBufF_SIZE  (1*1024)
char logBuf[logBufF_SIZE] = {0,};
int logBufSize = logBufF_SIZE;

void skLogFunction(const char *module,
                        skLogLevel_e level,
                        const char *file,
                        unsigned line,
                        const char *msg,
                        ...)
{

    char *logBufPtr = logBuf;
    size_t logBufRemaining = logBufSize;

    uint64_t timeInmSec = 0;
    skConnect_t *skConnect = sKonnectGetContext();

    skConnect->pep.os.ipc.fpMutexLock(&(skConnect->skLog.mutex));

    memset(logBuf,0x00,logBufF_SIZE);


    if ( skConnect->pep.os.time.fpGetSystickInmSec )
        timeInmSec = skConnect->pep.os.time.fpGetSystickInmSec();

#ifdef SK_WITH_LOGS_WITH_MODULE_NAME
    int pfresult = snprintf(logBufPtr, logBufRemaining,
                            "SK: [%"PRIu64"] %s [%s] [%s:%u]: ", timeInmSec, level2Str(level), module,
                            file, line);
#else
    int pfresult = snprintf(logBufPtr, logBufRemaining,
                            "SK: [%"PRIu64"] %s [%s:%u]: ", timeInmSec, level2Str(level),
                            file, line);
#endif

    if ( pfresult < 0 ) {

        // it's hard to imagine why snprintf() above might fail,
        // but well, let's be compliant and check it
        skConnect->pep.os.ipc.fpMutexUnlock(&(skConnect->skLog.mutex));
        return;
    }

    if ( (size_t) pfresult > logBufRemaining )
        pfresult = (int) logBufRemaining;

    logBufPtr += pfresult;
    logBufRemaining -= (size_t) pfresult;

    if ( logBufRemaining ) {

        va_list ap;
        va_start(ap, msg);
        pfresult = vsnprintf(logBufPtr, logBufRemaining, msg, ap);
        va_end(ap);

        if ( pfresult < 0 ) {

            skConnect->pep.os.ipc.fpMutexUnlock(&(skConnect->skLog.mutex));
            return;
        }

        if ( (size_t) pfresult > logBufRemaining ) {

            pfresult = (int) logBufRemaining - 1;
            logBufPtr = logBufPtr + pfresult - 3;
            for (int i = 0; i < 3; i++) {
                *logBufPtr = '.';
                ++logBufPtr;
            }
        }
    }
    skConnect->pep.os.fpLogHandler(level, module, logBuf);
    skConnect->pep.os.ipc.fpMutexUnlock(&(skConnect->skLog.mutex));
}
