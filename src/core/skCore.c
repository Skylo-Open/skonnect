/**
* @file         skCore.c
* @details      Skylo Konnect core file.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
* @copyright    Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdarg.h>
#include <inttypes.h>

#include <string.h>

#include <skCore.h>
#include <skLog.h>
#include <skStatus.h>

#include <modemHal.h>
#include <skUtils.h>
#include <skMsgP.h>
#include <appThread.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

#define sk_log(...) _sk_log(skCore, __VA_ARGS__)

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

skConnect_t *gpskConnect = NULL;  //temp for get method

extern skMsgPContext_t gSkMsgPContext;

/* rx thread receive buff */
static uint8_t rxThRecBuff[SMODEM_MAX_REC_SIZE+1];

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/**
 * @brief
 * Start Smodem(application) Data Rx Thread.
 *
 * @param[in] skConnect Skylo connect context
 *
 * @return None.
 */
static void runSmodemRxThread(skConnect_t *skConnect);

/**
 * @brief
 * Start Smodem(application) Data Tx Thread.
 *
 * @param[in] skConnect Skylo connect context
 *
 * @return None.
 */
static void runSmodemTxThread(skConnect_t *skConnect);

/**
 * @brief Smodem(application) RX thread.
 *
 * @param[in] arg   Thread argument
 */
static void *smodemRxThread(void *arg);

/**
 * @brief Smodem(application) TX thread.
 *
 * @param[in] arg   Thread argument
 */
static void *smodemTxThread(void *arg);

/**
 * @brief Initializes the tx circular buffer.
 *
 * @param[in]   sKonnect    Pointer to sKonnect.
 * 
 * @return skStatus_t
 */
void initializeTxCircBuffer(skConnect_t *sKonnect);

/**
 * @brief Checks if the tx circular buffer is empty.
 *
 * @param[in]   sKonnect    Pointer to sKonnect.
 * 
 * @return 1 if the buffer is empty, 0 otherwise.
 */
int isBufferEmpty(skConnect_t *sKonnect);

/**
 * @brief Checks if the tx circular buffer is full.
 *
 * @param[in]   sKonnect    Pointer to sKonnect.
 * 
 * @return 1 if the buffer is full, 0 otherwise.
 */
int isBufferFull(skConnect_t *sKonnect);

/*-------------------------------------------------------------------------
 * Function Definitions
 *-----------------------------------------------------------------------*/

skStatus_t sKonnectInit(skConnect_t *skConnect, skSDKInitTypes_e initType)
{
    skStatus_t ret = SK_ERROR;

    do {
        if ( !skConnect ) {

            // sk_log(SKYLO_DEBUG_E,_("Invalid Argument\n")); /* cause log mutex is initizled at bottom */
            ret = SK_ERR_INVALID_PARAM;
            break;
        }

        if ( initType == SK_FULL_INIT ) {

            /* Initialize log mutex */
            ret = skConnect->pep.os.ipc.fpMutexInit(&(skConnect->skLog.mutex));

            if ( ret != SK_OK )  {

                sk_log(SKYLO_CRIT_E, "Cannot create mutex" _("for log Err=")"%"PRIu32"\n", ret);
                ret = SK_ERROR;
                break;
            }

            if ( SK_OK != (ret = halModemNew(skConnect)) ) {

                sk_log(SKYLO_ERR_E, "Error while Initializing modem halModemNew\n");
                break;
            }

            if ( SK_MODEM_OK != (ret = halModemInit(skConnect)) ) {

                sk_log(SKYLO_ERR_E, "Error while Initializing modem halModemInit\n");
                skSetSystemResetReason(SK_RESET_REASON_MODEM_FAILED_INIT);
                break;
            }

            /* start UL/DL management threads */            
            runSmodemTxThread(skConnect);
            skMSecSleep(2000);
            runSmodemRxThread(skConnect);

        } else {

            if ( SK_MODEM_OK != (ret = altairModemPartialReset(skConnect)) ) {

                sk_log(SKYLO_ERR_E, "Error while Re-Initializing modem\n");
                skSetSystemResetReason(SK_RESET_REASON_MODEM_FAILED_INIT);
            }
        }

        // sk_log(SKYLO_DEBUG_E,_("SDK First Print ") "%s\n", "Initialise SDK...");
    } while ( 0 );

    return ret;
}

skConnect_t *sKonnectGetContext(void)
{
    return gpskConnect;
}

void sKonnectGSetContext(skConnect_t *pcreatedContext)
{
    gpskConnect = pcreatedContext;
}

void sleepCoreImpl(uint32_t time)
{
    if ( gpskConnect )
        gpskConnect->pep.os.time.fpSleep(time);
}


static void runSmodemRxThread(skConnect_t *skConnect)
{
    int result;

    sk_log(SKYLO_INFO_E,"Start Smodem Rx Thread\n");

    result = skyloThreadCreate(skConnect, APP_THREAD_SM_RX_INDEX_NO, smodemRxThread, skConnect);

    if ( result != SK_OK ) {

        sk_log(SKYLO_ERR_E,"Failed To Start Smodem Rx Thread With Error : %d\n", result);

        if( result == SK_ERR_INVALID )
            sk_log(SKYLO_ERR_E,"Rx Thread Fails With Error Invalid\n");
        else if( result == SK_ERR_NO_MEMORY )
            sk_log(SKYLO_ERR_E,"Rx Thread Fails With Error Memory\n");
        else if( result == SK_ERR_FAILED )
            sk_log(SKYLO_ERR_E,"Rx Thread Fails With Error\n");
        else
            sk_log(SKYLO_ERR_E,"Rx Thread Fails With Other Error\n");
    }
}

static void *smodemRxThread(void *arg)
{
    int16_t recLen = 0;

    skConnect_t *skConnect = (skConnect_t *) arg;

    sk_log(SKYLO_INFO_E,"Smodem Rx Thread Started\n");

    while ( 1 ) {

        if ( skConnect->m == NULL ) {

            skMSecSleep(1000);
            continue;
        }

        /* If smodem is not connected continue */
        if ( halModemIsAttached(skConnect) == PDN_DETACH_E ) {

            skMSecSleep(1000);
            continue;
        }

        memset(rxThRecBuff, 0x00, SMODEM_MAX_REC_SIZE);
        recLen = 0;

        if ( (recLen = halModemSubscribe(skConnect, rxThRecBuff, SMODEM_MAX_REC_SIZE)) > 0 ) {

            //Parse received Msgs
            if ( SK_OK != skMsgPMsgReceived(skConnect, rxThRecBuff, recLen) )
                sk_log(SKYLO_ERR_E, "Can't parse received data\n");
        } else if ( recLen < 0 ) {

            sk_log(SKYLO_ERR_E, "Not able to receive data\n");
        } else {

            /* signal wait timeout ignore*/
            skMSecSleep(100);
        }
    }

    sk_log(SKYLO_EMERG_E,"**************** Reset Device ****************\n");
    /* Reset the platform. */
    skSetSystemResetReason(SK_RESET_REASON_SMDM_RX_THD);
    skyloAppThreadStop(skConnect, APP_THREAD_SM_RX_INDEX_NO);

    return NULL;
}

static void runSmodemTxThread(skConnect_t *skConnect)
{
    int result;

    sk_log(SKYLO_INFO_E,"Starting Smodem Tx Thread\n");

    result = skyloThreadCreate(skConnect, APP_THREAD_SM_TX_INDEX_NO, smodemTxThread, skConnect);

    if ( result != SK_OK ) {

        sk_log(SKYLO_ERR_E,"Failed To Start Smodem Tx Thread With Error : %d\n", result);

        if( result == SK_ERR_INVALID )
            sk_log(SKYLO_ERR_E,"Tx Thread Fails With Error Invalid\n");
        else if( result == SK_ERR_NO_MEMORY )
            sk_log(SKYLO_ERR_E,"Tx Thread Fails With Error Memory\n");
        else if( result == SK_ERR_FAILED )
            sk_log(SKYLO_ERR_E,"Tx Thread Fails With Error\n");
        else
            sk_log(SKYLO_ERR_E,"Tx Thread Fails With Other Error\n");
    }
}

uint8_t txBuffer[TX_ARRAY_SIZE] = {0,};

static void *smodemTxThread(void *arg)
{
    int16_t recLen = 0, prevTxStatus = 0;

    skConnect_t *skConnect = (skConnect_t *) arg;
    skStatus_t stat = SK_MODEM_ERROR;

    sk_log(SKYLO_INFO_E,"Smodem Tx Thread Started\n");

    initializeTxCircBuffer(skConnect);

    while ( 1 ) {
        
        if ( skConnect->m == NULL ) {

            skMSecSleep(1000);
            continue;
        }

        /* If smodem is not connected continue */
        if ( halModemIsAttached(skConnect) == PDN_DETACH_E ) {

            skMSecSleep(1000);
            continue;
        }

        /* if not prev Tx in progress */
        if ( !prevTxStatus ) {

            memset(txBuffer,0x00,TX_ARRAY_SIZE);    
            recLen = 0;

            // sk_log(SKYLO_INFO_E,"Getting something to transmit!\n");

            /* check if anthing to transmit */
            if ( !(recLen = txCircBuffCheckDataAvailable(skConnect,txBuffer,TX_ARRAY_SIZE)) ) {
                
                skMSecSleep(100);
            } else {
                sk_log(SKYLO_INFO_E,"Got something to TX!\n");
            }
        } else {
            if ( recLen )
                sk_log(SKYLO_INFO_E,"Trying to TX prev msg\n");
        }

        /* if antyhing to TX in buff */
        if ( recLen ) {

            sk_log(SKYLO_INFO_E,"Trying to TX!\n");

            if ( SK_OK != (stat = halModemSend(skConnect, txBuffer, recLen/2)) ) {

                prevTxStatus = 1;       //TX did not work, try again
                sk_log(SKYLO_INFO_E,"TX Failure(%"PRId32")!\n",stat);
            } else {
                prevTxStatus = 0;
                sk_log(SKYLO_INFO_E,"TX Success!\n");
            }
            
            skMSecSleep(30*1000);       // 30 sec backoff in either case, NW requirement
        } else {
            // sk_log(SKYLO_INFO_E,"Nothing to do!\n");
            skMSecSleep(1000);
        }

    }

    sk_log(SKYLO_EMERG_E,"**************** Reset Device ****************\n");
    /* Reset the platform. */
    skSetSystemResetReason(SK_RESET_REASON_SMDM_TX_THD);
    skyloAppThreadStop(skConnect, APP_THREAD_SM_TX_INDEX_NO);

    return NULL;
}

skStatus_t sKonnectSendHB(skConnect_t *sKonnect)
{
    return skMsgPHb(sKonnect);
}

void initializeTxCircBuffer(skConnect_t *sKonnect) 
{
    // sk_log(SKYLO_INFO_E,"in TX circular buffer init!\n");
    sKonnect->txCircBuffer = sKonnect->pep.os.fpMalloc(sizeof(txCircularBuffer_t));

    // sk_log(SKYLO_INFO_E,"in TX circular buffer init [%X]!\n",sKonnect->txCircBuffer);
    
    if ( !sKonnect->txCircBuffer )  {

        sk_log(SKYLO_EMERG_E,"Malloc failure\n");
        skSetSystemResetReason(SK_RESET_REASON_MALLOC_FAILURE_E);
    }
        
    sKonnect->txCircBuffer->writeIndex = 0;
    sKonnect->txCircBuffer->readIndex = 0;
    sKonnect->txCircBuffer->count = 0;

    sk_log(SKYLO_INFO_E,"TX circular buffer initialized!\n");
}

int isBufferEmpty(skConnect_t *sKonnect) 
{
    return sKonnect->txCircBuffer->count == 0;
}

int isBufferFull(skConnect_t *sKonnect) 
{
    return sKonnect->txCircBuffer->count == CIRC_BUFFER_LENGTH;
}

int txCircBuffCheckDataAvailable(skConnect_t *sKonnect, uint8_t* buff, int length) 
{
    int actualLength = 0;

    if ( isBufferEmpty(sKonnect) ) {
        return 0; // No data available
    }

    actualLength = sKonnect->txCircBuffer->array[sKonnect->txCircBuffer->readIndex].data[0];

    int bytesToCopy = (length < actualLength) ? length : actualLength;

    // Copy the length and data to the buffer
    memcpy(buff, &sKonnect->txCircBuffer->array[sKonnect->txCircBuffer->readIndex].data[1], bytesToCopy);

    sKonnect->txCircBuffer->readIndex = (sKonnect->txCircBuffer->readIndex + 1) % CIRC_BUFFER_LENGTH;
    sKonnect->txCircBuffer->count--;

    sk_log(SKYLO_INFO_E,"TX circular buffer state in Read, Write(%d),Read(%d) and count(%d)!\n",
        sKonnect->txCircBuffer->writeIndex,
        sKonnect->txCircBuffer->readIndex,sKonnect->txCircBuffer->count);

    return bytesToCopy; // Return the actual data length
}

skStatus_t txCircBuffwriteData(skConnect_t *sKonnect, uint8_t* buff, int length)
{
    if ( !isBufferFull(sKonnect) ) {

        // Store length in the first byte of the chunk
        sKonnect->txCircBuffer->array[sKonnect->txCircBuffer->writeIndex].data[0] = (uint8_t)length;
        memcpy(&sKonnect->txCircBuffer->array[sKonnect->txCircBuffer->writeIndex].data[1], buff, length);

        sKonnect->txCircBuffer->writeIndex = (sKonnect->txCircBuffer->writeIndex + 1) % CIRC_BUFFER_LENGTH;
        sKonnect->txCircBuffer->count++;

    } else {

        // Buffer is full, overwrite old data
        sKonnect->txCircBuffer->array[sKonnect->txCircBuffer->writeIndex].data[0] = (uint8_t)length;
        memcpy(&sKonnect->txCircBuffer->array[sKonnect->txCircBuffer->writeIndex].data[1], buff, length);

        sKonnect->txCircBuffer->writeIndex = (sKonnect->txCircBuffer->writeIndex + 1) % CIRC_BUFFER_LENGTH;
        sKonnect->txCircBuffer->readIndex = (sKonnect->txCircBuffer->readIndex + 1) % CIRC_BUFFER_LENGTH;
    }

    sk_log(SKYLO_INFO_E,"TX circular buffer state in Write, Write(%d),Read(%d) and count(%d)!\n",
        sKonnect->txCircBuffer->writeIndex,
        sKonnect->txCircBuffer->readIndex,sKonnect->txCircBuffer->count);
    
    return SK_OK;
}