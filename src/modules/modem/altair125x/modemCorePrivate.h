/**
 * @file        modemCorePrivate.h
 * @brief       Modem Core Private Implementation HEADER for Altair1250 Chipset
 * @author      Ankit Patel (ankit@skylo.tech)
 *              Dharmesh Vasoya (dharmesh@skylo.tech)
 * @copyright   Copyright (c) 2019 Skylo Technologies, Inc.
 */

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __MODEM_CORE_PRIVATE_H__
#define __MODEM_CORE_PRIVATE_H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skStatus.h>
#include <skDefs.h>
#include <skModemStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Data Structures / Typedefs
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Modem Enumerations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Private Modem Core Functions Declaration
 *-----------------------------------------------------------------------*/

/**
 * @brief
 * Function to parse RSSI string from modem
 *
 * @details
 * Function to parse RSSI string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  rssiVar   Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespRssi(skConnect_t *skConnect, void * rssiVar);

/**
 * @brief
 * Function to parse RSRP string from modem
 *
 * @details
 * Function to parse RSRP string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  opVar     Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespRsrp(skConnect_t *skConnect, void * opVar);

/**
 * @brief
 * Function to parse RSRQ string from modem
 *
 * @details
 * Function to parse RSRQ string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  rsrqVar   Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespRsrq(skConnect_t *skConnect, void * rsrqVar);

/**
 * @brief
 * Function to parse SINR string from modem
 *
 * @details
 * Function to parse SINR string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  sinrVar   Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespSinr(skConnect_t *skConnect, void * sinrVar);

/**
 * @brief
 * Function to parse All signal levels
 *
 * @details
 * Function to parse All signal levels
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  var       Just for call back compatibility
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseSignalLevels(skConnect_t *skConnect, void * var);

/**
 * @brief
 * Function to parse Firmware Version string from modem
 *
 * @details
 * Function to parse Firmware Version string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  version   Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespVersion(skConnect_t *skConnect, void * version);

/**
 * @brief
 * Function to parse Firmware RK Version string from modem
 *
 * @details
 * Function to parse Firmware RK Version string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  version   Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespRkVersion(skConnect_t *skConnect, void * version);

/**
 * @brief
 * Function to parse IMEI string from modem
 *
 * @details
 * Function to parse IMEI string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  imeiVar   Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespImei(skConnect_t *skConnect, void * imeiVar);

/**
 * @brief
 * Function to parse ICCID string from modem
 *
 * @details
 * Function to parse ICCID string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  iccidVar  Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespIccid(skConnect_t *skConnect, void * iccidVar);

/**
 * @brief
 * Function to parse IMSI string from modem
 *
 * @details
 * Function to parse IMSI string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  imsiVar   Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespImsi(skConnect_t *skConnect, void * imsiVar);

/**
 * @brief
 * Function to parse NWOPER string from modem
 *
 * @details
 * Function to parse NWOPER string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  opVar     Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespNwoper(skConnect_t *skConnect, void * opVar);

/**
 * @brief
 * Function to parse Band string from modem
 *
 * @details
 * Function to parse Band string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  opVar     Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespGetBand(skConnect_t *skConnect, void * opVar);

/**
 * @brief
 * Function to parse OK/ERROR string from modem
 *
 * @details
 * Function to parse OK/ERROR string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  status    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespOkError(skConnect_t *skConnect, void * status);

/**
 * @brief
 * Function to parse Boot Mode string from modem
 *
 * @details
 * Function to parse Boot Mode string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  status    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespBootModeConfirm(skConnect_t *skConnect, void * status);

/**
 * @brief
 * Function to parse Flow Control success string from modem
 *
 * @details
 * Function to parse Flow Control success string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  status    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespFlowControlConfirm(skConnect_t *skConnect, void * status);

/**
 * @brief
 * Function to parse socket info string from modem
 *
 * @details
 * Function to parse socket info string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  status    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseSocketInfo(skConnect_t *skConnect, void * status);

/**
 * @brief
 * Function to parse socket ID after calll to create socket.
 *
 * @details
 * Function to parse socket ID after calll to create socket.
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  id        Id to just opened socket.
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseSocketId(skConnect_t *skConnect, void * id);

/**
 * @brief
 * Function to parse Received downlink message string from modem
 *
 * @details
 * Function to parse Received downlink string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleRxBuffer(skConnect_t *skConnect, void * buffer);

/**
 * @brief
 * Function to parse RTC string from modem
 *
 * @details
 * Function to parse RTC string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRtc(skConnect_t *skConnect, void * buffer);

/**
 * @brief
 * Function to parse Time Advance string from modem
 *
 * @details
 * Function to parse Time Advance string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseTa(skConnect_t *skConnect, void * buffer);

/**
 * @brief
 * Function to parse PDP NIP Status string from modem
 *
 * @details
 * Function to parse PDP NIP Status string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParsePdpNipStatus(skConnect_t *skConnect, void * buffer);

/**
 * @brief
 * Function to parse FOTA File string from modem
 *
 * @details
 * Function to parse FOTA File string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespFotaFile(skConnect_t *skConnect, void * buffer);

/**
 * @brief
 * Function to parse IGNSS Fix string from modem
 *
 * @details
 * Function to parse IGNSS Fix string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseIgnssFix(skConnect_t *skConnect, void * buffer);

/**
 * @brief
 * Function to parse IGNSS Number of satellite string from modem
 *
 * @details
 * Function to parse IGNSS Number of satellite string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseIgnssNumSat(skConnect_t *skConnect, void * buffer);

/**
 * @brief
 * Function to parse RRC state string from modem
 *
 * @details
 * Function to parse RRC state string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRrcState(skConnect_t *skConnect, void * buffer);

/**
 * @brief
 * Function to parse Cell ID string from modem
 *
 * @details
 * Function to parse Cell ID string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseCellId(skConnect_t *skConnect, void * buffer);

/**
 * @brief
 * Function to parse PMP NP version string from modem
 *
 * @details
 * Function to parse PMP NP version string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  version   Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespPmpNpVersion(skConnect_t *skConnect, void * version);

/**
 * @brief
 * Function to parse Baud Rate string from modem
 *
 * @details
 * Function to parse Baud Rate string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  version   Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseBaudrate(skConnect_t *skConnect, void * version);

/**
 * @brief
 * Function to parse MTD Parts string from modem
 *
 * @details
 * Function to parse MTD Parts string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  version   Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseMtdparts(skConnect_t *skConnect, void * version);

/**
 * @brief
 * Function to parse Board Info string from modem
 *
 * @details
 * Function to parse Board Info string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  status    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t  altairHandleParseCreateboardinfo(skConnect_t *skConnect, void * status);

/**
 * @brief
 * Function to parse Load Kermit string from modem
 *
 * @details
 * Function to parse Load Kermit string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  status    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t  altairHandleParseLoadKermit(skConnect_t *skConnect, void * status);

/**
 * @brief
 * Function to parse Boot Flash string from modem
 *
 * @details
 * Function to parse Boot Flash string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  status    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t  altairHandleParseBootFlash(skConnect_t *skConnect, void * status);

/**
 * @brief
 * Function to parse EARFCN string from modem
 *
 * @details
 * Function to parse EARFCN string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  earfcnVar Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRespEarfcn(skConnect_t *skConnect, void * earfcnVar);

/**
 * @brief
 * Function to parse Modem connection fail status string from modem
 *
 * @details
 * Function to parse Modem connection fail status string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  tempRecv  Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static void internalHandleParseModemFailStat(skConnect_t *skConnect, char * tempRecv);

/**
 * @brief
 * Function to parse Detach reject reason string from modem
 *
 * @details
 * Function to parse Detach reject reason string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  failResp  Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static void altairHandleParseDetachReject(modemFailResp_t *failResp);

/**
 * @brief
 * Function to parse Boot Notify string from modem
 *
 * @details
 * Function to parse Boot Notify string from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static void internalHandleParseBootNotify(skConnect_t *skConnect);

/**
 * @brief
 * Function to parse response to ratimgsel command. (LTE image currently running).
 *
 * @details
 * Function to parse response to ratimgsel command. (LTE image currently running).
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleImageSel(skConnect_t *skConnect, void * buffer);

/**
 * @brief
 * Function to parse response to ratact command. (NBIoT image currently running).
 *
 * @details
 * Function to parse response to ratact command. (NBIoT image currently running).
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and or converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseRatAct(skConnect_t *skConnect, void * buffer);

/**
 * @brief
 * Function to parse cgatt response.
 *
 * @details
 * Function to parse cgatt response.
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and or converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseCgatt(skConnect_t *skConnect, void * buffer);

/**
 * @brief
 * Function to parse Multi RAT enable response.
 *
 * @details
 * Function to parse Multi RAT enable response.
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  buffer    Store parsed and or converted value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure. \n
 *          For more error check SKModemStatus.h
 */
static skStatus_t altairHandleParseMultiRATRespnose(skConnect_t *skConnect, void * buffer);

/**
 * @brief Scheduled Tasks executor
 *
 * @param[in]   sKonnect        Pointer to sKonnect.
 * @param[in]   secondsCount    Seconds counter
 * 
 * @return None
 */
static void scheduledTaskExec(skConnect_t *skConnect, skTime_t secondsCount);

#endif /* __MODEM_CORE_PRIVATE_H__ */
