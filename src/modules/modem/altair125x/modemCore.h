/**
 * @file        modemCore.h
 * @brief       Modem Core Implementation HEADER (Altair1250 Chipset)
 * @author      Barry Winata (barry@skylo.tech)
 *              Dharmesh Vasoya (dharmesh@skylo.tech)
 * @copyright   Copyright (c) 2019 Skylo Technologies, Inc.
 */

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __MODEM_CORE_H__
#define __MODEM_CORE_H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <modemHal.h>
#include <math.h>
#include <skStatus.h>
#include <skDefs.h>
#include <skModemStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

#define MODEM_MALLOC_FAIL_ERR_MSG         "malloc failed\n"

/* ms */
#define BACKOFF_PERIOD          (5000)
/* Bytes */
#define AT_CMD_BUFFER_SIZE      (1024 * 3)
/* Bytes */
#define RX_DATA_BUFFER_SIZE     (AT_CMD_BUFFER_SIZE)
/* Bytes */
#define TX_DATA_MAX_LEN         (1024)
#define RX_DATA_MAX_LEN         (1152) 
/* Bytes */
#define AT_RESP_BUFFER_SIZE     (RX_DATA_MAX_LEN*2)

#define SMODEM_MAX_REC_SIZE     (RX_DATA_MAX_LEN)

#define MODEM_VER_LEN           (25)
#define MODEM_RK_VER_LEN        (17)
#define MODEM_VER_MAX_LEN       (32)
#define IMEI_MAX_LEN            (15)
#define IMSI_MAX_LEN            (15)
#define ICCID_MAX_LEN           (20)
#define RSSI_MAX_LEN            (2)
#define RSRP_MAX_LEN            (4)
#define RSRQ_MAX_LEN            (4)
#define SINR_MAX_LEN            (4)

#define ENUM_IMEI_RESP_FIELD    (0)
#define ENUM_ICCID_RESP_FIELD   (0)
#define ENUM_IMSI_RESP_FIELD    (0)
#define ENUM_RSSI_RESP_FIELD    (1)
#define ENUM_RSRP_RESP_FIELD    (5)
#define ENUM_RSRQ_RESP_FIELD    (8)
#define ENUM_SINR_RESP_FIELD    (11)

#define MODEM_FLASH_MAX_SIZE    (64)

/*-------------------------------------------------------------------------
 * Core Data Structures / Typedefs
 *-----------------------------------------------------------------------*/

/**
 * @brief
 * Function pointer for at response parser.
 *
 * @details
 * Function pointer for at response parser.
 *
 * @param[in]   skConnect Skylo connect context.
 * @param[in]   arg       Void argument
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_ERROR on failure.
 *          SK_MODEM_ERROR on failure.
 */
typedef skStatus_t (*responseParser_fp)(skConnect_t *skConnect, void *);

/**
 * @struct  at command response parser context.
 *
 * @details at command response parser context.
 */
typedef struct atCommandResponse_s {

    uint8_t           commandType;          /**< U-Boot Command or AT Command */
    const char       * command;             /**< AT command string */
    const char       * respTag;             /**< Respoonse tag */
    responseParser_fp  callback;            /**< parser callback for individual AT command */
} atCommandResponse_t;

/**
 * @struct  URC notify event context.
 *
 * @details URC notify event context.
 */
typedef struct urcNotifyEvent_s {

    modemNotifyEv_e  event; /**< event */
} urcNotifyEvent_t;

/**
 * @struct  Modem failure response context.
 *
 * @details Modem failure response context.
 */
typedef struct modemFailResp_s {

    char procedure[15]; /**< Proceedure */
    char failure[15];   /**< Failure */
} modemFailResp_t;

/**
 * @enum  Modem AT command index enum.
 *
 * @details Modem AT command index enum.
 */
typedef enum modemCommand_s {

    MODEM_CMD_AT_ONLINE_E,
    MODEM_CMD_GET_VERSION_E,
    MODEM_CMD_GET_RK_VERSION_E,
    MODEM_CMD_GET_IMEI_E,
    MODEM_CMD_GET_ICCID_E,
    MODEM_CMD_GET_IMSI_E,
    MODEM_CMD_GET_NW_OPERATOR_E,
    MODEM_CMD_SET_NW_OPERATOR_E,
    MODEM_CMD_GET_RSSI_E,
    MODEM_CMD_GET_RSRP_E,
    MODEM_CMD_GET_RSRQ_E,
    MODEM_CMD_GET_SINR_E,
    MODEM_CMD_GET_ALL_SIGNAL_LEVELS_E,
    MODEM_CMD_GET_CFG_BAND_E,
    MODEM_CMD_SET_CFG_BAND_E,
    MODEM_CMD_SET_CFG_EARFCN_E,
    MODEM_CMD_SEND_IP_E,
    MODEM_CMD_SEND_NIP_E,
    MODEM_CMD_REPORT_PACKET_GATEWAY_E,
    MODEM_CMD_REPORT_EVENTS_E,
    MODEM_CMD_FUN_OFF_E,
    MODEM_CMD_FUN_ON_E,
    MODEM_CMD_IS_SIM_READY_E,
    MODEM_CMD_CREATE_SOCK_E,
    MODEM_CMD_ACTIVATE_SOCK_E,
    MODEM_CMD_SOCK_INFO_E,
    MODEM_CMD_ENABLE_E,
    MODEM_CMD_DISABLE_E,
    MODEM_CMD_SET_CGEREP_E,
    MODEM_CMD_SET_NOTIFYEV_E,
    MODEM_CMD_READ_RX_BUFFER_E,
    MODEM_CMD_AT_CLI_E,
    MODEM_CMD_AT_HW_FLOW_E,
    MODEM_CMD_SET_RTC_E,
    MODEM_CMD_GET_RTC_E,
    MODEM_CMD_SET_CFG_LTE_INACTTMR_E,
    MODEM_CMD_GET_LTE_TIMING_ADVANCE_E,
    MODEM_CMD_SET_PDP_APN_E,
    MODEM_CMD_SET_NIP_CNTRL_PLANE_URC_E,
    MODEM_CMD_GET_PDP_NIP_STATUS_E,
    MODEM_CMD_SOFT_RESET_E,
    MODEM_CMD_AUTOCONNECT_MODE_E,
    MODEM_CMD_SLEEP_MODE_DISABLE_E,
    MODEM_CMD_LOCSRV_ENABLE_E,
    MODEM_CMD_LOCSRV_IGNSS_AUTO_RESTART_ENABLE_E,
    MODEM_CMD_LOCSRV_IGNSS_BLANKING_GUARD_E,
    /* RK_2.1.2.10.14 ONLY */
    MODEM_CMD_IGNSSEV_GNSSCBOOT_E,
    /* RK_2.1.2.10.14 ONLY END */
    /* RK_3.2 ONLY */
    MODEM_CMD_IGNSSEV_COLDSTART_E,
    /* RK_3.2 ONLY END */
    MODEM_CMD_IGNSSEV_NMEA_E,
    MODEM_CMD_IGNSSEV_EPHUPD_E,
    MODEM_CMD_TADJEV_ENABLE_E,
    MODEM_CMD_IGNSSCFG_NMEA_E,
    MODEM_CMD_IGNSS_DEACTIVATE_E,
    MODEM_CMD_IGNSS_ACTIVATE_P2_E,
    MODEM_CMD_IGNSS_ACTIVATE_P3_E,
    MODEM_CMD_IGNSS_INFO_FIX_E,
    MODEM_CMD_IGNSS_INFO_NUM_SAT_E,
    MODEM_CMD_CGATT_ENABLE_E,
    MODEM_CMD_QUERY_RRC_STATE_E,
    MODEM_CMD_QUERY_CELL_ID_E,
    MODEM_CMD_PMP_NP_VER_E,
    MODEM_CMD_BOOT_MODE_TRANSFER_E,
    MODEM_CMD_BOOT_DELAY_E,
    MODEM_CMD_ENABLE_MAC_LOGS_E,
    MODEM_CMD_BOOT_MODE_CONFIRM_E,
    MODEM_CMD_BOOT_SET_BAUDRATE_E,
    MODEM_CMD_BOOT_SET_HWFLOW_E,
    MODEM_CMD_BOOT_MODE_BAUDRATE_E,
    MODEM_CMD_BOOT_MODE_MTDPARTS_E,
    MODEM_CMD_BOOT_MODE_PARSE_FLASH_E,
    MODEM_CMD_BOOT_MODE_BOARDINFO_E,
    MODEM_CMD_BOOT_MODE_LOADKERMIT_E,
    /* RK_3.2 ONLY */
    MODEM_CMD_IGNSSCFG_SKYLO_E,
    MODEM_CMD_ENABLE_CELL_URC_E,
    MODEM_CMD_ENABLE_IGNSS_FIX_URC_E,
    MODEM_CMD_ENABLE_IGNSS_BLANKING_URC_E,
    MODEM_CMD_DISABLE_DATA_INACTIVITY_TIMER_E,
    MODEM_CMD_GET_EARFCN_E,
    MODEM_CMD_SYSTEM_FAILURE_E,
    MODEM_CMD_DISABLE_ECHO_E,
    MODEM_CMD_ENABLE_REBOOT_EVENT_E,
    /* RK_3.2 ONLY END */
    MODEM_CMD_IMG_CHECK_E,
    MODEM_CMD_IMG_SEL_E,
    MODEM_CMD_RESTART_MODEM_E,
    MODEM_CMD_SET_RAT_MODE_E,
    MODEM_CMD_GET_RAT_MODE_E,
    MODEM_CMD_SET_UE_POS_E,
    MODEM_CMD_CHECK_ATTACH_STATUS_E,
    MODEM_CMD_CHECK_MULTI_RAT_ENABLE_STATUS_E,
    MODEM_CMD_CHANGE_MULTI_RAT_ENABLE_STATUS_E,
    MODEM_CMD_TEMP_FIX_E

} modemCommand_e;

/*-------------------------------------------------------------------------
 * Functions Declaration
 *-----------------------------------------------------------------------*/

/**
 * @brief
 * Altair modem data receive thread.
 *
 * @details
 * Altair modem data receive thread.
 *
 * @param[in] arg   Thread argument
 *
 * @return void*
 */
void *altairModemRxThread(void * arg);

/**
 * @brief
 * Altair modem FSM thread.
 *
 * @details
 * Altair modem FSM thread.
 *
 * @param[in] arg   Thread argument
 *
 * @return void*
 */
void *altairModemFsmThread(void * arg);

/**
 * @brief
 * Altair modem Health Monitor thread.
 *
 * @details
 * Altair modem Health Monitor thread.
 *
 * @param[in] arg   Thread argument
 *
 * @return void*
 */
void *altairModemHealthMonitorThread(void * arg);

/**
 * @brief
 * Function to check if modem is online.
 *
 * @details
 * Function to check if modem is online.
 *
 * @param[in]  skConnect     Skylo connect context.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairCheckModemOnline(skConnect_t *skConnect);

/**
 * @brief
 * Function to fire at coomand.
 *
 * @details
 * Function to fire at command.
 *
 * @param[in]  skConnect    Skylo connect context.
 * @param[in]  cmd          Command string.
 * @param[in]  msg          Messsage index.
 * @param[out] resp         Messsage response.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairFireCommand(skConnect_t *skConnect, const char * cmd, modemCommand_e msg, void * resp);

/**
 * @brief
 * Function to check if sim is ready.
 *
 * @details
 * Function to check if sim is ready.
 *
 * @param[in]  skConnect     Skylo connect context.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skBool_t altairIsSimReady(skConnect_t *skConnect);

/**
 * @brief
 * Function to create socket.
 *
 * @details
 * Function to create socket.
 *
 * @param[in]  skConnect     Skylo connect context.
 * @param[in]  type          Socket type.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairCreateSocket(skConnect_t *skConnect, uint8_t type);

/**
 * @brief
 * Function to active socket.
 *
 * @details
 * Function to active socket.
 *
 * @param[in]  skConnect     Skylo connect context.
 * @param[in]  id            Socket ID to be activated.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairActivateSocket(skConnect_t *skConnect, int16_t id);

/**
 * @brief
 * Function to active socket.
 *
 * @details
 * Function to active socket.
 *
 * @param[in]  skConnect     Skylo connect context.
 * @param[in]  ip            IP.
 * @param[in]  port          Port.
 * @param[out] id            socket Id.
 * @param[out] sockType      socket Type TCP/UDP.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairAllocateSocket(skConnect_t *skConnect,const char* ip, uint16_t port,int16_t *id, uint8_t sockType);

/**
 * @brief
 * Function to check socket status.
 *
 * @details
 * Function to check socket status.
 *
 * @param[in]  skConnect     Skylo connect context.
 * @param[in]  id            socket ID
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairSocketStatus(skConnect_t *skConnect, int16_t id);

/**
 * @brief
 * Function to configure pgw reporting.
 *
 * @details
 * Function to configure pgw reporting.
 *
 * @param[in]  skConnect     Skylo connect context.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairConfigurePgwReporting(skConnect_t *skConnect);

/**
 * @brief
 * Function to notify ev reporting.
 *
 * @details
 * Function to notify ev reporting.
 *
 * @param[in]  skConnect     Skylo connect context.
 * @param[in]  type          Notification type.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairConfigureNotifyevReporting(skConnect_t *skConnect, modemNotifyEVTypes_e type);

/**
 * @brief
 * Function to read received buffer.
 *
 * @details
 * Function to read received buffer.
 *
 * @param[in]  skConnect     Skylo connect context.
 * @param[out] hexBuf        Hex buffer.
 * @param[out] length        Length of hexBuf.
 * @param[out] id            Socket ID.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairReadRxBuffer(skConnect_t *skConnect, char * hexBuf, uint32_t length, int16_t id);

/**
 * @brief
 * Function to read received buffer.
 *
 * @details
 * Function to read received buffer.
 *
 * @param[in]  skConnect     Skylo connect context.
 * @param[in]  data          Bianry data buffer.
 * @param[in]  length        Length of data.
 * @param[in]  id            Socket ID.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairSendRxBuffer(skConnect_t *skConnect, char *data, uint32_t length, int16_t id);

/**
 * @brief
 * Function to subscribe/rx data.
 *
 * @details
 * Function to subscribe/rx data.
 *
 * @param[in]  skConnect     Skylo connect context.
 * @param[out] buf           Buffer.
 * @param[out] len           Length.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairSubscribe(skConnect_t *skConnect, uint8_t * buf, uint16_t len);

/**
 * @brief
 * Function to configure LTE inactive timer.
 *
 * @details
 * Function to configure LTE inactive timer.
 *
 * @param[in]  skConnect     Skylo connect context.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairConfigureLteInacttmr(skConnect_t *skConnect);

/**
 * @brief
 * Function to configure RTC from GPS.
 *
 * @details
 * Function to configure RTC from GPS.
 *
 * @param[in]  skConnect     Skylo connect context.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairConfigureRtcFromGps(skConnect_t *skConnect);

/**
 * @brief
 * Function to get LTE timing advance.
 *
 * @details
 * Function to get LTE timing advance.
 *
 * @param[in]  skConnect     Skylo connect context.
 * @param[out]  TA            Timing advance.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairGetLteTimingAdvance(skConnect_t *skConnect, int16_t * TA);

/**
 * @brief
 * Function to get LTE image currently running.
 *
 * @details
 * Function to get LTE image currently running.
 *
 * @param[in]  skConnect     Skylo connect context.
 * @param[out] image         Image currently selected.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairGetImageSelected(skConnect_t *skConnect, modemImage_e * image);

/**
 * @brief
 * Function to restart modem via AT command.
 *
 * @details
 * Function to restart modem via AT command.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairRestartModem(skConnect_t *skConnect);

/**
 * @brief
 * Function to configure eData Delivery Notifications.
 *
 * @details
 * Function to configure Data Delivery Notifications.
 *
 * @param[in]  skConnect     Skylo connect context.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairCofigureDataDeliveryNotifications(skConnect_t *skConnect);

/**
 * @brief
 * Function to reset module.
 *
 * @details
 * Function to reset module.
 *
 * @param[in]  skConnect        Skylo connect context.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairModuleReset(skConnect_t *skConnect);

/**
 * @brief
 * Function to reset modem.
 *
 * @details
 * Function to reset modem.
 *
 * @param[in]  skConnect        Skylo connect context.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairModemReset(skConnect_t *skConnect);

/**
 * @brief
 * Function to reset modem partially.
 *
 * @details
 * Function to reset modem partially.
 *
 * @param[in]  skConnect        Skylo connect context.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairModemPartialReset(skConnect_t *skConnect);

/**
 * @brief
 * Function to modem functional reset.
 *
 * @details
 * Function to modem functional reset.
 *
 * @param[in]  skConnect        Skylo connect context.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairModemFunctionalReset(skConnect_t *skConnect);


/**
 * @brief
 * This function is used to reset Altair Modem
 *
 * @details
 * Using this Function we can reset Altair Modem using GPIO
 *
 * @return  0 on success, negative value on failure.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_ERROR on failure.
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairGpioResetInit(void);

/**
 * @brief
 * This function is used to Enable/Disable Wakeup Pin Of Altair
 *
 * @details
 * Using this Function we can control wakeup of Altair
 * Value = 0 - Sleep, Value = 1 - Wakeup
 *
 * @param[in]   Value Value to enable/disable wakeup mode.
 *
 * @return  0 on success, negative value on failure.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_ERROR on failure.
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairGpioWakeupState(uint8_t value);

/**
 * @brief
 * This function is used to control RFFE SP3T Switch
 *
 * @details
 * Using this Function we can control selection of Tx Rx Antenna
 * Value = 0 - RFFE Selection, Value = 1 - LTE Selection
 *
 * @param[in]   Value Value to control SP3T
 *
 * @return  0 on success, negative value on failure.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_ERROR on failure.
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairGpioSp3tSet(uint8_t value);

/**
 * @brief
 * This function is used to control RFFE LDO
 *
 * @details
 * Using this Function we can control RFFE LDO and enable/disble power mode.
 * Value = 0 LDO Disable, Value = 1 LDO Enable.
 *
 * @param[in]   Value Value to enable/disable LDO
 *
 * @return  0 on success, negative value on failure.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_ERROR on failure.
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairGpioRffeLdoSet(uint8_t value);

/**
 * @brief
 * This function is used to send AT command for FPTS RF test
 *
 * @details
 * This function is used to send AT command for FPTS RF test
 *
 * @param[in]   M     Pointer to modem context structure
 * @param[in]   Cmd   Pointer to AT command message
 * @param[in]   Msg   Context structure of type of AT command
 * @param[in]   Resp  Accepted response of command
 *
 * @return  0 on success, negative value on failure.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_ERROR on failure.
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t altairFireFptsCommand(skConnect_t *skConnect, const char * cmd, modemCommand_e msg, char * resp);

/**
 * @brief
 * Function to parser AT command response.
 *
 * @details
 * Function to parser AT command response.
 *
 * @param[in]  resp     Received response.
 * @param[out] result   Parsed result.
 * @param[in]  delim    Delimeter
 * @param[in]  msgField Message fiedl identifier.
 * @param[in]  maxLen   Maximum length
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_ERR_INVALID_PARAM on invalid parameter. \n
 *          SK_MODEM_ERROR on failure.
 */
skStatus_t parseAtResp(const char * resp, void * result, const char * delim, uint8_t msgField, uint16_t maxLen);

/**
 * @brief
 * Function to retrieve current EARFCN from modem
 *
 * @details
 * Function to retrieve current EARFCN from modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  EARFCN value
 */
uint32_t halModemGetEarfcn(skConnect_t *skConnect);

/**
 * @brief
 * Function to set modem failure status response
 *
 * @details
 * Function to set modem failure status response
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSetUnsolicitedStatus(skConnect_t *skConnect);

/**
 * @brief
 * Function to change LTE image running.
 *
 * @details
 * Function to change LTE image running.
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 * @param[in]  image     Image number that needs to run (Altair -> Lte cat m1->1, NB-Iot -> 2)
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemChangeLTEImage(skConnect_t *skConnect, uint16_t image);

/**
 * @brief
 * Function to change Modem RAT mode.
 *
 * @details
 * Function to change Modem RAT mode.
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 * @param[in]  image     RAT image @see modemNbiotImage_e
 * @param[in]  source    Source of RAT image change
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSetRATMode(skConnect_t *skConnect, modemNbiotImage_e image, uint8_t source);

/**
 * @brief
 * Function to get Modem RAT mode.
 *
 * @details
 * Function to get Modem RAT mode.
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemGetRATMode(skConnect_t *skConnect);

/**
 * @brief
 * Function to set pdpType and APN.
 *
 * @details
 * Function to set pdpType and APN.
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 * @param[in]  pdpType   pdp Type Ip / Non-Ip
 * @param[in]  APN       APN
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSetPdpAndAPN(skConnect_t *skConnect, const char * pdpType, const char *APN);

/**
 * @brief
 * Function to set Band.
 *
 * @details
 * Function to set Band.
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSetBand(skConnect_t *skConnect);

/**
 * @brief
 * Function to set earfcn scan range.
 *
 * @details
 * Function to set earfcn scan range.
 *
 * @param[in]  skConnect    Pointer to skConnect_t context
 * 
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSetEarfcn(skConnect_t *skConnect);

/**
 * @brief
 * Function to set UE position.
 *
 * @details
 * Function to set UE position.
 *
 * @param[in]  skConnect    Pointer to skConnect_t context
 * @param[in]  lat          Lattitude of device
 * @param[in]  lon          Longitude of device
 * @param[in]  alt          Altitude of device
 * @param[in]  expiry       expiry of position (0 is forever)
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSetUEPosition(skConnect_t *skConnect, float lat, float lon, float alt, uint32_t expiry);

/**
 * @brief
 * Function to get Multi RAT enable status.
 *
 * @details
 * Function to get Multi RAT enable status.
 *
 * @param[in]  skConnect    Pointer to skConnect_t context
 * @param[out] status       pointer to status
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemGetMultiRATEnableStatus(skConnect_t *skConnect, uint8_t *status);

/**
 * @brief
 * Function to enable/disble Multi RAT enable.
 *
 * @details
 * Function to enable/disble Multi RAT enable.
 *
 * @param[in]  skConnect    Pointer to skConnect_t context
 * @param[out] status       0->False else True
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSetMultiRATEnableStatus(skConnect_t *skConnect, uint8_t status);

skStatus_t halModemApplyTempFix(skConnect_t *skConnect);

/**
 * @brief Open socket for HB communication.
 *
 * @note Blocking API
 * 
 * @param[in]   sKonnect    Pointer to sKonnect.
 *
 * @return skStatus_t   SK_ERR_EAGAIN -> try again
 */
skStatus_t sKonnectOpenHBSocket(skConnect_t *sKonnect);

#endif /* _MODEM_CORE_H_ */
