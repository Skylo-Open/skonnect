/**
 * @file      : modemCore.c
 * @brief     : Modem Core Implementation (Altair1250 Chipset)
 * @author    : Barry Winata (barry@skylo.tech)
 *              Dharmesh Vasoya (dharmesh@skylo.tech)
 * @copyright : Copyright (c) 2023 SkyloTechnologies, Inc.
 */

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <modemHal.h>
#include <modemCore.h>
#include <modemCorePrivate.h>
#include <modemHealth.h>
#include <skLog.h>
#include <skUtils.h>
#include <appThread.h>
#include <skDefs.h>

#include <skCore.h>
#include <skModemStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Macros
 *-----------------------------------------------------------------------*/

#define Skylo_Printf(...) _sk_log(modemCore, __VA_ARGS__)

/*-------------------------------------------------------------------------
 * Constants
 *-----------------------------------------------------------------------*/

#define MAX_AT_CMD_LEN             (64)

#define THREAD_SIGNAL_RX_RESP      (0x00000001)
#define THREAD_SIGNAL_RX_URC       (0x00000002)
#define THREAD_SIGNAL_RX_SKMSG     (0x00000004)
#define THREAD_SIGNAL_RRC_CONN     (0x00000008)
#define THREAD_SIGNAL_RRC_IDLE     (0x00000010)
#define THREAD_SIGNAL_UE_ATTACH    (0x00000020)
#define THREAD_SIGNAL_UE_DETACH    (0x00000040)
#define THREAD_SIGNAL_RRC_UNKNOWN  (0x00000080)
/* 3 seconds */
#define GPS_NOT_READY_TO           (3000)
/* 15 mins */
#define GPS_NOT_READY_RESET_TO     (1000*60*15)
#define GPS_NOT_READY_RESET_CNT    (GPS_NOT_READY_RESET_TO/GPS_NOT_READY_TO)

/*-------------------------------------------------------------------------
 * Private prototype declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Core static variables
 *-----------------------------------------------------------------------*/

static responseParser_fp gActiveCallback = NULL;

atCommandResponse_t gCommandList[] = {
    {
        /* MODEM_CMD_AT_ONLINE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_GET_VERSION_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%VER",
        .callback  = altairHandleParseRespVersion,
    },
    {
        /* MODEM_CMD_GET_RK_VERSION_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%VER",
        .callback  = altairHandleParseRespRkVersion,
    },
    {
        /* MODEM_CMD_GET_IMEI_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CGSN",
        .callback  = altairHandleParseRespImei,
    },
    {
        /* MODEM_CMD_GET_ICCID_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%CCID",
        .callback  = altairHandleParseRespIccid,
    },
    {
        /* MODEM_CMD_GET_IMSI_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CIMI",
        .callback  = altairHandleParseRespImsi,
    },
    {
        /* MODEM_CMD_GET_NW_OPERATOR_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%NWOPER?",
        .callback  = altairHandleParseRespNwoper,
    },
    {
        /* MODEM_CMD_SET_NW_OPERATOR_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%NWOPER=\"default\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_GET_RSSI_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CSQ",
        .callback  = altairHandleParseRespRssi,
    },
    {
        /* MODEM_CMD_GET_RSRP_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%MEAS=\"8\"",
        .callback  = altairHandleParseRespRsrp,
    },
    {
        /* MODEM_CMD_GET_RSRQ_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%MEAS=\"8\"",
        .callback  = altairHandleParseRespRsrq,
    },
    {
        /* MODEM_CMD_GET_SINR_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%MEAS=\"8\"",
        .callback  = altairHandleParseSignalLevels,
    },
    {
        /* MODEM_CMD_GET_ALL_SIGNAL_LEVELS_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%MEAS=\"8\"",
        .callback  = altairHandleParseSignalLevels,
    },
    {
        /* MODEM_CMD_GET_CFG_BAND_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%GETCFG=\"BAND\"",
        .callback  = altairHandleParseRespGetBand,
    },
    {
        /* MODEM_CMD_SET_CFG_BAND_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%SETCFG=\"BAND\",\"%d\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_SET_CFG_EARFCN_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%SCANCFG=0,2,%d,%d,1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_SEND_IP_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%SOCKETDATA=\"SEND\",%d,%d,\"%s\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_SEND_NIP_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CSODCP=1,%d,\"%s\",2,0",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_REPORT_PACKET_GATEWAY_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CGEREP=1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_REPORT_EVENTS_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "",
        .callback  = NULL,
    },
    {
        /* MODEM_CMD_FUN_OFF_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CFUN=0,0",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_FUN_ON_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CFUN=1,0",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_IS_SIM_READY_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CPIN?",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_CREATE_SOCK_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%SOCKETCMD=\"ALLOCATE\",0,\"%s\",\"OPEN\",\"%s\",%d",
        .callback  = altairHandleParseSocketId,
    },
    {
        /* MODEM_CMD_ACTIVATE_SOCK_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%SOCKETCMD=\"ACTIVATE\",%d",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_SOCK_INFO_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%SOCKETCMD=\"INFO\",%d",
        .callback  = altairHandleParseSocketInfo,
    },
    {
        /* MODEM_CMD_ENABLE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%CMATT=1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_DISABLE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%CMATT=0",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_SET_CGEREP_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CGEREP=1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_SET_NOTIFYEV_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%NOTIFYEV=\"%s\",1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_READ_RX_BUFFER_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%SOCKETDATA=\"RECEIVE\",%d,%d",
        .callback  = altairHandleRxBuffer,
    },
    {
        /* MODEM_CMD_AT_CLI_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_AT_HW_FLOW_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "&K3",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_SET_RTC_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%CCLK=\"%02d/%02d/%02d,%02d:%02d:%02d-%02d\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_GET_RTC_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%CCLK?",
        .callback  = altairHandleParseRtc,
    },
    {
        /* MODEM_CMD_SET_CFG_LTE_INACTTMR_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%LTECMD=2,\"INACTTMR\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_GET_LTE_TIMING_ADVANCE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%LTEINFO=\"MAC\",\"TA\"",
        .callback  = altairHandleParseTa,
    },
    {
        /* MODEM_CMD_SET_PDP_APN_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CGDCONT=1,\"%s\",\"%s\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_SET_NIP_CNTRL_PLANE_URC_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CRTDCP=1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_GET_PDP_NIP_STATUS_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CGDCONT?",
        .callback  = altairHandleParsePdpNipStatus,
    },
    {
        /* MODEM_CMD_SOFT_RESET_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "Z",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_AUTOCONNECT_MODE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%setacfg=\"modem_apps.Mode.AutoConnectMode\",\"false\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_SLEEP_MODE_DISABLE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%setacfg=\"pm.conf.sleep_mode\",\"disable\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_LOCSRV_ENABLE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%setacfg=\"locsrv.operation.locsrv_enable\",\"true\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_LOCSRV_IGNSS_AUTO_RESTART_ENABLE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%setacfg=\"locsrv.internal_gnss.auto_restart\",\"enable\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_LOCSRV_IGNSS_BLANKING_GUARD_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%setacfg=\"locsrv.internal_gnss.blanking_guard\",700",
        .callback  = altairHandleParseRespOkError,
    },
    /* RK_2.1.2.10.14 ONLY */
    {
        /* MODEM_CMD_IGNSSEV_GNSSCBOOT_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSEV=\"GNSSCBOOT\",1",
        .callback  = altairHandleParseRespOkError,
    },
    /* RK_2.1.2.10.14 ONLY END */
    /* RK_3.2 ONLY */
    {
        /* MODEM_CMD_IGNSSEV_COLDSTART_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSEV=\"COLDSTART\",1",
        .callback  = altairHandleParseRespOkError,
    },
    /* RK_3.2 ONLY END */
    {
        /* MODEM_CMD_IGNSSEV_NMEA_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSEV=\"NMEA\",1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_IGNSSEV_EPHUPD_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSEV=\"EPHUPD\",1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_TADJEV_ENABLE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%TADJEV=1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_IGNSSCFG_NMEA_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSCFG=\"SET\",\"NMEA\",\"RMC\",\"GGA\",\"GSV\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_IGNSS_DEACTIVATE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSACT=0",
        .callback  = altairHandleParseRespOkError,
    },
    /* RK_2.1.2.10.14 ONLY */
    {
        /* MODEM_CMD_IGNSS_ACTIVATE_P2_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSACT=3,99999",
        .callback  = altairHandleParseRespOkError,
    },
    /* RK_2.1.2.10.14 ONLY END */
    /* RK_3.2 ONLY */
    {
        /* MODEM_CMD_IGNSS_ACTIVATE_P3_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSACT=1",
        .callback  = altairHandleParseRespOkError,

    },
    /* RK_3.2 ONLY END */
    {
        /* MODEM_CMD_IGNSS_INFO_FIX_E - DEPRECATED (RK-3.2) */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSINFO=\"FIX\"",
        .callback  = altairHandleParseIgnssFix,
    },
    {
        /* MODEM_CMD_IGNSS_INFO_NUM_SAT_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSINFO=\"SAT\"",
        .callback  = altairHandleParseIgnssNumSat,
    },
    {
        /* MODEM_CMD_CGATT_ENABLE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CGATT=1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_QUERY_RRC_STATE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%STATUS=\"RRC\"",
        .callback  = altairHandleParseRrcState,
    },
    {
        /* MODEM_CMD_QUERY_CELL_ID_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%PCONI",
        .callback  = altairHandleParseCellId,
    },
    {
        /* MODEM_CMD_PMP_NP_VER_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "",
        .callback  = altairHandleParseRespPmpNpVersion,
    },
    /* Commands for RK3.2 Flashing, Command type 1 shows the boot mode commands */
    {
        /* MODEM_CMD_BOOT_MODE_TRANSFER_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%EXE=\"setenv active_uart %d\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_BOOT_DELAY_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%SETBDELAY=2",
        .callback  = altairHandleParseRespOkError,

    },
    {
        /* MODEM_CMD_ENABLE_MAC_LOGS_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%setcfg=\"mac_log_sev\",\"0\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_BOOT_MODE_CONFIRM_E */
        .commandType = U_BOOT_MODE,
        .command   = "\r\n\r\n\r\n",
        .callback  = altairHandleParseRespBootModeConfirm,
    },
    {
        /* MODEM_CMD_BOOT_SET_BAUDRATE_E */
        .commandType = U_BOOT_MODE,
        .command   = "setenv baudrate %d",
        .callback  = altairHandleParseRespBootModeConfirm,
    },
    {
        /* MODEM_CMD_BOOT_SET_HWFLOW_E */
        .commandType = U_BOOT_MODE,
        .command   = "hwflow %s",
        .callback  = altairHandleParseRespFlowControlConfirm,
    },
    {
        /* MODEM_CMD_BOOT_MODE_BAUDRATE_E */
        .commandType = U_BOOT_MODE,
        .command   = "printenv baudrate",
        .callback  = altairHandleParseBaudrate,
    },
    {
        /* MODEM_CMD_BOOT_MODE_MTDPARTS_E */
        .commandType = U_BOOT_MODE,
        .command   = "printenv mtdparts",
        .callback  = altairHandleParseMtdparts,
    },
    {
        /* MODEM_CMD_BOOT_MODE_PARSE_FLASH_E */
        .commandType = U_BOOT_MODE,
        .command   = "bdinfo",
        .callback  = altairHandleParseBootFlash,
    },
    {
        /* MODEM_CMD_BOOT_MODE_BOARDINFO_E */
        .commandType = U_BOOT_MODE,
        .command   = "create_bdinfo 0 0 0 0",
        .callback  = altairHandleParseCreateboardinfo,
    },
    {
        /* MODEM_CMD_BOOT_MODE_LOADKERMIT_E */
        .commandType = U_BOOT_MODE,
        .command   = "loadb fullImage",
        .callback  = altairHandleParseLoadKermit,
    },
    /* RK_3.2 ONLY */
    {
        /* MODEM_CMD_IGNSSCFG_SKYLO_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSCFG=\"SET\",\"SKYLO\",1,1,6,10,6",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_ENABLE_CELL_URC_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%STATEV=1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_ENABLE_IGNSS_FIX_URC_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSEV=\"FIX\",1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_ENABLE_IGNSS_BLANKING_URC_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%IGNSSEV=\"BLANKING\",1",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_DISABLE_DATA_INACTIVITY_TIMER_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%SETCFG=\"DATA_INACTIVITY_DIS\",\"1\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_GET_EARFCN_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%MEAS=\"98\"",
        .callback  = altairHandleParseRespEarfcn,
    },
    {
        /* MODEM_CMD_SYSTEM_FAILURE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%CEER=%d,%d,%d",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_DISABLE_ECHO_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "E0",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_ENABLE_REBOOT_EVENT_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%SETACFG=\"manager.urcBootEv.enabled\",\"true\"",
        .callback  = altairHandleParseRespOkError,
    },
    /* RK_3.2 ONLY END */
    {
        /* MODEM_CMD_IMG_CHECK_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%ratimgsel?",
        .callback  = altairHandleImageSel,
    },
    {
        /* MODEM_CMD_IMG_SEL_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%ratimgsel=%d",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_RESTART_MODEM_E */
        .commandType = U_BOOT_MODE,
        .command   = "atz",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_SET_RAT_MODE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%RATACT=\"%s\",%d",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_GET_RAT_MODE_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%RATACT?",
        .callback  = altairHandleParseRatAct,
    },
    {
        /* MODEM_CMD_SET_UE_POS_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%NTNCFG=\"FIX\",%f,%f,%f,%d",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_CHECK_ATTACH_STATUS_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "+CGATT?",
        .callback  = altairHandleParseCgatt,
    },
    {
        /* MODEM_CMD_CHECK_MULTI_RAT_ENABLE_STATUS_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%GETACFG=radiom.config.multi_rat_enable",
        .callback  = altairHandleParseMultiRATRespnose,
    },
    {
        /* MODEM_CMD_CHANGE_MULTI_RAT_ENABLE_STATUS_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%%SETACFG=radiom.config.multi_rat_enable,\"%s\"",
        .callback  = altairHandleParseRespOkError,
    },
    {
        /* MODEM_CMD_TEMP_FIX_E */
        .commandType = AT_CHANNLE_MODE,
        .command   = "%LTECMD=1,\"PCOTYPE\",2",
        .callback  = altairHandleParseRespOkError,
    }
    
};

static const char * urcTags[] = {
    /* NOTIFY_UE_ATTACH */
    "+CGEV: ME PDN ACT 1",
    /* NOTIFY_UE_DETACH */
    "+CGEV: ME DETACH",
    /* NOTIFY_UE_RRC_CONNECTED_E */
    "%NOTIFYEV:\"RRCSTATE\",1",
    /* NOTIFY_UE_RRC_IDLE_E */
    "%NOTIFYEV:\"RRCSTATE\",0",
    /* NOTIFY_UE_RRC_UNKNOWN_E */
    "%NOTIFYEV:\"RRCSTATE\",2",
    /* NOTIFY_GNSS_COLD_BOOT_E */
    "%IGNSSEVU: \"GNSSCBOOT\"",
    /* NOTIFY_GNSS_EPHUPD_E */
    "%IGNSSEVU: \"EPHUPD\"",
    /* NOTIFY_RTT_REPORT_E */
    "%TADJEVU: \"RTT\"",
    /* NOTIFY_TA_REPORT_E */
    "%TADJEVU: \"TA\"",
    /* NOTIFY_NMEA_RMC_E */
    "GPRMC",
    /* NOTIFY_NMEA_GGA_E */
    "GPGGA",
    /* NOTIFY_NMEA_GSV_E */
    "GPGSV",
    /* RK_3.2 ONLY */
    /* NOTIFY_CELL_STATEV_E */
    "STATEV",
    /* NOTIFY_IGNSS_FIX_ACQ_E */
    "IGNSSEVU: \"FIX\",1",
    /* NOTIFY_IGNSS_FIX_LOST_E */
    "IGNSSEVU: \"FIX\",0",
    /* NOTIFY_IGNSS_BLANKING_STATE_E */
    "IGNSSEVU: \"BLANKING\"",
    /* NOTIFY_MODEM_FAILURE_STATE_E */
    "\%CEER: \"NAS-EMM\"",
    /* NOTIFY_BOOT_EVENT_E */
    "%BOOTEV:0",
    /* RK_3.2 ONLY END */
};

/*-------------------------------------------------------------------------
 * Modem Private Core Functions
 *-----------------------------------------------------------------------*/

skStatus_t parseAtResp(const char * resp, void * result, const char * delim, uint8_t msgField, uint16_t maxLen)
{

    if ( strstr(resp, "ERROR") )
        return SK_MODEM_ERROR;

    uint8_t counter = 0;
    char * token = strtok((char *)resp, delim);

    while ( token != NULL ) {
#if 0
        Skylo_Printf(SKYLO_DEBUG_E, "Token: %s, Counter: %d, msgField: %d\n", token, counter, msgField);
#endif
        if ( counter == msgField ) {

            strncpy((char *)result, token, maxLen);
#if 0
            Skylo_Printf(SKYLO_DEBUG_E, "Token: %s, Result: %s\n", token, (char*) result);
#endif
            break;
        }

        token = strtok(NULL, delim);
        counter++;
    }

    return SK_MODEM_OK;
}

static void internalHandleUeAttach(skConnect_t *skConnect)
{
    Skylo_Printf(SKYLO_NOTICE_E, "PDN: ATTACHED\n");
    skConnect->m->attachedState = PDN_ATTACH_E;
    altairModemSetCurrentState(skConnect, MODEM_STATE_UE_CONN_NW_ATTACHED_E);
}

static void internalHandleUeDetach(skConnect_t *skConnect)
{
    Skylo_Printf(SKYLO_NOTICE_E, "PDN: DETACHED\n");
    skConnect->m->attachedState = PDN_DETACH_E;
}

static void internalHandleUeRrcUnknown(skConnect_t *skConnect)
{
    Skylo_Printf(SKYLO_NOTICE_E, "RRC: UNKNOWN\n");

    int16_t TA = 0;
    skStatus_t stat = SK_MODEM_ERROR;

    if ( (stat = halModemGetLteTa(skConnect, &TA)) != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, _("Cannot get timing advance\n ")"TA errorno %"PRId32"\n", stat);

    skConnect->m->rrcState = UE_RRC_UNKNOWN_E;
}

// static void internalHandleUeEphupd(skConnect_t *skConnect)
// {
//     Skylo_Printf(SKYLO_DEBUG_E, _("GNSS: Eph. Update needed\n"));
//     altairModemSetCurrentState(skConnect, MODEM_STATE_GNSS_EPH_UPDATE_E);

// }

static void internalHandleUeRrcConnected(skConnect_t *skConnect)
{
    int8_t rsrp = halModemReadRsrp(skConnect);
    int8_t sinr = halModemReadSinr(skConnect);

    Skylo_Printf(SKYLO_INFO_E, "RRC: CONNECTED. RSRP: %d, SINR: %d\n", rsrp, sinr);

    skConnect->m->rrcState = UE_RRC_CONNECTED_E;
}

static void internalHandleUeRrcIdle(skConnect_t *skConnect)
{
    int8_t rsrp = halModemReadRsrp(skConnect);
    int8_t sinr = halModemReadSinr(skConnect);

    Skylo_Printf(SKYLO_INFO_E, "RRC: IDLE. RSRP: %d, SINR: %d\n", rsrp, sinr);

    /* When in IDLE mode, UE is camped on cell. So SIB16 is now available with GPS time / date */
    skConnect->m->gpsTimeAvail = true;

    int16_t TA = 0;
    skStatus_t stat = SK_MODEM_ERROR;

    if ( (stat = halModemGetLteTa(skConnect, &TA)) != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, "Cannot get timing advance. errorno %"PRId32"\n", stat);

    skConnect->m->rrcState = UE_RRC_IDLE_E;
}

static void internalHandleParseGpsNmeaRmc(skConnect_t *skConnect, char * tempRecv)
{
    /* Stroke IGNSS health monitor */
    strokeIgnssHealth();

    if ( skConnect->m->gpsNMEAHandler_fp != NULL )
        skConnect->m->gpsNMEAHandler_fp((void *)tempRecv);
}

static void internalHandleParseGpsNmeaGga(skConnect_t *skConnect, char * tempRecv)
{
    strokeIgnssHealth();
}

static void internal_handle_parse_gps_nmea_gsv(skConnect_t *skConnect, char * tempRecv)
{
    strokeIgnssHealth();
}

static void internalHandleParseBootNotify(skConnect_t *skConnect)
{
    Skylo_Printf(SKYLO_INFO_E, "[MODEM-BOOT_NOTIFY]: Modem Reboot Notified\n");

    skConnect->m->bootevFlag = 1;

    /* Check If Modem Reboot Happens Itself by Modem */
    if ( !skConnect->m->inInitApi && !skConnect->m->swtchContext.switchInitiated ) {

        Skylo_Printf(SKYLO_ERR_E, "[MODEM-BOOT_NOTIFY]: *****Modem Crash Notified*****\n");
        /* If Modem set the signal to Identify modem crash */
        skConnect->m->isModemCrash=1;
        skSetSystemResetReason(SK_RESET_REASON_MODEM_RESET_CRASH_E);
    }
}
static void internalHandleParseRttEv(skConnect_t *skConnect, char * tempRecv)
{
    uint8_t index = 0;
    int32_t oneWay = 0;
    int32_t dIta = 0;
    uint64_t tsMs = 0;

    char * token = strtok((char *)tempRecv, ",");
    if ( tempRecv != NULL ) {

        while ( token != NULL ) {

            switch ( index ) {

                /* Ignore */
                case 0:
                    break;

                /* one-way */
                case 1:

                    oneWay = atoi(token);
                    break;

                /* dIta */
                case 2:

                    dIta = atoi(token);
                    break;

                /* tsMs */
                case 3:

                    tsMs = (uint64_t)strtol(token, NULL, 10);
                    break;

                default:
                    Skylo_Printf(SKYLO_ERR_E, "Invalid index - %d\n", index);
            }

            index++;
            token = strtok(NULL, ",");
        }
    }

}

static void internalHandleParseTaEv(skConnect_t *skConnect, char * tempRecv)
{
    uint8_t index = 0, taType = 0;
    int16_t ta = 0;
    uint64_t tsMs = 0;

    char * token = strtok((char *)tempRecv, ",");
    if ( tempRecv != NULL ) {

        while ( token != NULL ) {

            switch ( index ) {

                /* Tag - ignore */
                case 0:
                    break;

                /* ta */
                case 1:

                    ta = atoi(token);
                    break;

                /* tsMs */
                case 2:

                    tsMs = (uint64_t)strtol(token, NULL, 10);
                    break;
                /* Type - Ignore */
                case 3:

                    taType = atoi(token);
                    break;

                default:
                    Skylo_Printf(SKYLO_ERR_E, "Invalid index - %d\n", index);
            }

            index++;
            token = strtok(NULL, ",");
        }
    }
}

static skStatus_t altairHandleParseRespRssi(skConnect_t *skConnect, void * rssiVar)
{
    /* Buffer to hold the CSQ response */
    char tempBuf[16] = {0};
    /* Buffer to hold RSSI value */
    char result[RSSI_MAX_LEN+1] = {0};

    if ( parseAtResp(skConnect->m->atRespBuf, tempBuf, "\r\n", 0, 16) != SK_MODEM_OK )
        return SK_MODEM_ERROR;

    if ( parseAtResp(tempBuf, result, " :,", ENUM_RSSI_RESP_FIELD, RSSI_MAX_LEN) != SK_MODEM_OK )
        return SK_MODEM_ERROR;

    int16_t rssi = atoi(result);

    if ( rssi >= 99 )
        rssi = -128;
    else
        rssi = -113 + (rssi*2);

    *((int16_t *)rssiVar) = rssi;

    return SK_MODEM_OK;
}
static void internalHandleParseModemFailStat(skConnect_t *skConnect, char * tempRecv)
{
    uint8_t index = 0;
    modemFailResp_t failResp;

    memset(&failResp,0x00,sizeof(modemFailResp_t));

    char * token = strtok((char *)tempRecv, ",");
    if ( tempRecv != NULL ) {

        while ( token != NULL ) {

            switch ( index ) {

                /* Ignore */
                case 0:
                    break;

                /* Procedure */
                case 1:

                    strcpy(failResp.procedure,token);
                    break;

                /* fail Resp */
                case 2:

                    strcpy(failResp.failure,token);
                    break;

                default:
                    Skylo_Printf(SKYLO_ERR_E, "[MODEM-FAIL-STATE] Invalid index - %d\n", index);
            }

            index++;
            token = strtok(NULL, ",");
        }
    }

    Skylo_Printf(SKYLO_DEBUG_E,_("[MODEM-FAIL-STATE] Modem Fail Procedure ")"[%s]\n", failResp.procedure);
    Skylo_Printf(SKYLO_DEBUG_E,_("[MODEM-FAIL-STATE] Modem Fail Reason ")"[%s]\n", failResp.failure);

    altairHandleParseDetachReject(&failResp);
}

static void altairHandleParseDetachReject(modemFailResp_t *failResp)
{
    /* Update timestamp If detach reject appears */
    if ( (strstr(failResp->procedure,"DETACH")) && (strstr(failResp->failure,"REJECT")) )
        Skylo_Printf(SKYLO_NOTICE_E,"[MODEM-FAIL-STATE] Modem Network NAS-EMM Detach Rejected.\n");
}

static skStatus_t altairHandleParseRespRsrp(skConnect_t *skConnect, void * rsrpVar)
{
    char result[RSRP_MAX_LEN+1] = {0};
    int16_t rsrp = -128;
    char * rsrp_ptr = NULL;

    if ( (rsrp_ptr = strstr(skConnect->m->atRespBuf, "RSRP")) ) {

        if ( parseAtResp(rsrp_ptr, result, " ", 2, RSRP_MAX_LEN) != SK_MODEM_OK )
            return SK_MODEM_ERROR;

        rsrp = !strncmp(result, "N/A,", 4) ? -128 : atoi(result);

        *((int16_t *)rsrpVar) = rsrp;
    } else {

        return SK_MODEM_ERROR;
    }


    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseRespRsrq(skConnect_t *skConnect, void * rsrqVar)
{
    char result[RSRQ_MAX_LEN+1] = {0};
    int16_t rsrq = -128;
    char * rsrqPtr = NULL;

    if ( (rsrqPtr = strstr(skConnect->m->atRespBuf, "RSRQ")) ) {

        if ( parseAtResp(rsrqPtr, result, " ", 2, RSRQ_MAX_LEN) != SK_MODEM_OK )
            return SK_MODEM_ERROR;

        rsrq = !strncmp(result, "N/A,", 4) ? -128 : atoi(result);

        *((int16_t *)rsrqVar) = rsrq;
    } else {

        return SK_MODEM_ERROR;
    }

    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseRespSinr(skConnect_t *skConnect, void * sinrVar)
{
    char result[SINR_MAX_LEN+1] = {0};
    int16_t sinr = -128;
    char * sinrPtr = NULL;

    if ( (sinrPtr = strstr(skConnect->m->atRespBuf, "SINR")) ) {

        if ( parseAtResp(sinrPtr, result, " ", 2, SINR_MAX_LEN) != SK_MODEM_OK )
            return SK_MODEM_ERROR;

       sinr = !strncmp(result, "N/A,", 4) ? -128 : atoi(result);

       /* TODO: Need to request Altair to fix this modem SINR range. Usually appears at first detection
        * of network downlink signal then operates in normal range
        */
        if ( sinr == 127 )
            sinr = -128;

       *((int16_t *)sinrVar) = sinr;

    } else {

        return SK_MODEM_ERROR;
    }

    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseSignalLevels(skConnect_t *skConnect, void * var)
{
    char result[SINR_MAX_LEN+1] = {0};
    int16_t level = -128;
    char * levelPtr = NULL, *tempStr = NULL;

    /* AT%MEAS="8"
    %MEAS: Signal Quality: RSRP = -71, RSRQ = -3, SINR = 7, RSSI = -68
    OK  */

    tempStr = malloc(strlen(skConnect->m->atRespBuf));
    
    if ( !tempStr )
        return SK_ERR_NO_MEMORY;
    memcpy(tempStr,skConnect->m->atRespBuf,strlen(skConnect->m->atRespBuf));

    if ( (levelPtr = strstr(tempStr, "RSRP")) ) {

        if ( parseAtResp(levelPtr, result, " ", 2, SINR_MAX_LEN) != SK_MODEM_OK ) {
            free(tempStr);
            return SK_MODEM_ERROR;
        }

       level = !strncmp(result, "N/A,", 4) ? -128 : atoi(result);
    //    Skylo_Printf(SKYLO_DEBUG_E, "MSLP RSRP is [%s]\n",result);

       skConnect->m->signal.rsrp = level;

    } else {
        free(tempStr);
        return SK_MODEM_ERROR;
    }

    level = -128;
    levelPtr = NULL;
    memset(result,0x00,SINR_MAX_LEN+1);
    memcpy(tempStr,skConnect->m->atRespBuf,strlen(skConnect->m->atRespBuf));

    if ( (levelPtr = strstr(tempStr, "RSRQ")) ) {

        if ( parseAtResp(levelPtr, result, " ", 2, SINR_MAX_LEN) != SK_MODEM_OK ) {
            free(tempStr);
            return SK_MODEM_ERROR;
        }

       level = !strncmp(result, "N/A,", 4) ? -128 : atoi(result);
    //    Skylo_Printf(SKYLO_DEBUG_E, "MSLP RSRQ is [%s]\n",result);

       skConnect->m->signal.rsrq = level;

    } else {
        free(tempStr);
        return SK_MODEM_ERROR;
    }

    level = -128;
    levelPtr = NULL;
    memset(result,0x00,SINR_MAX_LEN+1);
    memcpy(tempStr,skConnect->m->atRespBuf,strlen(skConnect->m->atRespBuf));

    if ( (levelPtr = strstr(tempStr, "SINR")) ) {

        if ( parseAtResp(levelPtr, result, " ", 2, SINR_MAX_LEN) != SK_MODEM_OK ) {
            free(tempStr);
            return SK_MODEM_ERROR;
        }

       level = !strncmp(result, "N/A,", 4) ? -128 : atoi(result);
    //    Skylo_Printf(SKYLO_DEBUG_E, "MSLP SINR is [%s]\n",result);

       /* TODO: Need to request Altair to fix this modem SINR range. Usually appears at first detection
        * of network downlink signal then operates in normal range
        */
        if ( level == 127 )
            level = -128;

       skConnect->m->signal.sinr = level;

    } else {
        free(tempStr);
        return SK_MODEM_ERROR;
    }

    free(tempStr);
    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseRespVersion(skConnect_t *skConnect, void * version)
{
    char tempBuf[64] = {0};

    char * start = strstr(skConnect->m->atRespBuf, "MAC Package Version");
    if ( start ) {

#if 0
        Skylo_Printf(SKYLO_ERR_E, "MAC Package Version OK\n");
#endif
        if ( parseAtResp(start, tempBuf, "\r\n", 0, 64)               != SK_MODEM_OK )
            return SK_MODEM_ERROR;

        if ( parseAtResp(tempBuf, version, " ", 3, MODEM_VER_MAX_LEN) != SK_MODEM_OK )
            return SK_MODEM_ERROR;
    }

    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseRespRkVersion(skConnect_t *skConnect, void * version)
{
    char tempBuf[64] = {0};

    char * start = strstr(skConnect->m->atRespBuf, "NP Package");
    if ( parseAtResp(start, tempBuf, "\r\n", 0, 64)               != SK_MODEM_OK )
        return SK_MODEM_ERROR;

    if ( parseAtResp(tempBuf, version, " ", 2, MODEM_VER_MAX_LEN) != SK_MODEM_OK )
        return SK_MODEM_ERROR;

    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseRespPmpNpVersion(skConnect_t *skConnect, void * version)
{
    char tempBufPmp[64] = {0};
    char tempBufNp[64] = {0};

    char * startPmp = NULL;
    char * startNp = NULL;

    if ( (startNp = strstr(skConnect->m->atRespBuf, "NP Build Time")) ) {

        if ( parseAtResp(startNp, tempBufNp, "\r\n", 0, 64 ) != SK_MODEM_OK )
            return SK_MODEM_ERROR;
        Skylo_Printf(SKYLO_DEBUG_E,"Np build time - %s\n", startNp);

    } else {

        return SK_MODEM_ERROR;
    }

    if ( (startPmp = strstr(skConnect->m->atRespBuf, "PMP Version")) ) {

        if ( parseAtResp(startPmp, tempBufPmp, "\r\n", 0, 64) != SK_MODEM_OK )
            return SK_MODEM_ERROR;
        Skylo_Printf(SKYLO_DEBUG_E,"Pmp Version - %s\n", startPmp);
    } else {

        return SK_MODEM_ERROR;
    }

    memcpy( version, tempBufPmp, strlen(tempBufPmp));
    strcat( (char * )version, "\r\n");
    strncat( (char * )version, tempBufNp, sizeof(tempBufNp));

    return SK_MODEM_OK;
}
static skStatus_t altairHandleParseRespImei(skConnect_t *skConnect, void * imeiVar)
{
    return parseAtResp(skConnect->m->atRespBuf, imeiVar, "\r\n", ENUM_IMEI_RESP_FIELD, IMEI_MAX_LEN);
}

static skStatus_t altairHandleParseRespImsi(skConnect_t *skConnect, void * imsiVar)
{
    return parseAtResp(skConnect->m->atRespBuf, imsiVar, "\r\n", ENUM_IMSI_RESP_FIELD, IMSI_MAX_LEN);
}

static skStatus_t altairHandleParseRespIccid(skConnect_t *skConnect, void * iccidVar)
{
    int16_t ret = SK_MODEM_OK;
    char *tempIccidParse = NULL;

    if ( (tempIccidParse = (char *)malloc(ICCID_MAX_LEN + 16)) != NULL ) {

#if 0
        Skylo_Printf(SKYLO_DEBUG_E, "[modedm] ICCID modem resp buff (%d)[%s]\n",
                strlen(skConnect->m->atRespBuf), skConnect->m->atRespBuf);
#endif
        ret = parseAtResp(skConnect->m->atRespBuf, tempIccidParse, "\r\n", ENUM_ICCID_RESP_FIELD, ICCID_MAX_LEN + 16);

        if ( ret == SK_MODEM_OK )
            ret = parseAtResp(tempIccidParse, iccidVar, " ", 1, ICCID_MAX_LEN);

        free(tempIccidParse);
    } else {

        Skylo_Printf(SKYLO_ERR_E, MODEM_MALLOC_FAIL_ERR_MSG);
        ret = SK_MODEM_ERR_NO_MEM;
    }

    return ret;
}

static skStatus_t altairHandleParseRespNwoper(skConnect_t *skConnect, void * opVar)
{
    Skylo_Printf(SKYLO_DEBUG_E, _("Parsing NWOPER\n"));
    return SK_MODEM_OK;
}


static skStatus_t altairHandleParseRespGetBand(skConnect_t *skConnect, void * opVar)
{
    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseRespOkError(skConnect_t *skConnect, void * status)
{
    return (strstr(skConnect->m->atRespBuf, "ERROR")) ? SK_MODEM_ERROR : SK_MODEM_OK;
}

static skStatus_t  altairHandleParseRespBootModeConfirm(skConnect_t *skConnect, void * status) {

   return (strstr(skConnect->m->atRespBuf, "#")) ? SK_MODEM_OK : SK_MODEM_ERROR;

}
static skStatus_t  altairHandleParseRespFlowControlConfirm(skConnect_t *skConnect, void * status) {

    skMSecSleep(1000);

    return (strstr(skConnect->m->atRespBuf, "flow control")) ? SK_MODEM_OK : SK_MODEM_ERROR;
}
static skStatus_t  altairHandleParseBaudrate(skConnect_t *skConnect, void * buffer)
{

        char tempBuf[32] = {0};
        char result[32] = {0};

        skMSecSleep(1000);

        char *start = strstr(skConnect->m->atRespBuf, "baudrate=");

        if ( !start )
            return SK_MODEM_ERROR;

        if ( parseAtResp(skConnect->m->atRespBuf, tempBuf, "\r\n", 1, 32) != SK_MODEM_OK )
            return SK_MODEM_ERROR;

        if ( parseAtResp(tempBuf, result, "=", 1, 32) != SK_MODEM_OK )
            return SK_MODEM_ERROR;


        *((uint32_t *)buffer) = atoi(result);
        Skylo_Printf(SKYLO_DEBUG_E, "Baudrate is %"PRIu32"\n",*((uint32_t *) buffer));

        return SK_MODEM_OK;
}
static skStatus_t  altairHandleParseMtdparts(skConnect_t *skConnect, void * status)
{
    skMSecSleep(1000);

    return (strstr(skConnect->m->atRespBuf, "mtdparts=alt12xx")) ? SK_MODEM_OK : SK_MODEM_ERROR;
}
static skStatus_t  altairHandleParseLoadKermit(skConnect_t *skConnect, void * status)
{
    skMSecSleep(1000);

    return (strstr(skConnect->m->atRespBuf, "download")) ? SK_MODEM_OK : SK_MODEM_ERROR;
}
static skStatus_t  altairHandleParseCreateboardinfo(skConnect_t *skConnect, void * status)
{
    skMSecSleep(1000);

    return (strstr(skConnect->m->atRespBuf, "done")) ? SK_MODEM_OK : SK_MODEM_ERROR;
}

static skStatus_t  altairHandleParseBootFlash(skConnect_t *skConnect, void * status)
{

    char tempBufFirst[64] = {0};
    char tempBufFinal[64] = {0};

    char * startFlashstart = NULL;
    char * startFlashsize = NULL;
    char *pEnd1 = NULL;
    char *pEnd2 = NULL;

    skMSecSleep(1000);
    if ( (startFlashsize = strstr(skConnect->m->atRespBuf, "flashsize")) ) {

        if ( parseAtResp(startFlashsize, tempBufFirst, "\r\n", 0, 64) != SK_MODEM_OK )
            return SK_MODEM_ERROR;

        if ( parseAtResp(tempBufFirst, tempBufFinal, "=", 1, 64) != SK_MODEM_OK )
            return SK_MODEM_ERROR;
        Skylo_Printf(SKYLO_DEBUG_E,_("flash size U-Boot ")"%s\n", tempBufFinal);
    } else {

        return SK_MODEM_ERROR;
    }

    skConnect->m->bootParam.flashSize = strtoll(tempBufFinal, &pEnd1, 16);

    Skylo_Printf(SKYLO_DEBUG_E,_("flash size U-Boot ")"%llx\n", skConnect->m->bootParam.flashSize);

    memset(tempBufFirst, 0x00, sizeof(tempBufFirst));
    memset(tempBufFinal, 0x00, sizeof(tempBufFinal));

    if ( (startFlashstart = strstr(skConnect->m->atRespBuf, "flashstart")) ) {

        if ( parseAtResp(startFlashstart, tempBufFirst, "\r\n", 0, 64) != SK_MODEM_OK )
            return SK_MODEM_ERROR;

        if ( parseAtResp(tempBufFirst, tempBufFinal, "=", 1, 64) != SK_MODEM_OK )
            return SK_MODEM_ERROR;
        Skylo_Printf(SKYLO_DEBUG_E,_("flash start U-Boot ")"[%s]\n", tempBufFinal);

    } else {

        return SK_MODEM_ERROR;
    }

    skConnect->m->bootParam.flashStart = strtoll(tempBufFinal, &pEnd2, 16);

    Skylo_Printf(SKYLO_DEBUG_E, _("flash start U-Boot ")"%llx\n", skConnect->m->bootParam.flashStart);

    return SK_MODEM_OK;
}
static skStatus_t altairHandleParseRespEarfcn(skConnect_t *skConnect, void * earfcnVar)
{
    char result[5+1] = {0};
    uint32_t earfcn = 0;
    char * earfcnPtr = NULL;

#if 0
    Skylo_Printf(SKYLO_INFO_E, "\n\n\n\n\nEARFCN all resp : %s\n\n\n\n\n", skConnect->m->atRespBuf);
#endif

    if ( (earfcnPtr = strstr(skConnect->m->atRespBuf, "EARFCN")) ) {

       if ( parseAtResp(earfcnPtr, result, "=", 1, 5) != SK_MODEM_OK )
        return SK_MODEM_ERROR;

       earfcn = atoi(result);

       *((uint32_t *)earfcnVar) = earfcn;
#if 0
        Skylo_Printf(SKYLO_INFO_E, "\n\n\nresult is %s, earfcn is %d\n\n\n", result, earfcn);
#endif
    } else {

        return SK_MODEM_ERROR;
    }

    return SK_MODEM_OK;
}
static skStatus_t altairHandleParseRespFotaFile(skConnect_t *skConnect, void * status)
{
    return ( (strstr(skConnect->m->atRespBuf, "ERROR")) ||
            (strstr(skConnect->m->atRespBuf, "\%FILEDATA:-1")) ) ? SK_MODEM_ERROR : SK_MODEM_OK;
}

static skStatus_t altairHandleParseSocketInfo(skConnect_t *skConnect, void * status)
{
    if ( strstr(skConnect->m->atRespBuf, "ERROR") )
        return SK_MODEM_ERR_SOCK_NOT_AVAIL;

    if ( strstr(skConnect->m->atRespBuf, "OK") ) {

        if ( strstr(skConnect->m->atRespBuf, "DEACTIVATED") )
            return SK_MODEM_ERR_SOCK_NOT_ACTIVE;

        if ( strstr(skConnect->m->atRespBuf, "ACTIVATED") )
            return SK_MODEM_OK;
    }

    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseSocketId(skConnect_t *skConnect, void * id)
{

    Skylo_Printf(SKYLO_NOTICE_E, "\nSOCKETCMD = %s\n", skConnect->m->atRespBuf);
    // %SOCKETCMD:1

    if ( strstr(skConnect->m->atRespBuf, "SOCKETCMD:") ) {

        char tempBuf[32] = {0};
        char result[4] = {0};

        if ( parseAtResp(skConnect->m->atRespBuf, tempBuf, "\r\n", 0, 32) != SK_MODEM_OK )
            return SK_MODEM_ERROR;
        if ( parseAtResp(tempBuf, result, ":", 1, 2) != SK_MODEM_OK )
            return SK_MODEM_ERROR;

        Skylo_Printf(SKYLO_NOTICE_E, "\nnSOCKETCMD result = %s\n", result);

        *((int16_t *)id) = (int16_t)atoi(result);

        Skylo_Printf(SKYLO_NOTICE_E, "\nnSOCKETCMD result = %d\n", (*((int16_t *)id)));
    } else {
        Skylo_Printf(SKYLO_ERR_E, "Could not handle SOCKETCMD parse\n");
    }

    return SK_MODEM_OK;
}

static skStatus_t altairHandleRxBuffer(skConnect_t *skConnect, void * buffer)
{
    if ( !buffer )
        return SK_MODEM_ERROR;

    /* Retrieve SOCKETDATA resp */
    char tempBuf[RX_DATA_BUFFER_SIZE+33] = {0};
    if ( parseAtResp(skConnect->m->atRespBuf, tempBuf, "\r\n", 0, RX_DATA_BUFFER_SIZE+33) )
        return SK_MODEM_ERROR;

    /* Retrieve hex string with double quotes */
    if ( parseAtResp(tempBuf, buffer, ",", 3, 200) != SK_MODEM_OK )
        return SK_MODEM_ERROR;

    /* Remove 2x double quotes on either side */
    int dataLen = strlen(buffer) - 2;
    if ( dataLen % 2 != 0 ) {

        Skylo_Printf(SKYLO_ERR_E, "Rx Hex string len is odd. HEX: %s\n", (char*) buffer);
        return SK_MODEM_ERROR;
    }

    strncpy(buffer, &((char *)buffer)[1], dataLen);
    /* Add NULL terminator */
    ((char*)buffer)[dataLen] = '\0';

    Skylo_Printf(SKYLO_DEBUG_E, _("Rx Data Buffer = ")"RxDB[%s]\n", (char*)buffer);

    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseRtc(skConnect_t *skConnect, void * buffer)
{
    rtcTm_t * currTime = (rtcTm_t *)buffer;
    char dateTime[20+1] = {0};
    char * pEnd = NULL;

    if ( parseAtResp(skConnect->m->atRespBuf, dateTime, "\"", 1, 20) != SK_MODEM_OK )
        return SK_MODEM_ERROR;

    currTime->year   = (uint8_t)strtol(dateTime, &pEnd, 10);
    currTime->month  = (uint8_t)strtol(++pEnd, &pEnd, 10);
    currTime->day    = (uint8_t)strtol(++pEnd, &pEnd, 10);
    currTime->hour   = (uint8_t)strtol(++pEnd, &pEnd, 10);
    currTime->minute = (uint8_t)strtol(++pEnd, &pEnd, 10);
    currTime->second = (uint8_t)strtol(++pEnd, &pEnd, 10);

    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseTa(skConnect_t *skConnect, void * buffer)
{
#ifdef DEBUG_SKCONNECT
    Skylo_Printf(SKYLO_NOTICE_E, "\nTA = %s\n", skConnect->m->atRespBuf);
#endif
    // /%LTEINFO: N/A, 0
    if ( !strstr(skConnect->m->atRespBuf, "N/A") ) {

        char tempBuf[32] = {0};
        char result[4] = {0};

        if ( parseAtResp(skConnect->m->atRespBuf, tempBuf, "\r\n", 0, 32) != SK_MODEM_OK )
            return SK_MODEM_ERROR;
        if ( parseAtResp(tempBuf, result, " ", 1, 4) != SK_MODEM_OK )
            return SK_MODEM_ERROR;

        *((int16_t *)buffer) = atoi(result);
    }

    return SK_MODEM_OK;
}

static skStatus_t altairHandleParsePdpNipStatus(skConnect_t *skConnect, void * buffer)
{
    return strstr(skConnect->m->atRespBuf, "+CGDCONT: 1,\"Non-IP\"") ? SK_MODEM_OK : SK_MODEM_ERROR;
}

static skStatus_t altairHandleParseIgnssFix(skConnect_t *skConnect, void * buffer)
{
#ifdef DEBUG_SKCONNECT
    Skylo_Printf(SKYLO_NOTICE_E, "\nGNSS FIX: [%s]\n", skConnect->m->atRespBuf);
#endif

    if ( strstr(skConnect->m->atRespBuf, "%IGNSSINFO: 0") )
        return SK_MODEM_ERR_GNSS_NO_FIX;
    else if ( strstr(skConnect->m->atRespBuf, "%IGNSSINFO: 1") || strstr(skConnect->m->atRespBuf, "%IGNSSINFO: 2") )
        return SK_MODEM_OK;

    return SK_MODEM_ERROR;
}

static skStatus_t altairHandleParseIgnssNumSat(skConnect_t *skConnect, void * buffer)
{
    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseRrcState(skConnect_t *skConnect, void * buffer)
{
    modemRrcState_e * rrcState = (modemRrcState_e *)buffer;

    if ( strstr(skConnect->m->atRespBuf, "IDLE") ) {

        *rrcState = UE_RRC_IDLE_E;
    } else if ( strstr(skConnect->m->atRespBuf, "CONNECTED") ) {

        *rrcState = UE_RRC_CONNECTED_E;
    } else if ( strstr(skConnect->m->atRespBuf, "UNKNOWN") ) {

        *rrcState = UE_RRC_UNKNOWN_E;
    } else {

        Skylo_Printf(SKYLO_ERR_E, "RRC state invalid\n");
        *rrcState = UE_RRC_UNKNOWN_E;
        return SK_MODEM_ERROR;
    }
    return SK_MODEM_OK;
}

static skStatus_t altairHandleImageSel(skConnect_t *skConnect, void * buffer)
{
#ifdef DEBUG_SKCONNECT
    Skylo_Printf(SKYLO_NOTICE_E, "\nImage sel = %s\n", skConnect->m->atRespBuf);
#endif
    // %RATIMGSEL: 2

    if ( strstr(skConnect->m->atRespBuf, "RATIMGSEL:") ) {

        char tempBuf[32] = {0};
        char result[4] = {0};

        if ( parseAtResp(skConnect->m->atRespBuf, tempBuf, "\r\n", 0, 32) != SK_MODEM_OK )
            return SK_MODEM_ERROR;
        if ( parseAtResp(tempBuf, result, " ", 1, 4) != SK_MODEM_OK )
            return SK_MODEM_ERROR;

        *((int16_t *)buffer) = atoi(result);
    } else {
        Skylo_Printf(SKYLO_ERR_E, "Could not handle Image check command\n");
    }

    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseRatAct(skConnect_t *skConnect, void * buffer)
{
#ifdef DEBUG_SKCONNECT
    Skylo_Printf(SKYLO_NOTICE_E, "\nratact = %s\n", skConnect->m->atRespBuf);
#endif
    // %RATACT: "NBNTN",1,1

    if ( strstr(skConnect->m->atRespBuf, "RATACT:") ) {

        char tempBuf[32] = {0};
        char result[8] = {0};

        if ( parseAtResp(skConnect->m->atRespBuf, tempBuf, "\r\n", 0, 32) != SK_MODEM_OK )
            return SK_MODEM_ERROR;
        if ( parseAtResp(tempBuf, result, "\"", 1, 6) != SK_MODEM_OK )
            return SK_MODEM_ERROR;

            
        Skylo_Printf(SKYLO_NOTICE_E, "\nratact result = %s\n", result);

        //%RATACT: ("DEFAULT","CATM","NBIOT","NBNTN","C2D","C2DUC","N2D","N2DUC"),(0-1),(0-2)
        // OK

        //c2d and crduc case not handled rght nor dont need it currently, we compare first 3 char of string
        if ( strnstr(result,"NBN",3) ) {
            (*(modemNbiotImage_e*)buffer) = (uint8_t) NBIOT_IMAGE_NTN_E;
        } else if ( strnstr(result,"NBI",3) ) { 
            (*(modemNbiotImage_e*)buffer)  = (uint8_t)NBIOT_IMAGE_TN_E;
        } else {
            (*(modemNbiotImage_e*)buffer) = (uint8_t) NBIOT_IMAGE_OTHER_E;
        }
    } else {
        Skylo_Printf(SKYLO_ERR_E, "Could not handle RATACT Image check command\n");
    }

    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseCgatt(skConnect_t *skConnect, void * buffer)
{
#ifdef DEBUG_SKCONNECT
    Skylo_Printf(SKYLO_NOTICE_E, "\ncgatt = %s\n", skConnect->m->atRespBuf);
#endif
    // %RATACT: "NBNTN",1,1

    if ( strstr(skConnect->m->atRespBuf, "CGATT:") ) {

        char tempBuf[32] = {0};
        char result[8] = {0};

        if ( parseAtResp(skConnect->m->atRespBuf, tempBuf, "\r\n", 0, 32) != SK_MODEM_OK )
            return SK_MODEM_ERROR;
        if ( parseAtResp(tempBuf, result, " ", 1, 2) != SK_MODEM_OK )
            return SK_MODEM_ERROR;

            
        Skylo_Printf(SKYLO_NOTICE_E, "\ncgatt result = %s\n", result);

        if ( strnstr(result,"1",1) ) {

            internalHandleUeAttach(skConnect);
        } else if ( strnstr(result,"0",1) ) {
            
            internalHandleUeDetach(skConnect);
        }
    } else {
        Skylo_Printf(SKYLO_ERR_E, "Could not handle CGATT check command\n");
    }

    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseMultiRATRespnose(skConnect_t *skConnect, void * buffer)
{
#ifdef DEBUG_SKCONNECT
    Skylo_Printf(SKYLO_NOTICE_E, "\nMulti RAT Enable = %s\n", skConnect->m->atRespBuf);
#endif
    // FALSE / TRUE

    if ( strstr(skConnect->m->atRespBuf, "TRUE") ) {

        *((uint8_t*)buffer) = 1;
    } else if ( strstr(skConnect->m->atRespBuf, "FALSE") ) {
        *((uint8_t*)buffer) = 0;
    } else {
        Skylo_Printf(SKYLO_ERR_E, "Could not handle Multi RAT enable check\n");
    }

    return SK_MODEM_OK;
}

static skStatus_t altairHandleParseCellId(skConnect_t *skConnect, void * buffer)
{

    char *ptr = NULL;
#if 0
    Skylo_Printf(SKYLO_DEBUG_E, "AT%%PCONI Response (%d)[%s]\n",
                        strlen(skConnect->m->atRespBuf), skConnect->m->atRespBuf);
#endif
    if ( strstr(skConnect->m->atRespBuf, "ERROR") ) {

        return SK_MODEM_ERROR;
    } else {

        /* Example String : Global Cell ID: 0x01A2D300 */
#if 0
        Skylo_Printf(SKYLO_DEBUG_E, "AT%%PCONI Response (%d)[%s]\n",
                            strlen(skConnect->m->atRespBuf), skConnect->m->atRespBuf);
#endif
        if ( (ptr = strstr(skConnect->m->atRespBuf, "Global Cell ID: ")) == NULL ) {

            Skylo_Printf(SKYLO_INFO_E, _("Something Unusual with command AT%%PCONI, Unable to find \"Global Cell ID\" ")
                                        "in response [%s]\n", skConnect->m->atRespBuf);
            return SK_MODEM_ERROR;
        } else {

            if ( (ptr = strstr(ptr, "0x")) == NULL ) {

                Skylo_Printf(SKYLO_INFO_E, _("Something Unusual with command AT%%PCONI, Unable to find \"0x\" ")
                                            "in response [%s]\n", skConnect->m->atRespBuf);
                return SK_MODEM_ERROR;
            } else {

                /* skip "0x" */
                ptr = ptr+2;
                *(ptr+8) = '\0';

                skConnect->m->cellId = (uint32_t) skHex2Int(ptr);
                Skylo_Printf(SKYLO_INFO_E, _("AT%%PCONI Global ")"Cell ID is (%"PRIu32")[%s]\n",
                                            skConnect->m->cellId, ptr);
            }
        }
    }

    return SK_MODEM_OK;
}

/*-------------------------------------------------------------------------
 * Modem Public Core Functions
 *-----------------------------------------------------------------------*/

skStatus_t altairCheckModemOnline(skConnect_t *skConnect)
{
    while ( !skConnect->m->atReadyState ) {

        Skylo_Printf(SKYLO_DEBUG_E, _("Check modem is online...\n"));
        skStatus_t stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_AT_ONLINE_E].command,
                                                MODEM_CMD_AT_ONLINE_E, NULL);
        if ( stat != SK_MODEM_OK ) {

            Skylo_Printf(SKYLO_ERR_E, _("Modem is not online. ")"errorno %"PRId32". retry in %d ms\n",
                            stat, BACKOFF_PERIOD);
            skMSecSleep(BACKOFF_PERIOD);
        } else {

            skConnect->m->atReadyState = true;
            Skylo_Printf(SKYLO_NOTICE_E, "Checking modem is online...OK\n");
        }
    }

    return SK_MODEM_OK;
}

char gTempRecv[RX_DATA_BUFFER_SIZE] = {0};

void *altairModemRxThread(void * arg)
{
    Skylo_Printf(SKYLO_DEBUG_E, _("Rx Thread Started\n"));

    skConnect_t *skConnect = (skConnect_t *)arg;

    const char * okFound    = NULL;
    const char * errorFound = NULL;
    const char * bootFound = NULL;

    char *lineStart = NULL;
    uint32_t lineLen = 0;
    char *endOfLine = NULL;
    char prevChar = '\0';

    uint32_t tempRecvLen = 0;

    while ( true ) {

        memset(gTempRecv, 0, RX_DATA_BUFFER_SIZE);
        tempRecvLen = sizeof(gTempRecv)-1;
        while ( (SK_OK == skConnect->pep.hal.fpModemRecv(gTempRecv, &tempRecvLen)) && tempRecvLen > 0 ) {

            lineStart = gTempRecv;
            endOfLine = NULL;
            while ( tempRecvLen ) {

                /* Only do operation if it is not first iteration */
                if ( endOfLine != NULL ) {

                    lineStart = endOfLine + 1;
                    *lineStart = prevChar;
                }

                endOfLine = strstr(lineStart,"\n");
                prevChar = *(endOfLine + 1);
                *(endOfLine + 1) = '\0';

                if ( endOfLine == NULL ) {

                    Skylo_Printf(SKYLO_CRIT_E, _("Something Went Wrong Expecting Line, ")
                                                "Unable to parse message without \"\\n\": [%s]\n", lineStart);
                    break;
                } else {

                    lineLen = endOfLine - lineStart + 1;
                    tempRecvLen -= lineLen;
                }
#ifdef DEBUG_SKCONNECT
                Skylo_Printf(SKYLO_DEBUG_E, "Received Message (%d)[%s]\n", lineLen, lineStart);
#endif

                /* Handle received data NIP URC and store in buffer for processing. Then break out of loop then function */
                if ( lineLen > 0 && (strstr(lineStart, "+CRTDCP: 1") || strstr(lineStart,"NBMCEVU: 1")) ) {

                    /* todo dex add multicast code */
                    Skylo_Printf(SKYLO_DEBUG_E, _("NIP Data Received on rx buffer: ")"NIPDRB: [%s]\n", lineStart);
    #if 0
                    strncat(skConnect->m->rxDataBuf, lineStart, lineLen);
    #endif

                    if ( (strstr(lineStart, "+CRTDCP: 1")) ) {

                        /* Unicast data */
                        strncpy(skConnect->m->rxDataBuf, lineStart, lineLen);
                        skConnect->pep.os.ipc.fpSignalSet(&skConnect->m->eventSignal, THREAD_SIGNAL_RX_SKMSG);
                    } else {

                        Skylo_Printf(SKYLO_ERR_E, "should not reach her\n");
                    }

                    Skylo_Printf(SKYLO_ERR_E, _("RX DATA : ")"[%s]\n",skConnect->m->rxDataBuf);
                } else {

                    /* Append to modem response array */
                    if ( strlen(skConnect->m->atRespBuf) + tempRecvLen  + 1 < AT_RESP_BUFFER_SIZE ) {

                        strncat(skConnect->m->atRespBuf, lineStart, lineLen);
                    } else {

                        Skylo_Printf(SKYLO_ERR_E, _("Truncating. Buffer cannot fit appended data. ")
                                                    "Can only fit %d bytes\n", AT_RESP_BUFFER_SIZE);
                        if ( AT_RESP_BUFFER_SIZE - strlen(skConnect->m->atRespBuf) + 1 > 0 ) {
                            strncat(skConnect->m->atRespBuf, lineStart,
                                        AT_RESP_BUFFER_SIZE - strlen(skConnect->m->atRespBuf));
                            //memset(skConnect->m->atRespBuf, 0, strlen(skConnect->m->atRespBuf));
                        }
                    }
                }

                okFound    = strstr(lineStart, "OK\r\n");
                errorFound = strstr(lineStart, "ERROR\r\n");
                bootFound = strstr(lineStart, "# ");

                /* Notify that a resp has been received from AT command */
                if ( okFound || errorFound || bootFound ) {

                    skConnect->pep.os.ipc.fpSignalSet(&skConnect->m->eventSignal, THREAD_SIGNAL_RX_RESP);
                } else {

                    /* Handle registered notifications (if any) */
                    int index = 0;
                    char * urc_tag_ptr = NULL;
                    uint32_t fsm_signal  = 0x00000000;
                    while ( index < NOTIFY_TOTAL_NUM_NOTIFY_EVS_E ) {
    #if 0
                            Skylo_Printf(SKYLO_DEBUG_E, "URC check: %s\n", urcTags[index]);
    #endif
                        if ( (urc_tag_ptr = strstr(lineStart, urcTags[index])) ) {
    #if 0
                            Skylo_Printf(SKYLO_DEBUG_E, "URC found: %s\n", urc_tag_ptr);
    #endif
                            switch ( index ) {

                                case NOTIFY_PDN_ATTACH_E:

                                    fsm_signal = THREAD_SIGNAL_UE_ATTACH;
                                    Skylo_Printf(SKYLO_DEBUG_E, _("URC THREAD_SIGNAL_UE_ATTACH received\n"));
                                    break;

                                case NOTIFY_PDN_DETACH_E:

                                    fsm_signal = THREAD_SIGNAL_UE_DETACH;
                                    break;

                                case NOTIFY_UE_RRC_CONNECTED_E:

                                    fsm_signal = THREAD_SIGNAL_RRC_CONN;
                                    break;

                                case NOTIFY_UE_RRC_IDLE_E:

                                    fsm_signal = THREAD_SIGNAL_RRC_IDLE;
                                    break;

                                case NOTIFY_UE_RRC_UNKNOWN_E:

                                    fsm_signal = THREAD_SIGNAL_RRC_UNKNOWN;
                                    break;

                                case NOTIFY_GNSS_COLD_BOOT_E:

                                    Skylo_Printf(SKYLO_DEBUG_E, "GNSS cold boot\n");
//                                    fsm_signal = THREAD_SIGNAL_GNSS_COLD_BOOT;
                                    break;

                                case NOTIFY_GNSS_EPHUPD_E:

                                    Skylo_Printf(SKYLO_DEBUG_E, "GNSS Ephemeris update required\n");
//                                    fsm_signal = THREAD_SIGNAL_GNSS_EPH_UPDATE;
                                    break;

                                case NOTIFY_RTT_REPORT_E:
#ifdef DEBUG_SKCONNECT
                                    Skylo_Printf(SKYLO_INFO_E, "[MODEM-DRTT] %s", urc_tag_ptr);
#endif
//                                    fsm_signal = THREAD_SIGNAL_RTT_REPORT;
                                    internalHandleParseRttEv(skConnect, lineStart);
                                    break;

                                case NOTIFY_TA_REPORT_E:
#ifdef DEBUG_SKCONNECT
                                    Skylo_Printf(SKYLO_INFO_E, "[MODEM-DRTT] %s", urc_tag_ptr);
#endif
//                                    fsm_signal = THREAD_SIGNAL_TA_REPORT;
                                    internalHandleParseTaEv(skConnect, lineStart);
                                    break;

                                case NOTIFY_NMEA_RMC_E:

                                    //Skylo_Printf(SKYLO_INFO_E, "[MODEM-GNSS: GPRMC] %s", urc_tag_ptr);
                                    internalHandleParseGpsNmeaRmc(skConnect, lineStart);
                                    break;

                                case NOTIFY_NMEA_GGA_E:

                                    //Skylo_Printf(SKYLO_INFO_E, "[MODEM-GNSS: GPGGA] %s", urc_tag_ptr);
                                    internalHandleParseGpsNmeaGga(skConnect, lineStart);
                                    break;

                                case NOTIFY_NMEA_GSV_E:

                                    //Skylo_Printf(SKYLO_INFO_E, "[MODEM-GNSS: GPGSV] %s", urc_tag_ptr);
                                    internal_handle_parse_gps_nmea_gsv(skConnect, lineStart);
                                    break;

    /* RK_3.2 ONLY */
                                case NOTIFY_CELL_STATEV_E:

                                    Skylo_Printf(SKYLO_INFO_E, "[MODEM-CELL]:\n");
                                    break;

                                case NOTIFY_IGNSS_FIX_ACQ_E:

                                    Skylo_Printf(SKYLO_INFO_E, "[MODEM-GNSS]: FIX\n");
                                    skConnect->m->gpsFix = true;
                                    break;

                                case NOTIFY_IGNSS_FIX_LOST_E:

                                    Skylo_Printf(SKYLO_INFO_E, "[MODEM-GNSS]: LOST\n");
                                    skConnect->m->gpsFix = false;
                                    break;

                                case NOTIFY_IGNSS_BLANKING_STATE_E:

                                    Skylo_Printf(SKYLO_INFO_E, "[MODEM-GNSS-BLNKG]: %s\n", urc_tag_ptr);
                                    break;

                                case NOTIFY_MODEM_FAILURE_STATE_E:

                                    Skylo_Printf(SKYLO_INFO_E, "[MODEM-FAIL-STATE]: [%s]\n", urc_tag_ptr);
                                    internalHandleParseModemFailStat(skConnect, lineStart);
                                    break;

                                case NOTIFY_BOOT_EVENT_E:
#ifdef DEBUG_SKCONNECT
                                    Skylo_Printf(SKYLO_INFO_E, "[MODEM-BOOT_NOTIFY]: %s\n", urc_tag_ptr);
#endif
                                    internalHandleParseBootNotify(skConnect);
                                    break;
    /* RK_3.2 ONLY END */
                                default:
                                    Skylo_Printf(SKYLO_ERR_E, "Invalid notify index - %d\n", index);
                            }
                        }
                        index++;
                    }

                    if ( fsm_signal != 0 ) {

                        skConnect->pep.os.ipc.fpSignalSet(&skConnect->m->eventSignal, fsm_signal);
                        memset(skConnect->m->atRespBuf, 0, strlen(skConnect->m->atRespBuf));
                    }
                }
            }
            tempRecvLen = sizeof(gTempRecv)-1;
            memset(gTempRecv, 0x00, RX_DATA_BUFFER_SIZE);
        }
    }


    Skylo_Printf(SKYLO_EMERG_E, "RX thread stopped\n");

    /* Reset the platform. */
    skSetSystemResetReason(SK_RESET_REASON_MODEM_RX_THD);
    skyloAppThreadStop(skConnect, APP_THREAD_MODEM_RX_INDEX_NO);
}

void *altairModemFsmThread(void * arg)
{
    skStatus_t status = SK_ERROR;

    skConnect_t *skConnect = (skConnect_t *)arg;
    uint32_t signalSet = 0, hectoMsec = 0;

    Skylo_Printf(SKYLO_DEBUG_E, _("FSM Thread Started\n"));

    for ( ;; ) {

        status = skConnect->pep.os.ipc.fpSignalWaitTimed(&skConnect->m->eventSignal,
                                        THREAD_SIGNAL_RRC_CONN  |
                                        THREAD_SIGNAL_RRC_IDLE  |
                                        THREAD_SIGNAL_UE_ATTACH |
                                        THREAD_SIGNAL_UE_DETACH |
                                        THREAD_SIGNAL_RRC_UNKNOWN, SK_SIG_ATTRIBUTE_WAIT_FOR_ANY
                                        | SK_SIG_ATTRIBUTE_CLEAR_MASK, &signalSet,SK_TIME_NO_WAIT);

        if ( status == SK_OK ) {

            if ( signalSet & THREAD_SIGNAL_RRC_CONN )
                internalHandleUeRrcConnected(skConnect);

            if ( signalSet & THREAD_SIGNAL_RRC_IDLE && skConnect->m->ratActImg == NBIOT_IMAGE_NTN_E )
                internalHandleUeRrcIdle(skConnect);

            if ( signalSet & THREAD_SIGNAL_UE_ATTACH ) {

                internalHandleUeAttach(skConnect);
            }

            if ( signalSet & THREAD_SIGNAL_UE_DETACH )
                internalHandleUeDetach(skConnect);

            if ( signalSet & THREAD_SIGNAL_RRC_UNKNOWN && skConnect->m->ratActImg == NBIOT_IMAGE_NTN_E )
                internalHandleUeRrcUnknown(skConnect);

//            if ( (signalSet & THREAD_SIGNAL_GNSS_COLD_BOOT) || (signalSet & THREAD_SIGNAL_GNSS_EPH_UPDATE) )
//                internalHandleUeEphupd(skConnect);

        }

        skMSecSleep(100);
        hectoMsec++;

        // check for scheduled task every second
        if ( hectoMsec % 10 == 0 ) {

            scheduledTaskExec(skConnect, hectoMsec/10);
        }

                            
        /* if switch flag is set, initialize modem partially i.e. AT init sequence only */
        if ( skConnect->m->swtchContext.switchInitiated ) {
            
            sKonnectInit(skConnect, SK_PARTIAL_INIT);
            skConnect->m->swtchContext.switchInitiated = 0;
            hectoMsec = 0;
        }

    }

    Skylo_Printf(SKYLO_EMERG_E, "Modem FSM thread stopped\n");

    /* Reset the platform. */
    skSetSystemResetReason(SK_RESET_REASON_MODEM_FSM_THD);
    skyloAppThreadStop(skConnect, APP_THREAD_MODEM_FSM_INDEX_NO);
}

skStatus_t altairFireCommand(skConnect_t *skConnect, const char * cmd, modemCommand_e msg, void * resp)
{
    int32_t ret = SK_MODEM_OK;
    int32_t timeoutStore = 10000;

#ifdef DEBUG_SKCONNECT
    Skylo_Printf(SKYLO_DEBUG_E, "fire command entry [%s]\n",cmd);
#endif

    memset(skConnect->m->atRespBuf, 0, strlen(skConnect->m->atRespBuf));
    /* Check if AT CLI is ready. Return if not ready with exception of MODEM_CMD_AT_ONLINE_E fire and needs
     * to be in non-CLI mode
     */
    if ( !skConnect->m->atReadyState && msg != MODEM_CMD_AT_ONLINE_E && !skConnect->options->cliMode ){
#ifdef DEBUG_SKCONNECT
        Skylo_Printf(SKYLO_ERR_E, "fire command exit partial [%s]\n",cmd);
#endif
        return SK_MODEM_ERR_AT_NOT_READY;
    }
    if ( gCommandList[msg].commandType == AT_CHANNLE_MODE && halModemGetBootMode(skConnect) ) {

        Skylo_Printf(SKYLO_ERR_E, _("Modem is in U-Boot Mode Can not Fire AT command\n"));
        return SK_MODEM_ERR_AT_NOT_READY;
    }

    if ( (skConnect->pep.os.ipc.fpMutexLockTimed(&(skConnect->m->transactionMutex), 500 )) != SK_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Transaction mutex busy,"_(" cant't fire ")"command %s\n", cmd);
        ret = SK_MODEM_ERR_RESOURCE_IN_USE;
        goto ping;
    }

    /* Check if busy */
    if ( gActiveCallback ) {

        Skylo_Printf(SKYLO_ERR_E, "Cannot fire command. Busy.\n");
        ret = SK_MODEM_ERR_FIRE_BUSY;
        goto reset;
    }

    /* Prime available callback for specific modem command */
    gActiveCallback = gCommandList[msg].callback;
    memset(skConnect->m->atCmdBuf,0,AT_CMD_BUFFER_SIZE);

    if ( !gCommandList[msg].commandType ) {

        /* Create AT command */
        if ( snprintf(skConnect->m->atCmdBuf, AT_CMD_BUFFER_SIZE, "AT%s\r\n", cmd) < 0 ) {

            Skylo_Printf(SKYLO_ERR_E, "Cannot create AT cmd: [%s]\n", skConnect->m->atCmdBuf);
            ret = SK_MODEM_ERROR;
            goto reset;
        }
    } else {

        /* Create Boot mode command */
        if ( snprintf(skConnect->m->atCmdBuf, AT_CMD_BUFFER_SIZE, "%s\r\n", cmd) < 0 ) {

            Skylo_Printf(SKYLO_ERR_E, "Cannot create Boot mode cmd: [%s]\n", skConnect->m->atCmdBuf);
            ret = SK_MODEM_ERROR;
            goto reset;
        }
    }

    /* Clear Any Previous Signal Which Is Arived Let And Is Of The Last Command Fired */
    skConnect->pep.os.ipc.fpSignalClear(&skConnect->m->eventSignal, THREAD_SIGNAL_RX_RESP);
    
    int stat = skConnect->pep.hal.fpModemSend(skConnect->m->atCmdBuf,strlen(skConnect->m->atCmdBuf));

    if ( stat != SK_OK ) {
        
        Skylo_Printf(SKYLO_ERR_E, "CORE : AT: [%s] command send failed %d\n", skConnect->m->atCmdBuf, stat);
        ret = SK_MODEM_ERR_DATA_SEND;
        goto reset;
    }

    /* Wait for response from modem for 10 seconds. If no resp, then timeout and reset. */
    uint32_t signal = 0;
    if ( msg == MODEM_CMD_CGATT_ENABLE_E ) {

        timeoutStore = skConnect->m->modemResponseTimeoutMsec;
        skConnect->m->modemResponseTimeoutMsec = 60000;
    }

    stat = skConnect->pep.os.ipc.fpSignalWaitTimed(&skConnect->m->eventSignal, THREAD_SIGNAL_RX_RESP,
                    SK_SIG_ATTRIBUTE_WAIT_FOR_ANY | SK_SIG_ATTRIBUTE_CLEAR_MASK, &signal,
                    skConnect->m->modemResponseTimeoutMsec);

    if ( msg == MODEM_CMD_CGATT_ENABLE_E )
        skConnect->m->modemResponseTimeoutMsec = timeoutStore;

    if ( stat != SK_OK ) {

        ret = SK_MODEM_ERR_AT_NO_RESP;
        skConnect->m->badATCount++;
        
        Skylo_Printf(SKYLO_ERR_E, "CORE : AT: [%s] command failed"_(" on signal wait with ")"errorno %d, badAT(%d)\n",
                        skConnect->m->atCmdBuf, stat, skConnect->m->badATCount);
        goto reset;
    } else {
        skConnect->m->badATCount  = 0;
    }

    // Skylo_Printf(SKYLO_INFO_E,"\n\n>><< Response from Modem [%s]\n\n", skConnect->m->atRespBuf);
    /* Invoke callback function */
    ret = gActiveCallback(skConnect, resp);

    if ( gCommandList[msg].commandType )
        Skylo_Printf(SKYLO_INFO_E, _(">><< Response from Modem ")"(errno :%"PRId32")[%s]\n",
                                    ret, skConnect->m->atRespBuf);

    if ( msg == MODEM_CMD_AT_CLI_E )
        Skylo_Printf(SKYLO_INFO_E, "%s", skConnect->m->atRespBuf);

reset:
    gActiveCallback = NULL;

    while ( SK_OK != skConnect->pep.os.ipc.fpMutexUnlock(&(skConnect->m->transactionMutex)) ) {
        Skylo_Printf(SKYLO_ERR_E, "Transaction mutex release failed\n");
        skMSecSleep(2000);
    }

ping:
#ifdef DEBUG_SKCONNECT
    Skylo_Printf(SKYLO_DEBUG_E, "fire command exit[%s]\n",cmd);
#endif
    return ret;
}

skBool_t altairIsSimReady(skConnect_t *skConnect)
{

    uint8_t retryCnt = 0;
    skBool_t status = false;

    while ( retryCnt <= 10 ) {

        /* Report thread health, since this will be3 called from smodem_tx_thread, Ping !! */
        if ( altairFireCommand(skConnect, gCommandList[MODEM_CMD_IS_SIM_READY_E].command,
                MODEM_CMD_IS_SIM_READY_E, NULL) != SK_MODEM_OK ) {

            retryCnt++;
            Skylo_Printf(SKYLO_ERR_E, "SIM ready check failed\n");
            Skylo_Printf(SKYLO_NOTICE_E, _("SIM is not ready or available. ")"retry in %d ms\n", BACKOFF_PERIOD);
            skMSecSleep(BACKOFF_PERIOD);
            status = false;
        } else {

            Skylo_Printf(SKYLO_INFO_E, _("Checking SIM is ready...OK\n"));
            status = true;
            break;
        }
    }

    return status;
}

skStatus_t altairActivateSocket(skConnect_t *skConnect, int16_t id)
{
    if ( !skConnect->m->attachedState )
        return SK_MODEM_ERR_NOT_ATTACHED;

    if ( skConnect->m->firstSocketId == -1 )
        return SK_MODEM_ERR_SOCK_NOT_AVAIL;
    
    char cmd[50] = {0};

    snprintf(cmd, sizeof(cmd), gCommandList[MODEM_CMD_ACTIVATE_SOCK_E].command,(int)skConnect->m->firstSocketId);

    Skylo_Printf(SKYLO_DEBUG_E, _("Activating socket..")" id(%d)\n",skConnect->m->firstSocketId);

    return altairFireCommand(skConnect, cmd,
                MODEM_CMD_ACTIVATE_SOCK_E, NULL) == SK_MODEM_OK ? SK_MODEM_OK : SK_MODEM_ERR_ACTIVATE_SOCK;
}

skStatus_t altairAllocateSocket(skConnect_t *skConnect,const char* ip, uint16_t port,int16_t *id, uint8_t sockType)
{
    if ( !skConnect->m->attachedState )
        return SK_MODEM_ERR_NOT_ATTACHED;

    char cmd[100] = {0};

    if ( sockType == MODEM_SOCK_UDP_E ) {

        snprintf(cmd, sizeof(cmd), gCommandList[MODEM_CMD_CREATE_SOCK_E].command,"UDP",ip,port);
    } else {
        snprintf(cmd, sizeof(cmd), gCommandList[MODEM_CMD_CREATE_SOCK_E].command,"TCP",ip,port);
    }

    Skylo_Printf(SKYLO_DEBUG_E, _("Allocating socket...\n"));

    return altairFireCommand(skConnect, cmd,
                MODEM_CMD_CREATE_SOCK_E, (void*)id) == SK_MODEM_OK ? SK_MODEM_OK : SK_MODEM_ERR_CREATE_SOCK;
}

skStatus_t altairSocketStatus(skConnect_t *skConnect, int16_t id)
{
    if ( !skConnect->m->attachedState )
        return SK_MODEM_ERR_NOT_ATTACHED;

    char cmd[50] = {0};

    snprintf(cmd, sizeof(cmd), gCommandList[MODEM_CMD_CREATE_SOCK_E].command,id);

    Skylo_Printf(SKYLO_DEBUG_E, _("Checking socket info...\n"));

    return altairFireCommand(skConnect, cmd, MODEM_CMD_SOCK_INFO_E, NULL);
}

skStatus_t altairConfigurePgwReporting(skConnect_t *skConnect)
{
    return altairFireCommand(skConnect, gCommandList[MODEM_CMD_SET_CGEREP_E].command, MODEM_CMD_SET_CGEREP_E, NULL);
}

skStatus_t altairConfigureNotifyevReporting(skConnect_t *skConnect, modemNotifyEVTypes_e type)
{
    skStatus_t stat = SK_MODEM_OK;
    char cmd[50] = {0};

    if ( type == MODEM_NOTIFYEV_RRCSTATE_E ) {
        snprintf(cmd, sizeof(cmd), gCommandList[MODEM_CMD_SET_NOTIFYEV_E].command,"RRCSTATE");
    } else if ( type == MODEM_NOTIFYEV_SIB31_E) {
        snprintf(cmd, sizeof(cmd), gCommandList[MODEM_CMD_SET_NOTIFYEV_E].command,"SIB31");
    } else {
        Skylo_Printf(SKYLO_ERR_E, _("NotifyEV no valid type ")"type %"PRId32"\n", type);
        return SK_ERR_NOT_SUPPORTED;
    }

    stat = altairFireCommand(skConnect, cmd, MODEM_CMD_SET_NOTIFYEV_E, NULL);
    if ( stat != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, _("Cannot set MC reporting. ")"errno %"PRId32"\n", stat);
        return stat;
    }

    return stat;
}

skStatus_t altairReadRxBuffer(skConnect_t *skConnect, char * hexBuf, uint32_t length, int16_t id)
{
    char cmd[50] = {0};

    snprintf(cmd, sizeof(cmd), gCommandList[MODEM_CMD_READ_RX_BUFFER_E].command, id, length);

    return altairFireCommand(skConnect, cmd, MODEM_CMD_READ_RX_BUFFER_E, hexBuf);
}

skStatus_t altairSendRxBuffer(skConnect_t *skConnect, char *data, uint32_t length, int16_t id)
{
    char * finalCmd = NULL;
    skStatus_t ret = SK_MODEM_ERR_NO_MEM;

    if ( !data )
        return SK_MODEM_ERROR;
        
    finalCmd = (char *) malloc(AT_CMD_BUFFER_SIZE);
    if ( finalCmd != NULL ) {

        memset(finalCmd, 0, AT_CMD_BUFFER_SIZE);
        snprintf(finalCmd, AT_CMD_BUFFER_SIZE, gCommandList[MODEM_CMD_SEND_IP_E].command, id,
                    length, data);

        ret = altairFireCommand(skConnect, finalCmd, MODEM_CMD_SEND_IP_E, NULL);
        free(finalCmd);
    }


    skMSecSleep(1000);

    return ret;
}

skStatus_t altairReadNipRxBuffer(skConnect_t *skConnect, char * hexBuf, int hexBufLen)
{
    if ( !hexBuf )
        return SK_MODEM_ERROR;

    memcpy((uint8_t *)hexBuf,skConnect->m->rxDataBuf,strlen(skConnect->m->rxDataBuf));

    Skylo_Printf(SKYLO_DEBUG_E, _("NIP Rx Hex Buffer ")"NIPRHB-(%zu)[%s]\n", strlen(hexBuf), hexBuf);

    char * termQuotePtr = NULL;
    char * startQuotePtr = NULL;

    if ( (startQuotePtr = strstr(hexBuf, "\"")) == NULL ) {

        Skylo_Printf(SKYLO_ERR_E, "Invalid hex string"_(" - no start quote")"\n");
        return SK_MODEM_ERROR;
    } else {

        /* Account for first double quote */
        strncpy(hexBuf, startQuotePtr+1, strlen(startQuotePtr+1));
        if ( (termQuotePtr = strstr(hexBuf, "\"")) == NULL ) {

            Skylo_Printf(SKYLO_ERR_E, "Invalid hex string"_(" - no terminating quote")"\n");
            return SK_MODEM_ERROR;
        } else {

            /* Add NULL terminator */
            *termQuotePtr = '\0';
        }

        if ( strlen(hexBuf) == 0 ) {

            Skylo_Printf(SKYLO_ERR_E, _("NIP RX Hex ")"Buffer is empty!!!\n");
            return SK_MODEM_ERROR;
        }
    }

    if ( strlen(hexBuf) % 2 != 0 ) {

        Skylo_Printf(SKYLO_ERR_E, _("Rx Hex string ")"len is odd [%s]\n", hexBuf);
        return SK_MODEM_ERROR;
    }

    Skylo_Printf(SKYLO_DEBUG_E, _("Rx Data Buffer = ")"(%zu)[%s]\n", strlen(hexBuf), hexBuf);

    return SK_MODEM_OK;
}

char hexBuf[AT_RESP_BUFFER_SIZE+1];

skStatus_t altairSubscribe(skConnect_t *skConnect, uint8_t * buf, uint16_t len)
{
    uint32_t signalSet = 0;

    /* Wait for data to be available in rx buffer */
    /* Returning length zero in case read times out */
    if ( SK_OK != skConnect->pep.os.ipc.fpSignalWaitTimed(&skConnect->m->eventSignal, THREAD_SIGNAL_RX_SKMSG,
        SK_SIG_ATTRIBUTE_WAIT_FOR_ANY | SK_SIG_ATTRIBUTE_CLEAR_MASK, &signalSet, SK_TIME_NO_WAIT) )
        return 0;

    memset(hexBuf, 0, sizeof(hexBuf));

    /* NIP based */
    int32_t ret = altairReadNipRxBuffer(skConnect, hexBuf, sizeof(hexBuf));

    if ( ret != SK_MODEM_OK )
        return ret;

    /* Decode into byte array */
	if ( skHexString2Binary(hexBuf, strlen(hexBuf) >> 1, buf) != 0 )
        return SK_MODEM_ERROR;

    /* Divide by 2 to get number of bytes */
    return strlen(hexBuf) >> 1;
}

skStatus_t altairConfigureLteInacttmr(skConnect_t *skConnect)
{
    return altairFireCommand(skConnect, gCommandList[MODEM_CMD_SET_CFG_LTE_INACTTMR_E].command,
                                MODEM_CMD_SET_CFG_LTE_INACTTMR_E, NULL);
}

skStatus_t altairGetLteTimingAdvance(skConnect_t *skConnect, int16_t * TA)
{
    return altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_LTE_TIMING_ADVANCE_E].command,
                                MODEM_CMD_GET_LTE_TIMING_ADVANCE_E, (void*)TA);
}

skStatus_t altairGetImageSelected(skConnect_t *skConnect, modemImage_e * image)
{
    return altairFireCommand(skConnect, gCommandList[MODEM_CMD_IMG_CHECK_E].command,
                                MODEM_CMD_IMG_CHECK_E, (void*)image);
}

skStatus_t altairRestartModem(skConnect_t *skConnect)
{
    return altairFireCommand(skConnect, gCommandList[MODEM_CMD_RESTART_MODEM_E].command,
                                MODEM_CMD_RESTART_MODEM_E,NULL);
}

skStatus_t altairModemReset(skConnect_t *skConnect)
{
    skConnect->m->hsuConnectedState = false;
    skConnect->m->resetTrig = true;
    skConnect->m->isInit = false;
    skConnect->m->atReadyState = false;
    skConnect->pep.hal.fpModemHWDeInit();

    /* increment counter */
    skConnect->m->modemResetCnter++;
    if ( skConnect->m->modemResetCnter >= 3 ) {

        Skylo_Printf(SKYLO_INFO_E, _("Modem reset ") "threshold "_("reached: ")"%d"_(" Resetting hub")"\n",
                                    skConnect->m->modemResetCnter);
        skSetSystemResetReason(SK_RESET_REASON_MODEM_EXCEED_RESET_THRES);
        skConnect->m->modemResetCnter = 0;
    }

    skMSecSleep(1000);

    if ( halModemInit(skConnect) != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Cannot re-init modem\n");
        return SK_MODEM_ERR_RESET;
    }

    halModemEnterAtMode(skConnect);
    skConnect->m->resetTrig = false;

    return SK_MODEM_OK;
}

skStatus_t altairModemPartialReset(skConnect_t *skConnect)
{
    skConnect->m->hsuConnectedState = false;
    skConnect->m->resetTrig          = true;
    skConnect->m->isInit             = false;
    skConnect->m->partReset          = true;

    skMSecSleep(1000);

    if ( halModemInit(skConnect) != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Cannot re-init modem\n");
        return SK_MODEM_ERR_RESET;
    }

    halModemEnterAtMode(skConnect);
    skConnect->m->resetTrig = false;
    skConnect->m->partReset = false;

    return SK_MODEM_OK;
}

skStatus_t altairCofigureDataDeliveryNotifications(skConnect_t *skConnect)
{
    skStatus_t stat = SK_MODEM_OK;

    stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_SET_NIP_CNTRL_PLANE_URC_E].command,
                                MODEM_CMD_SET_NIP_CNTRL_PLANE_URC_E, NULL);
    if ( stat != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, _("Cannot set PDP NIP URC reporting ")"errno %"PRId32"\n", stat);
        return stat;
    }

    return stat;
}

skStatus_t altairModemFunctionalReset(skConnect_t *skConnect)
{
    skStatus_t ret = SK_ERROR;
    if ( (ret = altairFireCommand(skConnect, gCommandList[MODEM_CMD_FUN_OFF_E].command,
                                    MODEM_CMD_FUN_OFF_E, NULL)) != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, _("Cannot turn OFF modem ")"errno %"PRId32"\n",ret);
        return SK_MODEM_ERR_FUN_OFF;
    }

    skMSecSleep(1000); /* Sleep for 1 second */

    if ( altairFireCommand(skConnect, gCommandList[MODEM_CMD_FUN_ON_E].command, MODEM_CMD_FUN_ON_E, NULL) != SK_MODEM_OK) {

        Skylo_Printf(SKYLO_ERR_E, _("Cannot turn On modem ")"errno %"PRId32"\n",ret);
        return SK_MODEM_ERR_FUN_ON;
    }

    skMSecSleep(1000);

    skConnect->m->rrcState = false;
    skConnect->m->attachedState = false;

    return SK_MODEM_OK;
}


uint32_t halModemGetEarfcn(skConnect_t *skConnect)
{
    skStatus_t stat = SK_MODEM_ERROR;

    if ( (stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_EARFCN_E].command,
                                        MODEM_CMD_GET_EARFCN_E, (void *)&skConnect->m->earfcn)) != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, _("Cannot retrieve ")"EARFCN, errorno=%"PRId32"\n", stat);

    return skConnect->m->earfcn;
}

skStatus_t halModemSetUnsolicitedStatus(skConnect_t *skConnect)
{
    char finalCmd[32] = {0};
    int mode = 0;
    int clearErr = 0;
    int repType = 0;

    /* 0 - Disabled (Default), 1 - Enable solicited modem status */
    mode = 1;

    /* 0 - Keep last stored error (Default), 1 - Clear last stored Error */
    clearErr = 1;

    /* 0 - regular, 1 - Extended with failure cell identity */
    repType = 0;

    snprintf(finalCmd, sizeof(finalCmd), gCommandList[MODEM_CMD_SYSTEM_FAILURE_E].command, mode, clearErr, repType);

    return altairFireCommand(skConnect, finalCmd, MODEM_CMD_SYSTEM_FAILURE_E, NULL);
}

skStatus_t halModemChangeLTEImage(skConnect_t *skConnect, uint16_t image)
{
    char finalCmd[32] = {0};

    snprintf(finalCmd, sizeof(finalCmd), gCommandList[MODEM_CMD_IMG_SEL_E].command, image);

    return altairFireCommand(skConnect, finalCmd, MODEM_CMD_IMG_SEL_E, NULL);
}


skStatus_t halModemSetRATMode(skConnect_t *skConnect, modemNbiotImage_e image, uint8_t source)
{
    char finalCmd[32] = {0};

    if ( image == NBIOT_IMAGE_TN_E ) {
        snprintf(finalCmd, sizeof(finalCmd), gCommandList[MODEM_CMD_SET_RAT_MODE_E].command, "NBIOT", source);
    } else {        //default mode is NTN
        snprintf(finalCmd, sizeof(finalCmd), gCommandList[MODEM_CMD_SET_RAT_MODE_E].command, "NBNTN", source);
    }

    Skylo_Printf(SKYLO_NOTICE_E, _("Firing command")"CMD : [%s]\n", finalCmd);

    return altairFireCommand(skConnect, finalCmd, MODEM_CMD_SET_RAT_MODE_E, NULL);   
}

skStatus_t halModemGetRATMode(skConnect_t *skConnect)
{
    skStatus_t stat = SK_MODEM_ERROR;

    if ( (stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_RAT_MODE_E].command,
                                        MODEM_CMD_GET_RAT_MODE_E, (void *)&skConnect->m->ratActImg)) != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, _("Cannot retrieve ")"RATACT, errorno=%"PRId32"\n", stat);

    return stat;
}

skStatus_t halModemSetPdpAndAPN(skConnect_t *skConnect, const char * pdpType, const char *APN)
{   
    skStatus_t status = SK_ERROR;

    char finalCmd[51] = {0};

    snprintf(finalCmd, sizeof(finalCmd), gCommandList[MODEM_CMD_SET_PDP_APN_E].command, pdpType, APN);


    if ( (status = altairFireCommand(skConnect, finalCmd, MODEM_CMD_SET_PDP_APN_E, NULL))
                            != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, "Configure PDP and APN...FAIL\n");
    else
        Skylo_Printf(SKYLO_NOTICE_E, "Configure PDP and APN...OK\n");
    
    // Non-Ip / IP
    if ( strchr(pdpType,'-') == NULL ) {    //IP pdpType
        skConnect->m->pdpType = MODEM_PDP_IP_E;
    } else {
        skConnect->m->pdpType = MODEM_PDP_NON_IP_E;     //we assume if its not IP its NON-IP, we dont want it to be unknown
    }

    return status;
}

skStatus_t halModemSetBand(skConnect_t *skConnect)
{   
    skStatus_t status = SK_ERROR;

    char finalCmd[32] = {0};

    if ( skConnect->m->modemConf.band ) {

        snprintf(finalCmd, sizeof(finalCmd), gCommandList[MODEM_CMD_SET_CFG_BAND_E].command, skConnect->m->modemConf.band);


        if ( (status = altairFireCommand(skConnect, finalCmd, MODEM_CMD_SET_CFG_BAND_E, NULL))
                                != SK_MODEM_OK )
            Skylo_Printf(SKYLO_ERR_E, "Setting Band...FAIL\n");
        else
            Skylo_Printf(SKYLO_NOTICE_E, "Setting Band...OK\n");
    } else {
        Skylo_Printf(SKYLO_NOTICE_E, "Invalid Band(%d)...OK\n", skConnect->m->modemConf.band);
    }

    return status;
}

skStatus_t halModemSetEarfcn(skConnect_t *skConnect)
{   
    skStatus_t status = SK_ERROR;

    char finalCmd[32] = {0};

    if ( skConnect->m->modemConf.earfcnStart || skConnect->m->modemConf.earfcnEnd ) {
        
        /* if any zero we will use other as both */
        if ( skConnect->m->modemConf.earfcnStart && skConnect->m->modemConf.earfcnEnd ) {

            snprintf(finalCmd, sizeof(finalCmd), gCommandList[MODEM_CMD_SET_CFG_EARFCN_E].command, 
                skConnect->m->modemConf.earfcnStart, skConnect->m->modemConf.earfcnEnd);
        } else if ( skConnect->m->modemConf.earfcnStart ) {
         
            snprintf(finalCmd, sizeof(finalCmd), gCommandList[MODEM_CMD_SET_CFG_EARFCN_E].command, 
                skConnect->m->modemConf.earfcnStart, skConnect->m->modemConf.earfcnStart);
        } else {
            
            snprintf(finalCmd, sizeof(finalCmd), gCommandList[MODEM_CMD_SET_CFG_EARFCN_E].command, 
                skConnect->m->modemConf.earfcnEnd, skConnect->m->modemConf.earfcnEnd);
        }      

        if ( (status = altairFireCommand(skConnect, finalCmd, MODEM_CMD_SET_CFG_EARFCN_E, NULL))
                                != SK_MODEM_OK )
            Skylo_Printf(SKYLO_ERR_E, "Configure earfcn range...FAIL\n");
        else
            Skylo_Printf(SKYLO_NOTICE_E, "Configure earfcn range...OK\n");

    } else {
        Skylo_Printf(SKYLO_ERR_E, "Invalid earfcn(%d)(%d)...OK\n", 
            skConnect->m->modemConf.earfcnStart,
            skConnect->m->modemConf.earfcnEnd);
    }

    return status;
}

skStatus_t halModemSetUEPosition(skConnect_t *skConnect, float lat, float lon, float alt, uint32_t expiry)
{   
    skStatus_t status = SK_ERROR;

    char finalCmd[101] = {0};

    snprintf(finalCmd, sizeof(finalCmd), gCommandList[MODEM_CMD_SET_UE_POS_E].command, lat, lon, alt, expiry);


    if ( (status = altairFireCommand(skConnect, finalCmd, MODEM_CMD_SET_UE_POS_E, NULL))
                            != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, "Setting UE position...FAIL\n");
    else
        Skylo_Printf(SKYLO_NOTICE_E, "Setting UE position...OK\n");

    return status;
}

skStatus_t halModemGetMultiRATEnableStatus(skConnect_t *skConnect, uint8_t *status)
{
    skStatus_t stat = SK_MODEM_ERROR;

    if ( (stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_CHECK_MULTI_RAT_ENABLE_STATUS_E].command,
                                        MODEM_CMD_CHECK_MULTI_RAT_ENABLE_STATUS_E, (void *)status)) != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, _("Cannot retrieve ")"Multi RAT enable status, errorno=%"PRId32"\n", stat);

    return stat;
}

skStatus_t halModemSetMultiRATEnableStatus(skConnect_t *skConnect, uint8_t status)
{   
    skStatus_t ret = SK_ERROR;

    char finalCmd[51] = {0};

    if ( status ) {

        snprintf(finalCmd, sizeof(finalCmd), gCommandList[MODEM_CMD_CHANGE_MULTI_RAT_ENABLE_STATUS_E].command, 
            "TRUE");
    } else {
        
        snprintf(finalCmd, sizeof(finalCmd), gCommandList[MODEM_CMD_CHANGE_MULTI_RAT_ENABLE_STATUS_E].command, 
            "FALSE");
    }     

    if ( (ret = altairFireCommand(skConnect, finalCmd, MODEM_CMD_CHANGE_MULTI_RAT_ENABLE_STATUS_E, NULL))
                            != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, "Configure earfcn range...FAIL\n");
    else
        Skylo_Printf(SKYLO_NOTICE_E, "Configure earfcn range...OK\n");


    return ret;
}

skStatus_t halModemApplyTempFix(skConnect_t *skConnect)
{
    skStatus_t stat = SK_MODEM_ERROR;

    if ( (stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_TEMP_FIX_E].command,
                                        MODEM_CMD_TEMP_FIX_E, (void *)&skConnect->m->ratActImg)) != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, _("Cannot Set ")"Temp Fix, errorno=%"PRId32"\n", stat);

    return stat;
}

static void scheduledTaskExec(skConnect_t *skConnect, skTime_t secondsCount)
{
    skStatus_t stat = SK_MODEM_ERROR;

    if ( halModemIsAttached(skConnect) == PDN_ATTACH_E )
        skConnect->m->swtchContext.timeofLastConn = skConnect->pep.os.time.fpGetUnixEpochInMSec();

    /* if modem is not connected for more than MAX disconnection time allowed, switch to other NW 
        if time based switching do it anyway */
    if ( ((( skConnect->pep.os.time.fpGetUnixEpochInMSec() - skConnect->m->swtchContext.timeofLastConn ) >= 
           MAX_DISCON_TIME_ALWD) || ( TIME_BASED_SWITCHING && 
           ( ( skConnect->pep.os.time.fpGetUnixEpochInMSec() - skConnect->m->swtchContext.modemInitTimer ) >= 
           TIMER_BASED_SWTCH_TIME) )) && !skConnect->m->swtchContext.switchInitiated ) {
        
        /* select image accordingly */
        if ( skConnect->m->ratActImg == NBIOT_IMAGE_NTN_E ) {

            stat = halModemSetRATMode(skConnect, NBIOT_IMAGE_TN_E, 1);

            if ( stat != SK_MODEM_OK ) {

                Skylo_Printf(SKYLO_ERR_E, _("Modem change RAT mode[NBIOT] ...FAIL\n"));
            } else {

                Skylo_Printf(SKYLO_NOTICE_E, _("Modem change RAT mode[NBIOT] ...OK\n"));
            }
        } else {

            stat = halModemSetRATMode(skConnect, NBIOT_IMAGE_NTN_E, 1);

            if ( stat != SK_MODEM_OK ) {

                Skylo_Printf(SKYLO_ERR_E, _("Modem change RAT mode[NTN] ...FAIL\n"));
            } else {

                Skylo_Printf(SKYLO_NOTICE_E, _("Modem change RAT mode[NTN] ...OK\n"));
            }
        }

        if ( stat == SK_MODEM_OK ) {

            if( ( skConnect->pep.os.time.fpGetUnixEpochInMSec() - skConnect->m->swtchContext.timeofLastConn ) >= 
            MAX_DISCON_TIME_ALWD ) {
                Skylo_Printf(SKYLO_INFO_E,"STE Switch due to no connection for last (%d) Minutes!\n",(MAX_DISCON_TIME_ALWD/(1000*60)));
            } else {
                Skylo_Printf(SKYLO_INFO_E,"STE Switch due to Timer (%d) Minutes!\n",(TIMER_BASED_SWTCH_TIME/(1000*60)));
            }
            
            skConnect->m->swtchContext.switchInitiated = 1;
        }

    }

    /* We don't want to stay longer in NTN if TN is available go for TN check */
    if ( skConnect->m->ratActImg == NBIOT_IMAGE_NTN_E && !TIME_BASED_SWITCHING && !skConnect->m->swtchContext.switchInitiated ) {

        if ( (skConnect->pep.os.time.fpGetUnixEpochInMSec() - skConnect->m->swtchContext.modemInitTimer) >= 
           NT_AVAIL_CHECK_TIME ) {

            stat = halModemSetRATMode(skConnect, NBIOT_IMAGE_TN_E, 1);

            if ( stat != SK_MODEM_OK ) {

                Skylo_Printf(SKYLO_ERR_E, _("Modem change RAT mode[NBIOT] ...FAIL\n"));
            } else {

                Skylo_Printf(SKYLO_NOTICE_E, _("Modem change RAT mode[NBIOT] ...OK\n"));
                skConnect->m->swtchContext.switchInitiated = 1;
            }
        }
    }

    static uint8_t lastFixStatus = 0;
    //set UE position at 10 sec or if failure 2 sec retry
    if ( (secondsCount % 10 == 0 && skConnect->m->ratActImg == NBIOT_IMAGE_NTN_E) || (lastFixStatus && secondsCount % 2 == 0) ) {

        Skylo_Printf(SKYLO_INFO_E,"Setting UE position lat(%f), lon(%f) and Alt(%f)!\n",
            skConnect->options->ueLat,
            skConnect->options->ueLong,
            skConnect->options->ueAlt);
        lastFixStatus = halModemSetUEPosition(skConnect,skConnect->options->ueLat, skConnect->options->ueLong, skConnect->options->ueAlt,0);
    }

    if ( secondsCount % 10 == 0 ) {
#ifdef DEBUG_SKCONNECT
        Skylo_Printf(SKYLO_INFO_E,"STE timeofLastConn(%"PRIu32"), modemInitTimer(%"PRIu32") and Epoch(%"PRIu32")!\n",
            skConnect->m->swtchContext.timeofLastConn,
            skConnect->m->swtchContext.modemInitTimer,
            skConnect->pep.os.time.fpGetUnixEpochInMSec());
#endif

        // Restart UART
        if ( skConnect->m->badATCount > BAD_AT_COUNT ) {
            
            Skylo_Printf(SKYLO_ERR_E, _("UART Comm bad, re initializing\n"));

            if ( (skConnect->pep.os.ipc.fpMutexLockTimed(&(skConnect->m->transactionMutex), 500 )) != SK_OK ) {

                Skylo_Printf(SKYLO_ERR_E, "Transaction mutex busy\n");
            } else {

                skConnect->pep.hal.fpModemHWDeInit();
                skConnect->pep.hal.fpModemHWInit();
                skConnect->m->badATCount  = 0;

                if ( (SK_OK != skConnect->pep.os.ipc.fpMutexUnlock(&(skConnect->m->transactionMutex)) )) {
                    Skylo_Printf(SKYLO_ERR_E, "Couldn't release mutex\n");
                }
            }
            
        }
    }
}

skStatus_t sKonnectOpenHBSocket(skConnect_t *skConnect)
{   
    skStatus_t ret = SK_MODEM_ERROR;
    uint8_t rtryCount = 5;

    if ( halModemIsAttached(skConnect) != PDN_ATTACH_E )
        ret = SK_ERR_EAGAIN;

    /* only one socket at the moment */
    if ( skConnect->m->firstSocketId != -1 )
        ret = SK_ERR_NO_RESOURCE;

    if ( skConnect->m->pdpType == MODEM_PDP_IP_E ) {

        do {
            do {

                if ( SK_OK != (ret = altairAllocateSocket(skConnect, skConnect->options->ip,skConnect->options->port,
                                                    &skConnect->m->firstSocketId,
                                                    skConnect->options->socketType)) ) {
                    
                    Skylo_Printf(SKYLO_DEBUG_E, "Socket Allocate error (%d)\n",ret);
                    skMSecSleep(10000);
                    rtryCount--;
                }

            } while ( ret != SK_MODEM_OK && rtryCount );
            rtryCount = 5;

            if ( ret != SK_MODEM_OK  )
                break;

            Skylo_Printf(SKYLO_DEBUG_E, "Socket opened with Id (%d)\n",skConnect->m->firstSocketId);

            skMSecSleep(100);
            rtryCount = 5;
            
            do {

                if ( SK_OK != (ret = altairActivateSocket(skConnect,skConnect->m->firstSocketId)) ) {
                    Skylo_Printf(SKYLO_ERR_E, "Error "_("while activating")" socket\n");
                    rtryCount--;
                    skMSecSleep(5000);
                }
            } while ( ret != SK_MODEM_OK && rtryCount );
        
        } while ( 0 );

    } else {
        ret = SK_ERR_NOT_SUPPORTED;
    }
    
    return ret;
}