/**
 * @file        modemHal.c
 * @brief       Modem HAL API
 * @author      Barry Winata (barry@skylo.tech)
 *              Dharmesh Vasoya (dharmesh@skylo.tech)
 * @copyright   Copyright (c) 2023 SkyloTechnologies, Inc.
 */

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <skUtils.h>
#include <modemHal.h>
#include <modemCore.h>
#include <modemHealth.h>
#include <appThread.h>

#include <skOSPlugin.h>

#include <skLog.h>
#include <skCore.h>
#include <skModemStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Macros
 *-----------------------------------------------------------------------*/

#define Skylo_Printf(...) _sk_log(modemHal, __VA_ARGS__)

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

extern atCommandResponse_t gCommandList[];

/*-------------------------------------------------------------------------
 * Constants
 *-----------------------------------------------------------------------*/

/* ms */
#define MODEM_TX_BACKOFF_DELAY  (1000)

/**
 * @brief
 * Function to start modem rx Thread
 *
 * @details
 * Function to start modem rx Thread
 *
 * @param[in]  arg skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative;
 */
static int startRxThread(skConnect_t *skConnect)
{
    return skyloThreadCreate(skConnect, APP_THREAD_MODEM_RX_INDEX_NO, altairModemRxThread, skConnect);
}

/**
 * @brief
 * Function to start modem fsm Thread
 *
 * @details
 * Function to start modem fsm Thread
 *
 * @param[in]  arg skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative;
 */
static int modemStartFsmThread(skConnect_t *skConnect)
{
    return skyloThreadCreate(skConnect, APP_THREAD_MODEM_FSM_INDEX_NO, altairModemFsmThread, skConnect);
}

/**
 * @brief
 * Function to start modem health monitor Thread
 *
 * @details
 * Function to start modem health monitor Thread
 *
 * @param[in]  arg skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative;
 */
static int modemStartHealthMonitorThread(skConnect_t *skConnect)
{
    return skyloThreadCreate(skConnect, APP_THREAD_MODEM_HEALTH_INDEX_NO, altairModemHealthMonitorThread, skConnect);
}

/*-------------------------------------------------------------------------
 * Modem Public Functions
 *-----------------------------------------------------------------------*/

skStatus_t halModemNew(skConnect_t *skConnect)
{
    int result = 0;

    /* Create new modem object and init. mutex */
    skConnect->m = (skModem_t *)malloc(sizeof(skModem_t));

    if ( !skConnect->m ) {

        Skylo_Printf(SKYLO_ERR_E, MODEM_MALLOC_FAIL_ERR_MSG);
        return SK_MODEM_ERR_NO_MEM;
    }

    skConnect->m->hsuConnectedState = false;
    skConnect->m->atReadyState      = false;
    skConnect->m->attachedState     = false;
    skConnect->m->rrcState          = UE_RRC_UNKNOWN_E;
    skConnect->m->atRespBuf         = (char *)malloc(sizeof(char) * AT_RESP_BUFFER_SIZE);
    if ( !skConnect->m->atRespBuf ) {

        Skylo_Printf(SKYLO_ERR_E, MODEM_MALLOC_FAIL_ERR_MSG);
        return SK_MODEM_ERR_NO_MEM;
    }

    skConnect->m->atCmdBuf          = (char *)malloc(sizeof(char) * AT_CMD_BUFFER_SIZE);
    if ( !skConnect->m->atCmdBuf ) {

        Skylo_Printf(SKYLO_ERR_E, MODEM_MALLOC_FAIL_ERR_MSG);
        return SK_MODEM_ERR_NO_MEM;
    }

    skConnect->m->rxDataBuf         = (char *)malloc(sizeof(char) * RX_DATA_BUFFER_SIZE);
    if ( !skConnect->m->rxDataBuf ) {

        Skylo_Printf(SKYLO_ERR_E, MODEM_MALLOC_FAIL_ERR_MSG);
        return SK_MODEM_ERR_NO_MEM;
    }

    skConnect->m->modemRtcValid                 = false;
    skConnect->m->signal.rssi                   = -128;
    skConnect->m->signal.rsrp                   = -140;
    skConnect->m->signal.rsrq                   = -128;
    skConnect->m->signal.sinr                   = -128;
    skConnect->m->resetTrig                     = false;
    skConnect->m->modemState                    = MODEM_STATE_ACQUIRING_GNSS_E;
    skConnect->m->gpsNMEAHandler_fp             = NULL;
    skConnect->m->gpsTimeAvail                  = false;
    skConnect->m->gpsEphupdStat                 = false;
    /* RK_3.2 ONLY */
    skConnect->m->gpsFix                        = false;
    /* RK_3.2 ONLY END */
    skConnect->m->isInit                        = false;
    skConnect->m->partReset                     = false;
    skConnect->m->isSimDataAvailable            = false;
    skConnect->m->isRma                         = false;
    skConnect->m->badATCount                    = 0;
    skConnect->m->modemResetCnter               = 0;
    skConnect->m->inInitApi                     = 1;
    skConnect->m->isModemCrash                  = 0;
    skConnect->m->ratActImg                     = NBIOT_IMAGE_NONE_E;
    skConnect->m->firstSocketId                 = -1;
    skConnect->m->pdpType                       = MODEM_PDP_UNKNOWN_E;
    skConnect->m->bootevFlag                    = 0;
    skConnect->m->swtchContext.timeofLastConn   = 0;
    skConnect->m->swtchContext.swtchNW          = NBIOT_IMAGE_NONE_E;
    skConnect->m->swtchContext.switchInitiated  = 0;
    skConnect->m->swtchContext.modemInitTimer   = 0;

    /* Parameters for RK3.2 full image upgrade to decide boot mode and flash partition */
    skConnect->m->bootParam.baudrate = 115200;
    skConnect->m->modemResponseTimeoutMsec = 5000;

    halModemEnterAtMode(skConnect);
    halModemEnterOnlineMode(skConnect);

    /* TESTING >>>>>>>>>>>>>>> */

    /* Retrieve modem specific configuration */
    skConnect->m->modemConf.band = skConnect->options->rfBand; /* 144; */
    skConnect->m->modemConf.earfcnStart = skConnect->options->rfEarfcnStart;
    skConnect->m->modemConf.earfcnEnd = skConnect->options->rfEarfcnEnd;
    Skylo_Printf(SKYLO_DEBUG_E, "BAND = %d\n", skConnect->m->modemConf.band);
    Skylo_Printf(SKYLO_DEBUG_E, "earfcn start= %"PRIu32", end= %"PRIu32"\n", 
            skConnect->m->modemConf.earfcnStart,
            skConnect->m->modemConf.earfcnEnd);

    Skylo_Printf(SKYLO_DEBUG_E, "Time Based switching Enabled?    (%d)\n",TIME_BASED_SWITCHING);
    Skylo_Printf(SKYLO_DEBUG_E, "Time Based switching time        (%d)mins\n",(TIMER_BASED_SWTCH_TIME/(60*1000)));
    Skylo_Printf(SKYLO_DEBUG_E, "No Connetion Based switching time(%d)mins\n",(MAX_DISCON_TIME_ALWD/(60*1000)));
    Skylo_Printf(SKYLO_DEBUG_E, "WHen in NTN check NT avail. time (%d)mins\n",(NT_AVAIL_CHECK_TIME/(60*1000)));

    /* Zero buffers */
    memset(skConnect->m->atRespBuf, 0x00, AT_RESP_BUFFER_SIZE);
    memset(skConnect->m->atCmdBuf, 0x00, AT_CMD_BUFFER_SIZE);
    memset(skConnect->m->rxDataBuf, 0x00, RX_DATA_BUFFER_SIZE);

    /* Init. thread signal(s) */
    result = skConnect->pep.os.ipc.fpSignalInit(&skConnect->m->eventSignal);
    if ( result != SK_OK ) {

        Skylo_Printf(SKYLO_CRIT_E, "Cannot create signal. errorno %d\n", result);
        return result;
    }

    /* Init. mutex(es) */
    result = skConnect->pep.os.ipc.fpMutexInit(&(skConnect->m->transactionMutex));
    if ( result != SK_OK ) {

        Skylo_Printf(SKYLO_CRIT_E, "Cannot create mutex for transaction. errorno %d\n", result);
        return result;
    }

    result = skConnect->pep.os.ipc.fpMutexInit(&(skConnect->m->stateMutex));
    if ( result != SK_OK ) {

        Skylo_Printf(SKYLO_CRIT_E, "Cannot create mutex for modem state. errorno %d\n", result);
        return result;
    }

    return result;
}

skStatus_t halModemInit(skConnect_t *skConnect)
{
    int stat = 0;
    uint8_t retryCnter = 0;

    if ( !skConnect )
        return SK_ERR_INVALID;

    if ( skConnect->m->hsuConnectedState )
        return SK_MODEM_ERR_ALREADY_INIT;

    skConnect->pep.hal.fpModemHWInit();

    /* It must be above altair reset only to detect Modem Reset by HUB */
    skConnect->m->inInitApi = 1;
    skConnect->m->isModemCrash = 0;

    /* Context Rest in case Re-init to avoid issues */
    skConnect->m->hsuConnectedState = false;
    skConnect->m->atReadyState      = false;
    skConnect->m->attachedState     = false;
    skConnect->m->rrcState          = UE_RRC_UNKNOWN_E;

    skConnect->m->modemRtcValid                 = false;
    skConnect->m->signal.rssi                   = -128;
    skConnect->m->signal.rsrp                   = -140;
    skConnect->m->signal.rsrq                   = -128;
    skConnect->m->signal.sinr                   = -128;
    skConnect->m->modemState                    = MODEM_STATE_ACQUIRING_GNSS_E;
    skConnect->m->gpsNMEAHandler_fp             = NULL;
    skConnect->m->gpsTimeAvail                  = false;
    skConnect->m->gpsEphupdStat                 = false;
    skConnect->m->badATCount                    = 0;
    /* RK_3.2 ONLY */
    skConnect->m->gpsFix                        = false;
    /* RK_3.2 ONLY END */
    skConnect->m->isSimDataAvailable            = false;
    skConnect->m->isRma                         = false;
    skConnect->m->modemResetCnter               = 0;
    skConnect->m->ratActImg                     = NBIOT_IMAGE_NONE_E;
    skConnect->m->firstSocketId                 = -1;
    skConnect->m->pdpType                       = MODEM_PDP_UNKNOWN_E;
    skConnect->m->bootevFlag                    = 0;

    /* Zero buffers */
    memset(skConnect->m->atRespBuf, 0x00, AT_RESP_BUFFER_SIZE);
    memset(skConnect->m->atCmdBuf, 0x00, AT_CMD_BUFFER_SIZE);
    memset(skConnect->m->rxDataBuf, 0x00, RX_DATA_BUFFER_SIZE);

    halModemEnterAtMode(skConnect);

    skConnect->m->hsuConnectedState = true;
    skConnect->m->modemResponseTimeoutMsec = 5000;

    Skylo_Printf(SKYLO_INFO_E, "Waiting for modem to initialize. Please wait...\n");
    skMSecSleep(2000);

    /* Start AT rx thread and start listening */
    if ( !skConnect->m->resetTrig )
        stat = startRxThread(skConnect);

    if ( !skConnect->m->resetTrig && stat != SK_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Failed to start rx thread. errorno %d\n", stat);
        return stat;
    }

    // uint8_t bootevwaitCount = 0;
    // while ( !skConnect->m->bootevFlag ) {

    //     Skylo_Printf(SKYLO_ERR_E, "Waiting for modem BOOTEV notification \n");
    //     skMSecSleep(2000);
    //     bootevwaitCount++;
    //     if ( bootevwaitCount > 20 ) {
    //         Skylo_Printf(SKYLO_ERR_E, "BOOTEV notification failure after 40 secs,rebooting system\n");
    //         skSetSystemResetReason(SK_RESET_REASON_MODEM_COMM_FAIL_INIT);
    //     }
    // }
    // skConnect->m->bootevFlag = 0;

    /* Wait here until modem is alive and responding over AT CLI */
    skConnect->m->modemResponseTimeoutMsec = 2000;
    skConnect->m->atReadyState = 0;
    altairCheckModemOnline(skConnect);
    skConnect->m->modemResponseTimeoutMsec = 5000;

    /* Disable ECHO in reply */
    retryCnter = 0;
    while ( altairFireCommand(skConnect, gCommandList[MODEM_CMD_DISABLE_ECHO_E].command , MODEM_CMD_DISABLE_ECHO_E, NULL)
                != SK_MODEM_OK && retryCnter < 3 ) {
        Skylo_Printf(SKYLO_ERR_E, "Disable ECHO...FAIL\n");
        retryCnter++;
    }

    if ( retryCnter != 3 )
        Skylo_Printf(SKYLO_DEBUG_E, "Disabled ECHO...OK\n");

    // Cell decamp CFUN=0
    stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_FUN_OFF_E].command, MODEM_CMD_FUN_OFF_E, NULL);

    if ( stat != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, _("Cell decamp...FAIL\n"));
        return SK_MODEM_ERR_FUN_OFF;
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, _("Cell decamp...OK\n"));
    }

    skMSecSleep(1000);

// CheckLTEImage:
//     if ( altairGetImageSelected(skConnect, &skConnect->m->runningImage) != SK_MODEM_OK ) {

//         retryCnter++;
//         if ( retryCnter <= 10 ) {

//             Skylo_Printf(SKYLO_ERR_E, "Check running FW image...FAIL. Trying in %d ms\n", BACKOFF_PERIOD);
//             skMSecSleep(BACKOFF_PERIOD);
//             goto CheckLTEImage;
//         }
//     } else {

//         Skylo_Printf(SKYLO_NOTICE_E, "Check running FW image...OK Image[%d]\n", skConnect->m->runningImage);
//     }
    retryCnter = 0;

    stat = halModemGetRATMode(skConnect);

    if ( stat != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, _("Modem get RAT mode...FAIL\n"));
        return SK_MODEM_ERR_FUN_OFF;
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, "Modem get RAT mode(%d) ...OK\n",skConnect->m->ratActImg);
    }

    uint8_t enabled = 0;
    stat = halModemGetMultiRATEnableStatus(skConnect,&enabled);

    if ( stat != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, _("Modem get Multi RAT Enabled?...FAIL\n"));
        return SK_MODEM_ERR_FUN_OFF;
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, "Modem get Multi RAT Enabled(%d) ...OK\n",enabled);
    }

    if ( !enabled ) {
        stat = halModemSetMultiRATEnableStatus(skConnect,1);

        if ( stat != SK_MODEM_OK ) {

            Skylo_Printf(SKYLO_ERR_E, _("Modem set Multi RAT Enabled...FAIL\n"));
            return SK_MODEM_ERR_FUN_OFF;
        } else {

            Skylo_Printf(SKYLO_NOTICE_E, "Modem set Multi RAT Enabled...OK\n");
        }
        skSetSystemResetReason(SK_RESET_REASON_MODEM_REQUIRED_REBOOT_E);
    }

    /* todo later implement proper switching logic */
    if ( skConnect->m->ratActImg == NBIOT_IMAGE_OTHER_E ) {

        stat = halModemSetRATMode(skConnect, NBIOT_IMAGE_TN_E, 1);

        if ( stat != SK_MODEM_OK ) {

            Skylo_Printf(SKYLO_ERR_E, _("Modem change RAT mode[NTN] ...FAIL\n"));
            return SK_MODEM_ERR_FUN_OFF;
        } else {

            Skylo_Printf(SKYLO_NOTICE_E, _("Modem change RAT mode[NTN] ...OK\n"));
        }
    }

    if ( SK_MODEM_OK != ( stat = halModemSetUnsolicitedStatus(skConnect)) )
        Skylo_Printf(SKYLO_ERR_E, "Enabling Unsolicited Modem Failure Resp...FAIL (%d)\n",stat);
    else
        Skylo_Printf(SKYLO_DEBUG_E, "Enabling Unsolicited Modem Failure Resp...OK\n");
    retryCnter = 0;

configureTempFix:
    /* Configure: Non-IP Data Transfer over Control Plane */
    if ( halModemApplyTempFix(skConnect) != SK_MODEM_OK ) {

        retryCnter++;
        if ( retryCnter <= 10 ) {

            Skylo_Printf(SKYLO_ERR_E, "Configuring Temp Fix....FAIL. Trying in %d ms\n", BACKOFF_PERIOD);
            skMSecSleep(BACKOFF_PERIOD);
            goto configureTempFix;
        } 
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, "Configuring Temp Fix...OK\n");
    }
    retryCnter = 0;

configureDataDelNotf:
    /* Configure: Non-IP Data Transfer over Control Plane */
    if ( altairCofigureDataDeliveryNotifications(skConnect) != SK_MODEM_OK ) {

        retryCnter++;
        if ( retryCnter <= 10 ) {

            Skylo_Printf(SKYLO_ERR_E, "Configuring Data Delivery Notifications....FAIL. Trying in %d ms\n", BACKOFF_PERIOD);
            skMSecSleep(BACKOFF_PERIOD);
            goto configureDataDelNotf;
        }
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, "Configuring Data Delivery Notifications...OK\n");
    }
    retryCnter = 0;

    /* Only configure / check here if not in CLI mode */
    // if ( (!skConnect->options->cliMode) && (!skConnect->m->partReset) ) {

    //     /* Set hardware flow control for modem */
    //     skConnect->m->modemResponseTimeoutMsec = 1000;
    //     // if ( !skConnect->m->resetTrig && SK_MODEM_OK != halModemSetHwFlowCtrl(skConnect) )
    //     //     Skylo_Printf(SKYLO_ERR_E, "Can't set Hardware Flow Control\n");

    //     /* This is to take care of condition when after very first boot for RK3.3 Full Iage upgrade,
    //      * modem flow control is disabled and due to above halModemSetHwFlowCtrl API
    //      * modem switches to flow control but QCA is without flow conorl, below altairCheckModemOnline API
    //      * internally takes care of switching flow control and help maintain modem init continuity
    //      * on very first boot after full image upgrade
    //      */
    //     skConnect->m->atReadyState = 0;
    //     altairCheckModemOnline(skConnect);
    //     skConnect->m->modemResponseTimeoutMsec = 10000;


    //     getModemVersion:
    //             /* - Get current modem version */
    //             skConnect->m->nbiotVersion = halModemGetVersion(skConnect);
    //             Skylo_Printf(SKYLO_NOTICE_E, "HAL: Getting modem version [%s]\n", skConnect->m->nbiotVersion);
    //             if ( strlen(skConnect->m->nbiotVersion) < MODEM_VER_LEN ) {

    //                 retryCnter++;
    //                 if ( retryCnter == 5 )
    //                     return SK_MODEM_ERR_INVALID_MDM_VER;
    //                 Skylo_Printf(SKYLO_ERR_E, "Invalid modem version. Trying in %d ms...\n", BACKOFF_PERIOD);
    //                 skMSecSleep(BACKOFF_PERIOD);
    //                 goto getModemVersion;
    //             }
    //             Skylo_Printf(SKYLO_NOTICE_E, "Got version: %s...OK\n", skConnect->m->nbiotVersion);
    //             retryCnter = 0;

    //             skMSecSleep(500);

    //     getModemRkVersion:
    //             /* - Get current modem RK version */
    //             skConnect->m->nbiotRkVersion = halModemGetRkVersion(skConnect);
    //             Skylo_Printf(SKYLO_NOTICE_E, "HAL: Getting modem RK version [%s]\n", skConnect->m->nbiotRkVersion);
    //             if ( strlen(skConnect->m->nbiotRkVersion) < MODEM_RK_VER_LEN ) {

    //                 retryCnter++;
    //                 if ( retryCnter == 5 )
    //                     return SK_MODEM_ERR_INVALID_MDM_VER;
    //                 Skylo_Printf(SKYLO_ERR_E, "Invalid modem version. Trying in %d ms...\n", BACKOFF_PERIOD);
    //                 skMSecSleep(BACKOFF_PERIOD);
    //                 goto getModemRkVersion;
    //             }
    //             Skylo_Printf(SKYLO_NOTICE_E, "Got version: %s...OK\n", skConnect->m->nbiotRkVersion);
    //             retryCnter = 0;

    //             skMSecSleep(500);

    //     getSimIccid:
    //             /* - Get ICCID */
    //             skConnect->m->sim.iccid = halModemGetIccid(skConnect);

    //             if ( strlen(skConnect->m->sim.iccid) < ICCID_MAX_LEN - 1 ) {

    //                 /* Report thread health, till not getting GPS Smodem_tx_thread will be stuck here, Ping !! */
    //                 retryCnter++;
    //                 Skylo_Printf(SKYLO_NOTICE_E, "Invalid ICCID. Trying in %d ms...\n", BACKOFF_PERIOD);
    //                 if ( retryCnter == 5 ) {

    //                     Skylo_Printf(SKYLO_ERR_E, "Could not retrieve ICCID after %d times. Skipping\n", retryCnter);
    //                 } else {

    //                     skMSecSleep(BACKOFF_PERIOD);
    //                     goto getSimIccid;
    //                 }
    //             } else {

    //                 Skylo_Printf(SKYLO_NOTICE_E, "Got ICCID: %s...OK\n", skConnect->m->sim.iccid);
    //             }
    //             retryCnter = 0;

    //             /* Sleep 500ms */
    //             skMSecSleep(500);

    //     getModemImei:
    //             /* - Get IMEI */
    //             skConnect->m->imei = halModemGetImei(skConnect);
    //             if ( strlen(skConnect->m->imei) < IMEI_MAX_LEN ) {

    //                 /* Report thread health, till not getting GPS Smodem_tx_thread will be stuck here, Ping !! */
    //                 retryCnter++;
    //                 Skylo_Printf(SKYLO_NOTICE_E, "Invalid IMEI. Trying in %d ms...\n", BACKOFF_PERIOD);
    //                 if ( retryCnter == 5 ) {

    //                     Skylo_Printf(SKYLO_ERR_E, "Could not retrieve IMEI after %d times. Skipping\n", retryCnter);
    //                 } else {

    //                     skMSecSleep(BACKOFF_PERIOD);
    //                     goto getModemImei;
    //                 }
    //             } else {

    //                 Skylo_Printf(SKYLO_NOTICE_E, "Got IMEI: %s...OK\n", skConnect->m->imei);
    //             }
    //             retryCnter = 0;

    //             /* Sleep 500ms */
    //             skMSecSleep(500);

    //     getSimImsi:
    //             /* - Get IMSI */
    //             skConnect->m->sim.imsi  = halModemGetImsi(skConnect);
    //             if ( strlen(skConnect->m->sim.imsi) < IMSI_MAX_LEN ) {

    //                 /* Report thread health, till not getting GPS Smodem_tx_thread will be stuck here, Ping !! */
    //                 retryCnter++;
    //                 Skylo_Printf(SKYLO_NOTICE_E, "Invalid IMSI [%s]. Trying in %d ms...\n",
    //                                 skConnect->m->sim.imsi, BACKOFF_PERIOD);
    //                 /* TODO: changed from 10 to 1 iteration, revert back one sony altair IMSI issue is resolved */
    //                 if ( retryCnter == 1 ) {

    //                     Skylo_Printf(SKYLO_ERR_E, "Could not retrieve IMSI after %d times. Skipping\n", retryCnter);
    //                 } else {

    //                     skMSecSleep(BACKOFF_PERIOD);
    //                     goto getSimImsi;
    //                 }
    //             } else {

    //                 Skylo_Printf(SKYLO_NOTICE_E, "Got IMSI: %s...OK\n", skConnect->m->sim.imsi);
    //             }
    //             retryCnter = 0;

    //             skConnect->m->isSimDataAvailable = 1;

    //             /* Store ALT1250 Modem Version Phase */
    //             if ( strstr(skConnect->m->nbiotRkVersion, "RK") ) {

    //                 Skylo_Printf(SKYLO_NOTICE_E, "Altair UE Phase 3\n");
    //                 skConnect->m->modemPhaseVer = MODEM_PHASE_3_E;
    //             } else {

    //                 Skylo_Printf(SKYLO_NOTICE_E, "Altair UE Phase 2\n");
    //                 skConnect->m->modemPhaseVer = MODEM_PHASE_2_E;
    //             }
    // }
        /* Sleep 500ms */
        skMSecSleep(250);

configureNotifyevReportingRRC:
    /* Configure: +NOTIFYEV (Notification RRCSTATE Events) */
    if ( altairConfigureNotifyevReporting(skConnect, MODEM_NOTIFYEV_RRCSTATE_E) != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Configuring Notify EV reporting(%d)...FAIL\n",MODEM_NOTIFYEV_RRCSTATE_E);
        retryCnter++;
        if ( retryCnter == 5 ) {

            Skylo_Printf(SKYLO_ERR_E, "Could not configure Notify EV after %d times. Skipping\n", retryCnter);
        } else {

            skMSecSleep(BACKOFF_PERIOD);
            goto configureNotifyevReportingRRC;
        }
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, "Configuring Notify EV reporting(%d)...OK\n",MODEM_NOTIFYEV_RRCSTATE_E);
    }
    retryCnter = 0;
    /* Sleep 500ms */
    skMSecSleep(500);
configureNotifyevReportingSIB31:
    /* Configure: +NOTIFYEV (Notification SIB32 Events) */
    if ( altairConfigureNotifyevReporting(skConnect, MODEM_NOTIFYEV_SIB31_E) != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Configuring Notify EV reporting(%d)...FAIL\n",MODEM_NOTIFYEV_SIB31_E);
        retryCnter++;
        if ( retryCnter == 5 ) {

            Skylo_Printf(SKYLO_ERR_E, "Could not configure Notify EV after %d times. Skipping\n", retryCnter);
        } else {

            skMSecSleep(BACKOFF_PERIOD);
            goto configureNotifyevReportingSIB31;
        }
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, "Configuring Notify EV reporting(%d)...OK\n",MODEM_NOTIFYEV_SIB31_E);
    }
    retryCnter = 0;
    /* Sleep 500ms */
    skMSecSleep(500);

configurePgwReporting:
    /* Configure: +CGEREP (Packet Domain Reporting) */
    if ( altairConfigurePgwReporting(skConnect) != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Configuring PGW URC reporting...FAIL\n");
        retryCnter++;
        if ( retryCnter == 5 ) {

            Skylo_Printf(SKYLO_ERR_E, "Could not configure PGW URC after %d times. Skipping\n", retryCnter);
        } else {

            skMSecSleep(BACKOFF_PERIOD);
            goto configurePgwReporting;
        }
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, "Configuring PGW URC reporting...OK\n");
    }
    retryCnter = 0;
    /* Sleep 500ms */
    skMSecSleep(500);

    char APN[32] = {0,};
    char pdpType[7] = {0,};

    if ( skConnect->m->ratActImg == NBIOT_IMAGE_NTN_E ) {

        // memcpy(APN,"skylo.poc",9);
        // memcpy(pdpType,"Non-IP",6);
        memcpy(APN,"internet.m2mportal.de",21);
        memcpy(pdpType,"IP",2);
    } else {

        memcpy(APN,"internet.m2mportal.de",21);
        memcpy(pdpType,"IP",2);
    }

    configurePDPAPN:
    /* Enable RTT / TA Events */
    /* Configure: +CGEREP (Packet Domain Reporting) */
    if ( halModemSetPdpAndAPN(skConnect,pdpType,APN) != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Configuring pdpType, APN...FAIL\n");
        retryCnter++;
        if ( retryCnter == 5 ) {

            Skylo_Printf(SKYLO_ERR_E, "Could not configure pdpType, APN %d times. Skipping\n", retryCnter);
        } else {

            skMSecSleep(BACKOFF_PERIOD);
            goto configurePDPAPN;
        }
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, "Configuring pdpType, APN...OK\n");
    }
    retryCnter = 0;

    /* Configure: NW operator - "default" */
    // if ( altairFireCommand(skConnect, gCommandList[MODEM_CMD_SET_NW_OPERATOR_E].command , MODEM_CMD_SET_NW_OPERATOR_E, NULL)
    //                         != SK_MODEM_OK )
    //     Skylo_Printf(SKYLO_ERR_E, "Configure NW op: default...FAIL\n");
    // else
    //     Skylo_Printf(SKYLO_NOTICE_E, "Configure NW op: default...OK\n");


    if ( skConnect->m->ratActImg == NBIOT_IMAGE_NTN_E ) { 

        retryCnter = 0;

        configureTADJEV:
        /* Enable RTT / TA Events */
        /* Configure: +CGEREP (Packet Domain Reporting) */
        if ( altairFireCommand(skConnect, gCommandList[MODEM_CMD_TADJEV_ENABLE_E].command, MODEM_CMD_TADJEV_ENABLE_E, NULL) != SK_MODEM_OK ) {

            Skylo_Printf(SKYLO_ERR_E, "Configuring TAJEV Notifications...FAIL\n");
            retryCnter++;
            if ( retryCnter == 5 ) {

                Skylo_Printf(SKYLO_ERR_E, "Could not configure TAJEV Notifications after %d times. Skipping\n", retryCnter);
            } else {

                skMSecSleep(BACKOFF_PERIOD);
                goto configureTADJEV;
            }
        } else {

            Skylo_Printf(SKYLO_NOTICE_E, "Configuring TAJEV Notifications...OK\n");
        }
        retryCnter = 0;
            
        /* Sleep 500ms */
        skMSecSleep(500);

        // set Band
        configureBand:
        stat = halModemSetBand(skConnect);
        if ( stat != SK_MODEM_OK ) {

            Skylo_Printf(SKYLO_ERR_E, _("Setting Band...FAILED\n"));
            retryCnter++;
            if ( retryCnter == 5 ) {

                Skylo_Printf(SKYLO_ERR_E, "Could not set Bandafter %d times.\n", retryCnter);  
                // return SK_MODEM_ERR_FUN_OFF;
            } else {

                skMSecSleep(BACKOFF_PERIOD);
                goto configureBand;
            }
        } else {

            Skylo_Printf(SKYLO_NOTICE_E, _("Setting Band...OK\n"));
        }
        retryCnter = 0;

        /* Sleep 500ms */
        skMSecSleep(500);

        //set earfcn scan
        configureEarfcn:
        stat = halModemSetEarfcn(skConnect);
        if ( stat != SK_MODEM_OK ) {

            Skylo_Printf(SKYLO_ERR_E, _("Configuring earfcn...FAILED\n"));
            retryCnter++;
            if ( retryCnter == 5 ) {

                Skylo_Printf(SKYLO_ERR_E, "Could not set earfcn after %d times.\n", retryCnter);  
                // return SK_MODEM_ERR_FUN_OFF;
            } else {

                skMSecSleep(BACKOFF_PERIOD);
                goto configureEarfcn;
            }
        } else {

            Skylo_Printf(SKYLO_NOTICE_E, _("Configuring earfcn...OK\n"));
        }

        /* Sleep 500ms */
        skMSecSleep(500);
    }

    // Cell camp on CFUN=1
    stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_FUN_ON_E].command, MODEM_CMD_FUN_ON_E, NULL);

    if ( stat != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, _("Cell camp...FAIL\n"));
        return SK_MODEM_ERR_FUN_OFF;
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, _("Cell camp...OK\n"));
    }

    /* Sleep 500ms */
    skMSecSleep(1000);

    /* Wait here until SIM health check is good and collect ICCID / IMSI */
    if ( !altairIsSimReady(skConnect) ) {

        Skylo_Printf(SKYLO_ERR_E, "SIM is not ready."_(" Failed to read state")"\n");
        return SK_MODEM_ERR_INVALID_SIM_STATE;
    }

    // skMSecSleep(2000);


    // if ( altairFireCommand(skConnect, gCommandList[MODEM_CMD_ENABLE_CELL_URC_E].command,
    //                         MODEM_CMD_ENABLE_CELL_URC_E, NULL) != SK_MODEM_OK )
    //     Skylo_Printf(SKYLO_ERR_E, "Configure Cell Stat URC...FAIL\n");
    // else
    //     Skylo_Printf(SKYLO_NOTICE_E, "Configure Cell Stat URC...OK\n");

    /* Disable Data Inactivity Timer */
    // if ( altairFireCommand(skConnect, gCommandList[MODEM_CMD_DISABLE_DATA_INACTIVITY_TIMER_E].command,
    //                         MODEM_CMD_DISABLE_DATA_INACTIVITY_TIMER_E, NULL) != SK_MODEM_OK )
    //     Skylo_Printf(SKYLO_ERR_E, "Disable Data Inactivity Timer...FAIL\n");
    // else
    //     Skylo_Printf(SKYLO_NOTICE_E, "Disable Data Inactivity Timer...OK\n");


    stat = halModemEnable(skConnect);

    if ( stat != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, _("Cmatt...FAIL\n"));
        return SK_MODEM_ERR_FUN_OFF;
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, _("Cmatt...OK\n"));
    }

    /* Start FSM thread */
    if ( !skConnect->m->resetTrig )
        stat = modemStartFsmThread(skConnect);

    if ( !skConnect->m->resetTrig && stat != SK_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Failed to start FSM thread. errorno %d\n", stat);
        return stat;
    }

    // if ( SK_MODEM_OK != (stat=halModemQueryCellId(skConnect)) )
    //     Skylo_Printf(SKYLO_ERR_E, "Failed to get modem cell id errorno %d\n", stat);

    /* Start modem health monitor thread */
    if ( !skConnect->m->resetTrig )
        stat = modemStartHealthMonitorThread(skConnect);

    if ( !skConnect->m->resetTrig && stat != SK_OK ) {

    Skylo_Printf(SKYLO_ERR_E, "Failed to start health monitor thread. errorno %d\n", stat);
        return stat;
    }

    /* Enabling Boot initmation Unsolicited response */
    if ( altairFireCommand(skConnect, gCommandList[MODEM_CMD_ENABLE_REBOOT_EVENT_E].command,
                                MODEM_CMD_ENABLE_REBOOT_EVENT_E, NULL) != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, "Enabling Boot Intimation Event...FAIL\n");
    else
        Skylo_Printf(SKYLO_NOTICE_E, "Enabling Boot Intimation Event...OK\n");

    /* set init flag to True */
    skConnect->m->isInit = 1;
    /* It must pass all this test Indicates MODEM init done */
    skConnect->m->inInitApi = 0;

    /* record the init time, for NTN to TN switch timer */
    skConnect->m->swtchContext.modemInitTimer = skConnect->pep.os.time.fpGetUnixEpochInMSec();

    /* just to avoid intial zero */
    skConnect->m->swtchContext.timeofLastConn = skConnect->pep.os.time.fpGetUnixEpochInMSec();

    return SK_MODEM_OK;
}

const char * halModemGetVersion(skConnect_t *skConnect)
{
    static char mdmVerBuf[MODEM_VER_MAX_LEN + 1] = {0};

    if ( strlen(mdmVerBuf) < MODEM_VER_LEN ) {

        int res = altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_VERSION_E].command,
                                        MODEM_CMD_GET_VERSION_E, mdmVerBuf);
        if (res != SK_MODEM_OK) return "Unavailable";

        mdmVerBuf[MODEM_VER_MAX_LEN] = 0;
    }
    return mdmVerBuf;
}

const char * halModemGetRkVersion(skConnect_t *skConnect)
{
    static char mdmVerBuf[MODEM_VER_MAX_LEN + 1] = {0};

    if ( strlen(mdmVerBuf) < MODEM_RK_VER_LEN ) {

        int res = altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_RK_VERSION_E].command,
                                        MODEM_CMD_GET_RK_VERSION_E, mdmVerBuf);
        if ( res != SK_MODEM_OK )
            return "Unavailable";

        mdmVerBuf[MODEM_VER_MAX_LEN] = 0;
    }
    return mdmVerBuf;
}

const char * halModemGetImei(skConnect_t *skConnect)
{
    static char imeiBuf[IMEI_MAX_LEN + 1] = {0};

    if ( strlen(imeiBuf) < IMEI_MAX_LEN ) {

        int res = altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_IMEI_E].command, MODEM_CMD_GET_IMEI_E, imeiBuf);
        if ( res != SK_MODEM_OK )
            return "Unavailable";

        imeiBuf[IMEI_MAX_LEN] = 0;
    }
    return imeiBuf;
}

const char * halModemGetIccid(skConnect_t *skConnect)
{
    static char iccidBuf[ICCID_MAX_LEN + 1] = {0};

    if ( strlen(iccidBuf) < ICCID_MAX_LEN - 1 ) {

        int res = altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_ICCID_E].command, MODEM_CMD_GET_ICCID_E, iccidBuf);
        if ( res != SK_MODEM_OK )
            return "Unavailable";

        iccidBuf[ICCID_MAX_LEN] = 0;
    }

    return iccidBuf;
}

const char * halModemGetImsi(skConnect_t *skConnect)
{
    static char imsiBuf[IMSI_MAX_LEN + 1] = {0};

    if ( strlen(imsiBuf) < IMSI_MAX_LEN ) {

        int res = altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_IMSI_E].command, MODEM_CMD_GET_IMSI_E, imsiBuf);
        if ( res != SK_MODEM_OK )
            return "Unavailable";

        imsiBuf[IMSI_MAX_LEN] = 0;
    }
    return imsiBuf;
}

int16_t halModemGetRssi(skConnect_t *skConnect)
{
    skStatus_t stat = SK_MODEM_ERROR;

    if ( (stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_RSSI_E].command, MODEM_CMD_GET_RSSI_E,
                                        (void *)&skConnect->m->signal.rssi)) != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, "Get RSSI errorno %"PRId32"\n", stat);
    return skConnect->m->signal.rssi;
}

int16_t halModemReadRssi(skConnect_t *skConnect)
{
    return skConnect->m->signal.rssi;
}

uint8_t halModemIsSimDataAvailable(skConnect_t *skConnect)
{
    return skConnect->m->isSimDataAvailable;
}

uint8_t halModemIsInitialized(skConnect_t *skConnect)
{
    return skConnect->m->isInit;
}

int16_t halModemGetRsrp(skConnect_t *skConnect)
{
    skStatus_t stat = SK_MODEM_ERROR;

    if ( (stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_RSRP_E].command, MODEM_CMD_GET_RSRP_E,
                                        (void *)&skConnect->m->signal.rsrp)) != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, "Get RSRP errorno %"PRId32"\n", stat);

    return skConnect->m->signal.rsrp;
}
int32_t halModemGetRsrpStatus(skConnect_t *skConnect, int16_t *rsrp)
{
    skStatus_t stat = SK_MODEM_ERROR;

    if ( (stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_RSRP_E].command, MODEM_CMD_GET_RSRP_E,
                                        (void *)&skConnect->m->signal.rsrp)) != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Get RSRP errorno %"PRId32"\n", stat);
        *rsrp = -128;
        return stat;
    }
    *rsrp =  skConnect->m->signal.rsrp;

    return stat;
}
int16_t halModemReadRsrp(skConnect_t *skConnect)
{
    return skConnect->m->signal.rsrp;
}

int16_t halModemGetRsrq(skConnect_t *skConnect)
{
    skStatus_t stat = SK_MODEM_ERROR;

    if ( (stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_RSRQ_E].command,
                                        MODEM_CMD_GET_RSRQ_E, (void *)&skConnect->m->signal.rsrq)) != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, "Get RSRQ errorno %"PRId32"\n", stat);

    return skConnect->m->signal.rsrq;
}

int16_t halModemReadRsrq(skConnect_t *skConnect)
{
    return skConnect->m->signal.rsrq;
}

int8_t halModemGetSinr(skConnect_t *skConnect)
{
    skStatus_t stat = SK_MODEM_ERROR;

    if ( (stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_SINR_E].command, 
                                        MODEM_CMD_GET_SINR_E,
                                        (void *)&skConnect->m->signal.sinr)) != SK_MODEM_OK )
        Skylo_Printf(SKYLO_ERR_E, "Get SINR errno%"PRId32"\n", stat);

    return skConnect->m->signal.sinr;
}

skStatus_t halModemGetAllSignalLevels(skConnect_t *skConnect)
{
    return altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_ALL_SIGNAL_LEVELS_E].command, 
        MODEM_CMD_GET_ALL_SIGNAL_LEVELS_E, NULL);
}

int8_t halModemReadSinr(skConnect_t *skConnect)
{
    return skConnect->m->signal.sinr;
}

skStatus_t halModemSetHwFlowCtrl(skConnect_t *skConnect)
{
    return altairFireCommand(skConnect, gCommandList[MODEM_CMD_AT_HW_FLOW_E].command, 
        MODEM_CMD_AT_HW_FLOW_E, NULL);
}

skStatus_t halModemSend(skConnect_t *skConnect, uint8_t * data, uint16_t len)
{
    skStatus_t ret = SK_MODEM_ERR_NO_MEM;

    if ( !data )
        return SK_MODEM_ERROR;

    if ( !skConnect->m->attachedState )
        return SK_MODEM_ERR_NOT_ATTACHED;

    if ( len > TX_DATA_MAX_LEN || len == 0 )
        return SK_MODEM_ERR_DATA_TOO_BIG;

    if ( halModemGetGnssLteState(skConnect) == GNSS_STATE_E )
        return SK_MODEM_ERR_DATA_SEND_GNSS;

    /* Try to open socket for communication */
    if ( skConnect->m->pdpType == MODEM_PDP_IP_E && skConnect->m->firstSocketId == -1 ) {
        
        if ( SK_OK != (ret = sKonnectOpenHBSocket(skConnect)) ) {
            
            Skylo_Printf(SKYLO_ERR_E, "While opening socket errno%"PRId32"\n", ret);
            return SK_MODEM_ERR_SOCK_NOT_ACTIVE;
        }
    }

    if ( altairModemGetCurrentState(skConnect) == MODEM_STATE_UE_IDLE_NW_ATTACHED_E ||
        altairModemGetCurrentState(skConnect) == MODEM_STATE_UE_CONN_NW_ATTACHED_E ) {

        // if Non-IP
        if ( skConnect->m->pdpType != MODEM_PDP_IP_E ) {
            
            char * finalCmd = NULL;
            finalCmd = (char *) malloc(AT_CMD_BUFFER_SIZE);
            if ( finalCmd != NULL ) {

                memset(finalCmd, 0, AT_CMD_BUFFER_SIZE);
                snprintf(finalCmd, AT_CMD_BUFFER_SIZE, gCommandList[MODEM_CMD_SEND_NIP_E].command,
                            len, data);
                
                ret = altairFireCommand(skConnect, finalCmd, MODEM_CMD_SEND_NIP_E, NULL);
                free(finalCmd);
            }

        } else {    //send via Socket
                         
            ret = altairSendRxBuffer(skConnect,(char*)data,len,skConnect->m->firstSocketId);
        }
        skMSecSleep(MODEM_TX_BACKOFF_DELAY);
        return ret;
    }
    return SK_MODEM_ERR_DATA_SEND;
}

skStatus_t halModemIsReady(skConnect_t *skConnect)
{
    return skConnect->m->hsuConnectedState ? SK_MODEM_OK : SK_MODEM_ERROR;
}

skStatus_t halModemEnable(skConnect_t *skConnect)
{
    return altairFireCommand(skConnect, gCommandList[MODEM_CMD_ENABLE_E].command, MODEM_CMD_ENABLE_E, NULL);
}

skStatus_t halModemDisable(skConnect_t *skConnect)
{
    return altairFireCommand(skConnect, gCommandList[MODEM_CMD_DISABLE_E].command, MODEM_CMD_DISABLE_E, NULL);
}

skStatus_t halModemRegisterGpsHandler(skConnect_t *skConnect, modemGpsHandler_fp handler)
{
    if (  !skConnect || !skConnect->m )
        return SK_MODEM_ERROR;

    if ( !handler )
        return SK_MODEM_ERR_INVALID_GPS_HDL;

    if ( !skConnect->m->gpsNMEAHandler_fp ) {

        skConnect->m->gpsNMEAHandler_fp = handler;
#if 0
        Skylo_Printf(SKYLO_NOTICE_E, ">>>>>>>>><<<<<<<<<<<< Register GPS Handler %p\n", skConnect->m->gpsNMEAHandler_fp);
#endif
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, "GPS Handler already registered\n");
    }

    return SK_MODEM_OK;
}

int16_t halModemSubscribe(skConnect_t *skConnect, uint8_t * buf, uint16_t maxLen)
{
    if ( !skConnect || !skConnect->m || !buf )
        return SK_MODEM_ERR_INVALID_MEM;

    if ( maxLen > RX_DATA_MAX_LEN )
        return SK_MODEM_ERR_SIZE_EXCEEDED;

    int16_t len = altairSubscribe(skConnect, buf, maxLen);
    if ( len < 0 )
        Skylo_Printf(SKYLO_ERR_E, "Cannot read data. errorno %d\n", len);

    return len;
}

skStatus_t halModemAtCli(skConnect_t *skConnect, const char * atCommand)
{
    skStatus_t status = SK_MODEM_ERR_INVALID_AT_CMD;

    if ( strstr(atCommand, "at") || strstr(atCommand, "AT") )
        status = altairFireCommand(skConnect, &atCommand[2], MODEM_CMD_AT_CLI_E, NULL);

    return status;
}

skStatus_t halModemSetRtc(skConnect_t *skConnect, rtcTm_t * currTime)
{
    char finalCmd[32] = {0};

    if ( ((int8_t)currTime->year)   < 0 || currTime->year   > 100 )
        return RTC_ERR_INVALID_BOUNDS_YEAR;
    if ( ((int8_t)currTime->month)  < 0 || currTime->month  > 12  )
        return RTC_ERR_INVALID_BOUNDS_MONTH;
    if ( ((int8_t)currTime->day)    < 0 || currTime->day    > 31  )
        return RTC_ERR_INVALID_BOUNDS_DAY;
    if ( ((int8_t)currTime->hour)   < 0 || currTime->hour   > 23  )
        return RTC_ERR_INVALID_BOUNDS_HOUR;
    if ( ((int8_t)currTime->minute) < 0 || currTime->minute > 60  )
        return RTC_ERR_INVALID_BOUNDS_MIN;
    if ( ((int8_t)currTime->second) < 0 || currTime->second > 60  )
        return RTC_ERR_INVALID_BOUNDS_SEC;

    snprintf(finalCmd, 32, gCommandList[MODEM_CMD_SET_RTC_E].command, currTime->year,
            currTime->month,  currTime->day,    currTime->hour,
            currTime->minute, currTime->second, currTime->tz);

    return altairFireCommand(skConnect, finalCmd, MODEM_CMD_SET_RTC_E, NULL);
}

skStatus_t halModemGetRtc(skConnect_t *skConnect, rtcTm_t * currTime)
{
    if ( !skConnect->m->gpsTimeAvail )
        return SK_MODEM_ERROR;

    return altairFireCommand(skConnect, gCommandList[MODEM_CMD_GET_RTC_E].command, MODEM_CMD_GET_RTC_E, (void *)currTime);
}

skStatus_t halModemGetLteTa(skConnect_t *skConnect, int16_t * TA)
{
    return altairGetLteTimingAdvance(skConnect, TA);

}

modemRrcState_e halModemIsRrcConnected(skConnect_t *skConnect)
{
    return skConnect->m->rrcState;
}

modemRrcState_e halModemQueryRrcState(skConnect_t *skConnect)
{
    modemRrcState_e rrcState = UE_RRC_UNKNOWN_E;

    if ( altairFireCommand(skConnect, gCommandList[MODEM_CMD_QUERY_RRC_STATE_E].command, MODEM_CMD_QUERY_RRC_STATE_E,
                                (void*)&rrcState) != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Cannot query RRC state\n");
        /* If cannot query, then use current RRC state */
        rrcState = skConnect->m->rrcState;
    }
    return rrcState;
}

skStatus_t halModemQueryCellId(skConnect_t *skConnect)
{
    skStatus_t status = SK_MODEM_ERROR;
    if ( (status = altairFireCommand(skConnect, gCommandList[MODEM_CMD_QUERY_CELL_ID_E].command,
                                        MODEM_CMD_QUERY_CELL_ID_E, NULL)) != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, _("Query Cell ID Failed ")"AT [%s], errno %"PRId32"\n",
                                    gCommandList[MODEM_CMD_QUERY_CELL_ID_E].command, status);
        return SK_MODEM_ERROR;
    }

    return SK_MODEM_OK;
}

uint32_t halModemGetCellId(skConnect_t *skConnect)
{
    return skConnect->m->cellId;
}

modemAttachState_e halModemIsAttached(skConnect_t *skConnect)
{
    return skConnect->m->attachedState;
}

void halModemEnterOnlineMode(skConnect_t *skConnect)
{
    Skylo_Printf(SKYLO_DEBUG_E, _("Entering online mode\n"));
    skConnect->m->onlineMode = true;
}

void halModemEnterOfflineMode(skConnect_t *skConnect)
{
    Skylo_Printf(SKYLO_DEBUG_E, _("Entering offline mode\n"));
    skConnect->m->onlineMode = false;
}

void halModemEnterBootMode(skConnect_t *skConnect)
{
    Skylo_Printf(SKYLO_DEBUG_E, _("Entering Boot mode\n"));
    skConnect->m->bootMode = true;
}

void halModemEnterAtMode(skConnect_t *skConnect)
{
    Skylo_Printf(SKYLO_DEBUG_E, _("Entering at channel mode\n"));
    skConnect->m->bootMode = false;
}

boolean halModemGetBootMode(skConnect_t *skConnect)
{
    return skConnect->m->bootMode;
}
modemSkyloState_e altairModemGetCurrentState(skConnect_t *skConnect)
{
    return skConnect->m->modemState;
}

skStatus_t altairModemSetCurrentState(skConnect_t *skConnect, modemSkyloState_e state)
{
    skConnect->pep.os.ipc.fpMutexLockTimed(&(skConnect->m->stateMutex), SK_TIME_WAIT_FOREVER);
    skConnect->m->modemState = state;
    skConnect->pep.os.ipc.fpMutexUnlock(&(skConnect->m->stateMutex));

    return SK_MODEM_OK;
}

skStatus_t halModemOnlineCheck(skConnect_t *skConnect)
{
    return altairFireCommand(skConnect, gCommandList[MODEM_CMD_AT_ONLINE_E].command, MODEM_CMD_AT_ONLINE_E, NULL);
}

skStatus_t halModemDeactivateGnss(skConnect_t *skConnect)
{
    return altairFireCommand(skConnect, gCommandList[MODEM_CMD_IGNSS_DEACTIVATE_E].command,
                                MODEM_CMD_IGNSS_DEACTIVATE_E, NULL);
}

modemGnssLteState_e halModemGetGnssLteState(skConnect_t *skConnect)
{
    modemGnssLteState_e state = LTE_STATE_E;

    switch ( altairModemGetCurrentState(skConnect) ) {

        case MODEM_STATE_ACQUIRING_GNSS_E:
        case MODEM_STATE_CELL_CAMPING_E:
        case MODEM_STATE_ACQUIRING_GEO_SYNC_E:
        case MODEM_STATE_GNSS_UPDATE_E:
        case MODEM_STATE_GNSS_EPH_UPDATE_E:

            state = GNSS_STATE_E;
            break;

        default:

            state = LTE_STATE_E;
            break;

    }

    return state;
}

modemGnssLteState_e halModemGetGnssNbiotState(skConnect_t *skConnect)
{
    modemGnssLteState_e state = LTE_STATE_E;

    switch ( altairModemGetCurrentState(skConnect) ) {

        case MODEM_STATE_ACQUIRING_GNSS_E:
        case MODEM_STATE_CELL_CAMPING_E:
        case MODEM_STATE_ACQUIRING_GEO_SYNC_E:
        case MODEM_STATE_WAITING_FOR_VALID_SINR_E:
        case MODEM_STATE_GNSS_UPDATE_E:
        case MODEM_STATE_GNSS_EPH_UPDATE_E:

            state = GNSS_STATE_E;
            break;

        default:

            state = LTE_STATE_E;
            break;
    }

    return state;
}

skStatus_t getFlashPartitionInfo(skConnect_t *skConnect)
{

    skStatus_t status = SK_MODEM_ERROR;

    if ( SK_MODEM_OK != ( status = altairFireCommand(skConnect, gCommandList[MODEM_CMD_BOOT_MODE_PARSE_FLASH_E].command,
                                                        MODEM_CMD_BOOT_MODE_PARSE_FLASH_E, NULL)) ) {

        Skylo_Printf(SKYLO_ERR_E, "Cannot fire command"_(" to check flashstart ")"[%s]\n",
                                    gCommandList[MODEM_CMD_BOOT_MODE_PARSE_FLASH_E].command);
        return status;
    }

    skMSecSleep(100);

    Skylo_Printf(SKYLO_ERR_E, "flash start: %llx and flash size: %llx\n",
                            skConnect->m->bootParam.flashStart, skConnect->m->bootParam.flashSize);

    return status;
}

skStatus_t setBaudrateInfo(skConnect_t *skConnect, uint32_t baudrate)
{

    char cmd[64] = {0};
    snprintf (cmd, 64, gCommandList[MODEM_CMD_BOOT_SET_BAUDRATE_E].command, baudrate );

    Skylo_Printf(SKYLO_DEBUG_E, "AT command "_("to set baudrate ")"[%s]\n", cmd);

    if ( altairFireCommand(skConnect, cmd, MODEM_CMD_BOOT_SET_BAUDRATE_E, NULL) != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Cannot fire command to set baudrate %s\n", cmd);
        return SK_MODEM_ERROR;
    }

    skMSecSleep(2000);

    Skylo_Printf(SKYLO_INFO_E, "Boot mode baudrate set %"PRIu32"\n", skConnect->m->bootParam.baudrate);

    return SK_MODEM_OK;

}

skStatus_t setFlowcontrolInfo(skConnect_t *skConnect, uint8_t status)
{
    char cmd[64] = {0};
    skStatus_t ret = SK_MODEM_ERROR;

    if ( status )
        snprintf (cmd, 64, gCommandList[MODEM_CMD_BOOT_SET_HWFLOW_E].command, "on");
    else
        snprintf (cmd, 64, gCommandList[MODEM_CMD_BOOT_SET_HWFLOW_E].command, "off");


    Skylo_Printf(SKYLO_DEBUG_E, "AT command "_("to set flowcontrol ")"[%s]\n", cmd);

    if ( SK_MODEM_OK != (ret = altairFireCommand(skConnect, cmd, MODEM_CMD_BOOT_SET_HWFLOW_E, NULL)) )
        Skylo_Printf(SKYLO_ERR_E, "Cannot fire command to set flowcontrol %s\n", cmd);
    else
        Skylo_Printf(SKYLO_DEBUG_E, "Success to fire command to set flowcontrol %s\n", cmd);

    return ret;
}

skStatus_t transferBootMode(skConnect_t *skConnect, uint32_t channel)
{
    skStatus_t status = SK_MODEM_ERROR;
    char cmd[64] = {0};
    snprintf (cmd, 64, gCommandList[MODEM_CMD_BOOT_MODE_TRANSFER_E].command, channel );

    if ( channel > 2 || channel <= 0 ) {

        Skylo_Printf(SKYLO_ERR_E, "Uboot transfer mode channel is not supported\n");
        return SK_MODEM_ERROR;
    }

    Skylo_Printf(SKYLO_DEBUG_E, _("fired command to change UBoot mode ")"%s\n", cmd);
    /* Fire the command to transfer U-Boot print on UART0 */
    for ( int i = 0; i<=2; i++ ) {

        if ( SK_MODEM_OK != (status = altairFireCommand(skConnect, cmd, MODEM_CMD_BOOT_MODE_TRANSFER_E, NULL)) ) {

            Skylo_Printf(SKYLO_ERR_E, "Cannot fire "_("command to transfer boot mode ")"AT[%s]\n",
                                        gCommandList[MODEM_CMD_BOOT_MODE_TRANSFER_E].command);
            skMSecSleep(100);
            continue;
        } else {

            Skylo_Printf(SKYLO_DEBUG_E, "Success to fir "_("command to transfer boot mode")"AT[%s]\n",
                                        gCommandList[MODEM_CMD_BOOT_MODE_TRANSFER_E].command);
            break;
        }
    }
    return status;
}



