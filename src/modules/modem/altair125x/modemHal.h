/**
 * @file        modemHal.h
 * @brief       Modem HAL API
 * @author      Barry Winata (barry@skylo.tech)
 *              Dharmesh Vasoya (dharmesh@skylo.tech)
 * @copyright   Copyright (c) 2019 Skylo Technologies, Inc.
 */

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __MODEM_HAL_H__
#define __MODEM_HAL_H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <modemHealth.h>

#include <skOSPlugin.h>
#include <skConnect.h>
#include <skModemStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

#define MODEM_LDO_ON                (1)
#define MODEM_LDO_OFF               (0)
#define MIN_RES_BUFF		        (13)
#define MAX_RES_BUFF	            (200)

#define AT_CHANNLE_MODE             (0)
#define U_BOOT_MODE                 (1)
#define MODEM_UNINIT_ERR_MSG        "Modem is not initialised yet\n"

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

typedef  unsigned char      boolean;     /* Boolean value type. */

typedef void    (*modemRxHandler_fp)(void);

typedef int32_t skStatus_t;

/**
 * @enum  Modem NoTIFYEV types enum.
 *
 * @details Modem NoTIFYEV types enum.
 */
typedef enum modemNotifyEVTypes_s {

    MODEM_NOTIFYEV_RRCSTATE_E = 0x00,   /**> RRCSTATE notifications  */
    MODEM_NOTIFYEV_SIB31_E,             /**> SIB31 notifications */
} modemNotifyEVTypes_e;

/*-------------------------------------------------------------------------
 * Modem Enumerations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Modem STATUS codes
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Modem Public API
 *-----------------------------------------------------------------------*/

/**
 * @brief
 * Function to disable modem radio
 *
 * @details
 * Function to disable modem radio
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemDisable(skConnect_t *skConnect);

/**
 * @brief
 * Function to enable modem radio
 *
 * @details
 * Function to enable modem radio
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemEnable(skConnect_t *skConnect);

/**
 * @brief
 * Function to retrieve current modem firmware version
 *
 * @details
 * Function to retrieve current modem firmware version
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Pointer of type char to version string
 */

const char * halModemGetVersion(skConnect_t *skConnect);

/**
 * @brief
 * Function to retrieve current modem firmware RK version
 *
 * @details
 * Function to retrieve current modem firmware RK version
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Pointer of type char to version string
 */
const char * halModemGetRkVersion(skConnect_t *skConnect);

/**
 * @brief
 * Function to retrieve IMEI number from modem
 *
 * @details
 * Function to retrieve IMEI number from modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Pointer of type char to IMEI
 */
const char * halModemGetImei(skConnect_t *skConnect);

/**
 * @brief
 * Function to retrieve ICCID number from the SIM
 *
 * @details
 * Function to retrieve ICCID number from the SIM
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Pointer of type char to ICCID
 */
const char * halModemGetIccid(skConnect_t *skConnect);

/**
 * @brief
 * Function to retrieve IMSI number from the SIM
 *
 * @details
 * Function to retrieve IMSI number from the SIM
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Pointer of type char to IMSI
 */
const char * halModemGetImsi(skConnect_t *skConnect);

/**
 * @brief
 * Function to retrieve the configured network
 *
 * @details
 * Function to retrieve the configured network
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Pointer of type char to NW operator
 */
const char * halGetNwOperator(skConnect_t *skConnect);

/**
 * @brief
 * Function to retrieve current RSSI strength value (in dBm) from modem
 *
 * @details
 * Function to retrieve current RSSI strength value (in dBm) from modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  RSSI value in dBm. A value of -113 indicates RSSI is at the noise floor
 */
int16_t halModemGetRssi(skConnect_t *skConnect);

/**
 * @brief
 * Function to read current RSSI strength value (in dBm) from modem
 *
 * @details
 * Function to read current RSSI strength value (in dBm) from modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  RSSI value in dBm. A value of -113 indicates RSSI is at the noise floor
 */
int16_t halModemReadRssi(skConnect_t *skConnect);

/**
 * @brief
 * Function to check if modem and sim data is available for mobile display
 *
 * @details
 * Function to check if modem and sim data is available for mobile display
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 if not initialized and 1 if initialized.
 */
uint8_t halModemIsSimDataAvailable(skConnect_t *skConnect);

/**
 * @brief
 * Function to check if modem initialization is completed
 *
 * @details
 * Function to check if modem initialization is completed
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 if not initialized and 1 if initialized.
 */
uint8_t halModemIsInitialized(skConnect_t *skConnect);

/**
 * @brief
 * Function to retrieve current RSRP strength from modem
 *
 * @details
 * Function to retrieve current RSRP strength from modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Reference Signal Received Power (RSRP) value in dBm
 */
int16_t halModemGetRsrp(skConnect_t *skConnect);

/**
 * @brief
 * Function to retrieve current RSRP strength from modem
 *
 * @details
 * Function to retrieve current RSRP strength from modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 * @param[in]  rsrp     Ppointer to the latest rsrp value
 *
 * @return  Reference Signal Received Power (RSRP) value in dBm
 */
int32_t halModemGetRsrpStatus(skConnect_t *skConnect, int16_t *rsrp);

/**
 * @brief
 * Function to read current RSRP strength from modem
 *
 * @details
 * Function to read current RSRP strength from modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Reference Signal Received Power (RSRP) value in dBm
 */
int16_t halModemReadRsrp(skConnect_t *skConnect);

/**
 * @brief
 * Function to retrieve current RSRQ strength from modem
 *
 * @details
 * Function to retrieve current RSRQ strength from modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Reference Signal Received Quality (RSRQ) value in dBm
 */
int16_t halModemGetRsrq(skConnect_t *skConnect);

/**
 * @brief
 * Function to read current RSRQ strength from modem
 *
 * @details
 * Function to read current RSRQ strength from modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Reference Signal Received Quality (RSRQ) value in dBm
 */
int16_t halModemReadRsrq(skConnect_t *skConnect);

/**
 * @brief
 * Function to retrieve current SINR strength from modem
 *
 * @details
 * Function to retrieve current SINR strength from modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Signal plus Interference to Noise Ratio (SINR) value in dBm
 */
int8_t halModemGetSinr(skConnect_t *skConnect);

/**
 * @brief
 * Function to retrieve All Signal levels
 * 
 * @note These API can be called before halModemRead(sinr,rsrq,rsrp,rssi) to avoid firing AT%MEAS="8" multiple times
 *
 * @details
 * Function to retrieve All Signal levels
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Signal plus Interference to Noise Ratio (SINR) value in dBm
 */
skStatus_t halModemGetAllSignalLevels(skConnect_t *skConnect);

/**
 * @brief
 * Function to read current SINR strength from modem
 *
 * @details
 * Function to read current SINR strength from modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Signal plus Interference to Noise Ratio (SINR) value in dBm
 */
int8_t halModemReadSinr(skConnect_t *skConnect);

/**
 * @brief
 * Function to set hardware flow control in modem
 *
 * @details
 * Function to set hardware flow control in modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSetHwFlowCtrl(skConnect_t *skConnect);


/**
 * @brief
 * Function to retrieve the configured network band from the modem
 *
 * @details
 * Function to retrieve the configured network band from the modem
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Returns configured modem RF band
 */
uint8_t halModemGetcfgBand(skConnect_t *skConnect);

/**
 * @brief
 * Function to configure the specified network band number for the modem
 *
 * @details
 * Function to configure the specified network band number for the modem
 *
 * @param[in] skConnect Pointer to skConnect_t context
 * @param[in] band      The specified network band
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSetcfgBand(skConnect_t *skConnect, uint8_t band);

/**
 * @brief
 * Function to configure the Satellite delay of the modem
 *
 * @details
 * Function to configure the Satellite delay of the modem
 *
 * @param[in] skConnect Pointer to skConnect_t context
 * @param[in] rtt       Maximum Round Trip Time (RTT) in ms between UE and BS
 * @param[in] delay     Specified delay (ms) between the UE and the Satellite
 * @param[in] ita       Initial Timing Advance (ita) delta (1ms == 15360)
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSetcfgSat(skConnect_t *skConnect, uint16_t rtt, uint16_t delay, uint32_t ita);

/**
 * @brief
 * Function to send binary data to the basestation
 *
 * @details
 * Function to send binary data to the basestation
 *
 * @param[in] skConnect Pointer to skConnect_t context
 * @param[in] data      Pointer to array of binary data
 * @param[in] len       Length of binary data (bytes)
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSend(skConnect_t *skConnect, uint8_t * data, uint16_t len);

/**
 * @brief
 * Function to subscribe to incoming UE data. This must be called in a loop.
 *
 * @details
 * Function to subscribe to incoming UE data. This must be called in a loop.
 *
 * @param[in] skConnect Pointer to skConnect_t context
 * @param[in] buf     Application defined pointer to buffer array to receive Skylo data
 * @param[in] maxLen  Maximum length of buffer
 *
 * @return Number of returned bytes
 */
int16_t halModemSubscribe(skConnect_t *skConnect, uint8_t * buf, uint16_t maxLen);

/**
 * @brief
 * Function to register for GPS notification from internal GNSS
 *
 * @details
 * Function to register for GPS notification from internal GNSS
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 * @param[in]  handler   Application specific to handle incoming notification event
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemRegisterGpsHandler(skConnect_t *skConnect, modemGpsHandler_fp handler);

/**
 * @brief
 * Function to check if modem is currently attached to network
 *
 * @details
 * Function to check if modem is currently attached to network
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return modem attached state
 */
modemAttachState_e halModemIsAttached(skConnect_t *skConnect);

/**
 * @brief
 * Function to disconnect (detach) modem from network
 *
 * @details
 * Function to disconnect (detach) modem from network
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemDisconnect(skConnect_t *skConnect);

/**
 * @brief
 * Function to initialize the modem Finite State Machine (FSM)
 *
 * @details
 * Function to initialize the modem Finite State Machine (FSM)
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemInit(skConnect_t *skConnect);

/**
 * @brief
 * Function to check if modem is in ready state (AT CLi is online)
 *
 * @details
 * Function to check if modem is in ready state (AT CLi is online)
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemIsReady(skConnect_t *skConnect);

/**
 * @brief
 * Function to execute direct AT command into modem CLI
 *
 * @details
 * Function to execute direct AT command into modem CLI
 *
 * @param[in] skConnect Pointer to skConnect_t context
 * @param[in] atCommand AT command to execute
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemAtCli(skConnect_t *skConnect, const char * atCommand);

#if 1
/**
 * @brief
 * Function to execute direct AT command into fpts test
 *
 * @details
 * Function to execute direct AT command into fpts test
 *
 * @param[in] skConnect Pointer to skConnect_t context
 * @param[in] atCommand AT command to execute
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemFptsCli(skConnect_t *skConnect, const char * atCommand);
#endif

/**
 * @brief
 * Function to set RTC time into modem
 *
 * @details
 * Function to set RTC time into modem
 *
 * @param[in] skConnect Pointer to skConnect_t context
 * @param[in] currTime  Current time structure
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSetRtc(skConnect_t *skConnect, rtcTm_t * currTime);

/**
 * @brief
 * Function to get RTC time from modem
 *
 * @details
 * Function to get RTC time from modem
 *
 * @param[in] skConnect Pointer to skConnect_t context
 * @param[in] currTime  Current time structure
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemGetRtc(skConnect_t *skConnect, rtcTm_t * currTime);

/**
 * @brief
 * Function to get LTE Timing Advance from modem
 *
 * @details
 * Function to get LTE Timing Advance from modem
 *
 * @param[in]   skConnect Pointer to skConnect_t context
 * @param[out]  TA        Pointer to Timing Advance value
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemGetLteTa(skConnect_t *skConnect, int16_t * TA);

/**
 * @brief
 * Function to query current modem state for NBIOT vs. GNSS (GNSS: 0, LTE: 1)
 *
 * @details
 * Function to query current modem state for NBIOT vs. GNSS (GNSS: 0, LTE: 1)
 *
 * @param[in] skConnect Pointer to skConnect_t context
 *
 * @return  Modem NBIOT / GNSS state
 */
modemGnssLteState_e halModemGetGnssNbiotState(skConnect_t *skConnect);

/**
 * @brief
 * Function to configure satellite channel raster offset
 *
 * @details
 * Function to configure satellite channel raster offset
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 * @param[in]  ulOffset  offset
 * @param[in]  dlOffset  offset
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemSetcfgSatChanRaster(skConnect_t *skConnect, uint16_t ulOffset, uint16_t dlOffset);

/**
 * @brief
 * Function to get RRC connection status
 *
 * @details
 * Function to get RRC connection status
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Boolean. True if connected, false if not
 */
modemRrcState_e halModemIsRrcConnected(skConnect_t *skConnect);

/**
 * @brief
 * Function to query RRC state directly from modem synchronously
 *
 * @details
 * Function to query RRC state directly from modem synchronously
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Modem RRC State
 */
modemRrcState_e halModemQueryRrcState(skConnect_t *skConnect);

/**
 * @brief
 * Function to query Cell ID directly from modem synchronously
 *
 * @details
 * Function to query Cell ID directly from modem synchronously
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemQueryCellId(skConnect_t *skConnect);

/**
 * @brief
 * Function to get current modem cell id
 *
 * @details
 * Function to get current modem cell id
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Modem cell id
 */
uint32_t halModemGetCellId(skConnect_t *skConnect);

/**
 * @brief
 * Function to query current modem state for LTE vs. GNSS (GNSS: 0, LTE: 1)
 *
 * @details
 * Function to query current modem state for LTE vs. GNSS (GNSS: 0, LTE: 1)
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  Modem LTE / GNSS state
 */
modemGnssLteState_e halModemGetGnssLteState(skConnect_t *skConnect);

/**
 * @brief
 * Function to create new skConnect_t object
 *
 * @details
 * Function to create new skConnect_t object
 *
 * @param[in]  skConnect      Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemNew(skConnect_t *skConnect);

/**
 * @brief
 * Function to enter online mode. Rx only
 *
 * @details
 * Function to enter online mode. Rx only
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  None
 */
void halModemEnterOnlineMode(skConnect_t *skConnect);

/**
 * @brief
 * Function to enter offline mode. Rx/Tx
 *
 * @details
 * Function to enter offline mode. Rx/Tx
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  None
 */
void halModemEnterOfflineMode(skConnect_t *skConnect);

/**
 * @brief
 * Function to get current modem state
 *
 * @details
 * Function to get current modem state
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  current modem state
 */
modemSkyloState_e altairModemGetCurrentState(skConnect_t *skConnect);

/**
 * @brief
 * Function to set current modem state
 *
 * @details
 * Function to set current modem state
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 * @param[in]  state    modem state to be set
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t altairModemSetCurrentState(skConnect_t *skConnect, modemSkyloState_e state);

/**
 * @brief
 * Function to send AT command to check modem is online or not
 *
 * @details
 * Function to Send AT command to check modem is online or not
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemOnlineCheck(skConnect_t *skConnect);

/**
 * @brief
 * Function to disable Modem GNSS
 *
 * @details
 * Function to disable Modem GNSS
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t halModemDeactivateGnss(skConnect_t *skConnect);

/**
 * @brief
 * Function to get Modem Pmp and Np version from Modem firmware version
 *
 * @details
 * Function to get Modem Pmp and Np version from Modem firmware version
 *
 * @param[in]  skConnect Pointer to skConnect_t context
 * @param[in]  atCommand Buffer of Command to be load
 *
 * @return  pointer on success, NULL on failure
 */
const char *halModemPmpNpVer(skConnect_t *skConnect, const char * atCommand);

/**
 * @brief
 * Function to transfer U-Boot print on AT channel(UART0)
 *
 *
 * @details
 * Function to transfer U-Boot print on AT channel, Must have to disable Rx modem thread before enable boot mode
 *
 * @param[in]   skConnect Pointer to skConnect structure
 * @param[in]   Channel   Channel of UART to transfer boot mode
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t transferBootMode(skConnect_t *skConnect, uint32_t channel);

/**
 * @brief
 * Function to get Modem flash partition info (UART0)
 *
 * @details
 * Function to get Modem flash partition info (UART0)
 *
 * @param[in]   skConnect     Pointer to skConnect structure
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t getFlashPartitionInfo(skConnect_t *skConnect);

/**
 * @brief
 * Function to set the baudrate in U-Boot
 *
 * @details
 * Function to set the baudrate in U-Boot
 *
 * @param[in]   skConnect Pointer to skConnect structure
 * @param[in]   baudrate  Baudrate need to set
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t setBaudrateInfo(skConnect_t *skConnect, uint32_t baudrate);

/**
 * @brief
 * Function to set Modem flow control info (UART0)
 *
 * @details
 * Function to set set the flow control in U-Boot mode
 *
 * @param[in]   skConnect Pointer to skConnect structure
 * @param[in]   status    1 - on , 0 - off
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
skStatus_t setFlowcontrolInfo(skConnect_t *skConnect, uint8_t status);

/**
 * @brief
 * Function to Enter boot mode. U-Boot only
 *
 * @details
 * Function Enter boot mode. U-Boot only
 *
 * @param[in]   skConnect     Pointer to skConnect structure
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
void halModemEnterBootMode(skConnect_t *skConnect);

/**
 * @brief
 * Function to Enter at channel mode. at channel only
 *
 * @details
 * Function to Enter at channel mode. at channel only
 *
 * @param[in]   skConnect     Pointer to skConnect structure
 *
 * @return  0 on ok and error code if negative; one of the following values:
 *          @par
 *          SK_MODEM_OK on success. \n
 *          SK_MODEM_ERROR on failure.
 *          For more error check SKModemStatus.h
 */
void halModemEnterAtMode(skConnect_t *skConnect);

/**
 * @brief
 * Function to Give status of boot mode type
 *
 * @details
 * Function to  Give status of boot mode type
 *
 * @param[in]   skConnect     Pointer to skConnect structure
 *
 * @return  true or false ( true -> U-Boot mode and False -> AT channel mode )
 */
skBool_t halModemGetBootMode(skConnect_t *skConnect);
#endif

