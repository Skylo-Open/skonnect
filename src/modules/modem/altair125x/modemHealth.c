/**
 * @file      : modemHealth.c
 * @brief     : Modem Health Monitoring Thread
 * @author    : Barry Winata (barry@skylo.tech)
 *              Dharmesh Vasoya (dharmesh@skylo.tech)
 * @copyright : Copyright (c) 2023 SkyloTechnologies, Inc.
 */

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <modemCore.h>
#include <modemHal.h>
#include <modemHealth.h>
//#include <modemSat.h>
#include <skUtils.h>
#include <appThread.h>

#include <skLog.h>
#include <skModemStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Macros
 *-----------------------------------------------------------------------*/

#define Skylo_Printf(...) _sk_log(modemHealth, __VA_ARGS__)
/* ms */
#define STUCK_STATE_TIMEOUT_MS      (60*7*1000)
#define NW_ATTACH_FAIL_CNTER        (5*60*1000)/2000 /* (min * 60sec * 1000ms) / 2000 (sleep) */
#define MODEM_IGNSS_NA_TIMEOUT      (60*1000*5)      /* 60 sec * 1000ms * 5 mins */

#define SWITCH_NON_ATTACH_CNTER     (15*60*1000)    /* in non attach state counter */

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

extern atCommandResponse_t gCommandList[];
static uint8_t gSatConnMode = 0;
static skTime_t gInssRecentTs = 0;

/**
 * @brief
 * Function to monitor modem IGNSS health
 *
 * @details
 * Function to monitor modem IGNSS health
 *
 * @param[in] skConnect Pointer to skConnect_t context
 *
 * @return 0 on ok and error code if negative; one of the following values:
 *               @par
 *               SK_MODEM_OK on success. \n
 *               SK_MODEM_ERROR on failure. \n
 *               For more error check SKModemStatus.h
 */
static skStatus_t monitorIgnssHealth(skConnect_t *skConnect)
{
    if ( (skGetTimeInMsec() - gInssRecentTs) >= MODEM_IGNSS_NA_TIMEOUT ) {

        Skylo_Printf(SKYLO_WARNING_E, "Waiting for IGNSS NMEAs since %d ms ago."_(" Resetting modem...")"\n",
                                    MODEM_IGNSS_NA_TIMEOUT);
        gInssRecentTs = skGetTimeInMsec();

        if ( altairModemReset(skConnect) != SK_MODEM_OK ) {

            Skylo_Printf(SKYLO_CRIT_E, _("Cannot hard reset modem\n"));
            return SK_MODEM_ERROR;
        }
    }
    return SK_MODEM_OK;
}

/**
 * @brief
 * Function to monitor modem crash
 *
 * @details
 * Function to monitor modem crash
 *
 * @param[in] skConnect Pointer to skConnect_t context
 *
 * @return None
 */
void monitorModemCrash(skConnect_t *skConnect)
{
    if ( skConnect->m->isModemCrash ) {

        Skylo_Printf(SKYLO_CRIT_E, "After Modem Crash"_(" -- Going For hard reset modem")"\n");
        if ( altairModemReset(skConnect) != SK_MODEM_OK )
            Skylo_Printf(SKYLO_CRIT_E, "After Modem Crash"_(" -- Cannot hard reset modem")"\n");

        skConnect->m->isModemCrash = 0;
        altairModemSetCurrentState(skConnect, MODEM_STATE_ACQUIRING_GNSS_E);
    }
}

/**
 * @brief
 * Function to monitor bad state and correct it
 *
 * @details
 * Function to monitor bad state and correct it
 *
 * @param[in] skConnect Pointer to skConnect_t context
 * @param[in] currState Enum for current satellite connection mode
 *
 * @return None
 */
static void monitorBadState(skConnect_t *skConnect, modemSkyloState_e currState)
{
    static modemSkyloState_e prevState = MODEM_STATE_ACQUIRING_GNSS_E;
    static skTime_t startTime = 0;

    if ( prevState != currState )
        startTime = skGetTimeInMsec();

    prevState = currState;

    if ( skGetTimeInMsec() - startTime >= STUCK_STATE_TIMEOUT_MS ) {

        switch ( currState ) {

            case MODEM_STATE_ACQUIRING_GEO_SYNC_E:

                if ( altairModemReset(skConnect) != SK_MODEM_OK )
                    Skylo_Printf(SKYLO_CRIT_E, "Cannot hard reset modem\n");

                /* Fall through */
            case MODEM_STATE_UE_IDLE_NW_DETACHED_E:
                /* Fall through */
            case MODEM_STATE_UE_IDLE_NW_ATTACHED_E:

                altairModemSetCurrentState(skConnect, MODEM_STATE_ACQUIRING_GNSS_E);
                Skylo_Printf(SKYLO_NOTICE_E, "stuck in state: %d\n", currState);
                break;

            default:
                break;
        }
    }
}

static modemSkyloState_e evaluteRrcCheck(skConnect_t *skConnect, modemSkyloState_e currState)
{
    modemSkyloState_e new_state = currState;

#if 0
    Skylo_Printf(SKYLO_NOTICE_E, " RRC state = %d\n", halModemQueryRrcState(skConnect));
#endif
    modemRrcState_e queriedRrcState = halModemQueryRrcState(skConnect);

    if ( skConnect->m->rrcState != queriedRrcState ) {

        Skylo_Printf(SKYLO_INFO_E, _("RRC: Mismatch. Curr state = ")"%d; "_("Queried State = ")"%d\n",
                        skConnect->m->rrcState, queriedRrcState);
        skConnect->m->rrcState = queriedRrcState;
    }

    switch ( skConnect->m->rrcState ) {

        case UE_RRC_IDLE_E:

            switch ( currState ) {

                case MODEM_STATE_UE_CONN_NW_ATTACHED_E:

                    new_state = MODEM_STATE_UE_IDLE_NW_ATTACHED_E;
                    break;
                case MODEM_STATE_UE_CONN_NW_DETACHED_E:

                    new_state = MODEM_STATE_UE_DICONN_NW_DETACHED_E;

                default:
                    break;
            }
            break;

        case UE_RRC_CONNECTED_E:

            switch ( currState ) {

                case MODEM_STATE_UE_IDLE_NW_DETACHED_E:

                    new_state = MODEM_STATE_UE_CONN_NW_DETACHED_E;
                    break;

                case MODEM_STATE_UE_IDLE_NW_ATTACHED_E:

                    new_state = MODEM_STATE_UE_CONN_NW_ATTACHED_E;
                    break;

                default:
                    break;
            }
            break;

        case UE_RRC_UNKNOWN_E:

            switch ( currState ) {

                case MODEM_STATE_UE_CONN_NW_ATTACHED_E:
                case MODEM_STATE_UE_CONN_NW_DETACHED_E:
                case MODEM_STATE_UE_IDLE_NW_ATTACHED_E:

                    new_state = MODEM_STATE_UE_DICONN_NW_DETACHED_E;
                    break;

                default:
                    break;
            }
            break;
    }

    /* Override new state if SINR is really bad */
    if ( halModemReadSinr(skConnect) == -128 ) {

        switch ( currState ) {

            case MODEM_STATE_WAITING_FOR_VALID_SINR_E:
            case MODEM_STATE_UE_IDLE_NW_DETACHED_E:
            case MODEM_STATE_UE_CONN_NW_ATTACHED_E:
            case MODEM_STATE_UE_IDLE_NW_ATTACHED_E:
            case MODEM_STATE_UE_CONN_NW_DETACHED_E:

                new_state = MODEM_STATE_UE_DICONN_NW_DETACHED_E;
                break;

            default:
                break;
        }
    }

    return new_state;
}

/**
 * @brief
 * Function to connect to base station
 *
 * @details
 * Function to connect to base station
 *
 * @param[in] skConnect Pointer to skConnect_t context
 *
 * @return 0 on ok and error code if negative; one of the following values:
 *               @par
 *               SK_MODEM_OK on success. \n
 *               SK_MODEM_ERROR on failure. \n
 *               For more error check SKModemStatus.h
 */
static skStatus_t attach2Network(skConnect_t *skConnect)
{
    skStatus_t stat = SK_MODEM_ERROR;

    stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_ENABLE_E].command, MODEM_CMD_ENABLE_E, NULL);

    if ( stat != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Attach to Network...FAIL."_(" Trying again")"\n");
        return SK_MODEM_ERROR;
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, _("Attach to Network...OK\n"));
    }
    return SK_MODEM_OK;
}

/**
 * @brief
 * Function to Check if device attached to NW
 *
 * @details
 * Function to connect to base station
 *
 * @param[in] skConnect Pointer to skConnect_t context
 *
 * @return 0 on ok and error code if negative; one of the following values:
 *               @par
 *               SK_MODEM_OK on success. \n
 *               SK_MODEM_ERROR on failure. \n
 *               For more error check SKModemStatus.h
 */
static skStatus_t isAttached2Network(skConnect_t *skConnect)
{
    skStatus_t stat = SK_MODEM_ERROR;

    stat = altairFireCommand(skConnect, gCommandList[MODEM_CMD_CHECK_ATTACH_STATUS_E].command, MODEM_CMD_CHECK_ATTACH_STATUS_E, NULL);

    if ( stat != SK_MODEM_OK ) {

        Skylo_Printf(SKYLO_ERR_E, "Attach to Network check...FAIL."_(" Trying again")"\n");
        return SK_MODEM_ERROR;
    } else {

        Skylo_Printf(SKYLO_NOTICE_E, _("Attach to Network check...OK\n"));
    }
    return SK_MODEM_OK;
}

/**
 * @brief
 * Modem health monitor thread
 *
 * @details
 * This thread monitors modem health and maintains connectivity and do state maintenance.
 *
 * @param[in] arg Pointer to skConnect_t context
 *
 * @return None
 */
void *altairModemHealthMonitorThread(void * arg)
{
    if ( !arg ) {

        Skylo_Printf(SKYLO_ERR_E, "Invalid argument"_("Stopping Modem Health Monitor thread")"\n");
        skyloAppThreadStop(NULL, APP_THREAD_MODEM_HEALTH_INDEX_NO);
        return NULL;
    }

    Skylo_Printf(SKYLO_DEBUG_E, _(">><< Health Monitor Thread Started\n"));
    skConnect_t *skConnect = (skConnect_t *)arg;
//    int32_t status = 0;
    int16_t sinr = 0, modemMetaCnter = 0;
    modemSkyloState_e state = MODEM_STATE_CELL_CAMPING_E;

    uint8_t /*gpsFailCnter = 0,*/ nwFailAttachCnter = 0;

    /* State Machine */
    for( ;; ) {

        while ( !halModemIsInitialized(skConnect) ) {

            Skylo_Printf(SKYLO_ERR_E, _(MODEM_UNINIT_ERR_MSG)" HLTH");
            skMSecSleep(5000);
        }

        state = altairModemGetCurrentState(skConnect);
        switch ( state ) {

            case MODEM_STATE_ACQUIRING_GNSS_E:

                Skylo_Printf(SKYLO_NOTICE_E, _("STATE: MODEM_STATE_ACQUIRING_GNSS_E\n"));
                // if ( (status = altairGetGpsFix(skConnect)) != SK_MODEM_OK ) {

                //     Skylo_Printf(SKYLO_ERR_E, _("Trying again, Got GPS Fix...FAIL with ")"errorno %"PRId32"\n", status);
                //     gpsFailCnter++;
                //     if ( gpsFailCnter >= 5 || status == SK_MODEM_ERR_FUN_OFF ) {

                //         Skylo_Printf(SKYLO_ERR_E, _("GPS query...FAIL. ")"errno %d\n",
                //                                 SK_RESET_REASON_MODEM_FAIL_QUERY_GPS);
                //         if ( altairModemReset(skConnect) != SK_MODEM_OK ) {

                //             Skylo_Printf(SKYLO_ERR_E, "Cannot reset modem - GPS query fail\n");
                //             Skylo_Printf(SKYLO_EMERG_E, "Reset hub with reason %d\n", SK_RESET_REASON_MODEM_NO_GPS);
                //             skSetSystemResetReason(SK_RESET_REASON_MODEM_FAIL_QUERY_GPS);
                //         }
                //         gpsFailCnter = 0;
                //     }
                //     altairModemSetCurrentState(skConnect, MODEM_STATE_ACQUIRING_GNSS_E);
                // } else {

                    // gpsFailCnter = 0;
                    // Skylo_Printf(SKYLO_NOTICE_E, _("Got GPS Fix...OK\n"));
                    Skylo_Printf(SKYLO_NOTICE_E, _("Bypasssing GPS FIX state and Cell camp\n"));
                    altairModemSetCurrentState(skConnect, MODEM_STATE_WAITING_FOR_VALID_SINR_E);
                // }
                break;

            case MODEM_STATE_CELL_CAMPING_E:

                Skylo_Printf(SKYLO_NOTICE_E, _("STATE: MODEM_STATE_CELL_CAMPING_E\n"));
                if ( altairFireCommand(skConnect, gCommandList[MODEM_CMD_FUN_ON_E].command, MODEM_CMD_FUN_ON_E, NULL)
                                            != SK_MODEM_OK ) {

                    Skylo_Printf(SKYLO_ERR_E, "Turning Cell Camp On...FAIL. Trying again\n");
                    altairModemSetCurrentState(skConnect, MODEM_STATE_CELL_CAMPING_E);
                } else {

                    Skylo_Printf(SKYLO_NOTICE_E, _("Turning Cell Camp On...OK\n"));
                    // altairModemSetCurrentState(skConnect, MODEM_STATE_ACQUIRING_GEO_SYNC_E);
                    altairModemSetCurrentState(skConnect, MODEM_STATE_WAITING_FOR_VALID_SINR_E);
                    skMSecSleep(1000);
                }
                break;

            case MODEM_STATE_ACQUIRING_GEO_SYNC_E:

                Skylo_Printf(SKYLO_NOTICE_E, _("STATE: MODEM_STATE_ACQUIRING_GEO_SYNC_E\n"));
                // if ( (status = altairWaitForGeoSync(skConnect) != SK_MODEM_OK) ) {

                //     Skylo_Printf(SKYLO_ERR_E, "Waiting for GEO Sync...Unavailable. Trying again\n");
                //     altairModemSetCurrentState(skConnect, MODEM_STATE_ACQUIRING_GEO_SYNC_E);
                // } else {

                //     Skylo_Printf(SKYLO_NOTICE_E, _("Waiting for GEO Sync...OK. Let's go!\n"));
                //     altairModemSetCurrentState(skConnect, MODEM_STATE_WAITING_FOR_VALID_SINR_E);
                // }
                altairModemSetCurrentState(skConnect, MODEM_STATE_WAITING_FOR_VALID_SINR_E);
                break;

            case MODEM_STATE_WAITING_FOR_VALID_SINR_E:

                Skylo_Printf(SKYLO_NOTICE_E, _("STATE: MODEM_STATE_WAITING_FOR_VALID_SINR_E\n"));
                if ( (sinr = halModemReadSinr(skConnect)) >= SINR_GOOD_THRESHOLD ) {

                    Skylo_Printf(SKYLO_NOTICE_E, _("Got Good SINR...OK. ")"SINR = %d\n", sinr);
                    altairModemSetCurrentState(skConnect, MODEM_STATE_UE_IDLE_NW_DETACHED_E);
                } else {

                    altairModemSetCurrentState(skConnect, MODEM_STATE_WAITING_FOR_VALID_SINR_E);
                    Skylo_Printf(SKYLO_NOTICE_E, _("SINR not great. Keep waiting. ")"SINR = %d\n", sinr);
                }
                break;

            case MODEM_STATE_UE_IDLE_NW_DETACHED_E:

                Skylo_Printf(SKYLO_NOTICE_E, _("STATE: MODEM_STATE_UE_IDLE_NW_DETACHED_E\n"));
                // if ( attach2Network(skConnect) != SK_MODEM_OK )
                //     altairModemSetCurrentState(skConnect, MODEM_STATE_UE_IDLE_NW_DETACHED_E);
                // else
                //     altairModemSetCurrentState(skConnect, MODEM_STATE_UE_CONN_NW_ATTACHED_E);
                
                ///explicit attach check if Unsolicited PDN ACT 1 is missed
                isAttached2Network(skConnect);
                break;

            case MODEM_STATE_UE_CONN_NW_ATTACHED_E:

                Skylo_Printf(SKYLO_NOTICE_E, _("STATE: MODEM_STATE_UE_CONN_NW_ATTACHED_E\n"));
                /* NB: This will not prevent against stale GNSS NMEA URCs
                 * (the underlying RC needs support from Altair)
                 */
                skConnect->m->modemResetCnter = 0;

                if (skConnect->m->attachedState == PDN_ATTACH_E )
                    altairModemSetCurrentState(skConnect, MODEM_STATE_UE_CONN_NW_ATTACHED_E);
                else
                    altairModemSetCurrentState(skConnect, MODEM_STATE_UE_CONN_NW_DETACHED_E);
                break;

            case MODEM_STATE_UE_IDLE_NW_ATTACHED_E:

                Skylo_Printf(SKYLO_NOTICE_E, _("STATE: MODEM_STATE_UE_IDLE_NW_ATTACHED_E\n"));
                /* NB: This will not prevent against stale GNSS NMEA URCs
                 * (the underlying RC needs support from Altair)
                 */
                skConnect->m->modemResetCnter = 0;
                altairModemSetCurrentState(skConnect, MODEM_STATE_UE_IDLE_NW_ATTACHED_E);
                break;

            case MODEM_STATE_UE_CONN_NW_DETACHED_E:

                Skylo_Printf(SKYLO_NOTICE_E, _("STATE: MODEM_STATE_UE_CONN_NW_DETACHED_E\n"));
                if ( skConnect->m->attachedState == PDN_ATTACH_E ) {

                    altairModemSetCurrentState(skConnect, MODEM_STATE_UE_CONN_NW_ATTACHED_E);
                    nwFailAttachCnter = 0;
                } else {

                    nwFailAttachCnter++;
                    // if ( attach2Network(skConnect) != SK_MODEM_OK )
                    //     altairModemSetCurrentState(skConnect, MODEM_STATE_UE_CONN_NW_DETACHED_E);

                    ///explicit attach check if Unsolicited PDN ACT 1 is missed
                    isAttached2Network(skConnect);

                    if ( nwFailAttachCnter >= NW_ATTACH_FAIL_CNTER ) {

                        nwFailAttachCnter = 0;
                        altairModemSetCurrentState(skConnect, MODEM_STATE_ACQUIRING_GNSS_E);
                    }
                }
                break;

            case MODEM_STATE_UE_DICONN_NW_DETACHED_E:

                Skylo_Printf(SKYLO_NOTICE_E,_("STATE: MODEM_STATE_UE_DICONN_NW_DETACHED_E\n"));
                altairModemSetCurrentState(skConnect, MODEM_STATE_ACQUIRING_GNSS_E);
                break;

            case MODEM_STATE_UE_DICONN_NW_ATTACHED_E:

                Skylo_Printf(SKYLO_NOTICE_E,_("STATE: MODEM_STATE_UE_DICONN_NW_ATTACHED_E\n"));
                altairModemSetCurrentState(skConnect, MODEM_STATE_ACQUIRING_GNSS_E);
                break;

            case MODEM_STATE_GNSS_UPDATE_E:

                Skylo_Printf(SKYLO_NOTICE_E, _("STATE: MODEM_STATE_GNSS_UPDATE_E\n"));
                break;

            case MODEM_STATE_GNSS_EPH_UPDATE_E:

                Skylo_Printf(SKYLO_NOTICE_E, _("STATE: MODEM_STATE_GNSS_EPH_UPDATE_E\n"));
                skConnect->m->gpsEphupdStat = true;
                altairModemSetCurrentState(skConnect, MODEM_STATE_ACQUIRING_GNSS_E);
                break;

            default:
                Skylo_Printf(SKYLO_ERR_E, _("STATE: Invalid = ")"%d\n", state);
        }

        /* Check current RRC states and evaluate next state */
        altairModemSetCurrentState(skConnect, evaluteRrcCheck(skConnect, altairModemGetCurrentState(skConnect)));
        skMSecSleep(100);

        monitorBadState(skConnect, altairModemGetCurrentState(skConnect));
        skMSecSleep(100);
        
        /* read all signal states in one go */
        halModemGetAllSignalLevels(skConnect);

        Skylo_Printf(SKYLO_NOTICE_E, "state:%d, RRC:%d, NW:%d, SINR:%d, RSRP:%d, RSRQ:%d\n",
            altairModemGetCurrentState(skConnect),
            halModemIsRrcConnected(skConnect),
            halModemIsAttached(skConnect),
            halModemReadSinr(skConnect),
            halModemReadRsrp(skConnect),
            halModemReadRsrq(skConnect));

        // if ( skConnect->m->ratActImg == NBIOT_IMAGE_NTN_E )
        //     Skylo_Printf(SKYLO_NOTICE_E, "EARFCN:%"PRIu32"\n",halModemGetEarfcn(skConnect));

                
        skMSecSleep(9800);

        modemMetaCnter++;
        // if ( modemMetaCnter >= 9 ) {

        //     /* 3600sec (hour) / 2 = 1800sec (half hour) / 2 = 900  (every 2 seconds) */
        //     Skylo_Printf(SKYLO_NOTICE_E, "IMEI:%s, IMSI:%s, ICCID:%s, VER:%s\n", skConnect->m->imei,
        //                     skConnect->m->sim.imsi, skConnect->m->sim.iccid, skConnect->m->nbiotRkVersion);
        //     modemMetaCnter = 0;
        // }
        
        // skMSecSleep(12000);

        // /* Monitor I-GNSS health */
        // if ( monitorIgnssHealth(skConnect) != SK_MODEM_OK ) {

        //     Skylo_Printf(SKYLO_ERR_E, "Cannot monitor I-GNSS health. Resetting hub...\n");
        //     skSetSystemResetReason(SK_RESET_REASON_MODEM_IGNSS_NA);
        // }
    }
}

/* PUBLIC */
skStatus_t setSatConnMode(modemConnState_e connMode)
{

    skStatus_t status = SK_MODEM_ERROR;

    if ( connMode >= MODEM_STATE_MAX_E ) {

        Skylo_Printf(SKYLO_ERR_E, "Invalid connection state type\n");
        return SK_MODEM_ERROR;
    }

    gSatConnMode = connMode;

    switch ( gSatConnMode ) {

        case MODEM_STATE_SAT_E:

            Skylo_Printf(SKYLO_INFO_E, _("Modem connection type changed to ")"mode %d\n", connMode);
            status = SK_MODEM_OK;
            break;

        case MODEM_STATE_LTE_E:

            Skylo_Printf(SKYLO_INFO_E, _("Modem connection type changed to ")"mode %d\n", connMode);
            status = SK_MODEM_OK;
            break;

        case MODEM_STATE_BAS_E:

            Skylo_Printf(SKYLO_INFO_E, _("Modem connection type changed to ")"mode %d\n", connMode);
            status = SK_MODEM_OK;
            break;

        default:

            Skylo_Printf(SKYLO_ERR_E, _("Modem connection type changed to ")"invalid mode %d\n", connMode);
            status = SK_MODEM_ERROR;
            break;
    }

    return status;
}

modemConnState_e getSatConnMode(void)
{
    return gSatConnMode;
}

void strokeIgnssHealth(void)
{
    gInssRecentTs = skGetTimeInMsec();
}



