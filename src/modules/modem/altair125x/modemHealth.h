/**
 * @file        modemHealth.h
 * @brief       Modem Health Monitoring Thread
 * @author      Barry Winata (barry@skylo.tech)
 *              Dharmesh Vasoya (dharmesh@skylo.tech)
 * @copyright   Copyright (c) 2023 SkyloTechnologies, Inc.
 */

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __MODEM_HEALTH_H__
#define __MODEM_HEALTH_H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skStatus.h>
#include <skModemStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/
#define SINR_GOOD_THRESHOLD           (-9)
/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/**
 * @enum
 * Modem connectivity state enum
 *
 * @details
 * Modem connectivity state enum
 */
typedef enum _modem_conn_state_e_ {
    MODEM_STATE_SAT_E,  /**< State for modem satellite mode */
    MODEM_STATE_LTE_E,  /**< State for modem LTE mode */
    MODEM_STATE_BAS_E,  /**< //TODO findout what it means */
    MODEM_STATE_MAX_E,  /**< Max state for modem network Mode */
} modemConnState_e;

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/**
 * @brief
 * Function to set satellite connection mode(SAT/LTE/BAS).
 *
 * @details
 * Function to set satellite connection mode(SAT/LTE/BAS)
 *
 * @param[in] connMode Enum for satellite connection change mode
 *
 * @return 0 on ok and error code if negative; one of the following values:
 *               @par
 *               SK_MODEM_OK on success. \n
 *               SK_MODEM_ERROR on failure. \n
 *               For more error check SKModemStatus.h
 */
skStatus_t setSatConnMode(modemConnState_e connMode);


/**
 * @brief
 * Function to get satellite connection mode(SAT/LTE/BAS).
 *
 * @details
 * Function to get satellite connection mode(SAT/LTE/BAS)
 *
 * @return satConnMode  Static variable value based upon modem connection state
 */
modemConnState_e getSatConnMode(void);

/**
 * @brief
 * Stroke I-GNSS timer upon every receipt of a NMEA coordinate
 * i.e. GPRMC, GPGGA, GPGSV etc. from the modem
 *
 * @details
 * If not stroked within timeout period, then there must be something
 * wrong with the I-GNSS module of the modem. Best to reset the modem
 * in this case and reacquire GNSS.
 *
 * @return None
 *
 */
void strokeIgnssHealth(void);

#endif
