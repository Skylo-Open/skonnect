/**
 * @file        appThread.h
 * @brief       Header file for appThread.c
 * @author      Dharmesh Vasoya (dharmesh@skylo.tech)
 *              Ankit Patel (ankit@skylo.tech)
* @copyright    Copyright (c) 2023 Skylo Technologies, Inc.
 */

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __APP_THREAD_H__
#define __APP_THREAD_H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <stdlib.h>

#include <skConnect.h>
#include <FreeRTOSConfig.h>
#include <skOSPlugin.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/* Thread keep alive(ping) states */
#define APP_THREAD_ALIVE                    (1)
#define APP_THREAD_DEAD                     (0)
#define APP_THREAD_NR                       (2) /* Not responding */

/* thread states */
#define APP_THREAD_RUNNABLE                 (0)     /* thread is runnable, but not created yet */
#define APP_THREAD_RUNNING                  (1)     /* thread is running */
#define APP_THREAD_SUSPEND                  (2)     /* signal to suspend thread (stop all activity and go to sleep untill stop/suspend is called) */
#define APP_THREAD_SUSPEND_READY            (3)     /* signal that thread is ready to be suspended */
#define APP_THREAD_RESUME                   (4)     /* signal to resume thread */
#define APP_THREAD_SUSPENDED                (5)     /* thread is suspended */
#define APP_THREAD_STOPPED                  (6)     /* thread is stopped */

/* periodicity at which to check health of threads */
#define APP_THREAD_HC_TIME                  (2)     /* seconds */

/* default deadlock time */
#define APP_THREAD_MAX_ALLOWED_DL_TIME      (120)    /* seconds */

/* Thread suspend retry parameters */
#define APP_THREAD_SUS_RETRY_COUNT          (5)
#define APP_THREAD_SUS_RETRY_WAIT_S         (1)     /* Seconds */

/* Thread suspend wait sleep time */
#define APP_SUS_WAIT_SLEEP_TIME             (1000)  /* mSec */

/* Below are used define thread which will be used in thread mode */

/* Thread 1 */
#define THREAD_MODEM_FSM_NAME               "MODEM_FSM"
#define THREAD_MODEM_FSM_PRIORITY           (configMAX_PRIORITIES-10)
#define THREAD_MODEM_FSM_STACK_SIZE         (5*256) // Bytes

/* Thread 2 */
#define THREAD_MODEM_HEALTH_NAME            "MODEM_HLT"
#define THREAD_MODEM_HEALTH_PRIORITY        (configMAX_PRIORITIES-9)
#define THREAD_MODEM_HEALTH_STACK_SIZE      (5*256) // Bytes

/* Thread 3 */
#define THREAD_MODEM_RX_NAME                "MODEM_RX"
#define THREAD_MODEM_RX_PRIORITY            (configMAX_PRIORITIES-3)
#define THREAD_MODEM_RX_STACK_SIZE          (5*256) // Bytes

/* Thread 4 */
#define THREAD_SM_RX_NAME                   "App_Rx_TH"
#define THREAD_SM_RX_PRIORITY               (configMAX_PRIORITIES-12)
#define THREAD_SM_RX_STACK_SIZE             (5*256) // Bytes

/* Thread 5 */
#define THREAD_SM_TX_NAME                   "App_Tx_TH"
#define THREAD_SM_TX_PRIORITY               (configMAX_PRIORITIES-12)
#define THREAD_SM_TX_STACK_SIZE             (5*256) // Bytes


/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/**
 * @struct
 * struct to for thread context.
 *
 * @details
 * struct to for thread context.
 */
typedef struct appThreadList_s {

    char threadName[10];                /**< thread name */
    uint16_t threadPriority;            /**< thread priority */
    uint32_t threadStackSize;           /**< thread stack size */
    skThread_t threadHandleId;          /**< thread handle */
    volatile uint8_t keepAliveF;        /**< thread keep alive flag */
    volatile uint8_t suspendF;          /**< thread suspend */
    int32_t sleepTime;                  /**< thread sleep time, while in suspend ready state */
    uint32_t considerDeadlockAfter;     /**< time after which thread should be considered deadlock (in seconds)*/
    volatile uint8_t deadStatusCount;   /**< Number of times thread's keep_alive was dead continuously*/
} appThreadList_t;

/**
 * @enum
 * enum to identify Thread Index.
 *
 * @details
 * enum to identify Thread Index.
 */
typedef enum appThread_e {

    APP_THREAD_MODEM_FSM_INDEX_NO,           /**< Modem FSM Thread*/
    APP_THREAD_MODEM_HEALTH_INDEX_NO,        /**< Modem Health Thread*/
    APP_THREAD_MODEM_RX_INDEX_NO,            /**< Modem RX Thread*/
    APP_THREAD_SM_RX_INDEX_NO,               /**< smodem rx thread */
    APP_THREAD_SM_TX_INDEX_NO,               /**< smodem tx thread */

    /* @note: Kindly add new app thread enum above
    when you rearrange threads index pleaTHREAD_SM_GEO_NAMEse rearrange them in app_thread.c and
    utils.h in reset reasons in modem deadlock section */
    APP_THREADS_MAX_INDEX_NO            /**< Max Index*/
} appThread_e;

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

extern appThreadList_t appThreadList[APP_THREADS_MAX_INDEX_NO];

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/**
 * @brief
 * Function to create the thread
 *
 * @details
 * Function to create the thread
 *
 * @param[in]    skConnect          Skylo connext Context
 * @param[in]    index              enum of thread to create
 * @param[in]    entry point        Function pointer to the thread
 * @param[in]    arg                Arguement
 *
 * @return  0 on success, negative value on failure.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_OK if supported. \n
 *          SK_ERROR if not supported/failure.
 */
int skyloThreadCreate(skConnect_t *skConnect, appThread_e index, void *(*entrypoint) (void *),void *arg);

/**
 * @brief
 * Function to stop thread in app mode only (called by thread itself,self destruct).
 *
 * @details
 * Function to stop thread in app mode only (called by thread itself). Which resets threads context to
 * default one and stops thread.
 *
 * @param[in]    skConnect          Skylo connext Context
 * @param[in]    App_threadIndex    Provide the thread index which is going to suspend.
 *
 * @return  None.
 */
void skyloAppThreadStop(skConnect_t *skConnect, appThread_e appThreadIndex);

/**
 * @brief
 * Function to suspend single thread
 *
 * @details
 * Function to suspend single thread, for mass suspend mode where suspend flag is pre set
 * give massMode argument as 1 other wise 0.
 *
 * @param[in]   skConnect       Skylo connext Context
 * @param[in]   appThreadIndex  Provide the thread enum which is going to suspend.
 * @param[in]   massMode        Whether api called for mass suspend
 * @param[in]   forceSuspend    If api is allowed to suspend thread forcefully (1->allowed, 0-> not)
 *
 * @return  0 on success, negative value on failure.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_OK if supported. \n
 *          SK_ERROR if not supported/failure.
 */
skStatus_t skyloThreadSuspend(skConnect_t *skConnect, appThread_e appThreadIndex, uint8_t massMode, uint8_t forceSuspend);

/**
 * @brief
 * Function to resume single thread
 *
 * @details
 * Function to resume single thread
 *
 * @param[in]   skConnect       Skylo connext Context
 * @param[in]   appThreadIndex  Provide the thread enum which is going to resume.
 *
 * @return  0 on success, negative value on failure.
 *
 * @return  An error code if negative; one of the following values:
 *          @par
 *          SK_OK if supported. \n
 *          SK_ERROR if not supported/failure.
 */
skStatus_t skyloThreadResume(skConnect_t *skConnect, appThread_e appThreadIndex);

#endif
