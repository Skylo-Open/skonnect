/**
 * @file        appThread.c
 * @brief       APIs for SMP engine.
 * @author      Dharmesh Vasoya (dharmesh@skylo.tech)
 *              Ankit Patel (ankit@skylo.tech)
* @copyright    Copyright (c) 2023 Skylo Technologies, Inc.
 */

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <stdio.h>
#include <stdint.h>
#include <appThread.h>
#include <skLog.h>
#include <skUtils.h>

#include <skOSPlugin.h>
#include <skCore.h>

/*-------------------------------------------------------------------------
 *  Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

#define Skylo_Printf(...) _sk_log(thread, __VA_ARGS__)

/*-------------------------------------------------------------------------
 *  Type Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 *  Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

uint8_t gThreadCounter = 0;

appThreadList_t appThreadList[APP_THREADS_MAX_INDEX_NO] =
{
    { /* APP_THREAD_MODEM_FSM_INDEX_NO */
        .threadName = THREAD_MODEM_FSM_NAME,
        .threadPriority = THREAD_MODEM_FSM_PRIORITY,
        .threadStackSize = THREAD_MODEM_FSM_STACK_SIZE,
        .threadHandleId = 0,
        .suspendF = APP_THREAD_RUNNABLE,
        .keepAliveF = APP_THREAD_DEAD,
        .sleepTime = APP_SUS_WAIT_SLEEP_TIME,
        .considerDeadlockAfter = APP_THREAD_MAX_ALLOWED_DL_TIME,
    },
    { /* APP_THREAD_MODEM_HEALTH_INDEX_NO */
        .threadName = THREAD_MODEM_HEALTH_NAME,
        .threadPriority = THREAD_MODEM_HEALTH_PRIORITY,
        .threadStackSize = THREAD_MODEM_HEALTH_STACK_SIZE,
        .threadHandleId = 0,
        .suspendF = APP_THREAD_RUNNABLE,
        .keepAliveF = APP_THREAD_DEAD,
        .sleepTime = APP_SUS_WAIT_SLEEP_TIME,
        .considerDeadlockAfter = APP_THREAD_MAX_ALLOWED_DL_TIME,
    },
    { /* APP_THREAD_MODEM_RX_INDEX_NO */
        .threadName = THREAD_MODEM_RX_NAME,
        .threadPriority = THREAD_MODEM_RX_PRIORITY,
        .threadStackSize = THREAD_MODEM_RX_STACK_SIZE,
        .threadHandleId = 0,
        .suspendF = APP_THREAD_RUNNABLE,
        .keepAliveF = APP_THREAD_DEAD,
        .sleepTime = APP_SUS_WAIT_SLEEP_TIME,
        .considerDeadlockAfter = APP_THREAD_MAX_ALLOWED_DL_TIME,
    },
    { /* APP_THREAD_SM_RX_INDEX_NO */
        .threadName = THREAD_SM_RX_NAME,
        .threadPriority = THREAD_SM_RX_PRIORITY,
        .threadStackSize = THREAD_SM_RX_STACK_SIZE,
        .threadHandleId = 0,
        .suspendF = APP_THREAD_RUNNABLE,
        .keepAliveF = APP_THREAD_DEAD,
        .sleepTime = APP_SUS_WAIT_SLEEP_TIME,
        .considerDeadlockAfter = APP_THREAD_MAX_ALLOWED_DL_TIME,
    },
    { /* APP_THREAD_SM_TX_INDEX_NO */
        .threadName = THREAD_SM_TX_NAME,
        .threadPriority = THREAD_SM_TX_PRIORITY,
        .threadStackSize = THREAD_SM_TX_STACK_SIZE,
        .threadHandleId = 0,
        .suspendF = APP_THREAD_RUNNABLE,
        .keepAliveF = APP_THREAD_DEAD,
        .sleepTime = APP_SUS_WAIT_SLEEP_TIME,
        .considerDeadlockAfter = APP_THREAD_MAX_ALLOWED_DL_TIME,
    }
};

/*-------------------------------------------------------------------------
 * Function Definitions
 *-----------------------------------------------------------------------*/

int skyloThreadCreate(skConnect_t *skConnect, appThread_e index, void *(*entrypoint) (void *),void *arg)
{
    int Result = SK_ERR_INVALID;
    skThreadAttributes_t threadAttribte = {0,};
    skThread_t threadHandle = 0;

    /* arg we are not checking for NULL because default arg is NULL */
    if ( !skConnect || !entrypoint  || index >= APP_THREADS_MAX_INDEX_NO ) {

        Skylo_Printf(SKYLO_ERR_E, "Invalid parameter received\n");
        return SK_ERR_INVALID_PARAM;
    }

    /* if thread is not in runnable thread don't spawn thread */
    if ( appThreadList[index].suspendF != APP_THREAD_RUNNABLE &&
         appThreadList[index].suspendF != APP_THREAD_STOPPED ) {

        Skylo_Printf(SKYLO_ERR_E, "Can't create thread(s), when it is in %d state\n", appThreadList[index].suspendF);
        return SK_ERROR;
    }


    // Skylo_Printf(SKYLO_INFO_E,"APP THREAD Index = %d P = %d SZ = %d \r\n",
    //     index,  appThreadList[index].threadPriority, appThreadList[index].threadStackSize);
    Skylo_Printf(SKYLO_DEBUG_E,"Starting [%s] thread\r\n", appThreadList[index].threadName);
    skConnect->pep.os.thread.fpAttrInit(&threadAttribte);
    skConnect->pep.os.thread.fpAttrSetName(&threadAttribte, (char *)(appThreadList[index].threadName));
    skConnect->pep.os.thread.fpAttrSetPriority(&threadAttribte, appThreadList[index].threadPriority);
    skConnect->pep.os.thread.fpAttrSetStackDepth(&threadAttribte, appThreadList[index].threadStackSize);
    Result = skConnect->pep.os.thread.fpCreate(&threadHandle, &threadAttribte, entrypoint, arg);

    if ( Result == SK_OK ) {

        appThreadList[index].threadHandleId = threadHandle;

        Skylo_Printf(SKYLO_DEBUG_E,"Thread Created : %s with handle ID : %"PRIu64"\n",
                    appThreadList[index].threadName,  appThreadList[index].threadHandleId);

        gThreadCounter++;
    } else {

        Skylo_Printf(SKYLO_ERR_E,"%s, ",
                (char *)(appThreadList[index].threadName));
        if(Result == SK_ERR_INVALID)
            Skylo_Printf(SKYLO_ERR_E,"Fails With Error Invalid\n");
        else if(Result == SK_ERR_NO_MEMORY)
            Skylo_Printf(SKYLO_ERR_E,"Fails With Error Memory\n");
        else if(Result == SK_ERR_NOTALLOWED)
            Skylo_Printf(SKYLO_ERR_E,"Fails With Error NotAllowed\n");
        else
            Skylo_Printf(SKYLO_ERR_E,"Fails With Error Other(%d)\n",Result);
        Skylo_Printf(SKYLO_DEBUG_E,"\r\n");
    }

    return Result;

}

void skyloAppThreadStop(skConnect_t *skConnect, appThread_e appThreadIndex)
{
    if ( !skConnect ) {

        Skylo_Printf(SKYLO_ERR_E, "Invalid parameter\n");
        return;
    }

    skConnect->pep.os.thread.fpStop(appThreadList[appThreadIndex].threadHandleId);
    /* reset context to default one */
    appThreadList[appThreadIndex].threadHandleId = 0;
    appThreadList[appThreadIndex].suspendF = APP_THREAD_STOPPED;
    appThreadList[appThreadIndex].keepAliveF = APP_THREAD_DEAD;
    gThreadCounter--;   /* decrease number of threads running */
    // appThreadList[appThreadIndex].sleepTime = APP_SUS_WAIT_SLEEP_TIME;
}

skStatus_t skyloThreadSuspend(skConnect_t *skConnect, appThread_e appThreadIndex, uint8_t massMode, uint8_t forceSuspend)
{
    int8_t status = SK_ERROR;
    uint32_t timeoutPer = 0;
    uint8_t susReady = 1;
    //appThread_e threadIndex = 0;

    if ( !skConnect || appThreadIndex >= APP_THREADS_MAX_INDEX_NO ) {

        Skylo_Printf(SKYLO_ERR_E, "Invalid parameter received\n");
        return SK_ERR_INVALID_PARAM;
    }

    /* if already suspended */
    if ( appThreadList[appThreadIndex].suspendF == APP_THREAD_SUSPENDED ||
         appThreadList[appThreadIndex].suspendF == APP_THREAD_STOPPED ||
         appThreadList[appThreadIndex].suspendF == APP_THREAD_RUNNABLE  )
        return SK_OK;

    if ( appThreadList[appThreadIndex].threadHandleId > 0 &&
                appThreadIndex < APP_THREADS_MAX_INDEX_NO ) {

        /* for mass suspend mode flag is already set before calling this api, in case thread is spawned
            after masss mode suspend flag set api and if thread is not suspend ready set to suspend
            so it can come to suspend ready when waiting below */
        if ( !massMode  || appThreadList[appThreadIndex].suspendF != APP_THREAD_SUSPEND_READY )
            appThreadList[appThreadIndex].suspendF = APP_THREAD_SUSPEND;

        /* if only thread is alive and responding wait for it to go suspend ready */
        if ( appThreadList[appThreadIndex].keepAliveF != APP_THREAD_NR ) {

            timeoutPer = skGetTimeInMsec();

            while ( skGetTimeInMsec() - timeoutPer <= appThreadList[appThreadIndex].considerDeadlockAfter*1000 ) {

                /* in case thread is spawned after reaching here and is not suspend ready make it suspend
                    so it can come to suspend ready state and gracefully suspended */
                if ( appThreadList[appThreadIndex].suspendF != APP_THREAD_SUSPEND_READY )
                    appThreadList[appThreadIndex].suspendF = APP_THREAD_SUSPEND;

                /* wait for thread to go in suspend ready state */
                if ( appThreadList[appThreadIndex].suspendF == APP_THREAD_SUSPEND_READY )
                    break;
                else
                    skMSecSleep(APP_SUS_WAIT_SLEEP_TIME);
            }

            /* if thread did not go in suspend ready mode */
            if ( skGetTimeInMsec() - timeoutPer >= appThreadList[appThreadIndex].considerDeadlockAfter*1000 ) {

                susReady = 0;

                if ( forceSuspend ) {

                    Skylo_Printf(SKYLO_ALERT_E,"Thread %s did not go in suspend ready mode, it will be force "
                        "suspended\n",appThreadList[appThreadIndex].threadName);
                } else {

                    Skylo_Printf(SKYLO_ALERT_E,"Thread %s did not go in suspend ready mode, system will reboot\n",
                    appThreadList[appThreadIndex].threadName);

                    skSetSystemResetReason(DEADLOCK_RESET_REASON_BASE + appThreadIndex);
                    return SK_ERROR;
                }
                //appThreadList[appThreadIndex].suspendF = APP_THREAD_RESUME;
                //return SK_ERROR;
            }
        } else {

            if ( forceSuspend ) {

                Skylo_Printf(SKYLO_ALERT_E,"Thread %s did not go in suspend ready mode, it will be force "
                    "suspended\n",appThreadList[appThreadIndex].threadName);
            } else {

                Skylo_Printf(SKYLO_ALERT_E,"Thread %s did not go in suspend ready mode, system will reboot\n",
                    appThreadList[appThreadIndex].threadName);

                skSetSystemResetReason(DEADLOCK_RESET_REASON_BASE + appThreadIndex);
                return SK_ERROR;
            }
        }

        /* if suspend ready or force suspend then only suspend */
        if ( susReady || forceSuspend ) {

            status = skConnect->pep.os.thread.fpSuspend(appThreadList[appThreadIndex].threadHandleId);

            if ( SK_OK == status ) {

                appThreadList[appThreadIndex].suspendF = APP_THREAD_SUSPENDED;

                Skylo_Printf(SKYLO_DEBUG_E,"%s thread with id %"PRIu64" has been suspended successfully\n",
                        appThreadList[appThreadIndex].threadName,
                        appThreadList[appThreadIndex].threadHandleId);
            } else {

                appThreadList[appThreadIndex].suspendF = APP_THREAD_RESUME;

                Skylo_Printf(SKYLO_ERR_E,"%s thread with id %"PRIu64"could not suspend with status %d\n",
                        appThreadList[appThreadIndex].threadName,
                        appThreadList[appThreadIndex].threadHandleId, status);
            }
        }
    } else {

        Skylo_Printf(SKYLO_ERR_E,"thread is not running or not found\n");
        return status;

    }
    return status;

}

skStatus_t skyloThreadResume(skConnect_t *skConnect, appThread_e appThreadIndex)
{
    int8_t status = SK_ERROR;

    if ( !skConnect || appThreadIndex >= APP_THREADS_MAX_INDEX_NO ) {

        Skylo_Printf(SKYLO_ERR_E, "Invalid parameter received\n");
        return SK_ERR_INVALID_PARAM;
    }

    /* if not suspended don't resume */
    if ( appThreadList[appThreadIndex].suspendF != APP_THREAD_SUSPENDED ) {

        Skylo_Printf(SKYLO_ERR_E, "Can't resume thread, Thread is not in suspended state(%d)\n",
             appThreadList[appThreadIndex].suspendF);
        return SK_ERROR;
    }

    /* if never started don't resume */
    if( appThreadList[appThreadIndex].suspendF == APP_THREAD_RUNNABLE ) {

        Skylo_Printf(SKYLO_ERR_E, "Can't resume thread, Thread never started (%d)\n", appThreadIndex);
        return SK_OK;
    }

    if ( appThreadList[appThreadIndex].threadHandleId > 0  &&
                appThreadIndex < APP_THREADS_MAX_INDEX_NO ) {

        status = skConnect->pep.os.thread.fpResume(appThreadList[appThreadIndex].threadHandleId);

        if ( SK_OK == status ) {

            /* set state to resume */
            appThreadList[appThreadIndex].suspendF = APP_THREAD_RESUME;

            Skylo_Printf(SKYLO_DEBUG_E,"%s thread with id %"PRIu64" has been resumed successfully\n",
                     appThreadList[appThreadIndex].threadName, appThreadList[appThreadIndex].threadHandleId);
        } else {

            Skylo_Printf(SKYLO_ERR_E,"%s thread with id %"PRIu64" could not resume with status %d\n",
                     appThreadList[appThreadIndex].threadName,
                     appThreadList[appThreadIndex].threadHandleId, status);
        }
    } else {

        Skylo_Printf(SKYLO_ERR_E,"thread is not running or not found\n");
        return status;

    }

    return status;
}