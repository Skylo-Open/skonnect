/**
 * @file        skMsgP.c
 * @brief       APIs for SMP engine.
 * @author      Dharmesh Vasoya (dharmesh@skylo.tech)
 *              Ankit Patel (ankit@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
 */

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Skylo Message Protocol api*/

#include <skLog.h>
#include <skMsgTypes.h>
#include <skMsgP.h>
#include <skCore.h>
#include <modemHal.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/* For controlling unit test*/
#define UNIT_TEST (0)
#define DEBUG(...) _sk_log(smp, __VA_ARGS__)
/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/* skmsg context */
skMsgPContext_t         gSkMsgPContext = {0, };

/* Sk_msg_p stack initialized flag */
static volatile uint8_t gIsStackInitialized = 0;

/* Skylo tx buffer */

skMsgPTxBuffer_t        gSkMsgPTxBuff = {0, };

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

static void skPrintMessageHEADER(skHeader_t* header);

static void skPrintMessageHB(sk_hb_2_t* hbMsg);

static int skMsgPGenerateHeader(skConnect_t *sKonnect, skHeader_t* header, int msgType);

static int skMsgPGenerateAndFillSeqId(skHeader_t* header);

static skStatus_t skMsgPParseMsgReceived(skConnect_t *skConnect,uint8_t *msg, int msgLen);

/*-------------------------------------------------------------------------
 * Function Definitions
 *-----------------------------------------------------------------------*/

uint16_t               gDataSeqId = 0;
uint16_t               gAppHBSeqId = 1;

int skMsgPHb(skConnect_t *sKonnect)
{
    int              status = -5;
    static sk_hb_2_t data   = {0, };


    // once connected allow for TX
    // if ( altairModemGetCurrentState(sKonnect) != MODEM_STATE_UE_IDLE_NW_ATTACHED_E &&
    //     altairModemGetCurrentState(sKonnect) != MODEM_STATE_UE_CONN_NW_ATTACHED_E ) {
        
    //     DEBUG(SKYLO_ERR_E,"skMsgPHb Cant send HB Modem is not connected \n");
    //     return -1;
    // }


    if (0 != skMsgPGenerateHeader(sKonnect,&data.header, 29))
    {

        DEBUG(SKYLO_ERR_E,"Header Generation error \n");
        return -1;
    }
    /* read all signal states in one go */
    status = halModemGetAllSignalLevels(sKonnect);

    if ( status != SK_MODEM_OK ) {

        DEBUG(SKYLO_ERR_E, _("GetAllSignals...FAIL\n"));
    } else {

        DEBUG(SKYLO_NOTICE_E, _("GetAllSignals ...OK\n"));
    }
    data.sModemSinr = (int16_t)skMsgPLitEnd2BigEnd2Byte(halModemReadSinr(sKonnect));
    data.sModemRSRP = (int16_t)skMsgPLitEnd2BigEnd2Byte(halModemReadRsrp(sKonnect));
    data.sModemRSSI = (int16_t)skMsgPLitEnd2BigEnd2Byte(halModemReadRssi(sKonnect));
    data.sModemRSRQ = (int16_t)skMsgPLitEnd2BigEnd2Byte(halModemReadRsrq(sKonnect));

    data.gpsFix     = sKonnect->options->gpsFix;
    data.sLat       = (float)skMsgPLitEnd2BigEnd4ByteFloat(sKonnect->options->ueLong);
    data.sLng       = (float)skMsgPLitEnd2BigEnd4ByteFloat(sKonnect->options->ueLong);

    data.appHBSeqId = (uint16_t)skMsgPLitEnd2BigEnd2Byte(gAppHBSeqId);
    data.timestamp  = (uint32_t)skMsgPLitEnd2BigEnd4Byte(sKonnect->pep.os.time.getGpsEpochInSec_fp);

    skPrintMessageHB(&data);

    const char *imsi = NULL;
    uint8_t *dataActual = NULL;
    uint32_t dataActualLen = 0;

    imsi = halModemGetImsi(sKonnect);
    DEBUG(SKYLO_DEBUG_E,"IMSI [%s]\n",imsi);

    if ( imsi == NULL ) {
        return status;
    }

    dataActual = malloc(sizeof(sk_hb_2_t)+strlen(imsi)+2);

    if ( dataActual == NULL ) {
        return status;
    }

    memcpy(&dataActual[0],imsi,strlen(imsi));
    dataActualLen = strlen(imsi);
    memcpy(&dataActual[dataActualLen],"::",2);
    dataActualLen += + 2;
    memcpy(&dataActual[dataActualLen],(uint8_t*)&data,sizeof(sk_hb_2_t));
    dataActualLen += sizeof(sk_hb_2_t);

    if (0 != (status = skMsgPMsgToSend(sKonnect ,dataActual, dataActualLen)))
    {
        DEBUG(SKYLO_ERR_E,"Could not send the HB Packet\n");
    }
    else
    {
        gDataSeqId++;
    }

    if(dataActual)
        free(dataActual);

    return status;
}

static void skPrintMessageHB(sk_hb_2_t* hbMsg)
{
  DEBUG(SKYLO_DEBUG_E,"====SK_MESSAGE: HB ====\n");

  skPrintMessageHEADER(&hbMsg->header);
  DEBUG(SKYLO_DEBUG_E,"\tSINR        = %d\n", (int16_t)skMsgPLitEnd2BigEnd2Byte(hbMsg->sModemSinr));
  DEBUG(SKYLO_DEBUG_E,"\tRSRP        = %d\n", (int16_t)skMsgPLitEnd2BigEnd2Byte(hbMsg->sModemRSRP));
  DEBUG(SKYLO_DEBUG_E,"\tRSSI        = %d\n", (int16_t)skMsgPLitEnd2BigEnd2Byte(hbMsg->sModemRSSI));
  DEBUG(SKYLO_DEBUG_E,"\tRSRQ        = %d\n", (int16_t)skMsgPLitEnd2BigEnd2Byte(hbMsg->sModemRSRQ));
  DEBUG(SKYLO_DEBUG_E,"\tLat         = %f\n", skMsgPLitEnd2BigEnd4ByteFloat(hbMsg->sLat));
  DEBUG(SKYLO_DEBUG_E,"\tLon         = %f\n", skMsgPLitEnd2BigEnd4ByteFloat(hbMsg->sLng));
  DEBUG(SKYLO_DEBUG_E,"\tFix         = %d\n", hbMsg->gpsFix);

  DEBUG(SKYLO_DEBUG_E,"===========================\n");
}

static void skPrintMessageHEADER(skHeader_t* header)
{
  DEBUG(SKYLO_DEBUG_E,"***HEADER***\n");
  DEBUG(SKYLO_DEBUG_E,"\tMsg Type           = %d\n", header->msgType);
  DEBUG(SKYLO_DEBUG_E,"\tVersion            = %d\n", header->fb.version);
  DEBUG(SKYLO_DEBUG_E,"\tCtrl pln Seq ID    = %d\n", skMsgPLitEnd2BigEnd2Byte(header->controlSeqId));
  DEBUG(SKYLO_DEBUG_E,"\tTimestamp          = %d\n", skMsgPLitEnd2BigEnd4Byte(header->timestamp));
}

static int skMsgPGenerateHeader(skConnect_t *sKonnect, skHeader_t* header, int msgType)
{
  if (!header)
  {

    DEBUG(SKYLO_ERR_E,"Invalid parameter\n");
    return -6;
  }

  /* Fill header data */
  header->msgType    = (uint8_t)msgType;

  header->fb.version = SK_MSG_PROTO_VER_2;
  skMsgPGenerateAndFillSeqId(header);

  header->timestamp  = (uint32_t)skMsgPLitEnd2BigEnd4Byte(sKonnect->pep.os.time.getGpsEpochInSec_fp);

  return 0;
}

static int skMsgPGenerateAndFillSeqId(skHeader_t* header)
{
  if (!header)
  {

    DEBUG(SKYLO_ERR_E,"Invalid parameter\n");
    return -2;
  }

  /* load and increment control plane sequence id */
  header->controlSeqId = (uint16_t)skMsgPLitEnd2BigEnd2Byte(++gSkMsgPContext.controlSeqId);

  return 0;
}

int skMsgPMsgToSend(skConnect_t *sKonnect, uint8_t* msg, int length)
{
  // int status = -1;
  skHeader_t* header    = NULL;
  int         sendState = -1;

  if (!msg)
  {

    DEBUG(SKYLO_ERR_E,"Invalid parameter\n");
    return -6;
  }

  header            = (skHeader_t*)msg;

  gSkMsgPTxBuff.len = 0;

  memset(&gSkMsgPTxBuff.txBuffer, 0x00, sizeof(gSkMsgPTxBuff.txBuffer));

  for (int i = 0; i < length; i++)
  {

    snprintf(&gSkMsgPTxBuff.txBuffer[gSkMsgPTxBuff.len], 3, "%02X", (uint8_t)msg[i]);
    gSkMsgPTxBuff.len += 2;
  }

  DEBUG(SKYLO_DEBUG_E,"[HB-Send] Control Seq ID (%u), Timestamp: %d,"
        "Generated Message (%d)[%s]\n", skMsgPLitEnd2BigEnd2Byte(header->controlSeqId),
        skMsgPLitEnd2BigEnd4Byte(header->timestamp), length,
        gSkMsgPTxBuff.txBuffer);

  sendState = txCircBuffwriteData(sKonnect, (uint8_t*)&gSkMsgPTxBuff.txBuffer, gSkMsgPTxBuff.len);

  if (sendState == 0)
  {
    gAppHBSeqId++;
  }
  else
  {

    DEBUG(SKYLO_ERR_E,"[HB-Send] Modem Failed to send data with status %d\n", sendState);
  }

  // DEBUG(SKYLO_ERR_E,"Send STAT = %d\n", sendState);
  return sendState;
}

skStatus_t skMsgPMsgReceived(skConnect_t *skConnect, uint8_t *msg, int msgLen)
{
    skStatus_t status = SK_ERROR;

    if ( !skConnect ) {

        DEBUG(SKYLO_ERR_E,"Invalid parameter\n");
        return SK_ERR_INVALID_PARAM;
    }

    // if ( skConnect->pep.os.ipc.fpMutexLock(&gSkMsgPContext.mutex) == SK_OK ) {

        /* increase total received msg count */
        gSkMsgPContext.totalRecMsgs++;

        status = skMsgPParseMsgReceived(skConnect, msg, msgLen);

    //     skConnect->pep.os.ipc.fpMutexUnlock(&gSkMsgPContext.mutex);
    // } else {

    //     DEBUG(_("SKMSG context") " mutex lock failed\n");
    // }

    return status;
}

static skStatus_t skMsgPParseMsgReceived(skConnect_t *skConnect,uint8_t *msg, int msgLen)
{
    skHeader_t *header = NULL;

    if ( !skConnect || !msg ||  msgLen < 0 || msgLen < (int)sizeof(skHeader_t)) {

        DEBUG(SKYLO_ERR_E,"Invalid parameter\n");
        return SK_ERR_INVALID_PARAM;
    }

    header = (skHeader_t *)msg;

    // if ( msgLen != ((header->msgLength)) ) {

    //     DEBUG(SKYLO_ERR_E,"Mesage length -> %d does not match header length ->%d\n",
    //         msgLen, header->msgLength);
    //     return SK_ERR_INVALID_PARAM;
    // }

    // Msg sorter
    switch ( header->msgType ) {
        default:
            // Give it to multicast engine
            // if ( header->msgType >= SK_MULTICAST_HL_E && header->msgType <= SK_MULTICAST_MAX_E ) {
            //     sk_mc_handle_MULTICAST_MSGS(msg, msgLen);
            // } else {
                DEBUG(SKYLO_ERR_E, "No known message type found "_("in received SkMsgP_UC Packet")"\n");
            // }
            break;
    }

    return SK_OK;
}

uint16_t skMsgPLitEnd2BigEnd2Byte(uint16_t num)
{
  uint16_t swapped = (num>>8) | (num<<8);
  return swapped;
}

uint32_t skMsgPLitEnd2BigEnd4Byte(uint32_t num)
{
  uint32_t swapped = ((num>>24)&0x000000ff) | // move byte 3 to byte 0
                     ((num<<8)&0x00ff0000) |  // move byte 1 to byte 2
                     ((num>>8)&0x0000ff00) |  // move byte 2 to byte 1
                     ((num<<24)&0xff000000);  // byte 0 to byte 3
  return swapped;
}

float skMsgPLitEnd2BigEnd4ByteFloat(float num)
{
  char* dst, * src;
  float swapped = 0.0;

  src    = (char*)&num;
  dst    = (char*)&swapped;
  dst[0] = src[3];
  dst[1] = src[2];
  dst[2] = src[1];
  dst[3] = src[0];

//   for (int i = 0; i < 4; i++)
//   {
//     DEBUG(SKYLO_DEBUG_E,"src %02hhX, dst %02hhX\n", src[i], dst[i]);
//   }

  return swapped;
}

/* END OF FILE */
