/**
 * @file        skMsgTypes.h
 * @brief       Header file SMP types/definitions.
 * @author      Dharmesh Vasoya (dharmesh@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
 */

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK_MSG_TYPES_H__
#define __SK_MSG_TYPES_H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <stdint.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/* ************************************* Unicast starts ************************************* */

typedef struct skMsgPTxBuffer_s
{

  char txBuffer[512];
  int  len;
}skMsgPTxBuffer_t;


typedef struct __attribute__((packed, aligned(1))) skFb_s
{
  uint8_t version : 5;   
  uint8_t ack_bit : 1;   
  uint8_t reserved : 2; 
} skFb_t;


typedef struct skMsgPContext_s
{     
  uint16_t totalRecMsgs;                  
  uint16_t controlSeqId;                  
}skMsgPContext_t;

typedef struct __attribute__((packed, aligned(1))) skHeader_s
{

  skFb_t   fb;                           
  uint8_t  msgType;                       
  uint16_t controlSeqId;                  
  uint32_t timestamp;                     
} skHeader_t;

typedef struct __attribute__((packed, aligned(1))) sk_hb_2_s
{

  skHeader_t header;                      /**< header */

  int16_t    sModemSinr;                  /**< Signal in dBm(1200-5) */
  int16_t    sModemRSRP;                  /**< Signal in dBm(1200-5) */
  int16_t    sModemRSSI;                  /**< Signal in dBm(1200-5) */
  int16_t    sModemRSRQ;                  /**< Signal in dBm(1200-5) */
  float      sLat;                        /**< GPS latitude */
  float      sLng;                        /**< GPS Longitude */
  int8_t     gpsFix;                      /**< gps fix value */
  uint16_t   appHBSeqId;                  /**< Application sequence ID */
  uint32_t   timestamp;                   /**< timestamp when HB collected */
} sk_hb_2_t;

#endif
