/**
 * @file        skMsgP.h
 * @brief       Header file for skMsgP.c
 * @author      Dharmesh Vasoya (dharmesh@skylo.tech)
 *              Ankit Patel (ankit@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
 */

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __SK_MSG_P_H__
#define __SK_MSG_P_H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <stdint.h>

#include <skMsgTypes.h>
#include <skConnect.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/* Skylo messaging protocol version for unicast */
#define SK_MSG_PROTO_VER_UC_MAJOR        (0)
#define SK_MSG_PROTO_VER_UC_MINOR        (9)

#define SK_MSG_PROTO_VER_2               (1)

/* Skylo messagin protocol version for multicast */
#define SK_MSG_PROTO_VER_MC_MAJOR        (0)
#define SK_MSG_PROTO_VER_MC_MINOR        (1)

/* ack wait timeout for activate, attach and provision */
#define SK_MSG_ALERT_NOTIFY_TIMEOUT      (45)               // second
#define SK_MSG_ACK_TIMEOUT               (60)               // second
#define SK_MSG_ATTACH_TIMEOUT_BASE       (2*60)             // seconds
#define SK_MSG_ATTACH_TIMEOUT_RATIO      (2)
#define SK_MSG_ATTACH_TIMEOUT_ITR_MAX    (2)
#define SK_MSG_PRVSN_ACK_TIMEOUT         (60)               // provision ack wait timeout
#define SK_MSG_PROVISION_TIMEOUT_BASE    (2*60)             // seconds
#define SK_MSG_PROVISION_TIMEOUT_RATIO   (2)
#define SK_MSG_PROVISION_TIMEOUT_ITR_MAX (2)
/* ack wait timeout upper threashold */
#define SK_MSG_ACK_TIMEOUT_UT            (240)              // second

/* New Skylo SMsgP return codes */
#define SK_SUCCESS                       (0x00)
#define SK_CHECKSUM_FAIL                 (0x01)
#define SK_LENGTH_MISMATCH               (0x02)
#define SK_ERR_IGNORE_PACKET             (0x05)
#define SK_MSG_ERROR                     (0x0F)
#define SK_FAILURE                       (0xFF)
#define SK_APP_NO_I_NET                  (0xFE)

#define SK_MSG_P_MAX_EVENT_NO            (24)

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/**
 * @brief
 * Send Attach message to skylo network.
 *
 * @details
 * Send Attach message to skylo network.
 * Hub skylo message protocol stack has to be already started (with skMsgPStartStack() )
 *
 * @param[in]   skConnect_t Skylo connect context
 *
 * @return      0 on success, negative value on failure.
 * @return      An error code if negative; one of the following values:
 *              @par
 *              SK_OK on success. \n
 *              SK_ERROR on failure.
 */
int skMsgPAttach(void);

/**
 * @brief
 * Send heartbeat to skylo network.
 *
 * @details
 * Send heartbeat to skylo network.
 * Hub skylo message protocol stack has to be already started (with skMsgPStartStack() )
 *
 * @param[in]   skConnect_t Skylo connect context
 *
 * @return      0 on success, negative value on failure.
 * @return      An error code if negative; one of the following values:
 *              @par
 *              SK_OK on success. \n
 *              SK_ERROR on failure.
 */
int skMsgPHb(skConnect_t *sKonnect);

/**
 * @brief
 * API to send SMP message at low level interface (i.e. handover to modem APIs).
 *
 * @details
 * API to send SMP message at low level interface (i.e. handover to modem APIs).
 *
 * @param[in]   skConnect_t     Skylo connect context
 * @param[in]   msg             Message to be sent.
 * @param[in]   length          Message length.
 *
 * @return      0 on success, negative value on failure.
 * @return      An error code if negative; one of the following values:
 *              @par
 *              SK_OK on success. \n
 *              SK_ERR_INVALID_PARAM on invalid parameter. \n
 *              SK_ERROR on failure.
 */
int skMsgPMsgToSend(skConnect_t *sKonnect, uint8_t* msg, int length);

/**
 * @brief
 * Indicate the reception of a skylo message protocol message
 * to be called by smodem to give the msg to skylo message protocol client
 *
 * @details
 * Indicate the reception of a skylo message protocol message
 * to be called by smodem to give the msg to skylo message protocol client
 *
 * @param[in]   skConnect_t Skylo connect context
 * @param[out]  msg    	    pointer to message received
 * @param[in]   length     	length of msg received
 *
 * @return      0 on success, negative value on failure.
 * @return      An error code if negative; one of the following values:
 *              @par
 *              SK_OK on success. \n
 *              SK_ERR_INVALID_PARAM on invalid parameter. \n
 *              SK_ERROR on failure.
 */
skStatus_t skMsgPMsgReceived(skConnect_t *skConnect, uint8_t *msg, int msg_len);

/* Helper functions */

/**
 * @brief
 * SMP Helper API to convert Short int/2 byte value to big endian.
 *
 * @details
 * SMP Helper API to convert Short int/2 byte value to big endian.
 *
 * @param[in out]   num Number to be converted
 *
 * @return      Coverted value.
 */
uint16_t skMsgPLitEnd2BigEnd2Byte(uint16_t num);

/**
 * @brief
 * SMP Helper API to convert unsigned int/4 byte value to big endian.
 *
 * @details
 * SMP Helper API to convert unsigned int/4 byte value to big endian.
 *
 * @param[in out]   num Number to be converted
 *
 * @return      Coverted value.
 */
uint32_t skMsgPLitEnd2BigEnd4Byte(uint32_t num);

/**
 * @brief
 * SMP Helper API to convert unsigned int/4 byte float value to big endian.
 *
 * @details
 * SMP Helper API to convert unsigned int/4 byte float value to big endian.
 *
 * @param[in out]   num float Number to be converted
 *
 * @return      Coverted value.
 */
float skMsgPLitEnd2BigEnd4ByteFloat(float num);

#endif
