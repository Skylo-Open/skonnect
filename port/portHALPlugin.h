

/**
* @file         portHALPlugin.h
* @brief        //TODO
* @details      HAL porting layer, this file includes function declaration for modem hardware interface.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __PORT_HAL_PLUGIN__H__
#define __PORT_HAL_PLUGIN__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skStatus.h>

/* stm32 includes */
#include "stm32l496g_discovery.h"


/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

#define PORT_HAL_LINE_BUFF_SIZE  (1024)

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/**
 * @struct      Structure of HAL port layer context
 * @details     Structure to contain HAL port context
 */
typedef struct portHALContext_s {

    uint8_t isInit;
    UART_HandleTypeDef *sHandleModem;
} portHalContext_t;

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/**
 * @brief This function is a porting API to Initialise modem hardware.
 *
 * @details This function is a porting API to Initialise modem hardware.
 * @todo    Add more details
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
skStatus_t portModemHWInit(void);

/**
 * @brief This function is a porting end point to DeInitialise modem hardware.
 *
 * @details This function is a porting end point to DeInitialise modem hardware,
 *          Complete hw level deinit of modem (do reverse order of init)
 * @todo    Add more details
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
skStatus_t portModemHWDeInit(void);

/**
 * @brief This function is a porting API to DeInitialise modem hardware.
 *
 * @details This function is a porting API to DeInitialise modem hardware.
 * @todo    Add more details
 *
 * @param[in] Enable 1 means to enable flow control, 0 means disable flow control
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
skStatus_t portModemSetHwFlowCtrlStatus(uint8_t enable);

/**
 * @brief This function is a porting API to control hardware flow control of UART device.
 *
 * @details This function is a porting API point to control hardware flow control of UART device.
 *
 * @todo    Add more details
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
skStatus_t portModemGetHwFlowCtrlStatus(void);

/**
 * @brief This function is a porting API to send data to modem over hardware interface.
 *
 * @details This function is a porting API to send data to modem over hardware interface.
 *
 * @todo    Add more details
 *
 * @param[in] Cmd     @todo update
 * @param[in] Length  @todo update
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
skStatus_t portModemSend(char *cmd, uint32_t length);

/**
 * @brief This function is a porting API to receive data from modem over hardware interface.
 *
 * @details This function is a porting API to receive data from modem over hardware interface.
 *          LineBuffered Message must only be called with single line at a time.
 *
 * @todo    Add more details
 *
 * @param[out]    LineBufferedMsg     @todo update
 * @param[in,out] Length              @todo update
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
skStatus_t portModemRecv(char *lineBufferedMsg, uint32_t *length);

/**
 * @brief This function is a porting API point to Hard reset modem hardware.
 *
 * @details This function is a porting API point to hard reset modem hardware,
 *          This could be GPIO based modem reset.
 *
 * @todo    Add more details
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skHALErrorCode.
 */
skStatus_t portModemHardReboot(void);


#endif //__PORT_HAL_PLUGIN__H__
