

/**
* @file         portGPSPlugin.h
* @brief        //TODO
* @details      GPS porting layer, this file includes function declaration for GPS interface.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __PORT_GPS_PLUGIN__H__
#define __PORT_GPS_PLUGIN__H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skStatus.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

typedef struct __attribute__((packed, aligned(1))) gpsLoc_s {
    uint8_t valid;
    float lat;
    float lng;
    float altitude;
    float speed;
    uint32_t timeUnixEpoch;
    char ddmmyy[6+1];
} gpsLoc_t;

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/**
 * @brief This function is a porting API to get location data from GPS module.
 *
 * @details This function is a porting API to get location data from GPS module.
 * @todo    Add more details
 *
 * @return  This function shall return 0 on success, and error code on failure.
 *          For return code details @ref skGPSErrorCode.
 */
skStatus_t portGetVLLASTD(uint8_t *valid, float *lat, float *lng, float *altitude, float*speed, uint32_t *timeUnixEpoch, char *ddmmyy);

#endif //__PORT_GPS_PLUGIN__H__
