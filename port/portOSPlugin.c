/**
* @file         portOSPlugin.c
* @brief        //TODO
* @details      Skylo Konnect demo OS porting plugins for linux.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

//#include <pthread.h>
//#include <errno.h>
//#include <semaphore.h>
//#include <time.h>
//#include <sys/time.h>
//#include <unistd.h>
//#include <sys/sysinfo.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <altConsole.h>

#include <portOSPlugin.h>
#include <skStatus.h>
#include <FreeRTOS.h>
#include <FreeRTOSConfig.h>
#include <event_groups.h>
#include <semphr.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

#define PORT_THREAD_ATTR_PRI_DEFAULT  15

#define PORT_THREAD_MIN_PRIO           0
#define PORT_THREAD_MAX_PRIO          configMAX_PRIORITIES
#define PORT_THREAD_ATTR_NAME_LEN     10

typedef long BaseType_t;

/* Unix thread attributes */
typedef struct portThreadAttr_s {
    char name[PORT_THREAD_ATTR_NAME_LEN];    /**< thread name. */
    unsigned short priority;                 /**< thread priority.   */
    unsigned int   stackSize;               /**< thread stack size. */
    void *stackAddr;                        /**< stack address base ptr */
} portThreadAttr_t;

/**
 * @struct      Signal type structure.
 * @details     Represents signal type object.
 * @todo Add more info.
 */
typedef struct portSignal_s {

    uint32_t signals;           /**< for signal bits */
    uint32_t waiting;           /**< mask for signals to wait for */
    uint32_t attribute;         /**< SK_SIG_ATTRIBUTE_WAIT_FOR_ANY | SK_SIG_ATTRIBUTE_WAIT_FOR_ALL | SK_SIG_ATTRIBUTE_CLEAR_MASK */
    void *mutex;                /* mutex for atomic op support */
} portSignal_t;

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Definitions
 *-----------------------------------------------------------------------*/
#if defined (__GNUC__)

uint32_t systemGetIpsr( void ) __attribute__ (( naked ));

uint32_t systemGetIpsr( void )
{
  __asm volatile
  (
		"	mrs r0, ipsr   \n"
		"	bx r14   \n"
  );
}

#endif /* __GNUC__ */

#if defined(__ARMCC_VERSION)

uint32_t systemGetIpsr( void );

__asm uint32_t systemGetIpsr( void )
{
	PRESERVE8

	mrs r0, ipsr
	bx r14
}

#endif /* __ARMCC_VERSION */


skStatus_t portThreadAttrInit(skThreadAttributes_t *attr)
{
    portThreadAttr_t *portAttr = (portThreadAttr_t *)attr;
    portAttr->name[0] = 0;
    portAttr->priority = PORT_THREAD_ATTR_PRI_DEFAULT;
    portAttr->stackSize = 0;
    portAttr->stackAddr = 0;
    return SK_OK;
}

skStatus_t portThreadAttrSetName(skThreadAttributes_t *attr, const char *name)
{
    portThreadAttr_t *portAttr = (portThreadAttr_t *)attr;
    memset(portAttr->name, 0, PORT_THREAD_ATTR_NAME_LEN);
    strncpy(portAttr->name, name, PORT_THREAD_ATTR_NAME_LEN-1);
    return SK_OK;
}

skStatus_t portThreadAttrSetPri(skThreadAttributes_t *attr, uint16_t priority)
{
    portThreadAttr_t *portAttr = (portThreadAttr_t *)attr;
    portAttr->priority = priority;
    return SK_OK;
}

skStatus_t portThreadAttrSetStackSize(skThreadAttributes_t *attr, uint16_t stackSize)
{
    portThreadAttr_t *portAttr = (portThreadAttr_t *)attr;
    portAttr->stackSize = stackSize;
    return SK_OK;
}

skStatus_t portThreadCreate(skThread_t *threadId, skThreadAttributes_t *attr,
        threadHandler_fp entrypoint, void *arg)
{
    portThreadAttr_t *portAttr = (portThreadAttr_t *) attr;

    int result = SK_ERROR;
    //    struct sched_param param;

    if ( (portAttr->priority > (PORT_THREAD_MAX_PRIO - 1)) || !threadId || !portAttr )
        return SK_ERR_INVALID_PARAM;

    result = xTaskCreate((TaskFunction_t) entrypoint, portAttr->name, portAttr->stackSize/sizeof(StackType_t),
                     arg, portAttr->priority, (void *)threadId);

    if ( result != pdPASS ) {
        if ( result == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY ) {
            return SK_ERR_NO_MEMORY;
        } else {
            return SK_ERR_FAILED ;
        }
    }

    return SK_OK;
}

skStatus_t portThreadStop(skThread_t threadId)
{
    vTaskDelete(NULL);

    return SK_OK;
}

// skStatus_t portThreadSuspend(void)
// {
//     return SK_OK;
// }

// skStatus_t portThreadResume(void)
// {
//     return SK_OK;
// }

/*-------------------------------------------------------------------------
 * IPC start
 *-----------------------------------------------------------------------*/

skStatus_t portMutexInit(skMutex_t *lock)
{
   SemaphoreHandle_t pMutex;

    if ( !lock )
        return SK_ERR_INVALID_PARAM;

    pMutex = xSemaphoreCreateRecursiveMutex();

    if ( NULL != pMutex ) {

        *lock = (skMutex_t)pMutex;
        return SK_OK;
    }

    return SK_ERR_FAILED;
}

skStatus_t portMutexDestroy(skMutex_t *lock)
{
    SemaphoreHandle_t pMutex;

    if ( !lock )
        return SK_ERR_INVALID_PARAM;

    pMutex = (SemaphoreHandle_t) *lock;
    if ( NULL != pMutex ) {

      vSemaphoreDelete(pMutex);
      *lock = 0;
    }

    return SK_OK;
}

skStatus_t portMutexLock(skMutex_t *lock)
{

    if ( !lock || !(*lock) )
        return SK_ERR_INVALID;

    SemaphoreHandle_t pMutex = (SemaphoreHandle_t) *lock;

    xSemaphoreTakeRecursive(pMutex , portMAX_DELAY);

    return SK_OK;
}

skStatus_t portMutexlockTimed(skMutex_t *lock, skMSectime_t timeoutMSec)
{
    if ( !(*lock) || !lock )
        return SK_ERR_INVALID;

    SemaphoreHandle_t pMutex = (SemaphoreHandle_t) *lock;

    if ( pdPASS != xSemaphoreTakeRecursive(pMutex , (TickType_t)timeoutMSec) )
        return SK_ERR_TIMEOUT;

    return SK_OK;
}

skStatus_t portMutexUnlock(skMutex_t *lock)
{

    if ( !lock || !(*lock) )
        return SK_ERR_INVALID_PARAM;

    SemaphoreHandle_t pMutex = (SemaphoreHandle_t)*lock;

    if ( pdPASS != xSemaphoreGiveRecursive(pMutex) )
        return SK_ERR_EAGAIN;

    return SK_OK;
}

skStatus_t portSignalInit(skSignal_t *signal)
{
    EventGroupHandle_t event;

    if ( !signal )
        return SK_ERR_INVALID_PARAM;

    event = xEventGroupCreate();
    if ( NULL != event ) {

        *signal = (skSignal_t *)event;
        return SK_OK;
    } else {

        return SK_ERR_FAILED;
    }

    return SK_ERROR;
}

skStatus_t portSignalDestroy(skSignal_t *signal)
{
    EventGroupHandle_t event;

    if ( !signal || (!*signal) )
        return SK_ERR_INVALID_PARAM;

    event = (EventGroupHandle_t)*signal;
    vEventGroupDelete(event);

    return SK_OK;
}

uint32_t portSignalWait(skSignal_t *signal, uint32_t mask, uint32_t attr)
{
    uint32_t set_signal = 0;

    portSignalWaitTimed(signal, mask, attr, &set_signal, portMAX_DELAY);
    return set_signal;
}

skStatus_t portSignalWaitTimed(skSignal_t *signal, uint32_t mask,
        uint32_t attr, uint32_t *currentSignals, skTime_t timeoutMSec)
{

    uint32_t setSignal = 0;
    EventGroupHandle_t event = NULL;
    uint32_t eventMask = 0;

    event = (EventGroupHandle_t)*signal;

    if ( !signal || (!*signal) || !currentSignals )
        return SK_ERR_INVALID_PARAM;

    setSignal = xEventGroupWaitBits(event,
                               mask,
                               (attr & SK_SIG_ATTRIBUTE_CLEAR_MASK)? pdTRUE: pdFALSE ,
                               (attr & SK_SIG_ATTRIBUTE_WAIT_FOR_ALL)? pdTRUE: pdFALSE ,
                               (TickType_t) timeoutMSec);
    *currentSignals = (uint32_t)setSignal;
    eventMask = setSignal & mask;

    if ( ((attr & SK_SIG_ATTRIBUTE_WAIT_FOR_ALL) != 0) && (eventMask ^ mask) ) {

        return SK_ERR_TIMEOUT;
    } else {

      if ( eventMask )
        return SK_OK;
    }

    return SK_ERROR;
}

skStatus_t portSignalSet(skSignal_t *signal, uint32_t mask)
{
    if ( !signal || !*signal || !mask )
        return SK_ERR_INVALID_PARAM;

    EventGroupHandle_t event;

    event = (EventGroupHandle_t)*signal;

    /* if not running in ISR */
    if ( 0 == systemGetIpsr() ) {

        xEventGroupSetBits(event, mask);
    } else {

        BaseType_t higher_prio_task_woken = pdFALSE;
        if ( pdFAIL != xEventGroupSetBitsFromISR(event, mask, &higher_prio_task_woken) ) {

            /* if higher_prio_task_woken is now set to pdTrue then a context switch should be requested */
            portYIELD_FROM_ISR(higher_prio_task_woken);
            return SK_ERROR;
        }
    }

    return SK_OK;
}

skStatus_t portSignalclear(skSignal_t *signal, uint32_t mask)
{
    if ( !signal || (!*signal) )
        return SK_ERR_INVALID_PARAM;

    EventGroupHandle_t event;

    event = (EventGroupHandle_t)*signal;

    /* if not running in ISR */
    if ( 0 == systemGetIpsr() )
        xEventGroupClearBits(event, mask);
    else
        xEventGroupClearBitsFromISR(event, mask);

    return SK_OK;
}

/*-------------------------------------------------------------------------
 * IPC end
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Time Start
 *-----------------------------------------------------------------------*/


skStatus_t portSleep(skMSectime_t duration)
{
    TickType_t ticks;


    // ticks = (((duration * configTICK_RATE_HZ) + 999) /1000);
    ticks = pdMS_TO_TICKS(duration);

    if ( duration != 0 && ticks == 0 )
        ticks = 1;

    vTaskDelay(ticks);

    return SK_OK;
}

skStatus_t portDelay(skMSectime_t duration)
{
    portSleep((TickType_t)duration);
    return SK_OK;
}

skTime_t portgetSystickInmSec(void)
{
    return  (xTaskGetTickCount() * (1000 / configTICK_RATE_HZ));
}

skTime_t portgetGpsEpochInSec(void)
{
    //TODO
    return portgetSystickInmSec()/1000;
}

skTime_t portgetGpsEpochMInSec(void)
{
    //TODO
    return portgetSystickInmSec();
}

skTime_t portgetUnixEpochInSec(void)
{
    //TODO
    return portgetSystickInmSec()/1000;
}

skTime_t portgetUnixEpochMInSec(void)
{
    //TODO
    return portgetSystickInmSec();
}

/*-------------------------------------------------------------------------
 * Time end
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Mem Start
 *-----------------------------------------------------------------------*/


void *portMalloc(size_t size)
{
    return malloc(size);
}

void portFree(void *ptr)
{
    free(ptr);
}

void *portCalloc(size_t nmemb, size_t size)
{
    return calloc(nmemb, size);
}

void *portRealloc(void *ptr, size_t size)
{
    return realloc(ptr, size);
}

/*-------------------------------------------------------------------------
 * Mem End
 *-----------------------------------------------------------------------*/

void portLogHandler(int level,const char *module, const char *msg)
{
    altprint(level, (char *)msg);
}

/*-------------------------------------------------------------------------
 * Messaging start
 *-----------------------------------------------------------------------*/

skStatus_t portTwoWayMsgHandler(int32_t msgId, skStatus_t status)
{
    printf("[portOs] MsgId %ld sent with Status %ld\n", msgId, status);

    return SK_OK;
}

/*-------------------------------------------------------------------------
 * Messaging End
 *-----------------------------------------------------------------------*/
