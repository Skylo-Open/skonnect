

/**
* @file         portHALPlugin.c
* @brief        //TODO
* @details      HAL porting layer, this file includes function deinition for modem hardware interface.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skHALPlugin.h>
#include <portHALPlugin.h>
#include <portOSPlugin.h>
#include <skGPSPlugin.h>
#include <skStatus.h>
#include <skLog.h>
#include <skPortSimulator.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* stm32 includes */
#include "stm32l496g_discovery.h"

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

#define pLog(...) _sk_log(portHALPlugin, __VA_ARGS__)

#define HAL_MAX_DELAY      0xFFFFFFFFU

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

UART_HandleTypeDef huart1;

static portHalContext_t portHalContext = {0};


#define CIRC_BUFF_SIZE 1024+1
typedef struct {
    char buffer[CIRC_BUFF_SIZE];
    volatile int read_count;
    volatile int write_count;
	uint8_t init;
} CircularBuffer;
CircularBuffer modemCircbuff = {0,};


uint8_t recvd_data; // receive buffer

/*-------------------------------------------------------------------------
 * Function Defs
 *-----------------------------------------------------------------------*/

static int MX_USART1_UART_Init(void);
void initializeBuffer(CircularBuffer *circularBuffer);

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

skStatus_t portModemHWInit(void) 
{
	skStatus_t status = SK_ERROR;

	if ( portHalContext.isInit == 1 ) {

		pLog(SKYLO_ERR_E, "HAL client Modem UART may be already initialised in func %s\n",__func__);
		return SK_OK;
	}
	//init uart connectd to modem
	if ( (status = MX_USART1_UART_Init()) ) {
		
		pLog(SKYLO_ERR_E, "HAL client Modem UART init error(%"PRIu32") in func %s\n",status,__func__);
	} else {

		portHalContext.sHandleModem = &huart1;
		portHalContext.isInit = 1;
		initializeBuffer(&modemCircbuff);
		status = SK_OK;
		
		HAL_UART_Receive_IT(&huart1,&recvd_data,1);
	}
  	

	return status;
}

skStatus_t portModemHWDeInit(void)
{
	deinitializeBuffer(&modemCircbuff);
	portHalContext.isInit = 0;
	return HAL_UART_DeInit(portHalContext.sHandleModem);
}

skStatus_t portModemSetHwFlowCtrlStatus(uint8_t enable)
{

	pLog(SKYLO_EMERG_E, "Function %s is not ported yet\n",__func__);
	return SK_OK;
}

skStatus_t portModemGetHwFlowCtrlStatus(void)
{

	pLog(SKYLO_EMERG_E, "Function %s is not ported yet\n",__func__);
	return SK_OK;
}

void initializeBuffer(CircularBuffer *circularBuffer) 
{
	if ( !circularBuffer->init ) {

		circularBuffer->read_count = 0;
		circularBuffer->write_count = 0;
		memset(circularBuffer->buffer,0x00,CIRC_BUFF_SIZE);		
		circularBuffer->init = 1;
	}
}

void deinitializeBuffer(CircularBuffer *circularBuffer) 
{
	if ( circularBuffer->init ) {

		circularBuffer->read_count = 0;
		circularBuffer->write_count = 0;
		circularBuffer->init = 0;
		memset(circularBuffer->buffer,0x00,CIRC_BUFF_SIZE);
	}
}

// Function to put a single character into the circular buffer
void putChar(CircularBuffer *circularBuffer, char data) {

	if ( circularBuffer->init ) {

		if ( (circularBuffer->write_count + 1) % CIRC_BUFF_SIZE != circularBuffer->read_count ) {
		} else {
			// Buffer is full, handle overflow if needed
			// pLog(SKYLO_ERR_E, "Circ Buffer overflow!\n");
			// circularBuffer->write_count = 0;
		}
		
		circularBuffer->buffer[circularBuffer->write_count] = data;
		circularBuffer->write_count = (circularBuffer->write_count + 1) % CIRC_BUFF_SIZE;
	}
}

// Function to get a string until the newline character ('\n') from the circular buffer
int getString(CircularBuffer *circularBuffer, char *outputString) 
{
    int i = 0,ret = 0;
	
	if ( circularBuffer->init ) {
		//If  No newline character found, return an empty string
		outputString[0] = '\0';
		ret = 0;
		
		while ( circularBuffer->read_count != circularBuffer->write_count ) {

			char currentChar = circularBuffer->buffer[circularBuffer->read_count];
			circularBuffer->read_count = (circularBuffer->read_count + 1) % CIRC_BUFF_SIZE;

			if ( currentChar == '\n' ) {
				// Found newline character, end the string
				outputString[i++] = '\n';
				outputString[i] = '\0';
				ret = i;

				break;
			}

			// Add the character to the output string
			outputString[i] = currentChar;
			i++;
		}

	} else {
		ret = 0;
	}

	return ret;
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
     // write some code when transmission is complete
}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){

	if ( recvd_data == '\n')
		BSP_LED_Toggle(LED_GREEN);
	putChar(&modemCircbuff,recvd_data);
	HAL_UART_Receive_IT(huart,&recvd_data,1);
}

skStatus_t portModemSend(char *cmd, uint32_t length)
{
	if ( !cmd || !portHalContext.isInit )
		return -1;
	
	return  HAL_UART_Transmit(portHalContext.sHandleModem,(uint8_t*)cmd,length, HAL_MAX_DELAY);
}

int modemRecvLinefromCB(UART_HandleTypeDef *huart, CircularBuffer *circularBuffer,char *outputString,uint32_t *length)
{
	// pLog(SKYLO_ERR_E, "modemRecvLinefromCB\n");
	int ret = 0;
		
	// pLog(SKYLO_DEBUG_E, "CB ent w(%d),r(%d)\n",
	// 	modemCircbuff.write_count,
	// 	modemCircbuff.read_count);
	
	while ( !(ret = getString(circularBuffer,outputString)) ) {
		portSleep(5);
	}

	
	// pLog(SKYLO_DEBUG_E, "CB ex w(%d),r(%d)\n",
	// 	modemCircbuff.write_count,
	// 	modemCircbuff.read_count);

	*length = ret;
	return HAL_OK;
}

skStatus_t portModemRecv(char *lineBufferedMsg, uint32_t *length)
{
	if ( !length || !lineBufferedMsg || !portHalContext.isInit ) {

		pLog(SKYLO_ERR_E, "Invalid argument\n");
		return SK_ERR_INVALID;
	}

	return modemRecvLinefromCB(portHalContext.sHandleModem, &modemCircbuff, lineBufferedMsg, length);
}

skStatus_t portModemHardReboot(void) {

	//Not used by SkConnect yet

	pLog(SKYLO_EMERG_E, "Function %s is not ported yet\n",__func__);
	return SK_OK;
}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static int MX_USART1_UART_Init(void)
{
  int ret = -1;
  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  
  ret = HAL_UART_Init(&huart1);

  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

  return ret;
}
