
/**
* @file         portOSPlugin.h
* @brief        //TODO
* @details      Skylo Konnect demo OS porting plugins Header file.
* @author       Dharmesh Vasoya(dharmesh@skylo.tech) <br>
*               Ankit Patel (ankit.patel@skylo.tech)
 * @copyright   Copyright (c) 2023 Skylo Technologies, Inc.
*/

/* * MIT License
* 
* Copyright (c) Skylo Technologies, Inc. [2023]
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
* */

#ifndef __PORT_OS_PLUGIN_H__
#define __PORT_OS_PLUGIN_H__

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/

#include <skOSPlugin.h>
#include <skStatus.h>
#include <inttypes.h>

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 *-----------------------------------------------------------------------*/

#define StackType_t uint32_t

/*-------------------------------------------------------------------------
 * Type Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Static & global Variable Declarations
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations
 *-----------------------------------------------------------------------*/

/**
 * @brief
 *
 * @param threadId
 * @param attr
 * @param entrypoint
 * @param arg
 * @return skStatus_t
 */
skStatus_t portThreadCreate(skThread_t *threadId, skThreadAttributes_t *attr,
        threadHandler_fp entrypoint, void *arg);

/**
 * @brief
 *
 * @param attr
 * @return skStatus_t
 */
skStatus_t portThreadAttrInit(skThreadAttributes_t *attr);

/**
 * @brief
 *
 * @param attr
 * @param name
 * @return skStatus_t
 */
skStatus_t portThreadAttrSetName(skThreadAttributes_t *attr, const char *name);

/**
 * @brief
 *
 * @param attr
 * @param priority
 * @return skStatus_t
 */
skStatus_t portThreadAttrSetPri(skThreadAttributes_t *attr, uint16_t priority);

/**
 * @brief
 *
 * @param attr
 * @param stack_size
 * @return skStatus_t
 */
skStatus_t portThreadAttrSetStackSize(skThreadAttributes_t *attr, uint16_t stack_size);

/**
 * @brief
 *
 * @return skStatus_t
 */
skStatus_t portThreadStop(skThread_t threadId);

/**
 * @brief
 *
 */
// skStatus_t portThreadSuspend(void);

/**
 * @brief
 *
 */
// skStatus_t portThreadResume(void);
/*-------------------------------------------------------------------------
 * IPC start
 *-----------------------------------------------------------------------*/

/**
 * @brief
 *
 * @param lock
 * @return skStatus_t
 */
skStatus_t portMutexInit(skMutex_t *lock);

/**
 * @brief
 *
 * @param lock
 * @return skStatus_t
 */
skStatus_t portMutexDestroy(skMutex_t *lock);

/**
 * @brief
 *
 * @param lock
 * @return skStatus_t
 */
skStatus_t portMutexLock(skMutex_t *lock);

/**
 * @brief
 *
 * @param lock
 * @param timeoutMSec
 * @return skStatus_t
 */
skStatus_t portMutexlockTimed(skMutex_t *lock, skMSectime_t timeoutMSec);

/**
 * @brief
 *
 * @param lock
 * @return skStatus_t
 */
skStatus_t portMutexUnlock(skMutex_t *lock);

/**
 * @brief
 *
 * @param signal
 * @return skStatus_t
 */
skStatus_t portSignalInit(skSignal_t *signal);

/**
 * @brief
 *
 * @param signal
 * @return skStatus_t
 */
skStatus_t portSignalDestroy(skSignal_t *signal);

/**
 * @brief
 *
 * @param signal
 * @param mask
 * @param attribute
 * @return skStatus_t
 */
uint32_t portSignalWait(skSignal_t *signal, uint32_t mask, uint32_t attribute);

/**
 * @brief
 *
 * @param signal
 * @param mask
 * @param attr
 * @param currentSignals
 * @param timeoutMSec
 * @return skStatus_t
 */
skStatus_t portSignalWaitTimed(skSignal_t *signal, uint32_t mask,
        uint32_t attr, uint32_t *currentSignals, skTime_t timeoutMSec);

/**
 * @brief
 *
 * @param signal
 * @param mask
 * @return skStatus_t
 */
skStatus_t portSignalSet(skSignal_t *signal, uint32_t mask);

/**
 * @brief
 *
 * @param signal
 * @param mask
 * @return skStatus_t
 */
skStatus_t portSignalclear(skSignal_t *signal, uint32_t mask);

/*-------------------------------------------------------------------------
 * IPC end
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Time Start
 *-----------------------------------------------------------------------*/

/**
 * @brief
 *
 * @param duration
 */
skStatus_t portSleep(skMSectime_t duration);

/**
 * @brief
 *
 * @param duration
 */
skStatus_t portDelay(skMSectime_t duration);

/**
 * @brief
 *
 * @return skTime_t
 */
skTime_t portgetSystickInmSec(void);

/**
 * @brief
 *
 * @return skTime_t
 */
skTime_t portgetGpsEpochInSec(void);

/**
 * @brief
 *
 * @return skTime_t
 */
skTime_t portgetGpsEpochMInSec(void);

/**
 * @brief
 *
 * @return skTime_t
 */
skTime_t portgetUnixEpochInSec(void);

/**
 * @brief
 *
 * @return skTime_t
 */
skTime_t portgetUnixEpochMInSec(void);

/*-------------------------------------------------------------------------
 * Time end
 *-----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Mem Start
 *-----------------------------------------------------------------------*/

/**
 * @brief
 *
 * @param size
 * @return void*
 */
void *portMalloc(size_t size);

/**
 * @brief
 *
 * @param ptr
 */
void portFree(void *ptr);

/**
 * @brief
 *
 * @param nmemb
 * @param size
 * @return void*
 */
void *portCalloc(size_t nmemb, size_t size);

/**
 * @brief
 *
 * @param ptr
 * @param size
 * @return void*
 */
void *portRealloc(void *ptr, size_t size);

/**
 * @brief
 *
 * @param level
 * @param module
 * @param msg
 */
void portLogHandler(int level,const char *module, const char *msg);

/**
 * @brief
 *
 * @param msgId
 * @param status
 */
skStatus_t portTwoWayMsgHandler(int32_t msgId, skStatus_t status);

#endif //__PORT_OS_PLUGIN_H__